# - Try to find jack-2.6
# Once done this will define
#
#  PORTAUDIO_FOUND - system has jack
#  PORTAUDIO_INCLUDE_DIRS - the jack include directory
#  PORTAUDIO_LIBRARIES - Link these to use jack
#

INCLUDE(TritiumPackageHelper)

TPH_FIND_PACKAGE(PORTAUDIO portaudio-2.0 portaudio.h portaudio)

INCLUDE(TritiumFindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(PORTAUDIO DEFAULT_MSG PORTAUDIO_LIBRARIES PORTAUDIO_INCLUDE_DIRS)

MARK_AS_ADVANCED(PORTAUDIO_INCLUDE_DIRS PORTAUDIO_LIBRARIES)
