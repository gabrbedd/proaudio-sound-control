.TH ${PROGRAM_NAME} 1 "February 03, 2011" ${PROGRAM_NAME} 1
.SH NAME
${PROGRAM_NAME} \- A graphical mixer application for the Linux sound system ALSA
.SH SYNOPSIS
.B ${PROGRAM_NAME}
.SH DESCRIPTION
.B ${PROGRAM_NAME}
is a graphical mixer application for the Linux sound system ALSA using the QT4 GUI libraries.
.SH SEE ALSO
.B alsamixer amixer
.SH AUTHOR
.B ${PROGRAM_NAME}
was written by Sebastian Holtermann <sebholt@xwmw.org>
