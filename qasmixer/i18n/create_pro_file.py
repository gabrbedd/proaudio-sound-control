#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

pro_text = ""


src_dir = "../src"
ts_dir = "l10n"


hpp_list = []
cpp_list = []

for root, dirs, files in os.walk ( src_dir ):
	for fl in files:
		fl_full = root + "/" + fl
		if ( fl.endswith ( ".hpp" ) ):
			hpp_list.append ( fl_full )
		if ( fl.endswith ( ".cpp" ) ):
			cpp_list.append ( fl_full )

hpp_list.sort()
if ( len ( hpp_list ) > 0 ):
	pro_text += "\n"
	pro_text += "HEADERS = \\\n"
	for hpp in hpp_list:
		pro_text += "\t"
		pro_text += hpp
		if ( hpp != hpp_list[-1] ):
			pro_text += " \\\n"
	pro_text += "\n"

cpp_list.sort()
if ( len ( cpp_list ) > 0 ):
	pro_text += "\n"
	pro_text += "SOURCES = \\\n"
	for cpp in cpp_list:
		pro_text += "\t"
		pro_text += cpp
		if ( cpp != cpp_list[-1] ):
			pro_text += " \\\n"
	pro_text += "\n"


# Translations

ts_dir_list = os.listdir( ts_dir )
ts_dir_list.sort()

ts_list = []
#for ts in ts_dir_list:
#	if ( ts.endswith ( ".ts" ) ):
#		ts_list.append ( ts )

if ( len ( ts_list ) > 0 ):
	pro_text += "\n"
	pro_text += "TRANSLATIONS = \\\n"
	for ts in ts_list:
		pro_text += "\t"
		pro_text += ts_dir + "/"
		pro_text += ts
		if ( ts != ts_list[-1] ):
			pro_text += " \\\n"
	pro_text += "\n"


fl = open ( 'app.pro', 'w' )
fl.write ( pro_text )
fl.close()
