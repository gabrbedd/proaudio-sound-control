<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>ALSA::CTL_Elem_IFace_Name</name>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="91"/>
        <source>CARD</source>
        <translation>Karte</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="92"/>
        <source>HWDEP</source>
        <translation>Gerätespez.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="93"/>
        <source>MIXER</source>
        <translation>Mixer</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="94"/>
        <source>PCM</source>
        <translation>PCM</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="95"/>
        <source>RAWMIDI</source>
        <translation>Rohmidi</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="96"/>
        <source>TIMER</source>
        <translation>Zeitgeber</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="97"/>
        <source>SEQUENCER</source>
        <translation>Sequencer</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="98"/>
        <location filename="../../src/qsnd/snd_mixer_ctl_data.cpp" line="105"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
</context>
<context>
    <name>ALSA::CTL_Elem_Type_Name</name>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="101"/>
        <source>NONE</source>
        <translation>Unbekannt</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="102"/>
        <source>BOOLEAN</source>
        <translation>Bool</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="103"/>
        <source>INTEGER</source>
        <translation>Integer</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="104"/>
        <source>ENUMERATED</source>
        <translation>Aufzählung</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="105"/>
        <source>BYTES</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="106"/>
        <source>IEC958</source>
        <translation>IEC958</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="107"/>
        <source>INTEGER64</source>
        <translation>Integer64</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="108"/>
        <location filename="../../src/qsnd/snd_mixer_ctl_data.cpp" line="65"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
</context>
<context>
    <name>ALSA::Channel_Name</name>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="80"/>
        <source>Front Left</source>
        <translation>Vorne links</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="81"/>
        <source>Front Right</source>
        <translation>Vorne rechts</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="82"/>
        <source>Rear Left</source>
        <translation>Hinten links</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="83"/>
        <source>Rear Right</source>
        <translation>Hinten rechts</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="84"/>
        <source>Front Center</source>
        <translation>Vorne Mitte</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="85"/>
        <source>Woofer</source>
        <translation>Tieftöner</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="86"/>
        <source>Side Left</source>
        <translation>Seite links</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="87"/>
        <source>Side Right</source>
        <translation>Seite rechts</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="88"/>
        <source>Rear Center</source>
        <translation>Hinten Mitte</translation>
    </message>
</context>
<context>
    <name>ALSA::Elem_Name</name>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="46"/>
        <source>Master</source>
        <translation>Hauptregler</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="54"/>
        <source>Speaker</source>
        <translation>Lautsprecher</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="40"/>
        <source>Headphone</source>
        <translation>Kopfhörer</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="41"/>
        <source>Headphone LFE</source>
        <translation>Kopfhörer LFE</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="42"/>
        <source>Headphone Center</source>
        <translation>Kopfhörer Mitte</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="37"/>
        <source>Front</source>
        <translation>Vorne</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="38"/>
        <source>Front Mic</source>
        <translation>Mik. Vorne</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="39"/>
        <source>Front Mic Boost</source>
        <translation>Mik. Vorne Verstärkung</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="47"/>
        <source>Mic</source>
        <translation>Mikrofon</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="48"/>
        <source>Mic Boost</source>
        <translation>Mik. Verstärkung</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="52"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="55"/>
        <source>Surround</source>
        <translation>Umgebung</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="57"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="32"/>
        <source>Center</source>
        <translation>Mitte</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="53"/>
        <source>Side</source>
        <translation>Seite</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="31"/>
        <source>Capture</source>
        <translation>Aufnahme</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="30"/>
        <source>Beep</source>
        <translation>Signalton</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="51"/>
        <source>PC Speaker</source>
        <translation>PC Lautsprecher</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="29"/>
        <source>Bass</source>
        <translation>Bässe</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="56"/>
        <source>Treble</source>
        <translation>Höhen</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="33"/>
        <source>Channel Mode</source>
        <translation>Kanal Modus</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="28"/>
        <source>Auto Gain Control</source>
        <translation>Autom. Verst.-Regelung</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="50"/>
        <source>Mic Select</source>
        <translation>Mikrofonwahl</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="44"/>
        <source>Internal Mic</source>
        <translation>Internes Mik.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="45"/>
        <source>Int Mic</source>
        <translation>Internes Mik.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="35"/>
        <source>External Mic</source>
        <translation>Externes Mik.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="36"/>
        <source>Ext Mic</source>
        <translation>Externes Mik.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="49"/>
        <source>Mic Boost (+20dB)</source>
        <translation>Mik. Verst. (+20dB)</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="34"/>
        <source>External Amplifier</source>
        <translation>Externer Verstärker</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="43"/>
        <source>Input Source</source>
        <translation>Eingangsquelle</translation>
    </message>
</context>
<context>
    <name>ALSA::Enum_Value</name>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="60"/>
        <source>1ch</source>
        <translation>1 Kan.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="61"/>
        <source>2ch</source>
        <translation>2 Kan.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="62"/>
        <source>3ch</source>
        <translation>3 Kan.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="63"/>
        <source>4ch</source>
        <translation>4 Kan.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="64"/>
        <source>5ch</source>
        <translation>5 Kan.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="65"/>
        <source>6ch</source>
        <translation>6 Kan.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="66"/>
        <source>7ch</source>
        <translation>7 Kan.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="67"/>
        <source>8ch</source>
        <translation>8 Kan.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="70"/>
        <source>Mic</source>
        <translation>Mikrofon</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="69"/>
        <source>Front Mic</source>
        <translation>Mik. Vorne</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="71"/>
        <source>Mic1</source>
        <translation>Mik. 1</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="72"/>
        <source>Mic2</source>
        <translation>Mik. 2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="73"/>
        <source>Mic3</source>
        <translation>Mik. 3</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="74"/>
        <source>Mic4</source>
        <translation>Mik. 4</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="76"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="77"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
</context>
<context>
    <name>Dialog_Alsa_Config</name>
    <message>
        <location filename="../../src/dialog_alsa_config.cpp" line="31"/>
        <source>Alsa configuration tree</source>
        <translation>Alsa Konfigurationsbaum</translation>
    </message>
    <message>
        <location filename="../../src/dialog_alsa_config.cpp" line="40"/>
        <source>&amp;Reload</source>
        <translation>&amp;Neu laden</translation>
    </message>
    <message>
        <location filename="../../src/dialog_alsa_config.cpp" line="41"/>
        <source>&amp;Close</source>
        <translation>&amp;Schließen</translation>
    </message>
</context>
<context>
    <name>Dialog_Info</name>
    <message>
        <location filename="../../src/dialog_info.cpp" line="37"/>
        <source>Info</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="43"/>
        <source>%1 - %2</source>
        <translation>%1 - %2</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="94"/>
        <source>About</source>
        <translation>Zusammenfassung</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="98"/>
        <source>%1 is a mixer application for the linux sound system %2.</source>
        <translation>%1 ist ein Lautstärkenregler-Programm für %2, die Klang-Architektur von Linux.</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="106"/>
        <source>Internet</source>
        <translation>Im Internet</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="109"/>
        <location filename="../../src/dialog_info.cpp" line="110"/>
        <source>Home page</source>
        <translation>Hauptseite</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="114"/>
        <location filename="../../src/dialog_info.cpp" line="115"/>
        <source>Project page</source>
        <translation>Projektseite</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="129"/>
        <source>Developers</source>
        <translation>Entwickler</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="133"/>
        <source>Translators</source>
        <translation>Übersetzer</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="134"/>
        <source>German</source>
        <translation>Deutsch</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="135"/>
        <source>Spanish</source>
        <translation>Spanisch</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="136"/>
        <source>Russian</source>
        <translation>Russisch</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="139"/>
        <source>Contributors</source>
        <translation>Mitwirkende</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="142"/>
        <source>Testing</source>
        <translation>Tests</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="142"/>
        <source>Ideas</source>
        <translation>Ideen</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="156"/>
        <source>The license file %1 is not available.</source>
        <translation>Die Lizenzdatei %1 ist nicht verfügbar.</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="164"/>
        <source>&amp;Information</source>
        <translation>&amp;Information</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="165"/>
        <source>&amp;People</source>
        <translation>&amp;Leute</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="166"/>
        <source>&amp;License</source>
        <translation>Li&amp;zenz</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="173"/>
        <source>&amp;Close</source>
        <translation>&amp;Schließen</translation>
    </message>
</context>
<context>
    <name>Dialog_Settings</name>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="38"/>
        <location filename="../../src/dialog_settings.cpp" line="119"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="43"/>
        <source>Appearance</source>
        <translation>Erscheinungsbild</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="44"/>
        <source>Input</source>
        <translation>Eingabe</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="45"/>
        <source>System tray</source>
        <translation>Systemleiste</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="46"/>
        <source>Mini mixer</source>
        <translation>Mini-Mixer</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="187"/>
        <source>Show slider status bar</source>
        <translation>Statusleiste anzeigen</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="219"/>
        <source>Rotation amount for a slider change from 0% to 100%</source>
        <translation>Rotationsweite für eine Schieberänderung von 0% auf 100%</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="221"/>
        <source>degrees</source>
        <translation>Grad</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="316"/>
        <source>on minimize</source>
        <translation>beim Minimieren</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="317"/>
        <source>on close</source>
        <translation>beim Schließen</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="354"/>
        <source>Default card</source>
        <translation>Standardkarte</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="168"/>
        <source>Side bar</source>
        <translation>Seitenleiste</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="171"/>
        <source>Show view type selection</source>
        <translation>Ansichtsauswahl anzeigen</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="184"/>
        <source>Simple mixer view</source>
        <translation>Standardmixeransicht</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="218"/>
        <source>Mouse wheel</source>
        <translation>Mausrad</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="247"/>
        <source>%1 turns</source>
        <translation>%1 Umdrehungen</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="290"/>
        <source>Show icon in system tray</source>
        <translation>Ikone in der Systemleiste anzeigen</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="292"/>
        <source>Don&apos;t use tray</source>
        <translation>Systemleiste nicht verwenden</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="293"/>
        <source>Only when minimized to tray</source>
        <translation>Nur wenn minimiert in Systemleiste</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="294"/>
        <source>Always (even when the mixer is visible)</source>
        <translation>Immer (auch wenn der Mixer sichtbar ist)</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="314"/>
        <source>Minimize to tray</source>
        <translation>Minimieren in Systemleiste</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="352"/>
        <source>Mini mixer device</source>
        <translation>Mini-Mixer-Gerät</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="355"/>
        <source>Current (same as in main mixer window)</source>
        <translation>Aktuell (das gleiche wie im Mixer Fenster)</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="356"/>
        <source>User defined</source>
        <translation>Benutzerdefiniert</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="365"/>
        <source>User device:</source>
        <translation>Benutzergerät:</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="366"/>
        <source>e.g. hw:0</source>
        <translation>z.B. hw:0</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="400"/>
        <source>Volume change notification</source>
        <translation>Lautstärkenänderungsbenachrichtigung</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="406"/>
        <source>Show balloon on a volume change</source>
        <translation>Ballon bei einer Lautstärkenänderung anzeigen</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="414"/>
        <source>Balloon lifetime</source>
        <translation>Ballon-Lebensdauer</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="416"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="73"/>
        <source>&amp;Close</source>
        <translation>&amp;Schließen</translation>
    </message>
</context>
<context>
    <name>Main_Mixer_Window</name>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="49"/>
        <source>Simple mixer</source>
        <translation>Standardmixer</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="50"/>
        <source>Element mixer</source>
        <translation>Elementmixer</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="51"/>
        <source>Control info</source>
        <translation>Modulinformation</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="105"/>
        <source>Ctrl+s</source>
        <translation>Ctrl+e</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="98"/>
        <source>&amp;Quit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="110"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Auffrischen</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="126"/>
        <source>Ctrl+1</source>
        <translation>Ctrl+1</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="127"/>
        <source>Ctrl+2</source>
        <translation>Ctrl+2</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="128"/>
        <source>Ctrl+3</source>
        <translation>Ctrl+3</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="141"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="178"/>
        <source>&amp;Extras</source>
        <translation>&amp;Extras</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="104"/>
        <source>&amp;Settings</source>
        <translation>&amp;Einstellungen</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="133"/>
        <source>&amp;Alsa configuration</source>
        <translation>Alsa-&amp;Konfiguration</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="134"/>
        <source>Ctrl+c</source>
        <translation>Ctrl+k</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="148"/>
        <source>&amp;Info</source>
        <translation>&amp;Über</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="157"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="165"/>
        <source>&amp;View</source>
        <translation>An&amp;sicht</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="183"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="275"/>
        <source>Normal &amp;screen</source>
        <translation>Normal&amp;bild</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="279"/>
        <source>Full&amp;screen</source>
        <translation>Voll&amp;bild</translation>
    </message>
</context>
<context>
    <name>Message_Widget</name>
    <message>
        <location filename="../../src/message_widget.cpp" line="36"/>
        <source>Mixer device couldn&apos;t be opened</source>
        <translation>Das Gerät konnte nicht geöffnet werden</translation>
    </message>
    <message>
        <location filename="../../src/message_widget.cpp" line="37"/>
        <source>No device selected</source>
        <translation>Kein Gerät ausgewählt</translation>
    </message>
    <message>
        <location filename="../../src/message_widget.cpp" line="77"/>
        <source>Device</source>
        <translation>Gerät</translation>
    </message>
    <message>
        <location filename="../../src/message_widget.cpp" line="78"/>
        <source>Function</source>
        <translation>Funktion</translation>
    </message>
    <message>
        <location filename="../../src/message_widget.cpp" line="79"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
</context>
<context>
    <name>Mini_Mixer</name>
    <message>
        <location filename="../../src/mini_mixer.cpp" line="71"/>
        <source>&amp;Show mixer</source>
        <translation>Mixer &amp;anzeigen</translation>
    </message>
    <message>
        <location filename="../../src/mini_mixer.cpp" line="72"/>
        <source>Ctrl+s</source>
        <translation>Ctrl+a</translation>
    </message>
    <message>
        <location filename="../../src/mini_mixer.cpp" line="77"/>
        <source>&amp;Close %1</source>
        <translation>%1 &amp;beenden</translation>
    </message>
    <message>
        <location filename="../../src/mini_mixer.cpp" line="106"/>
        <source>Volume at %1%</source>
        <translation>Lautstärke bei %1%</translation>
    </message>
</context>
<context>
    <name>QSnd::Alsa_Config_Model</name>
    <message>
        <location filename="../../src/qsnd/alsa_config_model.cpp" line="230"/>
        <source>Key</source>
        <translation>Schlüssel</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_config_model.cpp" line="232"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
</context>
<context>
    <name>QSnd::Alsa_Config_View</name>
    <message>
        <location filename="../../src/qsnd/alsa_config_view.cpp" line="32"/>
        <source>&amp;Expand</source>
        <translation>&amp;Ausklappen</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_config_view.cpp" line="33"/>
        <source>Co&amp;llapse</source>
        <translation>&amp;Einklappen</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_config_view.cpp" line="34"/>
        <source>&amp;Sort</source>
        <translation>Sor&amp;tieren</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_config_view.cpp" line="36"/>
        <source>Depth:</source>
        <translation>Tiefe:</translation>
    </message>
</context>
<context>
    <name>QSnd::Controls_Model</name>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="36"/>
        <source>Cards</source>
        <translation>Karten</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="43"/>
        <source>Plugins</source>
        <translation>Steuermodule</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="215"/>
        <source>Default card</source>
        <translation>Standardkarte</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="234"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="239"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="244"/>
        <source>Mixer name</source>
        <translation>Mixername</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_CTL</name>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="60"/>
        <source>yes</source>
        <translation>ja</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="61"/>
        <source>no</source>
        <translation>nein</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="63"/>
        <source>Range:</source>
        <translation>Bereich:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="64"/>
        <source>Elements of the type %1 are not supported</source>
        <translation>Elemente vom Typ %1 werden nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="72"/>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="172"/>
        <source>Element index</source>
        <translation>Elementindex</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="73"/>
        <source>Channel index</source>
        <translation>Kanalindex</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="76"/>
        <source>El. index: %1</source>
        <translation>El. Index: %1</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="78"/>
        <source>Channel: %2</source>
        <translation>Kanal: %2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="158"/>
        <source>Index:</source>
        <translation>Index:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="159"/>
        <source>Device:</source>
        <translation>Gerät:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="161"/>
        <source>Active:</source>
        <translation>Aktiv:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="162"/>
        <source>Volatile:</source>
        <translation>Volatil:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="163"/>
        <source>Readable:</source>
        <translation>Lesbar:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="164"/>
        <source>Writable:</source>
        <translation>Schreibbar:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="166"/>
        <source>Channels:</source>
        <translation>Kanäle:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="167"/>
        <source>Num. id:</source>
        <translation>Num. id:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="68"/>
        <source>Element name</source>
        <translation>Elementname</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="173"/>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="202"/>
        <source>Device</source>
        <translation>Gerät</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="175"/>
        <source>Is active</source>
        <translation>Ist aktiv</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="176"/>
        <source>Is volatile</source>
        <translation>Ist volatil</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="177"/>
        <source>Is readable</source>
        <translation>Ist lesbar</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="178"/>
        <source>Is writable</source>
        <translation>Ist schreibbar</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="180"/>
        <source>Channel count</source>
        <translation>Kanalanzahl</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="181"/>
        <source>Numeric Id</source>
        <translation>Numerische Id</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="203"/>
        <source>Subdevice</source>
        <translation>Untergerät</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="445"/>
        <source>Toggle all</source>
        <translation>Alle umschalten</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="1260"/>
        <source>Joined changing</source>
        <translation>Gemeinsam ändern</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="1237"/>
        <source>%1 - %2</source>
        <translation>%1 - %2</translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="1277"/>
        <source>%n elements</source>
        <translation>
            <numerusform>%n Element</numerusform>
            <numerusform>%n Elemente</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="1441"/>
        <source>No element selected</source>
        <translation>Kein Element ausgewählt</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_CTL_Table_Model</name>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="31"/>
        <source>%1,%2</source>
        <translation>%1,%2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="33"/>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="35"/>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="37"/>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="39"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="34"/>
        <source>r</source>
        <translation>l</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="36"/>
        <source>w</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="38"/>
        <source>a</source>
        <translation>a</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="40"/>
        <source>v</source>
        <translation>v</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="46"/>
        <source>not readable</source>
        <translation>nicht lesbar</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="47"/>
        <source>readable</source>
        <translation>lesbar</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="48"/>
        <source>not writable</source>
        <translation>nicht schreibbar</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="49"/>
        <source>writable</source>
        <translation>schreibbar</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="50"/>
        <source>not active</source>
        <translation>nicht aktiv</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="51"/>
        <source>active</source>
        <translation>aktiv</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="52"/>
        <source>not volatile</source>
        <translation>nicht volatil</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="53"/>
        <source>volatile</source>
        <translation>volatil</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="57"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="58"/>
        <source>Element name</source>
        <translation>Element name</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="60"/>
        <source>Idx</source>
        <translation>Idx</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="61"/>
        <source>Element index</source>
        <translation>Elementindex</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="63"/>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="64"/>
        <source>Device</source>
        <translation>Gerät</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="66"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="67"/>
        <source>Element type</source>
        <translation>Elementtyp</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="69"/>
        <source>Ch.</source>
        <translation>Kan.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="70"/>
        <source>Channel count</source>
        <translation>Kanalanzahl</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="72"/>
        <source>R</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="73"/>
        <source>Is readable</source>
        <translation>Ist lesbar</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="75"/>
        <source>W</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="76"/>
        <source>Is writable</source>
        <translation>Ist schreibbar</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="78"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="79"/>
        <source>Is active</source>
        <translation>Ist aktiv</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="81"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="82"/>
        <source>Is volatile</source>
        <translation>Ist volatil</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="84"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="85"/>
        <source>Numeric Id</source>
        <translation>Numerische Id</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_CTL_Tree_Model</name>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_tree_model.cpp" line="30"/>
        <source>Elements</source>
        <translation>Elemente</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_Sliders</name>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="58"/>
        <source>c</source>
        <translatorcomment>K für Kanäle</translatorcomment>
        <translation>k</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="62"/>
        <source>l</source>
        <translation>n</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="66"/>
        <source>m</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="75"/>
        <source>&amp;Mute</source>
        <translation>Aus&amp;schalten</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="76"/>
        <source>&amp;Mute all</source>
        <translation>Alle aus&amp;schalten</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="71"/>
        <source>&amp;Level channels</source>
        <translation>Kanäle &amp;nivellieren</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="73"/>
        <source>Split &amp;channels</source>
        <translation>&amp;Kanäle trennen</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="74"/>
        <source>Join &amp;channels</source>
        <translation>&amp;Kanäle vereinen</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="77"/>
        <source>Un&amp;mute</source>
        <translation>Ein&amp;schalten</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="78"/>
        <source>Un&amp;mute all</source>
        <translation>Alle Ein&amp;schalten</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="79"/>
        <source>Toggle &amp;mutes</source>
        <translation>Um&amp;schalten</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="322"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="106"/>
        <source>Playback slider</source>
        <translation>Wiedergabeschieber</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="107"/>
        <source>Capture slider</source>
        <translation>Aufnahmeschieber</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="108"/>
        <source>Playback switch</source>
        <translation>Wiedergabeschalter</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="109"/>
        <source>Capture switch</source>
        <translation>Aufnahmeschalter</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_Sliders_Status_Bar</name>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="63"/>
        <source>Vol.:</source>
        <translation>Lautst.:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="64"/>
        <source>Volume</source>
        <translation>Lautstärke</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="80"/>
        <source>Current volume</source>
        <translation>Aktuelle Lautstärke</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="91"/>
        <source>Volume range</source>
        <translation>Lautstärkebereich</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="115"/>
        <source>dB:</source>
        <translation>dB:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="116"/>
        <source>Decibel</source>
        <translation>Dezibel</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="135"/>
        <source>Current Decibel value</source>
        <translation>Aktueller Dezibel-Wert</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="146"/>
        <source>Decibel range</source>
        <translation>Dezibelbereich</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="167"/>
        <source>Elem.:</source>
        <translation>Elem.:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="168"/>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="169"/>
        <source>Element name</source>
        <translation>Elementname</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_Switches</name>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="50"/>
        <source>c</source>
        <translation>k</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="54"/>
        <source>Split &amp;channels</source>
        <translation>&amp;Kanäle trennen</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="55"/>
        <source>Join &amp;channels</source>
        <translation>&amp;Kanäle vereinen</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="361"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="376"/>
        <source>Playback selection</source>
        <translation>Wiedergabeauswahl</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="378"/>
        <source>Playback switch</source>
        <translation>Wiedergabeschalter</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="382"/>
        <source>Capture selection</source>
        <translation>Aufnahmeauswahl</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="384"/>
        <source>Capture switch</source>
        <translation>Aufnahmeschalter</translation>
    </message>
</context>
<context>
    <name>QSnd::Snd_CTL_Info_Model</name>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="32"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="33"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="34"/>
        <source>Driver</source>
        <translation>Treiber</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="35"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="36"/>
        <source>Long name</source>
        <translation>Langer Name</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="37"/>
        <source>Mixer name</source>
        <translation>Mixername</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="38"/>
        <source>Components</source>
        <translation>Komponenten</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="40"/>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="43"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="41"/>
        <source>Device index</source>
        <translation>Gerätindex</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="44"/>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="51"/>
        <source>Device id</source>
        <translation>Gerät-Id</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="46"/>
        <source>Id / Name</source>
        <translation>Id / Name</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="49"/>
        <source>%1 / %2</source>
        <translation>%1 / %2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="52"/>
        <source>Device name</source>
        <translation>Gerätname</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="54"/>
        <source>Subdevices</source>
        <translation>Untergeräte</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="57"/>
        <source>Playback</source>
        <translation>Wiedergabe</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="58"/>
        <source>Capture</source>
        <translation>Aufnahme</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="60"/>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="61"/>
        <source>%1 : %2 available</source>
        <translation>%1 : %2 verfügbar</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="63"/>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="64"/>
        <source>Existing : Available</source>
        <translation>Existent : Verfügbar</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="70"/>
        <source>Key</source>
        <translation>Schlüssel</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="71"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="78"/>
        <source>Control</source>
        <translation>Steuermodul</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="84"/>
        <source>Control plugin</source>
        <translation>Steuermodul</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="95"/>
        <source>Card</source>
        <translation>Karte</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="114"/>
        <source>Devices</source>
        <translation>Geräte</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="123"/>
        <source>Number of devices</source>
        <translation>Geräteanzahl</translation>
    </message>
</context>
<context>
    <name>QSnd::Snd_Mixer_CTL</name>
    <message>
        <location filename="../../src/qsnd/snd_mixer_ctl.cpp" line="109"/>
        <source>Empty device name</source>
        <translation>Leerer Gerätename</translation>
    </message>
</context>
<context>
    <name>QSnd::Snd_Mixer_Simple</name>
    <message>
        <location filename="../../src/qsnd/snd_mixer_simple.cpp" line="90"/>
        <source>Empty device name</source>
        <translation>Leerer Gerätename</translation>
    </message>
</context>
<context>
    <name>Side_Interface</name>
    <message>
        <location filename="../../src/side_interface.cpp" line="51"/>
        <source>Show stream</source>
        <translation>Stromregler</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="53"/>
        <source>&amp;Playback</source>
        <translation>&amp;Wiedergabe</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="54"/>
        <source>Show playback elements</source>
        <translation>Zeige Wiedergabe-Regler</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="56"/>
        <source>&amp;Capture</source>
        <translation>&amp;Aufnahme</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="72"/>
        <source>Interface</source>
        <translation>Schnittstelle</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="88"/>
        <source>View type</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="57"/>
        <source>Show capture elements</source>
        <translation>Zeige Aufnahme-Regler</translation>
    </message>
</context>
</TS>
