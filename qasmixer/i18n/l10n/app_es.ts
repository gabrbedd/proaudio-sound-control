<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>ALSA::CTL_Elem_IFace_Name</name>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="91"/>
        <source>CARD</source>
        <translation>Mapa</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="92"/>
        <source>HWDEP</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="93"/>
        <source>MIXER</source>
        <translatorcomment>Mezcla</translatorcomment>
        <translation>Mezclador</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="94"/>
        <source>PCM</source>
        <translatorcomment>Pulse Code Modulation</translatorcomment>
        <translation>PCM</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="95"/>
        <source>RAWMIDI</source>
        <translatorcomment>Midi puro</translatorcomment>
        <translation>RAW Midi</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="96"/>
        <source>TIMER</source>
        <translatorcomment>Tempo</translatorcomment>
        <translation>Temporizador</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="97"/>
        <source>SEQUENCER</source>
        <translatorcomment>Seq</translatorcomment>
        <translation>Secuenciador</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="98"/>
        <location filename="../../src/qsnd/snd_mixer_ctl_data.cpp" line="105"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
</context>
<context>
    <name>ALSA::CTL_Elem_Type_Name</name>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="101"/>
        <source>NONE</source>
        <translation>Nada</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="102"/>
        <source>BOOLEAN</source>
        <translatorcomment>Bool</translatorcomment>
        <translation>Booleano</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="103"/>
        <source>INTEGER</source>
        <translation>Entero</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="104"/>
        <source>ENUMERATED</source>
        <translatorcomment>Enumerado</translatorcomment>
        <translation>Numerado</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="105"/>
        <source>BYTES</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="106"/>
        <source>IEC958</source>
        <translatorcomment>S/PDIF</translatorcomment>
        <translation>IEC958</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="107"/>
        <source>INTEGER64</source>
        <translation>Entero64</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="108"/>
        <location filename="../../src/qsnd/snd_mixer_ctl_data.cpp" line="65"/>
        <source>Unknown</source>
        <translation>Desconocido</translation>
    </message>
</context>
<context>
    <name>ALSA::Channel_Name</name>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="80"/>
        <source>Front Left</source>
        <translation>Frontal Izquierdo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="81"/>
        <source>Front Right</source>
        <translation>Frontal Derecho</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="82"/>
        <source>Rear Left</source>
        <translation>Trasero Izquierdo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="83"/>
        <source>Rear Right</source>
        <translation>Trasero Derecho</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="84"/>
        <source>Front Center</source>
        <translation>Frontal Central</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="85"/>
        <source>Woofer</source>
        <translatorcomment>SubWoofer</translatorcomment>
        <translation>Graves</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="86"/>
        <source>Side Left</source>
        <translation>Lado Izquierdo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="87"/>
        <source>Side Right</source>
        <translation>Lado Derecho</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="88"/>
        <source>Rear Center</source>
        <translation>Trasero Central</translation>
    </message>
</context>
<context>
    <name>ALSA::Elem_Name</name>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="28"/>
        <source>Auto Gain Control</source>
        <translation>Ganancia automática</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="29"/>
        <source>Bass</source>
        <translatorcomment>Graves</translatorcomment>
        <translation>Bajos</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="30"/>
        <source>Beep</source>
        <translatorcomment>Señal</translatorcomment>
        <translation>Beep</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="31"/>
        <source>Capture</source>
        <translation>Captura</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="32"/>
        <source>Center</source>
        <translation>Centro</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="33"/>
        <source>Channel Mode</source>
        <translation>Modo de canal</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="34"/>
        <source>External Amplifier</source>
        <translation>Amplificador externo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="35"/>
        <source>External Mic</source>
        <translation>Micrófono externo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="36"/>
        <source>Ext Mic</source>
        <translation>Mix ext</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="37"/>
        <source>Front</source>
        <translation>Frontal</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="38"/>
        <source>Front Mic</source>
        <translation>Mic frontal</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="39"/>
        <source>Front Mic Boost</source>
        <translation>Ganancia mic frontal</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="40"/>
        <source>Headphone</source>
        <translation>Auriculares</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="41"/>
        <source>Headphone LFE</source>
        <translation>Auriculares lateral</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="42"/>
        <source>Headphone Center</source>
        <translation>Auriculares centro</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="43"/>
        <source>Input Source</source>
        <translation>Entrada</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="44"/>
        <source>Internal Mic</source>
        <translation>Micrófono interno</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="45"/>
        <source>Int Mic</source>
        <translation>Mic interno</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="46"/>
        <source>Master</source>
        <translation>Maestro</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="47"/>
        <source>Mic</source>
        <translation>Mic</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="48"/>
        <source>Mic Boost</source>
        <translation>Ganancia mic</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="49"/>
        <source>Mic Boost (+20dB)</source>
        <translation>Ganancia mic (+20dB)</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="50"/>
        <source>Mic Select</source>
        <translation>Selección mic</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="51"/>
        <source>PC Speaker</source>
        <translation>Altavoz PC</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="52"/>
        <source>Phone</source>
        <translation>Teléfono</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="53"/>
        <source>Side</source>
        <translation>Lado</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="54"/>
        <source>Speaker</source>
        <translation>Altavoz</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="55"/>
        <source>Surround</source>
        <translatorcomment>Surround</translatorcomment>
        <translation>Envolvente</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="56"/>
        <source>Treble</source>
        <translatorcomment>Altos</translatorcomment>
        <translation>Agudos</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="57"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
</context>
<context>
    <name>ALSA::Enum_Value</name>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="60"/>
        <source>1ch</source>
        <translatorcomment>o1</translatorcomment>
        <translation>canal 1</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="61"/>
        <source>2ch</source>
        <translatorcomment>o2</translatorcomment>
        <translation>canal 2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="62"/>
        <source>3ch</source>
        <translatorcomment>o3</translatorcomment>
        <translation>canal 3</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="63"/>
        <source>4ch</source>
        <translatorcomment>o4</translatorcomment>
        <translation>canal 4</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="64"/>
        <source>5ch</source>
        <translatorcomment>o5</translatorcomment>
        <translation>canal 5</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="65"/>
        <source>6ch</source>
        <translatorcomment>o6</translatorcomment>
        <translation>canal 6</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="66"/>
        <source>7ch</source>
        <translatorcomment>o7</translatorcomment>
        <translation>canal 7</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="67"/>
        <source>8ch</source>
        <translatorcomment>o8</translatorcomment>
        <translation>canal 8</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="69"/>
        <source>Front Mic</source>
        <translation>Micrófono frontal</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="70"/>
        <source>Mic</source>
        <translatorcomment>Mic</translatorcomment>
        <translation>Micrófono</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="71"/>
        <source>Mic1</source>
        <translatorcomment>i1</translatorcomment>
        <translation>Mic 1</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="72"/>
        <source>Mic2</source>
        <translatorcomment>i2</translatorcomment>
        <translation>Mic 2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="73"/>
        <source>Mic3</source>
        <translatorcomment>i3</translatorcomment>
        <translation>Mic 3</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="74"/>
        <source>Mic4</source>
        <translatorcomment>i4</translatorcomment>
        <translation>Mic 4</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="76"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="77"/>
        <source>Phone</source>
        <translation>Teléfono</translation>
    </message>
</context>
<context>
    <name>Dialog_Alsa_Config</name>
    <message>
        <location filename="../../src/dialog_alsa_config.cpp" line="31"/>
        <source>Alsa configuration tree</source>
        <translation>Configuración ALSA (arbol)</translation>
    </message>
    <message>
        <location filename="../../src/dialog_alsa_config.cpp" line="40"/>
        <source>&amp;Reload</source>
        <translation>&amp;Recargar</translation>
    </message>
    <message>
        <location filename="../../src/dialog_alsa_config.cpp" line="41"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
</context>
<context>
    <name>Dialog_Info</name>
    <message>
        <location filename="../../src/dialog_info.cpp" line="37"/>
        <source>Info</source>
        <translatorcomment>Info</translatorcomment>
        <translation>Información</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="43"/>
        <source>%1 - %2</source>
        <translation>%1 - %2</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="94"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="98"/>
        <source>%1 is a mixer application for the linux sound system %2.</source>
        <translation>%1 es un mezclador para el sistema de sonido %2</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="106"/>
        <source>Internet</source>
        <translation>Internet</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="109"/>
        <location filename="../../src/dialog_info.cpp" line="110"/>
        <source>Home page</source>
        <translation>Auspicia</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="114"/>
        <location filename="../../src/dialog_info.cpp" line="115"/>
        <source>Project page</source>
        <translation>Página</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="129"/>
        <source>Developers</source>
        <translation>Desarrolladores</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="133"/>
        <source>Translators</source>
        <translation>Traductores</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="134"/>
        <source>German</source>
        <translation>Alemán</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="135"/>
        <source>Spanish</source>
        <translation>Español</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="136"/>
        <source>Russian</source>
        <translation>Ruso</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="139"/>
        <source>Contributors</source>
        <translation>Colaboran</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="142"/>
        <source>Testing</source>
        <translation>Pruebas</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="142"/>
        <source>Ideas</source>
        <translation>Ideas</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="156"/>
        <source>The license file %1 is not available.</source>
        <translation>Licencia %1 no disponible.</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="164"/>
        <source>&amp;Information</source>
        <translation>&amp;Información</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="165"/>
        <source>&amp;People</source>
        <translation>&amp;Gente</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="166"/>
        <source>&amp;License</source>
        <translation>&amp;Licencia</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="173"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
</context>
<context>
    <name>Dialog_Settings</name>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="38"/>
        <location filename="../../src/dialog_settings.cpp" line="119"/>
        <source>Settings</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="43"/>
        <source>Appearance</source>
        <translation>Apariencia</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="44"/>
        <source>Input</source>
        <translation>Entrada</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="45"/>
        <source>System tray</source>
        <translation>Icono de bandeja</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="46"/>
        <source>Mini mixer</source>
        <translation>Mini mezclador</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="187"/>
        <source>Show slider status bar</source>
        <translation>Mostrar barra de estado</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="219"/>
        <source>Rotation amount for a slider change from 0% to 100%</source>
        <translation>Cantidad de rotación para cambio de volumen de 0% al 100%</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="221"/>
        <source>degrees</source>
        <translation>grados</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="316"/>
        <source>on minimize</source>
        <translation>al minimizar</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="317"/>
        <source>on close</source>
        <translation>al cerrar</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="354"/>
        <source>Default card</source>
        <translation>Interfaz predeterminada</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="365"/>
        <source>User device:</source>
        <translation>Dispositivo de usuario:</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="366"/>
        <source>e.g. hw:0</source>
        <translation>ej. hw:0</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="414"/>
        <source>Balloon lifetime</source>
        <translation>Tiempo de burbuja</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="416"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="168"/>
        <source>Side bar</source>
        <translation>Barra lateral</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="171"/>
        <source>Show view type selection</source>
        <translation>Tipo de vista</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="184"/>
        <source>Simple mixer view</source>
        <translation>Mezclador</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="218"/>
        <source>Mouse wheel</source>
        <translation>Rueda del ratón</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="247"/>
        <source>%1 turns</source>
        <translation>%1 vueltas</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="290"/>
        <source>Show icon in system tray</source>
        <translation>Mostrar icono en bandeja</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="292"/>
        <source>Don&apos;t use tray</source>
        <translation>No usar bandeja</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="293"/>
        <source>Only when minimized to tray</source>
        <translation>Al minimizar</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="294"/>
        <source>Always (even when the mixer is visible)</source>
        <translation>Siempre</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="314"/>
        <source>Minimize to tray</source>
        <translation>Minimizar a bandeja</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="352"/>
        <source>Mini mixer device</source>
        <translation>Control en bandeja</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="355"/>
        <source>Current (same as in main mixer window)</source>
        <translation>Control actual</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="356"/>
        <source>User defined</source>
        <translation>Control definido</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="400"/>
        <source>Volume change notification</source>
        <translation>Notificaciones</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="406"/>
        <source>Show balloon on a volume change</source>
        <translation>Mostrar en balón</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="73"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
</context>
<context>
    <name>Main_Mixer_Window</name>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="49"/>
        <source>Simple mixer</source>
        <translation>Mezclador simple</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="50"/>
        <source>Element mixer</source>
        <translation>Parámetros</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="51"/>
        <source>Control info</source>
        <translation>Información</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="98"/>
        <source>&amp;Quit</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="104"/>
        <source>&amp;Settings</source>
        <translation>&amp;Preferencias</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="105"/>
        <source>Ctrl+s</source>
        <translation>Ctrl+s</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="110"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Actualizar</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="126"/>
        <source>Ctrl+1</source>
        <translation>Ctrl+1</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="127"/>
        <source>Ctrl+2</source>
        <translation>Ctrl+2</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="128"/>
        <source>Ctrl+3</source>
        <translation>Ctrl+3</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="133"/>
        <source>&amp;Alsa configuration</source>
        <translation>Configuración &amp;Alsa</translation>
    </message>
    <message>
        <source>&amp;Alsa config.</source>
        <translation type="obsolete">&amp;Alsa config.</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="134"/>
        <source>Ctrl+c</source>
        <translation>Ctrl+c</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="141"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="148"/>
        <source>&amp;Info</source>
        <translation>&amp;Información</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="157"/>
        <source>&amp;File</source>
        <translation>&amp;Fichero</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="165"/>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="178"/>
        <source>&amp;Extras</source>
        <translation>&amp;Extras</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="183"/>
        <source>&amp;Help</source>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="275"/>
        <source>Normal &amp;screen</source>
        <translation>&amp;Ventana normal</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="279"/>
        <source>Full&amp;screen</source>
        <translation>&amp;Pantalla completa</translation>
    </message>
</context>
<context>
    <name>Message_Widget</name>
    <message>
        <location filename="../../src/message_widget.cpp" line="36"/>
        <source>Mixer device couldn&apos;t be opened</source>
        <translation>El dispositivo de mezcla no se pudo abrir</translation>
    </message>
    <message>
        <location filename="../../src/message_widget.cpp" line="37"/>
        <source>No device selected</source>
        <translation>Nada seleccionado</translation>
    </message>
    <message>
        <location filename="../../src/message_widget.cpp" line="77"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../../src/message_widget.cpp" line="78"/>
        <source>Function</source>
        <translation>Función</translation>
    </message>
    <message>
        <location filename="../../src/message_widget.cpp" line="79"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
</context>
<context>
    <name>Mini_Mixer</name>
    <message>
        <location filename="../../src/mini_mixer.cpp" line="71"/>
        <source>&amp;Show mixer</source>
        <translation>Alzar</translation>
    </message>
    <message>
        <location filename="../../src/mini_mixer.cpp" line="72"/>
        <source>Ctrl+s</source>
        <translation>Ctrl+s</translation>
    </message>
    <message>
        <location filename="../../src/mini_mixer.cpp" line="77"/>
        <source>&amp;Close %1</source>
        <translation>&amp;Cerrar %1</translation>
    </message>
    <message>
        <location filename="../../src/mini_mixer.cpp" line="106"/>
        <source>Volume at %1%</source>
        <translation>Volumen: %1%</translation>
    </message>
</context>
<context>
    <name>QSnd::Alsa_Config_Model</name>
    <message>
        <location filename="../../src/qsnd/alsa_config_model.cpp" line="230"/>
        <source>Key</source>
        <translation>Clave</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_config_model.cpp" line="232"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
</context>
<context>
    <name>QSnd::Alsa_Config_View</name>
    <message>
        <location filename="../../src/qsnd/alsa_config_view.cpp" line="32"/>
        <source>&amp;Expand</source>
        <translation>&amp;Expandir</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_config_view.cpp" line="33"/>
        <source>Co&amp;llapse</source>
        <translation>&amp;Colapsar</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_config_view.cpp" line="34"/>
        <source>&amp;Sort</source>
        <translation>&amp;Ordenar</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_config_view.cpp" line="36"/>
        <source>Depth:</source>
        <translation>Profundidad:</translation>
    </message>
</context>
<context>
    <name>QSnd::Controls_Model</name>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="36"/>
        <source>Cards</source>
        <translation>Interfaz</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="43"/>
        <source>Plugins</source>
        <translation>Plugins</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="215"/>
        <source>Default card</source>
        <translation>Interfaz por defecto</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="234"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="239"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="244"/>
        <source>Mixer name</source>
        <translation>Nombre del mezclador</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_CTL</name>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="60"/>
        <source>yes</source>
        <translation>si</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="61"/>
        <source>no</source>
        <translation>no</translation>
    </message>
    <message>
        <source>%1 elements</source>
        <translation type="obsolete">%1 elementos</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="63"/>
        <source>Range:</source>
        <translation>Rango:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="64"/>
        <source>Elements of the type %1 are not supported</source>
        <translation>Elementos del tipo %1 no están soportados</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="68"/>
        <source>Element name</source>
        <translation>Nombre del elemento</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="72"/>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="172"/>
        <source>Element index</source>
        <translation>Índice del elemento</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="73"/>
        <source>Channel index</source>
        <translation>Índice de canal</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="76"/>
        <source>El. index: %1</source>
        <translation>Índ. elem. %1</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="78"/>
        <source>Channel: %2</source>
        <translation>Canal: %2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="158"/>
        <source>Index:</source>
        <translation>Índice:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="159"/>
        <source>Device:</source>
        <translation>Dispositivo:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="161"/>
        <source>Active:</source>
        <translation>Activo:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="162"/>
        <source>Volatile:</source>
        <translation>Volatil:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="163"/>
        <source>Readable:</source>
        <translation>Lectura:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="164"/>
        <source>Writable:</source>
        <translation>Escritura:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="166"/>
        <source>Channels:</source>
        <translation>Canales:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="167"/>
        <source>Num. id:</source>
        <translation>Núm. Id.:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="173"/>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="202"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="175"/>
        <source>Is active</source>
        <translation>activado</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="176"/>
        <source>Is volatile</source>
        <translation>volatil</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="177"/>
        <source>Is readable</source>
        <translation>legible</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="178"/>
        <source>Is writable</source>
        <translation>escribible</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="180"/>
        <source>Channel count</source>
        <translation>Contador de canal</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="181"/>
        <source>Numeric Id</source>
        <translation>Id. numérico</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="203"/>
        <source>Subdevice</source>
        <translation>Subdispositivo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="445"/>
        <source>Toggle all</source>
        <translation>Activar todo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="1237"/>
        <source>%1 - %2</source>
        <translation>%1 - %2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="1260"/>
        <source>Joined changing</source>
        <translation>Cambio unido</translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="1277"/>
        <source>%n elements</source>
        <translation>
            <numerusform>%n elementos</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source> </source>
        <translation type="obsolete">.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="1441"/>
        <source>No element selected</source>
        <translation>Nada seleccionado</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_CTL_Table_Model</name>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="31"/>
        <source>%1,%2</source>
        <translation>%1,%2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="33"/>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="35"/>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="37"/>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="39"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="34"/>
        <source>r</source>
        <translation>r</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="36"/>
        <source>w</source>
        <translation>w</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="38"/>
        <source>a</source>
        <translation>a</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="40"/>
        <source>v</source>
        <translation>v</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="46"/>
        <source>not readable</source>
        <translation>no legible</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="47"/>
        <source>readable</source>
        <translation>legible</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="48"/>
        <source>not writable</source>
        <translation>no escribible</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="49"/>
        <source>writable</source>
        <translation>escribible</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="50"/>
        <source>not active</source>
        <translation>no activo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="51"/>
        <source>active</source>
        <translation>activo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="52"/>
        <source>not volatile</source>
        <translation>no volatil</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="53"/>
        <source>volatile</source>
        <translation>volatil</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="57"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="58"/>
        <source>Element name</source>
        <translation>Nombre elem.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="60"/>
        <source>Idx</source>
        <translation>Idx</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="61"/>
        <source>Element index</source>
        <translation>Elem. índice</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="63"/>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="64"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="66"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="67"/>
        <source>Element type</source>
        <translation>Tipo elem.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="69"/>
        <source>Ch.</source>
        <translation>Ch.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="70"/>
        <source>Channel count</source>
        <translation>Contador canal</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="72"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="73"/>
        <source>Is readable</source>
        <translation>legible</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="75"/>
        <source>W</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="76"/>
        <source>Is writable</source>
        <translation>escribible</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="78"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="79"/>
        <source>Is active</source>
        <translation>activo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="81"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="82"/>
        <source>Is volatile</source>
        <translation>volatil</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="84"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="85"/>
        <source>Numeric Id</source>
        <translation>Id. numérico</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_CTL_Tree_Model</name>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_tree_model.cpp" line="30"/>
        <source>Elements</source>
        <translation>Elementos</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_Sliders</name>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="51"/>
        <source>c</source>
        <translation>c</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="55"/>
        <source>l</source>
        <translation>l</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="59"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="64"/>
        <source>&amp;Level channels</source>
        <translation>&amp;nivel canales</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="66"/>
        <source>Split &amp;channels</source>
        <translation>Separar &amp;canales</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="67"/>
        <source>Join &amp;channels</source>
        <translation>Unir &amp;canales</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="68"/>
        <source>&amp;Mute</source>
        <translation>&amp;Silenciar</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="69"/>
        <source>&amp;Mute all</source>
        <translation>&amp;Silenciar todo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="70"/>
        <source>Un&amp;mute</source>
        <translation>Des&amp;silenciar</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="71"/>
        <source>Un&amp;mute all</source>
        <translation>Des&amp;silenciar todo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="72"/>
        <source>Toggle &amp;mutes</source>
        <translation>Alternat &amp;silencios</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="315"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="99"/>
        <source>Playback slider</source>
        <translation>Deslizador de reproducción</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="100"/>
        <source>Capture slider</source>
        <translation>Deslizador de captura</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="101"/>
        <source>Playback switch</source>
        <translation>Interruptor de reproducción</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="102"/>
        <source>Capture switch</source>
        <translation>Interruptor de captura</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_Sliders_Status_Bar</name>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="63"/>
        <source>Vol.:</source>
        <translation>Vol.:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="64"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="80"/>
        <source>Current volume</source>
        <translation>Volumen actual</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="91"/>
        <source>Volume range</source>
        <translation>Rango de volumen</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="115"/>
        <source>dB:</source>
        <translation>dB:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="116"/>
        <source>Decibel</source>
        <translation>Decibelios</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="135"/>
        <source>Current Decibel value</source>
        <translation>Valor actual de decibelios</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="146"/>
        <source>Decibel range</source>
        <translation>Rango de decibelios</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="167"/>
        <source>Elem.:</source>
        <translation>Elem.:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="168"/>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="169"/>
        <source>Element name</source>
        <translation>Nombre del elemento</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_Switches</name>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="47"/>
        <source>c</source>
        <translation>c</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="51"/>
        <source>Split &amp;channels</source>
        <translation>Separar &amp;canales</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="52"/>
        <source>Join &amp;channels</source>
        <translation>Unir &amp;canales</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="358"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="373"/>
        <source>Playback selection</source>
        <translation>Selección de reproducción</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="375"/>
        <source>Playback switch</source>
        <translation>Interruptor de reproducción</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="379"/>
        <source>Capture selection</source>
        <translation>Selección de captura</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="381"/>
        <source>Capture switch</source>
        <translation>Interruptor de captura</translation>
    </message>
</context>
<context>
    <name>QSnd::Snd_CTL_Info_Model</name>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="32"/>
        <source>Index</source>
        <translation>Índice</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="33"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="34"/>
        <source>Driver</source>
        <translation>Módulo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="35"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="36"/>
        <source>Long name</source>
        <translation>Nombre completo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="37"/>
        <source>Mixer name</source>
        <translation>Nombre del mezclador</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="38"/>
        <source>Components</source>
        <translation>Componentes</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="40"/>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="43"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="41"/>
        <source>Device index</source>
        <translation>Índice de interfaz</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="44"/>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="51"/>
        <source>Device id</source>
        <translation>Id de interfaz</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="46"/>
        <source>Id / Name</source>
        <translation>Id / Nombre</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="49"/>
        <source>%1 / %2</source>
        <translation>%1 / %2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="52"/>
        <source>Device name</source>
        <translation>Nombre del dispositivo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="54"/>
        <source>Subdevices</source>
        <translation>Subinterfaz</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="57"/>
        <source>Playback</source>
        <translation>Reproducción</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="58"/>
        <source>Capture</source>
        <translation>Captura</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="60"/>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="61"/>
        <source>%1 : %2 available</source>
        <translation>%1 : %2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="63"/>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="64"/>
        <source>Existing : Available</source>
        <translation>Existe : Disponible</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="70"/>
        <source>Key</source>
        <translation>Clave</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="71"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="78"/>
        <source>Control</source>
        <translation>Control</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="84"/>
        <source>Control plugin</source>
        <translation>Control de plugin</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="95"/>
        <source>Card</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="114"/>
        <source>Devices</source>
        <translation>Interfaz</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="123"/>
        <source>Number of devices</source>
        <translation>Número de interfaces</translation>
    </message>
    <message>
        <source> </source>
        <translation type="obsolete">.</translation>
    </message>
</context>
<context>
    <name>QSnd::Snd_Mixer_CTL</name>
    <message>
        <location filename="../../src/qsnd/snd_mixer_ctl.cpp" line="109"/>
        <source>Empty device name</source>
        <translation>Dispositivo sin nombre</translation>
    </message>
</context>
<context>
    <name>QSnd::Snd_Mixer_Simple</name>
    <message>
        <location filename="../../src/qsnd/snd_mixer_simple.cpp" line="91"/>
        <source>Empty device name</source>
        <translation>Dispositivo sin nombre</translation>
    </message>
</context>
<context>
    <name>Side_Interface</name>
    <message>
        <location filename="../../src/side_interface.cpp" line="51"/>
        <source>Show stream</source>
        <translation>Mostrar</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="53"/>
        <source>&amp;Playback</source>
        <translation>&amp;Reproducción</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="54"/>
        <source>Show playback elements</source>
        <translation>Mostrar elementos de reproducción</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="56"/>
        <source>&amp;Capture</source>
        <translation>&amp;Captura</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="57"/>
        <source>Show capture elements</source>
        <translation>Mostrar elementos de captura</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="72"/>
        <source>Interface</source>
        <translation>Interfaz</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="88"/>
        <source>View type</source>
        <translation>Tipo de vista</translation>
    </message>
</context>
</TS>
