<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru">
<context>
    <name>ALSA::CTL_Elem_IFace_Name</name>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="91"/>
        <source>CARD</source>
        <translation>CARD</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="92"/>
        <source>HWDEP</source>
        <translation>HWDEP</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="93"/>
        <source>MIXER</source>
        <translation>MIXER</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="94"/>
        <source>PCM</source>
        <translation>PCM</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="95"/>
        <source>RAWMIDI</source>
        <translation>RAWMIDI</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="96"/>
        <source>TIMER</source>
        <translation>TIMER</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="97"/>
        <source>SEQUENCER</source>
        <translation>SEQUENCER</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="98"/>
        <location filename="../../src/qsnd/snd_mixer_ctl_data.cpp" line="105"/>
        <source>Unknown</source>
        <translation>Unknown</translation>
    </message>
</context>
<context>
    <name>ALSA::CTL_Elem_Type_Name</name>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="101"/>
        <source>NONE</source>
        <translation>NONE</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="102"/>
        <source>BOOLEAN</source>
        <translation>BOOLEAN</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="103"/>
        <source>INTEGER</source>
        <translation>INTEGER</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="104"/>
        <source>ENUMERATED</source>
        <translation>ENUMERATED</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="105"/>
        <source>BYTES</source>
        <translation>BYTES</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="106"/>
        <source>IEC958</source>
        <translatorcomment>S/PDIF</translatorcomment>
        <translation>IEC958</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="107"/>
        <source>INTEGER64</source>
        <translation>INTEGER64</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="108"/>
        <location filename="../../src/qsnd/snd_mixer_ctl_data.cpp" line="65"/>
        <source>Unknown</source>
        <translation>Unknown</translation>
    </message>
</context>
<context>
    <name>ALSA::Channel_Name</name>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="80"/>
        <source>Front Left</source>
        <translation>Фронтальный левый</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="81"/>
        <source>Front Right</source>
        <translation>Фронтальный правый</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="82"/>
        <source>Rear Left</source>
        <translation>Задний левый</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="83"/>
        <source>Rear Right</source>
        <translation>Задний правый</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="84"/>
        <source>Front Center</source>
        <translation>Фронтальный центральный</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="85"/>
        <source>Woofer</source>
        <translatorcomment>SubWoofer</translatorcomment>
        <translation>Сабвуфер</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="86"/>
        <source>Side Left</source>
        <translation>Боковой левый</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="87"/>
        <source>Side Right</source>
        <translation>Боковой правый</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="88"/>
        <source>Rear Center</source>
        <translation>Задний центральный</translation>
    </message>
</context>
<context>
    <name>ALSA::Elem_Name</name>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="28"/>
        <source>Auto Gain Control</source>
        <translation type="unfinished">Auto Gain Control</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="29"/>
        <source>Bass</source>
        <translation type="unfinished">Bass</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="30"/>
        <source>Beep</source>
        <translation type="unfinished">Beep</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="31"/>
        <source>Capture</source>
        <translation type="unfinished">Capture</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="32"/>
        <source>Center</source>
        <translation type="unfinished">Center</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="33"/>
        <source>Channel Mode</source>
        <translation type="unfinished">Channel Mode</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="34"/>
        <source>External Amplifier</source>
        <translation type="unfinished">External Amplifier</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="35"/>
        <source>External Mic</source>
        <translation type="unfinished">External Mic</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="36"/>
        <source>Ext Mic</source>
        <translation type="unfinished">Ext Mic</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="37"/>
        <source>Front</source>
        <translation type="unfinished">Front</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="38"/>
        <source>Front Mic</source>
        <translation>Фронтальный микрофон</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="39"/>
        <source>Front Mic Boost</source>
        <translation type="unfinished">Front Mic Boost</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="40"/>
        <source>Headphone</source>
        <translation type="unfinished">Headphone</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="41"/>
        <source>Headphone LFE</source>
        <translation type="unfinished">Headphone LFE</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="42"/>
        <source>Headphone Center</source>
        <translation type="unfinished">Headphone Center</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="43"/>
        <source>Input Source</source>
        <translation type="unfinished">Input Source</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="44"/>
        <source>Internal Mic</source>
        <translation type="unfinished">Internal Mic</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="45"/>
        <source>Int Mic</source>
        <translation type="unfinished">Int Mic</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="46"/>
        <source>Master</source>
        <translation type="unfinished">Master</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="47"/>
        <source>Mic</source>
        <translation>Микрофон</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="48"/>
        <source>Mic Boost</source>
        <translation type="unfinished">Mic Boost</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="49"/>
        <source>Mic Boost (+20dB)</source>
        <translation type="unfinished">Mic Boost (+20dB)</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="50"/>
        <source>Mic Select</source>
        <translation type="unfinished">Mic Select</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="51"/>
        <source>PC Speaker</source>
        <translation type="unfinished">PC Speaker</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="52"/>
        <source>Phone</source>
        <translation type="unfinished">Phone</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="53"/>
        <source>Side</source>
        <translation type="unfinished">Side</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="54"/>
        <source>Speaker</source>
        <translation type="unfinished">Speaker</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="55"/>
        <source>Surround</source>
        <translation type="unfinished">Surround</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="56"/>
        <source>Treble</source>
        <translation type="unfinished">Treble</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="57"/>
        <source>Video</source>
        <translation type="unfinished">Video</translation>
    </message>
</context>
<context>
    <name>ALSA::Enum_Value</name>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="60"/>
        <source>1ch</source>
        <translation type="unfinished">1ch</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="61"/>
        <source>2ch</source>
        <translation type="unfinished">2ch</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="62"/>
        <source>3ch</source>
        <translation type="unfinished">3ch</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="63"/>
        <source>4ch</source>
        <translation type="unfinished">4ch</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="64"/>
        <source>5ch</source>
        <translation type="unfinished">5ch</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="65"/>
        <source>6ch</source>
        <translation type="unfinished">6ch</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="66"/>
        <source>7ch</source>
        <translation type="unfinished">7ch</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="67"/>
        <source>8ch</source>
        <translation type="unfinished">8ch</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="69"/>
        <source>Front Mic</source>
        <translation>Фронтальный микрофон</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="70"/>
        <source>Mic</source>
        <translation>Микрофон</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="71"/>
        <source>Mic1</source>
        <translation>Микрофон 1</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="72"/>
        <source>Mic2</source>
        <translation>Микрофон 2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="73"/>
        <source>Mic3</source>
        <translation>Микрофон 3</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="74"/>
        <source>Mic4</source>
        <translation>Микрофон 4</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="76"/>
        <source>Video</source>
        <translation type="unfinished">Video</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_i18n.hpp" line="77"/>
        <source>Phone</source>
        <translation type="unfinished">Phone</translation>
    </message>
</context>
<context>
    <name>Dialog_Alsa_Config</name>
    <message>
        <location filename="../../src/dialog_alsa_config.cpp" line="31"/>
        <source>Alsa configuration tree</source>
        <translation>Конфигурация ALSA</translation>
    </message>
    <message>
        <location filename="../../src/dialog_alsa_config.cpp" line="40"/>
        <source>&amp;Reload</source>
        <translation>&amp;Обновить</translation>
    </message>
    <message>
        <location filename="../../src/dialog_alsa_config.cpp" line="41"/>
        <source>&amp;Close</source>
        <translation>&amp;Закрыть</translation>
    </message>
</context>
<context>
    <name>Dialog_Info</name>
    <message>
        <location filename="../../src/dialog_info.cpp" line="37"/>
        <source>Info</source>
        <translatorcomment>Info</translatorcomment>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="43"/>
        <source>%1 - %2</source>
        <translation>%1 - %2</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="94"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="98"/>
        <source>%1 is a mixer application for the linux sound system %2.</source>
        <translation>%1 — это приложение-микшер для звуковой системы %2.</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="106"/>
        <source>Internet</source>
        <translation>Интернет</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="109"/>
        <location filename="../../src/dialog_info.cpp" line="110"/>
        <source>Home page</source>
        <translation>Домашняя страница</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="114"/>
        <location filename="../../src/dialog_info.cpp" line="115"/>
        <source>Project page</source>
        <translation>Страница проекта</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="129"/>
        <source>Developers</source>
        <translation>Программирование</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="133"/>
        <source>Translators</source>
        <translation>Перевод</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="134"/>
        <source>German</source>
        <translation>Немецкий</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="135"/>
        <source>Spanish</source>
        <translation>Испанский</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="136"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="139"/>
        <source>Contributors</source>
        <translation>Участники</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="142"/>
        <source>Testing</source>
        <translation>Тестирование</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="142"/>
        <source>Ideas</source>
        <translation>Идеи</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="156"/>
        <source>The license file %1 is not available.</source>
        <translation>Файл лицензии %1 не доступен.</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="164"/>
        <source>&amp;Information</source>
        <translation>&amp;Информация</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="165"/>
        <source>&amp;People</source>
        <translation>&amp;Авторы</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="166"/>
        <source>&amp;License</source>
        <translation>&amp;Лицезия</translation>
    </message>
    <message>
        <location filename="../../src/dialog_info.cpp" line="173"/>
        <source>&amp;Close</source>
        <translation>&amp;Закрыть</translation>
    </message>
</context>
<context>
    <name>Dialog_Settings</name>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="38"/>
        <location filename="../../src/dialog_settings.cpp" line="119"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="43"/>
        <source>Appearance</source>
        <translation>Внешний вид</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="44"/>
        <source>Input</source>
        <translation>Управление</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="45"/>
        <source>System tray</source>
        <translation>Трей</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="46"/>
        <source>Mini mixer</source>
        <translation>Минимикшер</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="187"/>
        <source>Show slider status bar</source>
        <translation>Отображать статусную строку</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="219"/>
        <source>Rotation amount for a slider change from 0% to 100%</source>
        <translation>На сколько нужно повернуть колесико мыши, чтобы изменить громкость от 0% до 100%</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="221"/>
        <source>degrees</source>
        <translation>градусов</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="316"/>
        <source>on minimize</source>
        <translation>При сворачивании</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="317"/>
        <source>on close</source>
        <translation>При закрытии</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="354"/>
        <source>Default card</source>
        <translation>Устройство по умолчанию</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="365"/>
        <source>User device:</source>
        <translation>Устройство:</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="366"/>
        <source>e.g. hw:0</source>
        <translation>Например: hw:0</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="414"/>
        <source>Balloon lifetime</source>
        <translation>Длительность всплывающей подсказки</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="416"/>
        <source>ms</source>
        <translation>мс</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="168"/>
        <source>Side bar</source>
        <translation>Боковая панель</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="171"/>
        <source>Show view type selection</source>
        <translation>Показывать режим отображения</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="184"/>
        <source>Simple mixer view</source>
        <translation>Режим простого микшера</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="218"/>
        <source>Mouse wheel</source>
        <translation>Колесико мыши</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="247"/>
        <source>%1 turns</source>
        <translation>%1 оборота</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="290"/>
        <source>Show icon in system tray</source>
        <translation>Отображать иконку в трее</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="292"/>
        <source>Don&apos;t use tray</source>
        <translation>Не использовать трей</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="293"/>
        <source>Only when minimized to tray</source>
        <translation>Только когда свернуто</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="294"/>
        <source>Always (even when the mixer is visible)</source>
        <translation>Всегда</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="314"/>
        <source>Minimize to tray</source>
        <translation>Сворачивать в трей</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="352"/>
        <source>Mini mixer device</source>
        <translation>Устройство для минимикшера</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="355"/>
        <source>Current (same as in main mixer window)</source>
        <translation>Текущее (то же устройство, что и в главном окне)</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="356"/>
        <source>User defined</source>
        <translation>Другое</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="400"/>
        <source>Volume change notification</source>
        <translation>Уведомление об изменении громкости</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="406"/>
        <source>Show balloon on a volume change</source>
        <translation>Отображать всплывающую подсказку при изменении громкости</translation>
    </message>
    <message>
        <location filename="../../src/dialog_settings.cpp" line="73"/>
        <source>&amp;Close</source>
        <translation>&amp;Закрыть</translation>
    </message>
</context>
<context>
    <name>Main_Mixer_Window</name>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="49"/>
        <source>Simple mixer</source>
        <translation>Простой микшер</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="50"/>
        <source>Element mixer</source>
        <translation>Поэлементный микшер</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="51"/>
        <source>Control info</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="98"/>
        <source>&amp;Quit</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="104"/>
        <source>&amp;Settings</source>
        <translation>&amp;Настройки</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="105"/>
        <source>Ctrl+s</source>
        <translation>Ctrl+s</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="110"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Обновить</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="126"/>
        <source>Ctrl+1</source>
        <translation>Ctrl+1</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="127"/>
        <source>Ctrl+2</source>
        <translation>Ctrl+2</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="128"/>
        <source>Ctrl+3</source>
        <translation>Ctrl+3</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="133"/>
        <source>&amp;Alsa configuration</source>
        <translation>Конфигурация &amp;ALSA</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="134"/>
        <source>Ctrl+c</source>
        <translation>Ctrl+c</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="141"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="148"/>
        <source>&amp;Info</source>
        <translation>&amp;Информация</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="157"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="165"/>
        <source>&amp;View</source>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="178"/>
        <source>&amp;Extras</source>
        <translation>&amp;Дополнительно</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="183"/>
        <source>&amp;Help</source>
        <translation>&amp;Помощь</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="275"/>
        <source>Normal &amp;screen</source>
        <translation>Выйти из п&amp;олноэкранного режима</translation>
    </message>
    <message>
        <location filename="../../src/main_mixer_window.cpp" line="279"/>
        <source>Full&amp;screen</source>
        <translation>П&amp;олноэкранный режим</translation>
    </message>
</context>
<context>
    <name>Message_Widget</name>
    <message>
        <location filename="../../src/message_widget.cpp" line="36"/>
        <source>Mixer device couldn&apos;t be opened</source>
        <translation>Не удалось открыть микшер</translation>
    </message>
    <message>
        <location filename="../../src/message_widget.cpp" line="37"/>
        <source>No device selected</source>
        <translation>Устройство не выбрано</translation>
    </message>
    <message>
        <location filename="../../src/message_widget.cpp" line="77"/>
        <source>Device</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <location filename="../../src/message_widget.cpp" line="78"/>
        <source>Function</source>
        <translation>Функция</translation>
    </message>
    <message>
        <location filename="../../src/message_widget.cpp" line="79"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
</context>
<context>
    <name>Mini_Mixer</name>
    <message>
        <location filename="../../src/mini_mixer.cpp" line="72"/>
        <source>&amp;Show mixer</source>
        <translation>&amp;Отображать микшер</translation>
    </message>
    <message>
        <location filename="../../src/mini_mixer.cpp" line="73"/>
        <source>Ctrl+s</source>
        <translation>Ctrl+s</translation>
    </message>
    <message>
        <location filename="../../src/mini_mixer.cpp" line="78"/>
        <source>&amp;Close %1</source>
        <translation>&amp;Закрыть %1</translation>
    </message>
    <message>
        <location filename="../../src/mini_mixer.cpp" line="107"/>
        <source>Volume at %1%</source>
        <translation>Громкость: %1%</translation>
    </message>
</context>
<context>
    <name>QSnd::Alsa_Config_Model</name>
    <message>
        <location filename="../../src/qsnd/alsa_config_model.cpp" line="230"/>
        <source>Key</source>
        <translation>Ключ</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_config_model.cpp" line="232"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
</context>
<context>
    <name>QSnd::Alsa_Config_View</name>
    <message>
        <location filename="../../src/qsnd/alsa_config_view.cpp" line="32"/>
        <source>&amp;Expand</source>
        <translation>&amp;Развернуть</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_config_view.cpp" line="33"/>
        <source>Co&amp;llapse</source>
        <translation>&amp;Свернуть</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_config_view.cpp" line="34"/>
        <source>&amp;Sort</source>
        <translation>&amp;Сортировать</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/alsa_config_view.cpp" line="36"/>
        <source>Depth:</source>
        <translation>Глубина:</translation>
    </message>
</context>
<context>
    <name>QSnd::Controls_Model</name>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="36"/>
        <source>Cards</source>
        <translation>Устройства</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="43"/>
        <source>Plugins</source>
        <translation>Плагины</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="215"/>
        <source>Default card</source>
        <translation>Устройство по умолчанию</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="234"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="239"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/controls_model.cpp" line="244"/>
        <source>Mixer name</source>
        <translation>Название микшера</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_CTL</name>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="60"/>
        <source>yes</source>
        <translation>да</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="61"/>
        <source>no</source>
        <translation>нет</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="63"/>
        <source>Range:</source>
        <translation>Диапазон:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="64"/>
        <source>Elements of the type %1 are not supported</source>
        <translation>Элемент типа %1 не поддерживается</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="68"/>
        <source>Element name</source>
        <translation>Название элемента</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="72"/>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="172"/>
        <source>Element index</source>
        <translation>Индекс элемента</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="73"/>
        <source>Channel index</source>
        <translation>Номер канала</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="76"/>
        <source>El. index: %1</source>
        <translation>Индекс элемента: %1</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="78"/>
        <source>Channel: %2</source>
        <translation>Номер канала: %2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="158"/>
        <source>Index:</source>
        <translation>Номер:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="159"/>
        <source>Device:</source>
        <translation>Устройство:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="161"/>
        <source>Active:</source>
        <translation>Активно:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="162"/>
        <source>Volatile:</source>
        <translation>Изменяющийся:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="163"/>
        <source>Readable:</source>
        <translation>Доступно на чтение:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="164"/>
        <source>Writable:</source>
        <translation>Доступно на запись:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="166"/>
        <source>Channels:</source>
        <translation>Количество каналов:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="167"/>
        <source>Num. id:</source>
        <translation>Числовой идентификатор:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="173"/>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="202"/>
        <source>Device</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="175"/>
        <source>Is active</source>
        <translation>Активно</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="176"/>
        <source>Is volatile</source>
        <translation>Изменяющийся</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="177"/>
        <source>Is readable</source>
        <translation>Доступно на чтение</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="178"/>
        <source>Is writable</source>
        <translation>Доступно на запись</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="180"/>
        <source>Channel count</source>
        <translation>Количество каналов</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="181"/>
        <source>Numeric Id</source>
        <translation>Числовой идентификатор</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="203"/>
        <source>Subdevice</source>
        <translation>Дочернее устройство</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="445"/>
        <source>Toggle all</source>
        <translation>Переключить</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="1237"/>
        <source>%1 - %2</source>
        <translation>%1 - %2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="1260"/>
        <source>Joined changing</source>
        <translation>Изменять одновременно все каналы</translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="1277"/>
        <source>%n elements</source>
        <translation>
            <numerusform>%n элемент</numerusform>
            <numerusform>%n элемента</numerusform>
            <numerusform>%n элементов</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl.cpp" line="1441"/>
        <source>No element selected</source>
        <translation>Элемент не выбран</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_CTL_Table_Model</name>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="31"/>
        <source>%1,%2</source>
        <translation>%1,%2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="33"/>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="35"/>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="37"/>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="39"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="34"/>
        <source>r</source>
        <translation>r</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="36"/>
        <source>w</source>
        <translation>w</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="38"/>
        <source>a</source>
        <translation>a</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="40"/>
        <source>v</source>
        <translation>v</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="46"/>
        <source>not readable</source>
        <translation>не доступен на чтение</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="47"/>
        <source>readable</source>
        <translation>доступен на чтение</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="48"/>
        <source>not writable</source>
        <translation>не доступен на запись</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="49"/>
        <source>writable</source>
        <translation>доступен на запись</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="50"/>
        <source>not active</source>
        <translation>не активен</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="51"/>
        <source>active</source>
        <translation>активен</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="52"/>
        <source>not volatile</source>
        <translation>неизменяющийся</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="53"/>
        <source>volatile</source>
        <translation>изменяющийся</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="57"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="58"/>
        <source>Element name</source>
        <translation>Название элемента</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="60"/>
        <source>Idx</source>
        <translation>Номер</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="61"/>
        <source>Element index</source>
        <translation>Номер элемента</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="63"/>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="64"/>
        <source>Device</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="66"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="67"/>
        <source>Element type</source>
        <translation>Тип элемента</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="69"/>
        <source>Ch.</source>
        <translation>Ch.</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="70"/>
        <source>Channel count</source>
        <translation>Количество каналов</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="72"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="73"/>
        <source>Is readable</source>
        <translation>Доступнен на чтение</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="75"/>
        <source>W</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="76"/>
        <source>Is writable</source>
        <translation>Доступнен на запись</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="78"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="79"/>
        <source>Is active</source>
        <translation>Активен</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="81"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="82"/>
        <source>Is volatile</source>
        <translation>Изменяющийся</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="84"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_table_model.cpp" line="85"/>
        <source>Numeric Id</source>
        <translation>Числовой идентификатор</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_CTL_Tree_Model</name>
    <message>
        <location filename="../../src/qsnd/mixer_ctl_tree_model.cpp" line="30"/>
        <source>Elements</source>
        <translation>Элементы</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_Sliders</name>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="51"/>
        <source>c</source>
        <translation>r</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="55"/>
        <source>l</source>
        <translation>y</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="59"/>
        <source>m</source>
        <translation>d</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="64"/>
        <source>&amp;Level channels</source>
        <translation>Уров&amp;ень каналов</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="66"/>
        <source>Split &amp;channels</source>
        <translation>Разделить &amp;каналы</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="67"/>
        <source>Join &amp;channels</source>
        <translation>Объединить &amp;каналы</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="68"/>
        <source>&amp;Mute</source>
        <translation>&amp;Выключить звук</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="69"/>
        <source>&amp;Mute all</source>
        <translation>&amp;Выключить звук у всех каналов</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="70"/>
        <source>Un&amp;mute</source>
        <translation>&amp;Включить звук</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="71"/>
        <source>Un&amp;mute all</source>
        <translation>&amp;Включить звук у всех каналов</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="72"/>
        <source>Toggle &amp;mutes</source>
        <translation>&amp;Включить/выключить звук</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="315"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="99"/>
        <source>Playback slider</source>
        <translation>Регулятор громкости воспроизведения</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="100"/>
        <source>Capture slider</source>
        <translation>Регулятор громкости записи</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="101"/>
        <source>Playback switch</source>
        <translation>Выключатель воспроизведения</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders.cpp" line="102"/>
        <source>Capture switch</source>
        <translation>Выключатель записи</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_Sliders_Status_Bar</name>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="63"/>
        <source>Vol.:</source>
        <translation>Громкость:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="64"/>
        <source>Volume</source>
        <translation>Громкость</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="80"/>
        <source>Current volume</source>
        <translation>Текущая громкость</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="91"/>
        <source>Volume range</source>
        <translation>Диапазон изменения громкости</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="115"/>
        <source>dB:</source>
        <translation>дБ:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="116"/>
        <source>Decibel</source>
        <translation>Громкость (дБ)</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="135"/>
        <source>Current Decibel value</source>
        <translation>Текущая громкость (дБ)</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="146"/>
        <source>Decibel range</source>
        <translation>Диапазон изменения громкости (дБ)</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="167"/>
        <source>Elem.:</source>
        <translation>Элемент:</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="168"/>
        <location filename="../../src/qsnd/mixer_sliders_status_bar.cpp" line="169"/>
        <source>Element name</source>
        <translation>Название элемента</translation>
    </message>
</context>
<context>
    <name>QSnd::Mixer_Switches</name>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="47"/>
        <source>c</source>
        <translation>r</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="51"/>
        <source>Split &amp;channels</source>
        <translation>Разделить &amp;каналы</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="52"/>
        <source>Join &amp;channels</source>
        <translation>Объединить &amp;каналы</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="358"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="373"/>
        <source>Playback selection</source>
        <translation type="unfinished">Playback selection</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="375"/>
        <source>Playback switch</source>
        <translation>Выключатель воспроизведения</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="379"/>
        <source>Capture selection</source>
        <translation type="unfinished">Capture selection</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/mixer_switches.cpp" line="381"/>
        <source>Capture switch</source>
        <translation>Выключатель записи</translation>
    </message>
</context>
<context>
    <name>QSnd::Snd_CTL_Info_Model</name>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="32"/>
        <source>Index</source>
        <translation>Номер</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="33"/>
        <source>Id</source>
        <translation>Идентификатор</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="34"/>
        <source>Driver</source>
        <translation>Драйвер</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="35"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="36"/>
        <source>Long name</source>
        <translation>Полное название</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="37"/>
        <source>Mixer name</source>
        <translation>Название микшера</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="38"/>
        <source>Components</source>
        <translation>Компоненты</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="40"/>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="43"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="41"/>
        <source>Device index</source>
        <translation>Номер устройства</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="44"/>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="51"/>
        <source>Device id</source>
        <translation>Идентификатор устройства</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="46"/>
        <source>Id / Name</source>
        <translation>Идентификатор / Название</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="49"/>
        <source>%1 / %2</source>
        <translation>%1 / %2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="52"/>
        <source>Device name</source>
        <translation>Название устройства</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="54"/>
        <source>Subdevices</source>
        <translation>Дочерние устройства</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="57"/>
        <source>Playback</source>
        <translation>Воспроизведение</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="58"/>
        <source>Capture</source>
        <translation>Запись</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="60"/>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="61"/>
        <source>%1 : %2 available</source>
        <translation>%1 : %2</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="63"/>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="64"/>
        <source>Existing : Available</source>
        <translation>Установлены : Доступны</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="70"/>
        <source>Key</source>
        <translation>Ключ</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="71"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="78"/>
        <source>Control</source>
        <translation type="unfinished">Control</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="84"/>
        <source>Control plugin</source>
        <translation type="unfinished">Control plugin</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="95"/>
        <source>Card</source>
        <translation type="unfinished">Card</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="114"/>
        <source>Devices</source>
        <translation>Устройства</translation>
    </message>
    <message>
        <location filename="../../src/qsnd/snd_ctl_info_model.cpp" line="123"/>
        <source>Number of devices</source>
        <translation>Количество устройств</translation>
    </message>
</context>
<context>
    <name>QSnd::Snd_Mixer_CTL</name>
    <message>
        <location filename="../../src/qsnd/snd_mixer_ctl.cpp" line="109"/>
        <source>Empty device name</source>
        <translation>Имя устройства пусто</translation>
    </message>
</context>
<context>
    <name>QSnd::Snd_Mixer_Simple</name>
    <message>
        <location filename="../../src/qsnd/snd_mixer_simple.cpp" line="91"/>
        <source>Empty device name</source>
        <translation>Имя устройства пусто</translation>
    </message>
</context>
<context>
    <name>Side_Interface</name>
    <message>
        <location filename="../../src/side_interface.cpp" line="51"/>
        <source>Show stream</source>
        <translation>Показывать</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="53"/>
        <source>&amp;Playback</source>
        <translation>&amp;Воспроизведение</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="54"/>
        <source>Show playback elements</source>
        <translation>Отображать элементы воспроизведения</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="56"/>
        <source>&amp;Capture</source>
        <translation>&amp;Запись</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="57"/>
        <source>Show capture elements</source>
        <translation>Отображать элементы записи</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="72"/>
        <source>Interface</source>
        <translation>Интерфейс</translation>
    </message>
    <message>
        <location filename="../../src/side_interface.cpp" line="88"/>
        <source>View type</source>
        <translation>Режим отображения</translation>
    </message>
</context>
</TS>
