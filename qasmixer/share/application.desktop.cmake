[Desktop Entry]
Type=Application
Name=${PROGRAM_TITLE}
GenericName=Audio mixer
Exec=${PROGRAM_NAME}
Icon=${PROGRAM_NAME}
StartupNotify=false
Terminal=false
Categories=AudioVideo;Audio;Mixer
Comment=Graphical mixer application for the ALSA
Comment[de]=Graphische Mixeranwendung für ALSA
Comment[es]=Mezclador gráfico para ALSA
Comment[ru]=Графический микшер для звуковой системы ALSA
