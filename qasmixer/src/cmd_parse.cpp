//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "cmd_parse.hpp"

#include <iostream>
#include <getopt.h>

#include "config.hpp"


const char copy_info_txt[] =
"\
QasMixer - A mixer application for the Linux sound system ALSA.\n\
\n\
Copyright (C) 2011  Sebastian Holtermann <sebholt@xwmw.org>\n\
\n\
This program is free software: you can redistribute it and/or modify\n\
it under the terms of the GNU General Public License as published by\n\
the Free Software Foundation, either version 3 of the License, or\n\
(at your option) any later version.\n\
\n\
This program is distributed in the hope that it will be useful,\n\
but WITHOUT ANY WARRANTY; without even the implied warranty of\n\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n\
GNU General Public License for more details.\n\
\n\
You should have received a copy of the GNU General Public License\n\
along with this program.  If not, see <http://www.gnu.org/licenses/>.\n\
";


int
parse_options (
	int argc,
	char * argv[],
	CMD_Options * cmd_opts_n )
{
	// Suppresses error messages by getopt_long
	opterr = 0;

	// Clear optinos
	cmd_opts_n->start_minimized = false;
	cmd_opts_n->no_single = false;

	QString card_idx_str;
	QString device_str;

	bool flag_print_version ( false );
	bool flag_print_help ( false );
	bool flag_print_copy_info ( false );

	bool scan_further ( true );

	while ( scan_further ) {

		static struct option long_opts[] =
		{
			{ "help",   no_argument,       0, 'h' },
			{ "card",   required_argument, 0, 'c' },
			{ "device", required_argument, 0, 'D' },

			{ "tray",      no_argument,  0, 't' },
			{ "no-single", no_argument,  0, 'n' },

			{ "copying", no_argument,  0, 'i' },
			{ "version", no_argument,  0, 'v' },
			{0, 0, 0, 0}
		};

		// getopt_long stores the option index here.
		int long_opts_idx ( 0 );
		int opt_char = getopt_long ( argc, argv, "hc:D:tniv",
			long_opts, &long_opts_idx );

		// Leave loop
		if ( opt_char < 0 ) {
			break;
		}

		switch ( opt_char ) {
			case 0:
				break;

			case 'h':
				flag_print_help = true;
				scan_further = false;
				break;

			case 'c':
				card_idx_str = optarg;
				break;

			case 'D':
				device_str = optarg;
				break;


			case 't':
				cmd_opts_n->start_minimized = true;
				break;

			case 'n':
				cmd_opts_n->no_single = true;
				break;


			case 'i':
				flag_print_copy_info = true;
				break;

			case 'v':
				flag_print_version = true;
				break;


			default:
				// Dont't break, as the option may be for QT
				break;
		}
	}

	if ( flag_print_help ) {
		::std::cout << "Usage: " << PROGRAM_NAME << " [options]\n";
		::std::cout << "Options:\n";
		::std::cout << " \n";
		::std::cout << "  -h, --help          prints this help\n";
		::std::cout << "  -c, --card=NUMBER   selects a card\n";
		::std::cout << "  -D, --device=NAME   selects a mixer device (e.g. hw:1)\n";
		::std::cout << " \n";
		::std::cout << "  -t, --tray          start minimized in tray\n";
		::std::cout << "  -n, --no-single     allow multiple instances\n";
		::std::cout << " \n";
		::std::cout << "  -i, --copying       prints copying informations\n";
		::std::cout << "  -v, --version       prints the program version\n";
		return -1;
	}

	if ( flag_print_version ) {
		std::cout << PROGRAM_NAME << " " << VERSION << std::endl;
		return -1;
	}

	if ( flag_print_copy_info ) {
		std::cout << "\n";
		std::cout << copy_info_txt;
		std::cout << "\n";
		return -1;
	}

	if ( !card_idx_str.isEmpty() && device_str.isEmpty() ) {
		bool is_numeric;
		int card_idx ( card_idx_str.toInt ( &is_numeric ) );
		if ( is_numeric ) {
			if ( card_idx < 0 ) {
				card_idx = 0;
			}
			device_str = QString ( "hw:%1" ).arg ( card_idx );
		}
	}

	cmd_opts_n->ctl_address = device_str;

	return 0;
}
