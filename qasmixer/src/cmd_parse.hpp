//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_cmd_parse_hpp__
#define __INC_cmd_parse_hpp__

#include <QString>


///
/// @brief Command line options state
///
struct CMD_Options {

	bool start_minimized;
	bool no_single;

	QString ctl_address;
};


///
/// @brief Command line option parsing
///
int
parse_options (
	int argc,
	char * argv[],
	CMD_Options * cmd_opts_n );


#endif
