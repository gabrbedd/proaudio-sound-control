//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "dialog_alsa_config.hpp"

#include "config.hpp"

#include <QVBoxLayout>
#include <QHBoxLayout>

#include <iostream>


Dialog_Alsa_Config::Dialog_Alsa_Config (
	QSnd::Alsa_Config_View_Settings & settings_n,
	QWidget * parent ) :
QDialog ( parent ),
_cfg_view ( settings_n )
{
	{
		const QString str_settings ( tr ( "Alsa configuration tree" ) );
		setWindowTitle (
			QString ( "%1 - %2" ).arg ( PROGRAM_TITLE ).arg ( str_settings ) );
	}

	QHBoxLayout * lay_bottom ( new QHBoxLayout );
	lay_bottom->setContentsMargins ( 0, 0, 0, 0 );

	{
		_btn_reload.setText ( tr ( "&Reload" ) );
		_btn_close.setText ( tr ( "&Close" ) );
		_btn_close.setDefault ( true );

		if ( QIcon::hasThemeIcon ( "application-exit" ) ) {
			_btn_close.setIcon ( QIcon::fromTheme ( "application-exit" ) );
		}

		if ( QIcon::hasThemeIcon ( "view-refresh" ) ) {
			_btn_reload.setIcon ( QIcon::fromTheme ( "view-refresh" ) );
		}

		connect ( &_btn_close, SIGNAL ( clicked() ),
			this, SLOT ( close() ) );

		lay_bottom->addWidget ( &_btn_reload );
		lay_bottom->addStretch ( 1 );
		lay_bottom->addWidget ( &_btn_close );
	}

	{
		QVBoxLayout * lay_vbox ( new QVBoxLayout );
		lay_vbox->addWidget ( &_cfg_view );
		lay_vbox->addLayout ( lay_bottom );
		setLayout ( lay_vbox );
	}
}


void
Dialog_Alsa_Config::set_model (
	QAbstractItemModel * model_n )
{
	if ( _cfg_view.model() != 0 ) {
		disconnect ( &_btn_reload, 0, _cfg_view.model(), 0 );
	}

	_cfg_view.set_model ( model_n );

	if ( _cfg_view.model() != 0 ) {
		connect ( &_btn_reload, SIGNAL ( clicked() ),
			_cfg_view.model(), SLOT ( revert() ) );
	}
}

