//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_dialog_alsa_config_hpp__
#define __INC_dialog_alsa_config_hpp__

#include <QDialog>
#include <QPushButton>
#include <QAbstractItemModel>

#include "qsnd/alsa_config_view_settings.hpp"
#include "qsnd/alsa_config_view.hpp"


///
/// @brief Dialog_Alsa_Config
///
class Dialog_Alsa_Config :
	public QDialog
{
	Q_OBJECT

	// Public methods
	public:

	Dialog_Alsa_Config (
		QSnd::Alsa_Config_View_Settings & settings_n,
		QWidget * parent = 0 );


	void
	set_model (
		QAbstractItemModel * model_n );


	// Private attributes
	private:

	QSnd::Alsa_Config_View _cfg_view;

	QPushButton _btn_reload;
	QPushButton _btn_close;
};


#endif
