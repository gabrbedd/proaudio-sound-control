//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_dialog_info_hpp__
#define __INC_dialog_info_hpp__

#include <QDialog>
#include <QLabel>
#include <QTabWidget>
#include "wdg/text_browser.hpp"


///
/// @brief Dialog_Info
///
class Dialog_Info :
	public QDialog
{
	Q_OBJECT

	// Public methods
	public:

	Dialog_Info (
		QWidget * parent = 0 );


	// Protected methods
	protected:

	bool
	read_utf8_file (
		const QString & filename_n,
		QString & txt_n ) const;


	// Private attributes
	private:

	QLabel _title;

	QTabWidget _tabs;

	Wdg::Text_Browser _txt_info;

	Wdg::Text_Browser _txt_people;

	Wdg::Text_Browser _txt_license;
};


#endif
