//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "dialog_settings.hpp"

#include "config.hpp"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLocale>
#include <QCheckBox>
#include <QPushButton>
#include <QSplitter>

#include <iostream>


Dialog_Settings::Dialog_Settings (
	Mixer_Settings & settings_n,
	QWidget * parent ) :
QDialog ( parent ),
_settings ( settings_n ),
_title_mask ( "<h3>%1</h3>" )
{
	setWindowTitle (
		QString ( "%1 - %2" )
		.arg ( PROGRAM_TITLE )
		.arg ( tr ( "Settings" ) ) );

	_vspace = qMax ( 0, fontMetrics().height() );

	{
		_it_general = create_item ( tr ( "Appearance" ) );
		_it_input = create_item ( tr ( "Input" ) );
		_it_sys_tray = create_item ( tr ( "System tray" ) );
		_it_mini_mixer = create_item ( tr ( "Mini mixer" ) );

		_it_sys_tray->appendRow ( _it_mini_mixer );

		_model.appendRow ( _it_general );
		_model.appendRow ( _it_input );
		_model.appendRow ( _it_sys_tray );
	}

	_tree_view.setHeaderHidden ( true );
	_tree_view.setRootIsDecorated ( false );
	_tree_view.setModel ( &_model );
	_tree_view.expand_recursive ( QModelIndex(), 2 );

	connect ( &_tree_view, SIGNAL ( activated ( const QModelIndex & ) ),
		this, SLOT ( page_selected ( const QModelIndex & ) ) );


	init_page_general();
	init_page_input();
	init_page_sys_tray();
	init_page_mini_mixer();


	QHBoxLayout * lay_close ( new QHBoxLayout );
	lay_close->setContentsMargins ( 0, 0, 0, 0 );
	{
		QPushButton * btn_close ( new QPushButton ( tr ( "&Close" ) ) );

		if ( QIcon::hasThemeIcon ( "application-exit" ) ) {
			btn_close->setIcon ( QIcon::fromTheme ( "application-exit" ) );
		}

		connect ( btn_close, SIGNAL ( clicked() ),
			this, SLOT ( close() ) );

		//lay_close->addStretch ( 1 );
		lay_close->addWidget ( btn_close );
		lay_close->addStretch ( 1 );
	}


	{
		QVBoxLayout * lay_vbox ( new QVBoxLayout );
		lay_vbox->setContentsMargins ( 0, 0, 0, 0 );
		lay_vbox->addWidget ( &_tree_view, 1 );
		lay_vbox->addLayout ( lay_close, 0 );

		_navi.setLayout ( lay_vbox );
	}


	QVBoxLayout * lay_pages ( new QVBoxLayout );
	{
		_page_general->hide();
		_page_input->hide();
		_page_sys_tray->hide();
		_page_mini_mixer->hide();

		_pages_stack = new QStackedLayout;
		_pages_stack->setContentsMargins ( 0, 0, 0, 0 );
		_pages_stack->addWidget ( _page_general );
		_pages_stack->addWidget ( _page_input );
		_pages_stack->addWidget ( _page_sys_tray );
		_pages_stack->addWidget ( _page_mini_mixer );
		lay_pages->addLayout ( _pages_stack );

		_pages.setLayout ( lay_pages );
	}


	{
		QLabel * lbl_title ( new QLabel );
		lbl_title->setText ( QString ( "<h2>%1</h2>" ).arg ( tr ( "Settings" ) ) );

		QSplitter * hsplit ( new QSplitter );
		hsplit->setChildrenCollapsible ( false );

		hsplit->addWidget ( &_navi );
		hsplit->addWidget ( &_pages );

		hsplit->setStretchFactor ( 0, 2 );
		hsplit->setStretchFactor ( 1, 5 );

		QVBoxLayout * lay_vbox ( new QVBoxLayout );
		lay_vbox->addWidget ( lbl_title, 0 );
		lay_vbox->addWidget ( hsplit, 1 );
		setLayout ( lay_vbox );
	}

	{
		QMargins mgs ( lay_pages->contentsMargins() );
		mgs.setTop ( 0 );
		mgs.setBottom ( 0 );
		lay_pages->setContentsMargins ( mgs );
	}

	update_inputs();
	update_inputs_visibility();

	_tree_view.setCurrentIndex ( _model.index ( 0, 0 ) );
}


QStandardItem *
Dialog_Settings::create_item (
	const QString & txt_n )
{
	QStandardItem * res ( new QStandardItem ( txt_n ) );
	res->setToolTip ( txt_n );
	res->setSelectable ( true );
	res->setEditable ( false );
	return res;
}


void
Dialog_Settings::init_page_general ( )
{
	QVBoxLayout * lay_siface ( new QVBoxLayout );
	lay_siface->setContentsMargins ( 0, 0, 0, 0 );
	{
		QLabel * lbl_title ( new QLabel ( _title_mask.arg ( tr ( "Side bar" ) ) ) );

		_btn_view_selection =
			new QCheckBox ( tr ( "Show view type selection" ) );

		connect ( _btn_view_selection, SIGNAL ( toggled ( bool ) ),
			this, SLOT ( app_change_view_selection ( bool ) ) );

		lay_siface->addWidget ( lbl_title );
		lay_siface->addWidget ( _btn_view_selection );
	}


	QVBoxLayout * lay_smixer ( new QVBoxLayout );
	lay_smixer->setContentsMargins ( 0, 0, 0, 0 );
	{
		QLabel * lbl_title ( new QLabel ( _title_mask.arg ( tr ( "Simple mixer view" ) ) ) );

		_btn_show_slider_status_bar =
			new QCheckBox ( tr ( "Show slider status bar" ) );

		connect ( _btn_show_slider_status_bar, SIGNAL ( toggled ( bool ) ),
			this, SLOT ( app_change_slider_status ( bool ) ) );

		lay_smixer->addWidget ( lbl_title );
		lay_smixer->addWidget ( _btn_show_slider_status_bar );
	}


	// Layout
	QVBoxLayout * lay_wdg ( new QVBoxLayout );
	lay_wdg->setContentsMargins ( 0, 0, 0, 0 );
	lay_wdg->addLayout ( lay_siface );
	lay_wdg->addSpacing ( _vspace );
	lay_wdg->addLayout ( lay_smixer );
	lay_wdg->addStretch ( 1 );


	_page_general = new QWidget ( this );
	_page_general->setLayout ( lay_wdg );
}



void
Dialog_Settings::init_page_input ( )
{
	QVBoxLayout * lay_mwheel ( new QVBoxLayout );
	lay_mwheel->setContentsMargins ( 0, 0, 0, 0 );
	{
		QLabel * lbl_title ( new QLabel ( _title_mask.arg ( tr ( "Mouse wheel" ) ) ) );
		QLabel * lbl_info ( new QLabel ( tr ( "Rotation amount for a slider change from 0% to 100%" ) ) );

		QLabel * lbl_degrees ( new QLabel ( tr ( "degrees" ) ) );
		QLabel * lbl_pre_turns ( new QLabel ( " (" ) );
		QLabel * lbl_post_turns ( new QLabel ( ")" ) );

		const int range[2] = { 30, 360*15 };
		const int single_step ( 15 );

		_mwheel_degrees_slider = new QSlider ( Qt::Horizontal );
		_mwheel_degrees_slider->setRange ( range[0], range[1] );
		_mwheel_degrees_slider->setSingleStep ( single_step );
		_mwheel_degrees_slider->setPageStep ( single_step*4 );

		connect ( _mwheel_degrees_slider, SIGNAL ( valueChanged ( int ) ),
			this, SLOT ( input_change_mwheel_degrees ( int ) ) );

		_mwheel_degrees_input = new Wdg::Covered_Spinbox;
		_mwheel_degrees_input->set_range ( range[0], range[1] );
		_mwheel_degrees_input->spinbox()->setSingleStep ( 1 );
		_mwheel_degrees_input->spinbox()->setButtonSymbols ( QAbstractSpinBox::NoButtons );

		connect ( _mwheel_degrees_input, SIGNAL ( sig_value_changed ( int ) ),
			this, SLOT ( input_change_mwheel_degrees ( int ) ) );


		// Turns label

		_mwheel_turns_mask = tr ( "%1 turns" );

		_mwheel_turns = new Wdg::Label_Width;
		_mwheel_turns->set_min_text ( _mwheel_turns_mask.arg ( range[1] / 360.0, 0, 'f', 2 ) );
		_mwheel_turns->setAlignment ( Qt::AlignRight | Qt::AlignVCenter );


		// Layout

		QHBoxLayout * lay_input ( new QHBoxLayout );
		lay_input->setContentsMargins ( 0, 0, 0, 0 );
		lay_input->addWidget ( _mwheel_degrees_input );
		lay_input->addWidget ( lbl_degrees );
		lay_input->addWidget ( lbl_pre_turns );
		lay_input->addWidget ( _mwheel_turns );
		lay_input->addWidget ( lbl_post_turns );
		lay_input->addStretch ( 1 );

		lay_mwheel->addWidget ( lbl_title );
		lay_mwheel->addWidget ( lbl_info );
		lay_mwheel->addLayout ( lay_input );
		lay_mwheel->addWidget ( _mwheel_degrees_slider );
	}


	// Layout
	QVBoxLayout * lay_wdg ( new QVBoxLayout );
	lay_wdg->setContentsMargins ( 0, 0, 0, 0 );
	lay_wdg->addLayout ( lay_mwheel );
	lay_wdg->addStretch ( 1 );

	_page_input = new QWidget ( this );
	_page_input->setLayout ( lay_wdg );
}


void
Dialog_Settings::init_page_sys_tray ( )
{
	QVBoxLayout * lay_general ( new QVBoxLayout );
	lay_general->setContentsMargins ( 0, 0, 0, 0 );

	{
		QLabel * lbl_title ( new QLabel ( _title_mask.arg ( tr ( "Show icon in system tray" ) ) ) );

		_btn_tray_show[0] = new QRadioButton ( tr ( "Don't use tray" ) );
		_btn_tray_show[1] = new QRadioButton ( tr ( "Only when minimized to tray" ) );
		_btn_tray_show[2] = new QRadioButton ( tr ( "Always (even when the mixer is visible)" ) );

		_btn_grp_tray_show = new QButtonGroup ( this );
		_btn_grp_tray_show->addButton ( _btn_tray_show[0], 0 );
		_btn_grp_tray_show->addButton ( _btn_tray_show[1], 1 );
		_btn_grp_tray_show->addButton ( _btn_tray_show[2], 2 );

		connect ( _btn_grp_tray_show, SIGNAL ( buttonClicked ( int ) ),
			this, SLOT ( tray_change_show ( int ) ) );

		lay_general->addWidget ( lbl_title );
		lay_general->addWidget ( _btn_tray_show[0] );
		lay_general->addWidget ( _btn_tray_show[1] );
		lay_general->addWidget ( _btn_tray_show[2] );
	}

	{
		QVBoxLayout * lay_mini ( new QVBoxLayout );
		lay_mini->setContentsMargins ( 0, 0, 0, 0 );

		QLabel * lbl_title ( new QLabel ( _title_mask.arg ( tr ( "Minimize to tray" ) ) ) );

		_btn_tray_on_minimize = new QCheckBox ( tr ( "on minimize" ) );
		_btn_tray_on_close = new QCheckBox ( tr ( "on close" ) );

		connect ( _btn_tray_on_minimize, SIGNAL ( toggled ( bool ) ),
			this, SLOT ( tray_change_on_minimize ( bool ) ) );

		connect ( _btn_tray_on_close, SIGNAL ( toggled ( bool ) ),
			this, SLOT ( tray_change_on_close ( bool ) ) );

		lay_mini->addWidget ( lbl_title );
		lay_mini->addWidget ( _btn_tray_on_minimize );
		lay_mini->addWidget ( _btn_tray_on_close );

		_tray_minimize = new QWidget;
		_tray_minimize->setLayout ( lay_mini );
	}

	QVBoxLayout * lay_wdg ( new QVBoxLayout );
	lay_wdg->setContentsMargins ( 0, 0, 0, 0 );
	lay_wdg->addLayout ( lay_general );
	lay_wdg->addSpacing ( _vspace );
	lay_wdg->addWidget ( _tray_minimize );
	lay_wdg->addStretch ( 1 );


	_page_sys_tray = new QWidget ( this );
	_page_sys_tray->setLayout ( lay_wdg );
}


void
Dialog_Settings::init_page_mini_mixer ( )
{
	QVBoxLayout * lay_device ( new QVBoxLayout );
	lay_device->setContentsMargins ( 0, 0, 0, 0 );
	{
		QLabel * lbl_title ( new QLabel ( _title_mask.arg ( tr ( "Mini mixer device" ) ) ) );

		_btn_mini_device[0] = new QRadioButton ( tr ( "Default card" ) );
		_btn_mini_device[1] = new QRadioButton ( tr ( "Current (same as in main mixer window)" ) );
		_btn_mini_device[2] = new QRadioButton ( tr ( "User defined" ) );

		_btn_grp_mini_device = new QButtonGroup ( this );
		_btn_grp_mini_device->addButton ( _btn_mini_device[0], 0 );
		_btn_grp_mini_device->addButton ( _btn_mini_device[1], 1 );
		_btn_grp_mini_device->addButton ( _btn_mini_device[2], 2 );

		_mini_dev_user = new QWidget;
		_mini_dev_user_edit = new QLineEdit;
		QLabel * lbl_user ( new QLabel ( tr ( "User device:" ) ) );
		QLabel * lbl_example ( new QLabel ( tr ( "e.g. hw:0" ) ) );

		QHBoxLayout * lay_dev_user ( new QHBoxLayout );
		lay_dev_user->setContentsMargins ( 0, 0, 0, 0 );
		lay_dev_user->addWidget ( lbl_user );
		lay_dev_user->addWidget ( _mini_dev_user_edit );
		lay_dev_user->addWidget ( lbl_example );
		lay_dev_user->addStretch ( 1 );
		_mini_dev_user->setLayout ( lay_dev_user );

		QVBoxLayout * lay_dev_sel ( new QVBoxLayout );
		lay_dev_sel->setContentsMargins ( 0, 0, 0, 0 );
		lay_dev_sel->addWidget ( _btn_mini_device[0] );
		lay_dev_sel->addWidget ( _btn_mini_device[1] );
		lay_dev_sel->addWidget ( _btn_mini_device[2] );
		lay_dev_sel->addWidget ( _mini_dev_user );
		lay_dev_sel->addStretch ( 1 );

		lay_device->addWidget ( lbl_title );
		lay_device->addLayout ( lay_dev_sel );

		connect ( _btn_grp_mini_device, SIGNAL ( buttonClicked ( int ) ),
			this, SLOT ( mini_change_dev ( int ) ) );

		connect ( _mini_dev_user_edit, SIGNAL ( editingFinished() ),
			this, SLOT ( mini_change_dev_user() ) );
	}


	QVBoxLayout * lay_balloon ( new QVBoxLayout );
	lay_balloon->setContentsMargins ( 0, 0, 0, 0 );
	{
		QLabel * lbl_title ( new QLabel );
		{
			QString txt ( tr ( "Volume change notification" ) );
			txt = _title_mask.arg ( txt );
			lbl_title->setText ( txt );
		}

		_balloon_show = new QCheckBox;
		_balloon_show->setText ( tr ( "Show balloon on a volume change" ) );

		connect ( _balloon_show, SIGNAL ( toggled ( bool ) ),
			this, SLOT ( mini_change_show_balloon ( bool ) ) );

		QVBoxLayout * lay_balloon_time ( new QVBoxLayout );
		lay_balloon_time->setContentsMargins ( 0, 0, 0, 0 );
		{
			QString str_lt ( tr ( "Balloon lifetime" ) );
			QLabel * lbl_lifetime ( new QLabel ( str_lt + ":" ) );
			QLabel * lbl_ms ( new QLabel ( tr ( "ms" ) ) );

			const int range[2] = { 500, 10000 };

			_balloon_time_slider = new QSlider ( Qt::Horizontal );
			_balloon_time_slider->setRange ( range[0], range[1] );
			_balloon_time_slider->setToolTip ( str_lt );

			connect ( _balloon_time_slider, SIGNAL ( valueChanged ( int ) ),
				this, SLOT ( mini_change_balloon_time ( int ) ) );

			_balloon_time_input = new Wdg::Covered_Spinbox;
			_balloon_time_input->set_range ( range[0], range[1] );
			_balloon_time_input->setToolTip ( str_lt );

			connect ( _balloon_time_input, SIGNAL ( sig_value_changed ( int ) ),
				this, SLOT ( mini_change_balloon_time ( int ) ) );

			QHBoxLayout * lay_input ( new QHBoxLayout );
			lay_input->setContentsMargins ( 0, 0, 0, 0 );
			lay_input->addWidget ( lbl_lifetime );
			lay_input->addWidget ( _balloon_time_input );
			lay_input->addWidget ( lbl_ms );
			lay_input->addStretch ( 1 );

			lay_balloon_time->addLayout ( lay_input );
			lay_balloon_time->addWidget ( _balloon_time_slider );
		}
		_balloon_time = new QWidget;
		_balloon_time->setLayout ( lay_balloon_time );


		lay_balloon->addWidget ( lbl_title );
		lay_balloon->addWidget ( _balloon_show );
		lay_balloon->addWidget ( _balloon_time );
	}

	QVBoxLayout * lay_wdg ( new QVBoxLayout );
	lay_wdg->setContentsMargins ( 0, 0, 0, 0 );
	lay_wdg->addLayout ( lay_device );
	lay_wdg->addSpacing ( _vspace );
	lay_wdg->addLayout ( lay_balloon );
	lay_wdg->addStretch ( 1 );


	_page_mini_mixer = new QWidget ( this );
	_page_mini_mixer->setLayout ( lay_wdg );
}


void
Dialog_Settings::update_inputs ( )
{
	_btn_view_selection->setChecked ( _settings.show_view_selection );
	_btn_show_slider_status_bar->setChecked ( _settings.smixer.show_slider_status_bar );

	_mwheel_degrees_slider->setValue ( _settings.wheel_degrees );
	_mwheel_degrees_input->set_value ( _settings.wheel_degrees );
	update_mouse_wheel_turns();

	{
		unsigned int idx ( 0 );
		if ( _settings.show_tray_icon < 3 ) {
			idx = _settings.show_tray_icon;
		}
		_btn_tray_show[idx]->setChecked ( true );
		_tray_minimize->setEnabled ( idx != Mixer_Settings::TRAY_ICON_NEVER );
	}

	_btn_tray_on_minimize->setChecked ( _settings.tray_on_minimize );
	_btn_tray_on_close->setChecked ( _settings.tray_on_close );

	{
		unsigned int idx ( 1 );
		if ( _settings.mini.device_mode < 3 ) {
			idx = _settings.mini.device_mode;
		}
		_btn_mini_device[idx]->setChecked ( true );
	}
	_mini_dev_user_edit->setText ( _settings.mini.user_device );
	_balloon_show->setChecked ( _settings.mini.show_balloon );
	_balloon_time_input->set_value ( _settings.mini.balloon_lifetime );
	_balloon_time_slider->setValue ( _settings.mini.balloon_lifetime );
}


void
Dialog_Settings::page_selected (
	const QModelIndex & index_n )
{
	QStandardItem * item ( _model.itemFromIndex ( index_n ) );
	if ( item == 0 ) {
		return;
	}

	const unsigned int num_pages ( _pages_stack->count() );
	unsigned int idx ( num_pages );
	if ( item == _it_general ) {
		idx = 0;
	} else if ( item == _it_input ) {
		idx = 1;
	} else if ( item == _it_sys_tray ) {
		idx = 2;
	} else if ( item == _it_mini_mixer ) {
		idx = 3;
	}

	if ( idx < num_pages ) {
		_pages_stack->setCurrentIndex ( idx );
	}
}


void
Dialog_Settings::update_mouse_wheel_turns ( )
{
	QString txt ( _mwheel_turns_mask );
	txt = txt.arg ( _settings.wheel_degrees / 360.0, 0, 'f', 2 );

	_mwheel_turns->setText ( txt );
}


void
Dialog_Settings::update_inputs_visibility ( )
{
	_mini_dev_user->setEnabled (
		( _settings.mini.device_mode == Mini_Mixer_Settings::DEV_USER ) );
	_balloon_time->setEnabled ( _settings.mini.show_balloon );
}


void
Dialog_Settings::app_change_view_selection (
	bool flag_n )
{
	if ( _settings.show_view_selection != flag_n ) {
		_settings.show_view_selection = flag_n;
		emit sig_settings_changed_mixer();
	}
}


void
Dialog_Settings::app_change_slider_status (
	bool flag_n )
{
	if ( _settings.smixer.show_slider_status_bar != flag_n ) {
		_settings.smixer.show_slider_status_bar = flag_n;
		emit sig_settings_changed_mixer();
	}
}


void
Dialog_Settings::input_change_mwheel_degrees (
	int value_n )
{
	if ( value_n < 0 ) {
		return;
	}

	_mwheel_degrees_slider->setValue ( value_n );
	_mwheel_degrees_input->set_value ( value_n );

	unsigned int val ( value_n );
	if ( _settings.wheel_degrees != val ) {
		_settings.wheel_degrees = val;
		update_mouse_wheel_turns();
		emit sig_settings_changed_mixer();
		emit sig_settings_changed_mini();
	}
}


void
Dialog_Settings::tray_change_show  (
	int idx_n )
{
	if ( ( idx_n < 0 ) || ( idx_n > Mixer_Settings::TRAY_ICON_LAST ) ) {
		return;
	}

	const unsigned int idx ( idx_n );
	if ( _settings.show_tray_icon != idx ) {
		_settings.show_tray_icon = idx_n;
		_btn_tray_show[idx_n]->setChecked ( true );
		_tray_minimize->setEnabled ( idx != Mixer_Settings::TRAY_ICON_NEVER );
		emit sig_settings_changed_tray();
	}
}


void
Dialog_Settings::tray_change_on_minimize  (
	bool flag_n )
{
	if ( _settings.tray_on_minimize != flag_n ) {
		_settings.tray_on_minimize = flag_n;
		update_inputs_visibility();
		emit sig_settings_changed_tray();
	}
}


void
Dialog_Settings::tray_change_on_close  (
	bool flag_n )
{
	if ( _settings.tray_on_close != flag_n ) {
		_settings.tray_on_close = flag_n;
		update_inputs_visibility();
		emit sig_settings_changed_tray();
	}
}


void
Dialog_Settings::mini_change_dev  (
	int idx_n )
{
	if ( ( idx_n < 0 ) || ( idx_n > Mini_Mixer_Settings::DEV_LAST ) ) {
		return;
	}

	if ( (int)_settings.mini.device_mode != idx_n ) {
		_settings.mini.device_mode = idx_n;
		update_inputs_visibility();
		emit sig_settings_changed_mini();
	}
}


void
Dialog_Settings::mini_change_dev_user ( )
{
	if ( _mini_dev_user_edit->text() != _settings.mini.user_device ) {
		_settings.mini.user_device = _mini_dev_user_edit->text();
		emit sig_settings_changed_mini();
	}
}


void
Dialog_Settings::mini_change_show_balloon (
	bool flag_n )
{
	if ( _settings.mini.show_balloon != flag_n ) {
		_settings.mini.show_balloon = flag_n;
		update_inputs_visibility();
		emit sig_settings_changed_mini();
	}
}


void
Dialog_Settings::mini_change_balloon_time (
	int time_n )
{
	if ( time_n < 0 ) {
		return;
	}

	_balloon_time_input->set_value ( time_n );
	_balloon_time_slider->setValue ( time_n );

	unsigned int msec ( time_n );
	if ( _settings.mini.balloon_lifetime != msec ) {
		_settings.mini.balloon_lifetime = msec;
		emit sig_settings_changed_mini();
	}
}
