//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_dialog_settings_hpp__
#define __INC_dialog_settings_hpp__

#include <QDialog>
#include <QLabel>
#include <QCheckBox>
#include <QRadioButton>
#include <QButtonGroup>
#include <QComboBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QSlider>
#include <QStackedLayout>

#include <QStandardItemModel>
#include "wdg/tree_view_kv.hpp"
#include "wdg/label_width.hpp"
#include "wdg/covered_spinbox.hpp"

#include "mixer_settings.hpp"


///
/// @brief Dialog_Settings
///
class Dialog_Settings :
	public QDialog
{
	Q_OBJECT

	// Public methods
	public:

	Dialog_Settings (
		Mixer_Settings & settings_n,
		QWidget * parent = 0 );


	// Signals
	signals:

	void
	sig_settings_changed_mixer ( );

	void
	sig_settings_changed_tray ( );

	void
	sig_settings_changed_mini ( );


	// Protected slots
	protected slots:

	void
	page_selected (
		const QModelIndex & index_n );


	void
	app_change_view_selection (
		bool flag_n );

	void
	app_change_slider_status (
		bool flag_n );


	void
	input_change_mwheel_degrees (
		int value_n );


	void
	tray_change_show (
		int idx_n );

	void
	tray_change_on_minimize (
		bool flag_n );

	void
	tray_change_on_close (
		bool flag_n );



	void
	mini_change_dev (
		int idx_n );

	void
	mini_change_dev_user ( );

	void
	mini_change_show_balloon (
		bool flag_n );

	void
	mini_change_balloon_time (
		int time_n );


	// Protected methods
	protected:

	QStandardItem *
	create_item (
		const QString & txt_n );

	void
	init_page_general ( );

	void
	init_page_input ( );

	void
	init_page_sys_tray ( );

	void
	init_page_mini_mixer ( );


	void
	update_inputs ( );

	void
	update_mouse_wheel_turns ( );

	void
	update_inputs_visibility ( );


	// Private attributes
	private:

	Mixer_Settings & _settings;

	QStandardItemModel _model;

	QWidget _navi;
	Wdg::Tree_View_KV _tree_view;

	QWidget _pages;
	QStackedLayout * _pages_stack;

	QWidget * _page_general;
	QWidget * _page_input;
	QWidget * _page_sys_tray;
	QWidget * _page_mini_mixer;

	QStandardItem * _it_general;
	QStandardItem * _it_input;
	QStandardItem * _it_sys_tray;
	QStandardItem * _it_mini_mixer;


	// General

	QCheckBox * _btn_show_slider_status_bar;
	QCheckBox * _btn_view_selection;

	QString _mwheel_turns_mask;
	QSlider * _mwheel_degrees_slider;
	Wdg::Covered_Spinbox * _mwheel_degrees_input;
	Wdg::Label_Width * _mwheel_turns;


	// Tray icon

	QButtonGroup * _btn_grp_tray_show;
	QRadioButton * _btn_tray_show[3];

	QWidget * _tray_minimize;
	QCheckBox * _btn_tray_on_minimize;
	QCheckBox * _btn_tray_on_close;


	// Mini mixer

	QButtonGroup * _btn_grp_mini_device;
	QRadioButton * _btn_mini_device[3];

	QWidget * _mini_dev_user;
	QLineEdit * _mini_dev_user_edit;

	QCheckBox * _balloon_show;
	QWidget * _balloon_time;
	Wdg::Covered_Spinbox * _balloon_time_input;
	QSlider * _balloon_time_slider;

	// Layout & Styling

	unsigned int _vspace;

	QString _title_mask;
};


#endif
