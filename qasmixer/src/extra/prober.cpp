//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "prober.hpp"

#include <QCoreApplication>
#include <QWidget>
#include <QPainter>
#include <iostream>


Prober::Prober (
	QWidget * parent_n ) :
QDialog ( parent_n ),
_updating_overlay ( false )
{
	setWindowTitle ( "Widget prober" );
	QCoreApplication::instance()->installEventFilter ( this );
}


Prober::~Prober ( )
{
	if ( _overlay ) {
		delete _overlay;
	}
}


void
Prober::update_overlay ( )
{
	_updating_overlay = true;

	if ( _overlay == 0 ) {
		_overlay = new Prober_Overlay;
	}

	if ( ( _wdg == 0 ) || ( _wdg == this ) ) {
		_overlay->hide();
		_overlay->setParent ( this );
	}

	if ( _wdg != 0 ) {
		QPoint wpos = QPoint ( 0, 0 );
		QSize wsize = _wdg->size();
		if ( !wsize.isValid() ) {
			return;
		}
		_overlay->setParent ( _wdg );
		_overlay->setGeometry ( QRect ( wpos, wsize ) );
		_overlay->show();
	}

	_updating_overlay = false;
}


bool
Prober::eventFilter (
	QObject * watched_n,
	QEvent * event_n )
{
	if ( _updating_overlay ) {
		return false;
	}

	if ( !isActiveWindow() ) {
		if ( _wdg != 0 ) {
			_wdg = 0;
			update_overlay();
		}
		return false;
	}

	if ( ( watched_n == _overlay ) || ( watched_n == this ) ) {
		return false;
	}

	QWidget * wdg ( dynamic_cast < QWidget * > ( watched_n ) );

	if (
		( event_n->type() == QEvent::Enter ) ||
		( event_n->type() == QEvent::MouseMove ) )
	{
		//std::cout << "Enter " << wdg << "\n";
		_wdg = wdg;
		update_overlay();
	} else if (
		( event_n->type() == QEvent::Leave ) )
	{
		_wdg = 0;
		update_overlay();
	}

	return false;
}
