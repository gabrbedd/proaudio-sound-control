//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_qt_prober_hpp__
#define __INC_qt_prober_hpp__

#include <QWidget>
#include <QDialog>
#include <QPointer>
#include "prober_overlay.hpp"


class Prober :
	public QDialog
{
	Q_OBJECT

	// Public methods
	public:

	Prober (
		QWidget * parent_n = 0 );

	~Prober ( );


	// Protected methods
	protected:

	void
	update_overlay ( );

	bool
	eventFilter (
		QObject * watched_n,
		QEvent * event_n );


	// Private attributes
	private:

	QPointer < QWidget > _wdg;

	QPointer < Prober_Overlay > _overlay;

	bool _updating_overlay;
};


#endif
