//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "prober_overlay.hpp"

#include <QPaintEvent>
#include <QPainter>
#include <QLayout>
#include <iostream>


Prober_Overlay::Prober_Overlay (
	QWidget * parent_n ) :
QWidget ( parent_n )
{
	setAutoFillBackground ( false );
	setAttribute ( Qt::WA_TranslucentBackground, true );
	setAttribute ( Qt::WA_TransparentForMouseEvents, true );

	QColor col;

	col = Qt::magenta;
	col.setAlpha ( 128 );
	_br_margins.setColor ( col );
	_br_margins.setStyle ( Qt::SolidPattern );

	col = Qt::green;
	col.setAlpha ( 128 );
	_br_lay_margins.setColor ( col );
	_br_lay_margins.setStyle ( Qt::SolidPattern );

	col = Qt::yellow;
	col.setAlpha ( 128 );
	_br_contents.setColor ( col );
	_br_contents.setStyle ( Qt::SolidPattern );
}


Prober_Overlay::~Prober_Overlay ( )
{
}


void
Prober_Overlay::paintEvent (
	QPaintEvent * event_n )
{
	QPainter pnt ( this );

	pnt.setPen ( Qt::NoPen );

	QRect re_all ( rect() );
	QRect re_content ( contentsRect() );
	QRect re_lay_contents ( re_content );
	if ( parentWidget() != 0 ) {
		if ( parentWidget()->layout() != 0 ) {
			QMargins mgs ( parentWidget()->layout()->contentsMargins() );
			re_lay_contents.adjust ( mgs.left(), mgs.top(), -mgs.right(), -mgs.bottom() );
		}
	}

	pnt.setBrush ( _br_margins );
	draw_Frame ( pnt, re_all, re_content );

	pnt.setBrush ( _br_lay_margins );
	draw_Frame ( pnt, re_content, re_lay_contents );

	pnt.setBrush ( _br_contents );
	draw_Frame ( pnt, re_lay_contents, QRect() );
}


void
Prober_Overlay::draw_Frame (
	QPainter & pnt,
	const QRect & outer_n,
	const QRect & inner_n )
{
	if ( outer_n == inner_n ) {
		return;
	}
	if ( !outer_n.isValid() ) {
		return;
	}
	if ( !inner_n.isValid() ) {
		pnt.drawRect ( outer_n );
		return;
	}

	QRect rec;

	rec = outer_n;
	rec.setHeight ( inner_n.top() - outer_n.top() );
	pnt.drawRect ( rec );

	rec = outer_n;
	rec.setHeight ( outer_n.bottom() - inner_n.bottom() );
	rec.moveTop ( outer_n.top() + outer_n.height() - rec.height() );
	pnt.drawRect ( rec );

	rec = outer_n;
	rec.setWidth ( inner_n.left() - outer_n.left() );
	pnt.drawRect ( rec );

	rec = outer_n;
	rec.setWidth ( outer_n.right() - inner_n.right() );
	rec.moveLeft ( outer_n.left() + outer_n.width() - rec.width() );
	pnt.drawRect ( rec );
}

