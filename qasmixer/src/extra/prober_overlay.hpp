//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_qt_prober_overlay_hpp__
#define __INC_qt_prober_overlay_hpp__

#include <QWidget>


class Prober_Overlay :
	public QWidget
{
	Q_OBJECT

	// Public methods
	public:

	Prober_Overlay (
		QWidget * parent_n = 0 );

	~Prober_Overlay ( );


	// Protected methods
	protected:

	void
	paintEvent (
		QPaintEvent * event_n );

	void
	draw_Frame (
		QPainter & pnt,
		const QRect & outer_n,
		const QRect & inner_n );


	// Private attributes
	private:

	QBrush _br_margins;
	QBrush _br_lay_margins;
	QBrush _br_contents;
};


#endif
