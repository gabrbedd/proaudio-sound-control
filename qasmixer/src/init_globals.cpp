//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "init_globals.hpp"

#include "qsnd/mixer_events.hpp"


int
init_globals ( )
{
	QSnd::init_mixer_events();

	return 0;
}
