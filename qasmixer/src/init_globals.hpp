//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_init_globals_hpp__
#define __INC_init_globals_hpp__


/// @brief Initializes program wide used variables one time
int
init_globals ( );


#endif
