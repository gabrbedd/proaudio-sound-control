//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include <iostream>
#include <memory>

#include <QTranslator>
#include <QLibraryInfo>
#include <QDesktopWidget>
#include <QString>
#include <QFile>

#include "single_application.hpp"
#include "main_mixer_window.hpp"

#include "init_globals.hpp"
#include "cmd_parse.hpp"

//#include "extra/prober.hpp"


///
/// @brief Debugging and testing
///
void
debug_tests (
	QWidget * wdg_n );


///
/// @brief The main function
///
int
main (
	int argc,
	char * argv[] )
{
	init_globals();

	int res ( 0 );
	std::auto_ptr < CMD_Options > cmd_opts ( new CMD_Options );


	//
	// QT Application
	//

	Single_Application app ( argc, argv );
	app.setQuitOnLastWindowClosed ( false );

	app.setOrganizationName ( QString ( PROGRAM_NAME ).toLower() );
	app.setApplicationName ( PROGRAM_NAME );


	//
	// Parse remaining command line options
	//
	res = parse_options ( argc, argv, cmd_opts.get() );
	if ( res < 0 ) {
		return res;
	}
	if ( !cmd_opts->no_single ) {
		app.set_unique_key ( SINGLE_APPLICATION_KEY );
	}


	//
	// Send a message and quit if an other instance is running
	//

	if ( app.is_running() ) {
		QString msg ( "new_instance\n" );
		if ( !cmd_opts->ctl_address.isEmpty() ) {
			msg.append ( "ctl_address=" );
			msg.append ( cmd_opts->ctl_address );
			msg.append ( "\n" );
		}
		if ( !cmd_opts->start_minimized ) {
			msg.append ( "raise\n" );
		}
		if ( app.send_message ( msg ) ) {
			return 0;
		}
	}


	//
	// Translators setup
	//

	QTranslator translator_qt;
	QTranslator translator_local;
	{
		translator_qt.load ( "qt_" + QLocale::system().name(),
			QLibraryInfo::location ( QLibraryInfo::TranslationsPath ) );

		translator_local.load ( "app_" + QLocale::system().name(),
			INSTALL_DIR_L10N );

		app.installTranslator ( &translator_qt );
		app.installTranslator ( &translator_local );
	}


	// Application icon setup
	{
		QIcon icon;

		QString icon_path ( INSTALL_DIR_ICONS_SVG );
		icon_path += "/";
		icon_path += PROGRAM_NAME;
		icon_path += ".svg";

		if ( QFile::exists ( icon_path ) ) {
			icon = QIcon ( icon_path );
		} else {
			const QString iname ( "multimedia-volume-control" );
			if ( QIcon::hasThemeIcon ( iname  ) ) {
				icon = QIcon::fromTheme ( iname  );
			}
		}
		if ( !icon.isNull() ) {
			app.setWindowIcon ( icon );
		}
	}


	//
	// Main mixer window
	//

	Main_Mixer_Window mixer_win;

	// Connect single instance messages
	QObject::connect ( &app, SIGNAL ( sig_message_available ( QString ) ),
		&mixer_win, SLOT ( parse_message ( QString ) ) );

	QObject::connect ( &app, SIGNAL ( commitDataRequest ( QSessionManager & ) ),
		&mixer_win, SLOT ( silent_close() ) );

	QObject::connect ( &mixer_win, SIGNAL ( sig_full_close() ),
		&app, SLOT ( quit() ), Qt::QueuedConnection );


	// Adjust startup size
	{
		const QRect avail_rect ( app.desktop()->availableGeometry() );
		QRect wrect ( 0, 0, 800, 450 );

		if ( avail_rect.isValid() ) {
			//std::cout << "avail_rect " << avail_rect.width() << ":" << avail_rect.height() << "\n";
			if ( avail_rect.width() <= 1024 ) {
				// Small screens - occupy more space
				wrect.setWidth ( ( avail_rect.width() * 3 ) / 4 );
			} else {
				// Larger screens
				wrect.setWidth ( ( avail_rect.width() * 2 ) / 3 );
			}

			wrect.setHeight ( ( wrect.width() * 9 ) / 16 );
			wrect.setHeight ( qMin ( avail_rect.height(), wrect.height() ) );
			wrect.moveTop ( ( avail_rect.height() - wrect.height() ) / 2 );
			wrect.moveLeft ( ( avail_rect.width() - wrect.width() ) / 2 );
			mixer_win.resize ( wrect.size() );
			mixer_win.move ( wrect.topLeft() );
		} else {
			mixer_win.resize ( wrect.size() );
		}
	}


	mixer_win.enter_setup_stage();

	if ( app.isSessionRestored() ) {
		// Restore session
		if ( mixer_win.settings().is_tray_minimized ) {
			cmd_opts->start_minimized = true;
		}
	}

	// Select control device
	if ( cmd_opts->ctl_address.isEmpty() ) {
		mixer_win.select_default_ctl();
	} else {
		mixer_win.select_ctl ( cmd_opts->ctl_address );
	}

	// Select view type
	mixer_win.select_view_type ( mixer_win.settings().view_type );

	mixer_win.finish_setup_stage();


	// Show mixer window
	{
		bool show_mixer ( true );
		if ( cmd_opts->start_minimized ) {
			if ( mixer_win.minimize_to_tray() ) {
				show_mixer = false;
			}
		}
		if ( show_mixer ) {
			mixer_win.raise_from_tray();
		}
	}


	// Clear command line option buffer
	cmd_opts.reset();

	debug_tests ( &mixer_win );

	res = app.exec();
	return res;
}


void
debug_tests (
	QWidget * )
{
	// DS_Slider test dialog
	//Wdg::Slider_Test_Dialog sl_test ( wdg_n );
	//sl_test.show();

	// Debug prober
	//Prober * prober ( new Prober ( wdg_n ) );
	//prober->show();
}
