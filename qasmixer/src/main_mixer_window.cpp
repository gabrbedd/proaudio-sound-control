//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "main_mixer_window.hpp"

#include <QVBoxLayout>
#include <QCoreApplication>
#include <QCloseEvent>

#include "main_view_mixer_simple.hpp"
#include "main_view_mixer_ctl.hpp"
#include "main_view_info.hpp"
#include "dialog_settings.hpp"
#include "dialog_info.hpp"
#include "dialog_alsa_config.hpp"

#include <iostream>

//#include "slider_test_dialog.hpp"


Main_Mixer_Window::Main_Mixer_Window (
	QWidget * parent,
	Qt::WindowFlags flags ) :
#ifndef CONFIG_MAIN_QWIDGET
QMainWindow ( parent, flags ),
#else
QWidget ( parent, flags ),
#endif
_side_iface ( _mixer_style ),
_view_type ( _num_view_types ),
_view ( 0 ),
_setup_stage ( true ),
_selecting_ctl ( false ),
_is_minimized_in_tray ( false ),
_silent_close ( false ),
_force_close ( false ),
_fully_closed ( false )
{
	setWindowTitle ( PROGRAM_TITLE );
	setObjectName ( PROGRAM_TITLE );

	_view_names[0] = tr ( "Simple mixer" );
	_view_names[1] = tr ( "Element mixer" );
	_view_names[2] = tr ( "Control info" );

	// Init menus
	init_menus();
	init_side_interface();
	init_mini_mixer();


	// Central view widget
	_wdg_view.setLayout ( new QVBoxLayout );


	// Horizontal splitter
	{
		_split_hor.setOrientation ( Qt::Horizontal );
		_split_hor.addWidget ( &_wdg_view );
		_split_hor.addWidget ( &_side_iface );

		_split_hor.setCollapsible ( 0, false );
		_split_hor.setCollapsible ( 1, false );
		_split_hor.setStretchFactor ( 0, 1 );
		_split_hor.setStretchFactor ( 1, 0 );
	}

#ifndef CONFIG_MAIN_QWIDGET
	setCentralWidget ( &_split_hor );
#else
	QHBoxLayout *lay = new QHBoxLayout;
	lay->addWidget( &_split_hor );
	setLayout(lay);
#endif

	_settings.load();

	update_visibility_view_selection();
	update_visibility_tray_icon();

	_wdg_view.show();
	side_iface().show();
	_split_hor.show();
}


Main_Mixer_Window::~Main_Mixer_Window ( )
{
	clear_view();
}


void
Main_Mixer_Window::init_menus ( )
{
#ifndef CONFIG_MAIN_QWIDGET
	// Action: Exit / Quit
	QAction * act_quit = new QAction ( tr ( "&Quit" ), this );
	act_quit->setShortcut ( QKeySequence ( QKeySequence::Quit ) );
	act_quit->setIcon ( QIcon::fromTheme ( "application-exit" ) );
	act_quit->setIconVisibleInMenu ( true );

	// Action: Settings
	QAction * act_settings = new QAction ( tr ( "&Settings" ), this );
	act_settings->setShortcut ( QKeySequence ( tr ( "Ctrl+s" ) ) );
	act_settings->setIcon ( QIcon::fromTheme ( "preferences-desktop" ) );
	act_settings->setIconVisibleInMenu ( true );

	// Action: Refresh
	QAction * act_refresh = new QAction ( tr ( "&Refresh" ), this );
	act_refresh->setShortcut ( QKeySequence ( QKeySequence::Refresh ) );
	act_refresh->setIcon ( QIcon::fromTheme ( "view-refresh" ) );
	act_refresh->setIconVisibleInMenu ( true );
#endif // CONFIG_MAIN_QWIDGET

	// View type actions
	{
		QActionGroup * view_grp ( new QActionGroup ( this ) );
		for ( unsigned int vii=0; vii < num_view_types(); ++vii ) {
			_act_view[vii] = new QAction ( _view_names[vii], this );
			_act_view[vii]->setCheckable ( true );
			view_grp->addAction ( _act_view[vii] );
		}

		// Action: View simple mixer
		_act_view[0]->setShortcut ( QKeySequence ( tr ( "Ctrl+1" ) ) );
		_act_view[1]->setShortcut ( QKeySequence ( tr ( "Ctrl+2" ) ) );
		_act_view[2]->setShortcut ( QKeySequence ( tr ( "Ctrl+3" ) ) );
	}


#ifndef CONFIG_MAIN_QWIDGET
	// Action: Alsa configuration view
	_act_alsa_cfg = new QAction ( tr ( "&Alsa configuration" ), this );
	_act_alsa_cfg->setShortcut ( QKeySequence ( tr ( "Ctrl+c" ) ) );
	_act_alsa_cfg->setIcon ( QIcon::fromTheme ( "preferences-system" ) );
	_act_alsa_cfg->setIconVisibleInMenu ( true );


	// Action: Fullscreen
	_act_fullscreen = new QAction ( this );
	_act_fullscreen->setShortcut ( QKeySequence ( tr ( "Ctrl+F" ) ) );
	_act_fullscreen->setCheckable ( true );
	_act_fullscreen->setIconVisibleInMenu ( true );
	update_fullscreen_action();


	// Action: Info
	QAction * act_info = new QAction ( tr ( "&Info" ), this );
	act_info->setShortcut ( QKeySequence ( QKeySequence::HelpContents ) );
	act_info->setIcon ( QIcon::fromTheme ( "help-about" ) );
	act_info->setIconVisibleInMenu ( true );

	// Menus

	{
		QMenu * menu_file = menuBar()->addMenu ( tr ( "&File" ) );
		menu_file->addAction ( act_settings );
		menu_file->addSeparator();
		menu_file->addAction ( act_quit );
	}


	{
		QMenu * menu_view = menuBar()->addMenu ( tr ( "&View" ) );

		menu_view->addSeparator();
		for ( unsigned int vii=0; vii < num_view_types(); ++vii ) {
			menu_view->addAction ( _act_view[vii] );
		}
		menu_view->addSeparator();

		menu_view->addAction ( act_refresh );
		menu_view->addAction ( _act_fullscreen );
	}

	{
		QMenu * menu_extra = menuBar()->addMenu ( tr ( "&Extras" ) );
		menu_extra->addAction ( _act_alsa_cfg );
	}

	{
		QMenu * menu_help = menuBar()->addMenu ( tr ( "&Help" ) );
		menu_help->addAction ( act_info );
	}


	// Connect actions

    connect ( act_quit, SIGNAL ( triggered() ),
		this, SLOT ( force_close() ) );
#endif // CONFIG_MAIN_QWIDGET


	// View actions

    connect ( _act_view[0], SIGNAL ( triggered() ),
		this, SLOT ( select_view_simple_mixer() ) );

    connect ( _act_view[1], SIGNAL ( triggered() ),
		this, SLOT ( select_view_element_mixer() ) );

    connect ( _act_view[2], SIGNAL ( triggered() ),
		this, SLOT ( select_view_control_info() ) );

#ifndef CONFIG_MAIN_QWIDGET
    connect ( _act_alsa_cfg, SIGNAL ( triggered() ),
		this, SLOT ( show_dialog_alsa_config() ) );


    connect ( act_refresh, SIGNAL ( triggered() ),
		this, SLOT ( refresh_data() ) );

    connect ( _act_fullscreen, SIGNAL ( toggled ( bool ) ),
		this, SLOT ( set_fullscreen ( bool ) ) );



	// Info action

    connect ( act_settings, SIGNAL ( triggered() ),
		this, SLOT ( show_dialog_settings() ) );

    connect ( act_info, SIGNAL ( triggered() ),
		this, SLOT ( show_dialog_info() ) );
#endif // CONFIG_MAIN_QWIDGET
}


void
Main_Mixer_Window::init_side_interface ( )
{
	// View type selection
	for ( unsigned int ii=0; ii < num_view_types(); ++ii ) {
		side_iface().view_selection()->addItem ( _view_names[ii] );
	}

	connect ( side_iface().view_selection(), SIGNAL ( currentIndexChanged ( int ) ),
		this, SLOT ( select_view_type_int ( int ) ) );

	connect ( &side_iface(), SIGNAL ( sig_control_changed() ),
		this, SLOT ( select_ctl_from_side_iface() ) );



	// Stream selection

	connect ( side_iface().stream_check ( 0 ), SIGNAL ( toggled ( bool ) ),
		this, SLOT ( show_stream_playback ( bool ) ) );

	connect ( side_iface().stream_check ( 1 ), SIGNAL ( toggled ( bool ) ),
		this, SLOT ( show_stream_capture ( bool ) ) );
}


void
Main_Mixer_Window::init_mini_mixer ( )
{
	connect (
		&_mini_mixer, SIGNAL ( sig_activated() ),
		this, SLOT ( tray_icon_activation() ) );

	connect (
		&_mini_mixer, SIGNAL ( sig_show() ),
		this, SLOT ( tray_icon_activation() ) );

	connect (
		&_mini_mixer, SIGNAL ( sig_close() ),
		this, SLOT ( force_close() ) );
}


void
Main_Mixer_Window::update_fullscreen_action ( )
{
	if ( isFullScreen() ) {
		_act_fullscreen->setText ( tr ( "Normal &screen" ) );
		_act_fullscreen->setIcon ( QIcon::fromTheme ( "view-restore" ) );
		_act_fullscreen->setChecked ( true );
	} else {
		_act_fullscreen->setText ( tr ( "Full&screen" ) );
		_act_fullscreen->setIcon ( QIcon::fromTheme ( "view-fullscreen" ) );
		_act_fullscreen->setChecked ( false );
	}
}


void
Main_Mixer_Window::clear_view ( )
{
	if ( _view != 0 ) {
		delete _view;
		_view = 0;
	}
}


void
Main_Mixer_Window::create_view ( )
{
	bool show_stream_dir ( false );
	bool show_interfaces ( false );

	if ( view_type() == 0 ) {

		Main_View_Mixer_Simple * mview =
			new Main_View_Mixer_Simple ( _current_ctl, this );
		mview->set_mixer_style ( &_mixer_style );
		mview->mixer_sliders().set_wheel_degrees ( _settings.wheel_degrees );

		_view = mview;
		show_stream_dir = true;

	} else if ( view_type() == 1 ) {

		Main_View_Mixer_CTL * mview =
			new Main_View_Mixer_CTL ( _current_ctl, this );
		mview->set_mixer_style ( &_mixer_style );

		connect ( mview, SIGNAL ( sig_ifaces_list_changed() ),
			this, SLOT ( ifaces_list_changed() ) );

		connect ( side_iface().iface_buttons(), SIGNAL ( buttonClicked  ( int ) ),
			mview, SLOT ( select_iface ( int ) ) );

		_view = mview;
		show_interfaces = true;

	} else if ( view_type() == 2 ) {

		Main_View_Info * mview =
			new Main_View_Info ( _current_ctl, this );
		mview->set_mixer_style ( &_mixer_style );

		_view = mview;
	}

	side_iface().wdg_stream_dir()->setVisible ( show_stream_dir );
	side_iface().wdg_interface()->setVisible ( show_interfaces );

	apply_mixer_config();

	if ( _view != 0 ) {
		_wdg_view.layout()->addWidget ( _view );
	}
}


void
Main_Mixer_Window::reload_mini_mixer ( )
{
	QString dev_str;

	// Mini mixer
	if ( _settings.mini.device_mode == Mini_Mixer_Settings::DEV_DEFAULT ) {
		dev_str = side_iface().default_ctl().ctl_id();
	} else if ( _settings.mini.device_mode == Mini_Mixer_Settings::DEV_CURRENT ) {
		dev_str = _current_ctl.ctl_id();
	} else if ( _settings.mini.device_mode == Mini_Mixer_Settings::DEV_USER ) {
		dev_str = _settings.mini.user_device;
	}

	_mini_mixer.set_wheel_degrees ( settings().wheel_degrees );
	_mini_mixer.set_ctl_address ( dev_str );
}


void
Main_Mixer_Window::enter_setup_stage ( )
{
	_setup_stage = true;

	clear_view();
}


void
Main_Mixer_Window::finish_setup_stage ( )
{
	_setup_stage = false;

	settings_changed_mixer();
	settings_changed_mini_mixer();

	create_view();

	if ( _view != 0 ) {
		_view->reload();
		_view->show();
	}
}


void
Main_Mixer_Window::select_view_simple_mixer ( )
{
	select_view_type ( 0 );
}


void
Main_Mixer_Window::select_view_element_mixer ( )
{
	select_view_type ( 1 );
}


void
Main_Mixer_Window::select_view_control_info ( )
{
	select_view_type ( 2 );
}


void
Main_Mixer_Window::select_view_type_int (
	int type_n )
{
	if ( ( type_n >= 0 ) ||
		( type_n < (int)num_view_types() ) )
	{
		select_view_type ( (unsigned int)type_n );
	}
}


void
Main_Mixer_Window::select_view_type (
	unsigned int type_n )
{
	if ( ( _view_type == type_n ) ||
		( type_n >= num_view_types() ) )
	{
		return;
	}

	if ( !setup_stage() ) {
		clear_view();
	}

	_view_type = type_n;
	_settings.view_type = type_n;

	_act_view[view_type()]->setChecked ( true );
	side_iface().view_selection()->setCurrentIndex ( view_type() );

	if ( !setup_stage() ) {
		create_view();
		if ( _view != 0 ) {
			_view->reload();
			_view->show();
		}
	}
}


void
Main_Mixer_Window::select_ctl (
	const QSnd::Snd_Control_Address & ctl_n )
{
	if ( !_selecting_ctl && ( _current_ctl != ctl_n ) ) {
		_selecting_ctl = true;
		_current_ctl = ctl_n;

		side_iface().update_selected_ctl ( _current_ctl );

		if ( _view != 0 ) {
			_view->reload();
		}

		if ( settings().mini.device_mode == Mini_Mixer_Settings::DEV_CURRENT ) {
			reload_mini_mixer();
		}

		_selecting_ctl = false;
	}
}


void
Main_Mixer_Window::select_default_ctl ( )
{
	select_ctl ( side_iface().default_ctl() );
}


void
Main_Mixer_Window::select_ctl_from_side_iface ( )
{
	select_ctl ( side_iface().selected_ctl() );
}


void
Main_Mixer_Window::show_stream_playback (
	bool flag_n )
{
	bool changed ( false );
	if ( flag_n == false ) {
		if ( !_settings.smixer.show_stream[1] ) {
			flag_n = true;
			changed = true;
		}
	}
	if ( flag_n != _settings.smixer.show_stream[0] ) {
		_settings.smixer.show_stream[0] = flag_n;
		changed = true;
	}
	if ( changed ) {
		settings_changed_mixer();
	}
}


void
Main_Mixer_Window::show_stream_capture (
	bool flag_n )
{
	bool changed ( false );
	if ( flag_n == false ) {
		if ( !_settings.smixer.show_stream[0] ) {
			flag_n = true;
			changed = true;
		}
	}
	if ( flag_n != _settings.smixer.show_stream[1] ) {
		_settings.smixer.show_stream[1] = flag_n;
		changed = true;
	}
	if ( changed ) {
		settings_changed_mixer();
	}
}


void
Main_Mixer_Window::apply_mixer_config ( )
{
	if ( view_type() == 0 ) {

		Main_View_Mixer_Simple * mview (
			dynamic_cast < Main_View_Mixer_Simple * > ( _view ) );
		if ( mview != 0 ) {
			mview->set_mixer_settings ( settings().smixer );
			mview->mixer_sliders().set_wheel_degrees ( settings().wheel_degrees );
		}

	} else if ( view_type() == 1 ) {

		Main_View_Mixer_CTL * mview (
			dynamic_cast < Main_View_Mixer_CTL * > ( _view ) );
		if ( mview != 0 ) {
			mview->mixer_ctl().set_wheel_degrees ( settings().wheel_degrees );
		}

	}
}


void
Main_Mixer_Window::ifaces_list_changed ( )
{
	if ( view_type() == 1 ) {
		Main_View_Mixer_CTL * mview (
			dynamic_cast < Main_View_Mixer_CTL * > ( _view ) );
		if ( mview != 0 ) {
			side_iface().set_interface_names ( mview->iface_avail_names() );

			unsigned int idx ( mview->selected_iface() );
			QAbstractButton * btn (
				side_iface().iface_buttons()->button ( idx ) );
			if ( btn != 0 ) {
				btn->setChecked ( true );
			}
		}
	}
}


void
Main_Mixer_Window::refresh_data ( )
{
	_side_iface.refresh_data();
	if ( _view != 0 ) {
		_view->reload_delayed();
	}
}


void
Main_Mixer_Window::set_fullscreen (
	bool flag_n )
{
	if ( flag_n != isFullScreen() ) {
		if ( flag_n ) {
			showFullScreen();
		} else {
			showNormal();
		}
		update_fullscreen_action();
	}
}


void
Main_Mixer_Window::show_dialog_settings ( )
{
	if ( _dialog_settings == 0 ) {
		_dialog_settings = new Dialog_Settings ( _settings, this );
		_dialog_settings->setAttribute ( Qt::WA_DeleteOnClose );

		connect ( _dialog_settings, SIGNAL ( sig_settings_changed_mixer() ),
			this, SLOT ( settings_changed_mixer() ) );

		connect ( _dialog_settings, SIGNAL ( sig_settings_changed_tray() ),
			this, SLOT ( settings_changed_tray() ) );

		connect ( _dialog_settings, SIGNAL ( sig_settings_changed_mini() ),
			this, SLOT ( settings_changed_mini_mixer() ) );

		adjust_dialog_size ( _dialog_settings, 2, 3 );
	}
	_dialog_settings->show();
}


void
Main_Mixer_Window::show_dialog_info ( )
{
	if ( _dialog_info == 0 ) {
		_dialog_info = new Dialog_Info ( this );
		_dialog_info->setAttribute ( Qt::WA_DeleteOnClose );
	}
	_dialog_info->show();
}


void
Main_Mixer_Window::show_dialog_alsa_config ( )
{
	if ( _dialog_alsa_config == 0 ) {
		Dialog_Alsa_Config * dialog ( new Dialog_Alsa_Config ( _settings.alsa_cfg, this ) );
		dialog->set_model ( &side_iface().config_model() );

		_dialog_alsa_config = dialog;
		_dialog_alsa_config->setAttribute ( Qt::WA_DeleteOnClose );

		adjust_dialog_size ( _dialog_alsa_config, 4, 5 );
	}
	_dialog_alsa_config->show();
}


void
Main_Mixer_Window::adjust_dialog_size (
	QWidget * dialog_n,
	unsigned int numerator_n,
	unsigned int denominator_n )
{
	// Adjust dialog size
	const QSize shint ( dialog_n->sizeHint() );
	QSize snew (
		width() * numerator_n / denominator_n,
		height() * numerator_n / denominator_n );
	if ( snew.width() < shint.width() ) {
		snew.setWidth ( shint.width() );
	}
	if ( snew.height() < shint.height() ) {
		snew.setHeight ( shint.height() );
	}
	dialog_n->resize ( snew );
}


void
Main_Mixer_Window::settings_changed_mixer ( )
{
	if ( setup_stage() ) {
		return;
	}

	update_visibility_view_selection();

	// Update side iface buttons
	for ( unsigned int ii=0; ii < 2; ++ii ) {
		QAbstractButton * btn ( side_iface().stream_check ( ii ) );
		const bool check ( settings().smixer.show_stream[ii] );
		if ( btn->isChecked() != check ) {
			btn->blockSignals ( true );
			btn->setChecked ( check );
			btn->blockSignals ( false );
		}
	}

	apply_mixer_config();
}


void
Main_Mixer_Window::settings_changed_tray ( )
{
	update_visibility_tray_icon();
}


void
Main_Mixer_Window::settings_changed_mini_mixer ( )
{
	update_visibility_tray_icon();
	reload_mini_mixer();
}


void
Main_Mixer_Window::parse_message (
	QString msg_n )
{
	//std::cout << "Main_Mixer_Window::parse_message " << "\n";

	enter_setup_stage();

	QStringList rows ( msg_n.split ( "\n", QString::SkipEmptyParts ) );

	for ( int ii=0; ii < rows.size(); ++ii ) {
		const QString & row ( rows[ii] );

		//std::cout << "row[" << ii << "]: " << row.toLocal8Bit().constData() << "\n";

		if ( row.contains ( "raise", Qt::CaseInsensitive ) ) {

			if ( !isActiveWindow() ) {
				raise_from_tray();
			}

		} else if ( row.contains ( "ctl_address=", Qt::CaseInsensitive ) ) {

			const int idx ( row.indexOf ( "=" ) + 1 );
			QString sstr ( row.mid ( idx ).trimmed() );
			if ( !sstr.isEmpty() ) {
				select_ctl ( sstr );
			}

		}
	}

	finish_setup_stage();

}


void
Main_Mixer_Window::tray_icon_activation ( )
{
	if ( isVisible() && isActiveWindow() ) {
		minimize_to_tray();
	} else {
		raise_from_tray();
	}
}


void
Main_Mixer_Window::save_settings ( )
{
	_settings.write();
}


void
Main_Mixer_Window::silent_close ( )
{
	_silent_close = true;
	force_close();
}


void
Main_Mixer_Window::force_close ( )
{
	_force_close = true;
	close();
}


void
Main_Mixer_Window::changeEvent (
	QEvent * event_n )
{
#ifndef CONFIG_MAIN_QWIDGET
	QMainWindow::changeEvent ( event_n );
#else
	QWidget::changeEvent ( event_n );
#endif

	if ( event_n->type() == QEvent::WindowStateChange ) {
		QWindowStateChangeEvent * wst_ev (
			dynamic_cast < QWindowStateChangeEvent * > ( event_n ) );
		if ( wst_ev != 0  ) {
			if ( settings().tray_on_minimize &&
				( settings().show_tray_icon != Mixer_Settings::TRAY_ICON_NEVER ) &&
				( ( windowState()      & Qt::WindowMinimized ) != 0 ) &&
				( ( wst_ev->oldState() & Qt::WindowMinimized ) == 0 ) )
			{
				minimize_to_tray();
			}
		}
	}
}


void
Main_Mixer_Window::closeEvent (
	QCloseEvent * event_n )
{
	//std::cout << "Main_Mixer_Window::closeEvent()" << "\n";

#ifndef CONFIG_MAIN_QWIDGET
	QMainWindow::closeEvent ( event_n );
#else
	QWidget::closeEvent ( event_n );
#endif

	bool full_close ( true );

	const bool try_to_minimize (
		!_force_close &&
		!_fully_closed &&
		settings().tray_on_close &&
		( settings().show_tray_icon != Mixer_Settings::TRAY_ICON_NEVER ) );
	if ( try_to_minimize ) {
		if ( minimize_to_tray() ) {
			full_close = false;
		}
	}

	if ( full_close && !_fully_closed ) {
		_fully_closed = true;
		save_settings();
		if ( !_silent_close ) {
			emit sig_full_close();
		}
	}
}


bool
Main_Mixer_Window::minimize_to_tray ( )
{
	if ( QSystemTrayIcon::isSystemTrayAvailable() ) {

		// Hide dialogs
		if ( _dialog_settings ) {
			_dialog_settings->hide();
		}
		if ( _dialog_info ) {
			_dialog_info->hide();
		}
		if ( _dialog_alsa_config ) {
			_dialog_alsa_config->hide();
		}

		hide();

		_is_minimized_in_tray = true;
		_settings.is_tray_minimized = true;

		update_visibility_tray_icon();
		return true;
	}
	return false;
}


void
Main_Mixer_Window::raise_from_tray ( )
{
	if ( isMinimized() ) {
		showNormal();
	}
	show();
	raise();

	// Show dialogs
	if ( _dialog_settings ) {
		_dialog_settings->show();
	}
	if ( _dialog_info ) {
		_dialog_info->show();
	}
	if ( _dialog_alsa_config ) {
		_dialog_alsa_config->show();
	}

	_is_minimized_in_tray = false;
	_settings.is_tray_minimized = false;

	update_visibility_tray_icon();
}


void
Main_Mixer_Window::update_visibility_view_selection ( )
{
#ifndef CONFIG_MAIN_QWIDGET
	const bool vis ( _settings.show_view_selection );
#else
	const bool vis = false;
#endif
	side_iface().wdg_view()->setVisible ( vis );
}


void
Main_Mixer_Window::update_visibility_tray_icon ( )
{
	bool show_icon ( false );
	const unsigned int sti ( _settings.show_tray_icon );
	if ( sti == Mixer_Settings::TRAY_ICON_ALWAYS ) {
		show_icon = true;
	} else if (
		( sti == Mixer_Settings::TRAY_ICON_MINIMIZED ) &&
		is_minimized_in_tray() )
	{
		show_icon = true;
	}

	_mini_mixer.show_tray_icon ( show_icon );
	_mini_mixer.set_show_balloon ( settings().mini.show_balloon );
	_mini_mixer.set_balloon_lifetime ( settings().mini.balloon_lifetime );
}

