//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_qmixer_window_hpp__
#define __INC_qmixer_window_hpp__

#define CONFIG_MAIN_QWIDGET

#include "config.hpp"

#ifndef CONFIG_MAIN_QWIDGET
#include <QMainWindow>
#else
#include <QWidget>
#endif

#include <QMenuBar>
#include <QStatusBar>
#include <QSplitter>
#include <QLabel>
#include <QButtonGroup>

#include <QPointer>
#include <QSystemTrayIcon>

#include "qsnd/mixer_style.hpp"
#include "mixer_settings.hpp"

#include "mini_mixer.hpp"
#include "main_view.hpp"
#include "side_interface.hpp"


#ifndef CONFIG_MAIN_QWIDGET
class Main_Mixer_Window :
	public QMainWindow
#else
class Main_Mixer_Window :
	public QWidget
#endif
{
	Q_OBJECT

	// Public methods
	public:

	Main_Mixer_Window (
		QWidget * parent = 0,
		Qt::WindowFlags flags = 0 );

	~Main_Mixer_Window ( );


	Side_Interface &
	side_iface ( );

	Mini_Mixer &
	mini_mixer ( );

	const Mixer_Settings &
	settings ( ) const;


	unsigned int
	view_type ( ) const;

	unsigned int
	num_view_types ( ) const;


	const QSnd::Snd_Control_Address &
	current_ctl ( ) const;

	void
	select_ctl (
		const QSnd::Snd_Control_Address & ctl_n );

	void
	select_default_ctl ( );


	bool
	setup_stage ( ) const;

	void
	enter_setup_stage ( );

	void
	finish_setup_stage ( );

	bool
	is_minimized_in_tray ( ) const;


	// Signals
	signals:

	void
	sig_full_close ( );


	// Public slots
	public slots:

	// View type selection

	void
	select_view_simple_mixer ( );

	void
	select_view_element_mixer ( );

	void
	select_view_control_info ( );

	void
	select_view_type (
		unsigned int type_n );

	void
	select_view_type_int (
		int type_n );


	void
	select_ctl_from_side_iface ( );


	// Show stream

	void
	show_stream_playback (
		bool flag_n );

	void
	show_stream_capture (
		bool flag_n );


	void
	refresh_data ( );


	void
	set_fullscreen (
		bool flag_n );

	bool
	minimize_to_tray ( );

	void
	raise_from_tray ( );


	void
	save_settings ( );

	void
	silent_close ( );

	void
	force_close ( );


	void
	show_dialog_info ( );

	void
	show_dialog_settings ( );

	void
	show_dialog_alsa_config ( );

	void
	adjust_dialog_size (
		QWidget * dialog_n,
		unsigned int numerator_n,
		unsigned int denominator_n );


	void
	parse_message (
		QString msg_n );


	// Protected slots
	protected slots:

	void
	ifaces_list_changed ( );


	void
	settings_changed_mixer ( );

	void
	settings_changed_tray ( );

	void
	settings_changed_mini_mixer ( );


	void
	tray_icon_activation ( );


	// Protected methods
	protected:

	void
	clear_view ( );

	void
	create_view ( );

	void
	reload_mini_mixer ( );

	void
	apply_mixer_config ( );

	void
	update_fullscreen_action ( );


	void
	changeEvent (
		QEvent * event_n );

	void
	closeEvent (
		QCloseEvent * event );


	void
	update_visibility_view_selection ( );

	void
	update_visibility_tray_icon ( );


	void
	init_menus ( );

	void
	init_side_interface ( );

	void
	init_mini_mixer ( );


	// Private attributes
	private:

	// Shared storages and settings

	QSnd::Mixer_Style _mixer_style;

	Mixer_Settings _settings;


	// Base widgets

	QSplitter _split_hor;

	QWidget _wdg_view;

	Side_Interface _side_iface;


	// View stuff

	static const unsigned int _num_view_types = 3;

	unsigned int _view_type;

	Main_View * _view;

	QString _view_names[_num_view_types];

	QSnd::Snd_Control_Address _current_ctl;


	// Menubar

	QAction * _act_view[_num_view_types];

	QAction * _act_alsa_cfg;

	QAction * _act_fullscreen;


	// System tray

	Mini_Mixer _mini_mixer;


	bool _setup_stage;

	bool _selecting_ctl;

	bool _is_minimized_in_tray;

	bool _silent_close;

	bool _force_close;

	bool _fully_closed;


	// Dialogs

	QPointer < QWidget > _dialog_settings;

	QPointer < QWidget > _dialog_info;

	QPointer < QWidget > _dialog_alsa_config;
};


inline
Side_Interface &
Main_Mixer_Window::side_iface ( )
{
	return _side_iface;
}


inline
Mini_Mixer &
Main_Mixer_Window::mini_mixer ( )
{
	return _mini_mixer;
}


inline
const Mixer_Settings &
Main_Mixer_Window::settings ( ) const
{
	return _settings;
}


inline
bool
Main_Mixer_Window::setup_stage ( ) const
{
	return _setup_stage;
}


inline
unsigned int
Main_Mixer_Window::view_type ( ) const
{
	return _view_type;
}


inline
const QSnd::Snd_Control_Address &
Main_Mixer_Window::current_ctl ( ) const
{
	return _current_ctl;
}


inline
unsigned int
Main_Mixer_Window::num_view_types ( ) const
{
	return _num_view_types;
}


inline
bool
Main_Mixer_Window::is_minimized_in_tray ( ) const
{
	return _is_minimized_in_tray;
}


#endif
