//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "main_view.hpp"

#include "qsnd/mixer_events.hpp"
#include <QCoreApplication>
#include <iostream>


Main_View::Main_View (
	QSnd::Snd_Control_Address & ctl_n,
	QWidget * parent_n ) :
QWidget ( parent_n ),
_message_wdg ( this ),
_current_ctl ( ctl_n ),
_reload_delayed ( false )
{
	_message_wdg.hide();
	QStackedLayout * lay ( new QStackedLayout ( this ) );
	lay->setContentsMargins ( 0, 0, 0, 0 );
	lay->addWidget ( &_message_wdg );
}


Main_View::~Main_View ( )
{
}


void
Main_View::reload ( )
{
	// Dummy implementation
	//std::cout << "Main_View::reload()\n";
}


void
Main_View::reload_delayed ( )
{
	//std::cout << "Main_View::reload_delayed " << "\n";

	if ( !_reload_delayed ) {
		_reload_delayed = true;
		QCoreApplication::postEvent (
			this, new QEvent ( QSnd::event_type_reload ) );
	}
}


bool
Main_View::event (
	QEvent * event_n )
{
	if ( event_n->type() == QSnd::event_type_reload ) {
		_reload_delayed = false;
		this->reload();
		return true;
	}

	return QWidget::event ( event_n );
}

