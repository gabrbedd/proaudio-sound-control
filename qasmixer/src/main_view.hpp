//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_main_view_hpp__
#define __INC_main_view_hpp__

#include <QWidget>
#include <QStackedLayout>

#include "qsnd/snd_control_address.hpp"
#include "message_widget.hpp"


class Main_View :
	public QWidget
{
	Q_OBJECT

	// Public methods
	public:

	Main_View (
		QSnd::Snd_Control_Address & ctl_n,
		QWidget * parent_n = 0 );

	virtual
	~Main_View ( );


	QStackedLayout *
	lay_stack ( ) const;

	Message_Widget &
	message_wdg ( );

	QSnd::Snd_Control_Address &
	current_ctl ( );


	bool
	event (
		QEvent * event_n );


	// Public slots
	public slots:

	virtual
	void
	reload ( );

	void
	reload_delayed ( );


	// Private attributes
	private:

	Message_Widget _message_wdg;

	QSnd::Snd_Control_Address & _current_ctl;

	bool _reload_delayed;
};


inline
QStackedLayout *
Main_View::lay_stack ( ) const
{
	return dynamic_cast < QStackedLayout * > ( layout() );
}


inline
QSnd::Snd_Control_Address &
Main_View::current_ctl ( )
{
	return _current_ctl;
}


inline
Message_Widget &
Main_View::message_wdg ( )
{
	return _message_wdg;
}


#endif
