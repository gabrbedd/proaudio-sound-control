//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "main_view_info.hpp"

#include "qsnd/mixer_events.hpp"

#include <QCoreApplication>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include <iostream>


Main_View_Info::Main_View_Info (
	QSnd::Snd_Control_Address & ctl_n,
	QWidget * parent_n ) :
Main_View ( ctl_n, parent_n )
{
	connect ( &_pcm_info, SIGNAL ( sig_reload_request() ),
		this, SLOT ( reload_delayed() ) );

	_tree_view.setRootIsDecorated ( false );
	_tree_view.setModel ( &_pcm_info_model );

	lay_stack()->addWidget ( &_tree_view );
}


void
Main_View_Info::set_mixer_style (
	QSnd::Mixer_Style * mstyle_n )
{
	if ( mstyle_n != 0 ) {
		_pcm_info_model.brush_snd_dir(0).setColor (
			mstyle_n->palette_play().color ( QPalette::WindowText ) );

		_pcm_info_model.brush_snd_dir(1).setColor (
			mstyle_n->palette_cap().color ( QPalette::WindowText ) );
	}
}


void
Main_View_Info::reload ( )
{
	//std::cout << "Main_View_Info::reload" << "\n";
	Main_View::reload();

	int stack_idx ( 0 );
	QSnd::Snd_CTL_Info * pcm_info_p ( 0 );

	if ( !current_ctl().is_clear() ) {
		_pcm_info.open ( current_ctl().ctl_id() );
		if ( _pcm_info.is_open() ) {
			pcm_info_p = &_pcm_info;
			stack_idx = 1;
		} else {
			message_wdg().set_mixer_open_fail (
				current_ctl().ctl_id(),
				_pcm_info.err_message(),
				_pcm_info.err_func() );
		}
	} else {
		message_wdg().set_no_device();
	}

	_pcm_info_model.set_pcm_info ( pcm_info_p );

	lay_stack()->setCurrentIndex ( stack_idx );
	if ( stack_idx != 0 ) {
		_tree_view.expandToDepth ( 3 );
		_tree_view.adjust_first_column_width();
	}
}

