//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_main_view_information_hpp__
#define __INC_main_view_information_hpp__

#include "main_view.hpp"
#include "wdg/tree_view_kv.hpp"
#include "qsnd/snd_ctl_info_model.hpp"
#include "qsnd/mixer_style.hpp"

#include <QLabel>


class Main_View_Info :
	public Main_View
{
	Q_OBJECT

	// Public methods
	public:

	Main_View_Info (
		QSnd::Snd_Control_Address & ctl_n,
		QWidget * parent_n = 0 );

	void
	set_mixer_style (
		QSnd::Mixer_Style * mstyle_n );


	// Public slots
	public slots:

	void
	reload ( );


	// Private attributes
	private:

	QSnd::Snd_CTL_Info _pcm_info;

	QSnd::Snd_CTL_Info_Model _pcm_info_model;

	Wdg::Tree_View_KV _tree_view;
};



#endif
