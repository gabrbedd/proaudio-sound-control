//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "main_view_mixer_ctl.hpp"

#include "qsnd/mixer_events.hpp"

#include <QCoreApplication>
#include <QHeaderView>

#include <iostream>


Main_View_Mixer_CTL::Main_View_Mixer_CTL (
	QSnd::Snd_Control_Address & ctl_n,
	QWidget * parent_n ) :
Main_View ( ctl_n, parent_n ),
_selected_iface ( 0 )
{
	_default_iface_type_idx =
		_snd_mixer.data().iface_type_idx ( SND_CTL_ELEM_IFACE_MIXER );

	connect ( &_snd_mixer, SIGNAL ( sig_mixer_reload_request() ),
		this, SLOT ( reload_delayed() ) );


	// Tree model

	_tree_model.set_snd_mixer ( &_snd_mixer );
	{
		QFont fnt ( _tree_view.font() );
		fnt.setBold ( true );
		_tree_model.base_item()->setFont ( fnt );
	}

	// Tree view

	_tree_view.setHeaderHidden ( true );
	_tree_view.setRootIsDecorated ( false );
	_tree_view.setTextElideMode ( Qt::ElideMiddle );
	_tree_view.setFrameStyle ( QFrame::StyledPanel | QFrame::Plain );
	_tree_view.setModel ( &_tree_model );

	connect ( &_tree_view, SIGNAL ( activated ( const QModelIndex & ) ),
		this, SLOT ( tree_element_selected ( const QModelIndex & ) ) );


	// Table view

	_table_view.setFrameStyle ( QFrame::StyledPanel | QFrame::Plain );
	_table_view.setModel ( &_table_model );
	{
		QHeaderView * vhv ( _table_view.verticalHeader() );
		if ( vhv != 0 ) {
			vhv->hide();
		}
	}
	_table_view.setSelectionMode ( QAbstractItemView::SingleSelection );
	_table_view.setSelectionBehavior ( QAbstractItemView::SelectRows );

	connect ( &_table_view, SIGNAL ( activated ( const QModelIndex & ) ),
		this, SLOT ( table_element_selected ( const QModelIndex & ) ) );


	// Tree view container widget
	{
		QVBoxLayout * lay_wdg_tree ( new QVBoxLayout );
		lay_wdg_tree->addWidget ( &_tree_view );
		_wdg_tree.setLayout ( lay_wdg_tree );
	}

	{
		// Set parent in advance to avoid flicker
		_table_view.setParent ( &_wdg_center);
		_mixer_ctl.setParent ( &_wdg_center);

		_lay_center_stack = new QStackedLayout;
		_lay_center_stack->setContentsMargins ( 0, 0, 0, 0 );
		_lay_center_stack->addWidget ( &_table_view );
		_lay_center_stack->addWidget ( &_mixer_ctl );

		QVBoxLayout * lay_center ( new QVBoxLayout );
		lay_center->addLayout ( _lay_center_stack );
		_wdg_center.setLayout ( lay_center );
	}


	// Horizontal splitter
	{
		_hsplit.setOrientation ( Qt::Horizontal );
		_hsplit.addWidget ( &_wdg_tree );
		_hsplit.addWidget ( &_wdg_center );

		_hsplit.setCollapsible ( 0, false );
		_hsplit.setCollapsible ( 1, false );
		_hsplit.setStretchFactor ( 1, 7 );

		lay_stack()->addWidget ( &_hsplit );
	}


	// Adjust margins
	{
		QLayout * lay;
		lay = _wdg_tree.layout();
		if ( lay != 0 ) {
			QMargins mgs ( lay->contentsMargins() );
			mgs.setLeft ( 0 );
			mgs.setTop ( 0 );
			mgs.setBottom ( 0 );
			lay->setContentsMargins ( mgs );
		}

		lay = _wdg_center.layout();
		if ( lay != 0 ) {
			QMargins mgs ( lay->contentsMargins() );
			mgs.setRight ( 0 );
			mgs.setTop ( 0 );
			mgs.setBottom ( 0 );
			lay->setContentsMargins ( mgs );
		}

		lay = _mixer_ctl.layout();
		if ( lay != 0 ) {
			lay->setContentsMargins ( 0, 0, 0, 0 );
		}
	}

	_tree_view.setCurrentIndex ( _tree_model.base_item()->index() );
}


Main_View_Mixer_CTL::~Main_View_Mixer_CTL ( )
{
	_mixer_ctl.set_snd_elem_group ( 0 );
	_snd_mixer.close();
}


void
Main_View_Mixer_CTL::set_mixer_style (
	QSnd::Mixer_Style * mstyle_n )
{
	_mixer_ctl.set_mixer_style ( mstyle_n );
}


void
Main_View_Mixer_CTL::reload ( )
{
	//std::cout << "Main_View_Mixer_CTL::reload" << "\n";
	Main_View::reload();

	// Close mixer and clean up
	_mixer_ctl.set_snd_elem_group ( 0 );
	_tree_model.set_snd_mixer ( 0 );
	_table_model.set_snd_mixer ( 0 );
	_snd_mixer.close();
	_selected_iface = 0;

	int stack_idx ( 0 );

	// Open mixer
	if ( !current_ctl().is_clear() ) {
		_snd_mixer.open ( current_ctl().ctl_id() );
		if ( _snd_mixer.is_open() ) {
			stack_idx = 1;
		} else {
			message_wdg().set_mixer_open_fail (
				current_ctl().ctl_id(),
				_snd_mixer.err_message(),
				_snd_mixer.err_func() );
		}
	} else {
		message_wdg().set_no_device();
	}

	// Set interface type in tree and table model
	if ( _snd_mixer.iface_types_avail() > 0 ) {

		unsigned int iface_type_idx ( _tree_model.iface_type_idx() );

		if ( _snd_mixer.iface_types_avail() > 0 ) {

			for ( unsigned int ii=0; ii < _snd_mixer.iface_types_avail(); ++ii ) {
				if ( _snd_mixer.iface_avail_type ( ii ) == _default_iface_type_idx ) {
					_selected_iface = ii;
					break;
				};
			}

			iface_type_idx = _snd_mixer.iface_avail_type ( _selected_iface );
		}

		_tree_model.set_iface_type_idx ( iface_type_idx );
		_table_model.set_iface_type_idx ( iface_type_idx );
		_tree_model.set_snd_mixer ( &_snd_mixer );
		_table_model.set_snd_mixer ( &_snd_mixer );
	}

	expand_tree_items();
	adjust_table_columns();


	// Update interface selection

	_iface_avail_names.clear();
	for ( unsigned int ii=0; ii < _snd_mixer.iface_types_avail(); ++ii ) {
		unsigned int type_idx ( _snd_mixer.iface_avail_type ( ii ) );
		_iface_avail_names.append (
			_snd_mixer.data().iface_display_name ( type_idx ) );
	}

	emit sig_ifaces_list_changed();

	lay_stack()->setCurrentIndex ( stack_idx );
}


void
Main_View_Mixer_CTL::expand_tree_items ( )
{
	const QModelIndex base_idx ( _tree_model.base_item()->index() );
	const int items_limit ( 8 );

	// Expand base
	_tree_view.expand ( base_idx );

	const unsigned int num_rows ( _tree_model.rowCount ( base_idx ) );
	for ( unsigned int ii=0; ii < num_rows; ++ii ) {

		QModelIndex mod_idx ( _tree_model.index ( ii, 0, base_idx ) );
		if ( _tree_model.rowCount ( mod_idx ) > items_limit ) {
			continue;
		}

		QSnd::Snd_Mixer_CTL_Elem_Group * elem_grp ( 0 );
		unsigned int elem_idx ( 0 );
		_tree_model.elem_group ( mod_idx, &elem_grp, &elem_idx );
		if ( elem_grp == 0 ) {
			continue;
		}

		QSnd::Snd_Mixer_CTL_Elem * elem ( elem_grp->elem ( 0 ) );
		if (
			elem->is_boolean() ||
			elem->is_integer() ||
			elem->is_enumerated() )
		{
			_tree_view.expand ( mod_idx );
		}
	}

}


void
Main_View_Mixer_CTL::adjust_table_columns ( )
{
	_table_view.resizeColumnsToContents();
	_table_view.resizeRowsToContents();
}


void
Main_View_Mixer_CTL::select_iface (
	int index_n )
{
	if ( index_n < 0 ) {
		return;
	}

	unsigned int iface_type_idx ( index_n );
	if ( iface_type_idx < _snd_mixer.iface_types_avail() ) {

		_selected_iface = iface_type_idx;

		iface_type_idx = _snd_mixer.iface_avail_type ( iface_type_idx );
		if ( iface_type_idx != _tree_model.iface_type_idx() ) {

			_mixer_ctl.set_snd_elem_group ( 0, 0 );
			_tree_model.set_iface_type_idx ( iface_type_idx );
			_table_model.set_iface_type_idx ( iface_type_idx );

			expand_tree_items();
			adjust_table_columns();
		}
	}
}


void
Main_View_Mixer_CTL::tree_element_selected (
	const QModelIndex & idx_n )
{
	if ( idx_n.isValid() ) {
		if ( idx_n == _tree_model.base_item()->index() ) {
			// Show table view
			_lay_center_stack->setCurrentIndex ( 0 );
		} else {
			// Show element mixer view
			QSnd::Snd_Mixer_CTL_Elem_Group * elem_grp ( 0 );
			unsigned int elem_index ( 0 );
			_tree_model.elem_group ( idx_n, &elem_grp, &elem_index );
			_mixer_ctl.set_snd_elem_group ( elem_grp, elem_index );
			_lay_center_stack->setCurrentIndex ( 1 );
		}
	}
}


void
Main_View_Mixer_CTL::table_element_selected (
	const QModelIndex & idx_n )
{
	QSnd::Snd_Mixer_CTL_Elem * elem ( _table_model.elem ( idx_n ) );
	if ( elem != 0 ) {
		QModelIndex tree_idx ( _tree_model.elem_index ( elem ) );
		if ( tree_idx.isValid() ) {
			_tree_view.setCurrentIndex ( tree_idx );
		}
	}
}

