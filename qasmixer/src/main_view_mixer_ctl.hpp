//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_main_view_elements_hpp__
#define __INC_main_view_elements_hpp__

#include "main_view.hpp"
#include "qsnd/snd_mixer_ctl.hpp"
#include "qsnd/mixer_ctl_tree_model.hpp"
#include "qsnd/mixer_ctl_table_model.hpp"
#include "qsnd/mixer_ctl.hpp"
#include "wdg/tree_view_kv.hpp"

#include <QLabel>
#include <QSplitter>
#include <QComboBox>
#include <QTableView>
#include <QStackedLayout>


class Main_View_Mixer_CTL :
	public Main_View
{
	Q_OBJECT

	// Public methods
	public:

	Main_View_Mixer_CTL (
		QSnd::Snd_Control_Address & ctl_n,
		QWidget * parent_n = 0 );

	~Main_View_Mixer_CTL ( );


	// Interface

	unsigned int
	selected_iface ( ) const;

	const QStringList &
	iface_avail_names ( ) const;


	// Pixmaps hub

	void
	set_mixer_style (
		QSnd::Mixer_Style * mstyle_n );


	// Mixer widget

	QSnd::Mixer_CTL &
	mixer_ctl ( );


	// Public signals
	signals:

	void
	sig_ifaces_list_changed ( );


	// Public slots
	public slots:

	void
	reload ( );

	void
	select_iface (
		int index_n );


	// Protected slots
	protected slots:

	void
	tree_element_selected (
		const QModelIndex & idx_n );

	void
	table_element_selected (
		const QModelIndex & idx_n );


	// Protected methods
	protected:

	void
	expand_tree_items ( );

	void
	adjust_table_columns ( );


	// Private attributes
	private:

	QSnd::Snd_Mixer_CTL _snd_mixer;

	QSnd::Mixer_CTL_Tree_Model _tree_model;

	QSnd::Mixer_CTL_Table_Model _table_model;


	// Widgets

	QSplitter _hsplit;


	QWidget _wdg_tree;

	Wdg::Tree_View_KV _tree_view;


	QWidget _wdg_center;

	QStackedLayout * _lay_center_stack;

	QTableView _table_view;

	QSnd::Mixer_CTL _mixer_ctl;


	unsigned int _default_iface_type_idx;

	unsigned int _selected_iface;

	QStringList _iface_avail_names;
};


inline
unsigned int
Main_View_Mixer_CTL::selected_iface ( ) const
{
	return _selected_iface;
}


inline
const QStringList &
Main_View_Mixer_CTL::iface_avail_names ( ) const
{
	return _iface_avail_names;
}


inline
QSnd::Mixer_CTL &
Main_View_Mixer_CTL::mixer_ctl ( )
{
	return _mixer_ctl;
}


#endif
