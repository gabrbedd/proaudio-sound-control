//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "main_view_mixer_simple.hpp"

#include <QCoreApplication>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QStackedLayout>

#include <iostream>


Main_View_Mixer_Simple::Main_View_Mixer_Simple (
	QSnd::Snd_Control_Address & ctl_n,
	QWidget * parent_n ) :
Main_View ( ctl_n, parent_n ),
_qsnd_mixer ( this )
{
	connect ( &_qsnd_mixer, SIGNAL ( sig_mixer_reload_request() ),
		this, SLOT ( reload_delayed() ) );


	// Vertical splitter
	{
		_mixer_split.setOrientation ( Qt::Vertical );
		_mixer_split.addWidget ( &mixer_sliders() );
		_mixer_split.addWidget ( &mixer_switches() );
		_mixer_split.setCollapsible ( 0, false );
		_mixer_split.setCollapsible ( 1, false );
		_mixer_split.setStretchFactor ( 0, 1 );
		_mixer_split.setStretchFactor ( 1, 0 );
		lay_stack()->addWidget ( &_mixer_split );
	}

	// Adjust layout margins
	{
		QMargins mgs ( _mixer_sliders.contentsMargins() );
		mgs.setTop ( 0 );
		_mixer_sliders.setContentsMargins ( mgs );
	}

	if ( _mixer_sliders.layout() != 0 ) {
		QMargins mgs ( _mixer_sliders.layout()->contentsMargins() );
		mgs.setTop ( 0 );
		_mixer_sliders.layout()->setContentsMargins ( mgs );
	}
}


Main_View_Mixer_Simple::~Main_View_Mixer_Simple ( )
{
	mixer_sliders().set_mixer_simple ( 0 );
	mixer_switches().set_mixer_simple ( 0 );
	_qsnd_mixer.close();
}


void
Main_View_Mixer_Simple::set_mixer_style (
	QSnd::Mixer_Style * mstyle_n )
{
	if ( mstyle_n != 0 ) {
		mixer_sliders().set_mixer_style ( mstyle_n );
	}
}


void
Main_View_Mixer_Simple::reload ( )
{
	//std::cout << "Main_View_Mixer_Simple::reload()" << "\n";

	Main_View::reload();

	_mixer_split.hide();

	// Close mixer
	if ( _qsnd_mixer.is_open() ) {
		mixer_sliders().set_mixer_simple ( 0 );
		mixer_switches().set_mixer_simple ( 0 );
		_qsnd_mixer.close();
	}

	unsigned int stack_idx ( 0 );

	// Open mixer
	if ( !current_ctl().is_clear() ) {
		_qsnd_mixer.open ( current_ctl().ctl_id() );

		// Set mixer object
		if ( _qsnd_mixer.is_open() ) {
			mixer_sliders().set_mixer_simple ( &_qsnd_mixer );
			mixer_switches().set_mixer_simple ( &_qsnd_mixer );
			stack_idx = 1;
		} else {
			message_wdg().set_mixer_open_fail (
				current_ctl().ctl_id(),
				_qsnd_mixer.err_message(),
				_qsnd_mixer.err_func() );
		}

	} else {
		message_wdg().set_no_device();
	}

	lay_stack()->setCurrentIndex ( stack_idx );
	if ( stack_idx == 1 ) {
		update_mixer_visibility();
	}
}


void
Main_View_Mixer_Simple::set_mixer_settings (
	const QSnd::Simple_Mixer_Settings & cfg_n )
{
	_mixer_split.hide();

	// Set mixer config
	mixer_sliders().set_mixer_settings ( cfg_n );
	mixer_switches().set_mixer_settings ( cfg_n );

	update_mixer_visibility();
}


void
Main_View_Mixer_Simple::update_mixer_visibility ( )
{
	bool vis = ( mixer_sliders().num_visible() > 0 );
	mixer_sliders().setVisible ( vis);

	vis = ( mixer_switches().num_visible() > 0 );
	mixer_switches().setVisible ( vis );

	{
		QList<int> sizes;
		sizes.append ( 10000 );
		sizes.append ( 1 ); // Sets the switches area to it's minimum height
		_mixer_split.setSizes ( sizes );
	}

	_mixer_split.show();
}
