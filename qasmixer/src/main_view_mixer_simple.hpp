//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_main_view_mixer_hpp__
#define __INC_main_view_mixer_hpp__

#include <QSplitter>

#include "main_view.hpp"
#include "qsnd/snd_mixer_simple.hpp"
#include "qsnd/mixer_sliders.hpp"
#include "qsnd/mixer_switches.hpp"
#include "qsnd/mixer_style.hpp"


class Main_View_Mixer_Simple :
	public Main_View
{
	Q_OBJECT

	// Public methods
	public:

	Main_View_Mixer_Simple (
		QSnd::Snd_Control_Address & ctl_n,
		QWidget * parent_n = 0 );

	~Main_View_Mixer_Simple ( );

	QSnd::Mixer_Sliders &
	mixer_sliders ( );

	QSnd::Mixer_Switches &
	mixer_switches ( );

	void
	set_mixer_style (
		QSnd::Mixer_Style * mstyle_n );

	void
	set_mixer_settings (
		const QSnd::Simple_Mixer_Settings & cfg_n );


	// Public slots
	public slots:

	void
	reload ( );


	// Protected methods
	protected:

	void
	update_mixer_visibility ( );


	// Private attributes
	private:

	QSplitter _mixer_split;

	QSnd::Mixer_Sliders _mixer_sliders;

	QSnd::Mixer_Switches _mixer_switches;


	QSnd::Snd_Mixer_Simple _qsnd_mixer;
};


inline
QSnd::Mixer_Sliders &
Main_View_Mixer_Simple::mixer_sliders ( )
{
	return _mixer_sliders;
}


inline
QSnd::Mixer_Switches &
Main_View_Mixer_Simple::mixer_switches ( )
{
	return _mixer_switches;
}


#endif
