//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_tray_icon_hpp__
#define __INC_tray_icon_hpp__

#include <QObject>
#include <QSystemTrayIcon>
#include <QLabel>
#include <QMenu>

#include "qsnd/snd_mixer_simple_single.hpp"
#include "wdg/label_width.hpp"
#include "wdg/balloon_widget.hpp"


///
/// @brief Mini_Mixer
///
class Mini_Mixer :
	public QObject
{
	Q_OBJECT

	// Public methods
	public:

	Mini_Mixer (
		QObject * parent_n = 0 );


	void
	show_tray_icon (
		bool flag_n );

	void
	set_ctl_address (
		const QString & addr_n );

	void
	set_elem_name (
		const QString & name_n );

	void
	set_wheel_degrees (
		unsigned int degrees_n );

	void
	set_show_balloon (
		bool flag_n );

	void
	set_balloon_lifetime (
		unsigned int ms_n );


	bool
	eventFilter (
		QObject * watched_n,
		QEvent * event_n );


	// Public signals
	signals:

	void
	sig_activated ( );

	void
	sig_show ( );

	void
	sig_close ( );


	// Protected slots
	protected slots:

	void
	mixer_values_changed ( );

	void
	activation (
		QSystemTrayIcon::ActivationReason reason_n );


	// Protected methods
	protected:

	void
	wheel_delta (
		int wheel_delta_n );

	void
	reload_mixer ( );

	void
	clear_mixer_elem ( );

	void
	update_mixer_elem ( );

	bool
	update_volume_permille ( );

	void
	update_icon ( );


	void
	raise_balloon ( );

	void
	close_balloon ( );


	// Private attributes
	private:

	// Mixer

	QSnd::Snd_Mixer_Simple_Single _mixer;

	QSnd::Snd_Mixer_Simple_Elem * _mx_elem;

	QString _ctl_address;

	unsigned int _volume_permille;

	unsigned int _wheel_degrees;


	// State flags

	bool _volume_muted;

	bool _show_balloon;

	bool _mixer_changes;


	// Icons and pixmaps

	QIcon _icon_default;
	QIcon _icons_volume[3];
	QIcon _icon_muted;

	QPixmap _pixmap_default;
	QPixmap _pixmaps_volume[3];
	QPixmap _pixmap_muted;


	// Tray icon

	QSystemTrayIcon _tray_icon;

	QMenu _tray_icon_menu;


	// Ballon widget

	QString _balloon_value_mask;

	Wdg::Balloon_Widget _balloon;

	QLabel * _lbl_ballon_icon;

	Wdg::Label_Width * _lbl_ballon_value;
};


#endif
