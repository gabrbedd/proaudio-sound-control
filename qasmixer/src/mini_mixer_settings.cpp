//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "mini_mixer_settings.hpp"

Mini_Mixer_Settings::Mini_Mixer_Settings ( ) :
device_mode ( DEV_DEFAULT ),
user_device ( "hw:0" ),
show_balloon ( true ),
balloon_lifetime ( 4000 )
{
}
