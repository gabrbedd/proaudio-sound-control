//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_mini_mixer_settings_hpp__
#define __INC_mini_mixer_settings_hpp__

#include <QString>


///
/// @brief Mini_Mixer_Settings
///
class Mini_Mixer_Settings
{
	// Public typedefs
	public:

	enum {
		DEV_DEFAULT  = 0,
		DEV_CURRENT  = 1,
		DEV_USER     = 2,
		DEV_LAST     = DEV_USER
	};


	// Public methods
	public:

	Mini_Mixer_Settings ( );


	// Public attributes

	unsigned int device_mode;

	QString user_device;

	bool show_balloon;

	unsigned int balloon_lifetime;
};


#endif
