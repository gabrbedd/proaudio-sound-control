//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "mixer_settings.hpp"

#include <QSettings>


Mixer_Settings::Mixer_Settings ( ) :
view_type ( 0 ),
show_tray_icon ( TRAY_ICON_ALWAYS ),
wheel_degrees ( 720 ),
show_view_selection ( false ),
tray_on_minimize ( true ),
tray_on_close ( true ),
is_tray_minimized ( false )
{
}


void
Mixer_Settings::load ( )
{
	QSettings settings;

	view_type = settings.value (
		"view_type",
		view_type ).toUInt();

	show_view_selection = settings.value (
		"show_view_selection",
		show_view_selection ).toBool();

	wheel_degrees = settings.value (
		"wheel_degrees",
		wheel_degrees ).toUInt();


	show_tray_icon = settings.value (
		"show_tray_icon",
		show_tray_icon ).toUInt();

	tray_on_minimize = settings.value (
		"tray_on_minimize",
		tray_on_minimize ).toBool();

	tray_on_close = settings.value (
		"tray_on_close",
		tray_on_close ).toBool();

	is_tray_minimized = settings.value (
		"is_tray_minimized",
		is_tray_minimized ).toBool();


	//
	// Simple mixer
	//

	settings.beginGroup ( "simple_mixer" );

	smixer.show_stream[0] = settings.value (
		"show_stream_playback",
		smixer.show_stream[0] ).toBool();

	smixer.show_stream[1] = settings.value (
		"show_stream_capture",
		smixer.show_stream[1] ).toBool();

	smixer.show_slider_status_bar = settings.value (
		"show_slider_status_bar",
		smixer.show_slider_status_bar ).toBool();

	settings.endGroup();


	//
	// Alsa config view
	//

	settings.beginGroup ( "alsa_config_view" );

	alsa_cfg.sorted = settings.value (
		"sorted",
		alsa_cfg.sorted ).toBool();

	settings.endGroup();


	//
	// Mini mixer
	//

	settings.beginGroup ( "mini_mixer" );

	mini.device_mode = settings.value (
		"device_mode",
		mini.device_mode ).toInt();

	mini.user_device = settings.value (
		"user_device",
		mini.user_device ).toString();

	mini.show_balloon = settings.value (
		"show_balloon",
		mini.show_balloon ).toBool();

	mini.balloon_lifetime = settings.value (
		"balloon_lifetime",
		mini.balloon_lifetime ).toUInt();

	settings.endGroup();


	//
	// Sanitize values
	//

	if ( view_type > 2 ) {
		view_type = 0;
	}

	if ( show_tray_icon > TRAY_ICON_LAST ) {
		show_tray_icon = TRAY_ICON_ALWAYS;
	}

	if ( mini.device_mode > Mini_Mixer_Settings::DEV_LAST ) {
		mini.device_mode = Mini_Mixer_Settings::DEV_DEFAULT;
	}

	if ( !( smixer.show_stream[0] || smixer.show_stream[1] ) ) {
		smixer.show_stream[0] = true;
	}
}


void
Mixer_Settings::write ( )
{
	QSettings settings;


	//
	// General
	//

	settings.setValue (
		"view_type",
		view_type );

	settings.setValue (
		"show_view_selection",
		show_view_selection );

	settings.setValue (
		"wheel_degrees",
		wheel_degrees );


	settings.setValue (
		"show_tray_icon",
		show_tray_icon );

	settings.setValue (
		"tray_on_minimize",
		tray_on_minimize );

	settings.setValue (
		"tray_on_close",
		tray_on_close );

	settings.setValue (
		"is_tray_minimized",
		is_tray_minimized );


	//
	// Simple mixer
	//

	settings.beginGroup ( "simple_mixer" );

	settings.setValue (
		"show_stream_playback",
		smixer.show_stream[0] );

	settings.setValue (
		"show_stream_capture",
		smixer.show_stream[1] );

	settings.setValue (
		"show_slider_status_bar",
		smixer.show_slider_status_bar );

	settings.endGroup();


	//
	// Alsa config view
	//

	settings.beginGroup ( "alsa_config_view" );

	settings.setValue (
		"sorted",
		alsa_cfg.sorted );

	settings.endGroup();


	//
	// Mini mixer
	//

	settings.beginGroup ( "mini_mixer" );

	settings.setValue (
		"device_mode",
		mini.device_mode );

	settings.setValue (
		"user_device",
		mini.user_device );

	settings.setValue (
		"show_balloon",
		mini.show_balloon );

	settings.setValue (
		"balloon_lifetime",
		mini.balloon_lifetime );

	settings.endGroup();
}
