//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_mixer_settings_hpp__
#define __INC_mixer_settings_hpp__

#include "qsnd/simple_mixer_settings.hpp"
#include "qsnd/alsa_config_view_settings.hpp"
#include "mini_mixer_settings.hpp"
#include <QString>


///
/// @brief Mixer_Settings
///
class Mixer_Settings
{
	// Public typedefs
	public:

	enum {
		TRAY_ICON_NEVER     = 0,
		TRAY_ICON_MINIMIZED = 1,
		TRAY_ICON_ALWAYS    = 2,
		TRAY_ICON_LAST      = TRAY_ICON_ALWAYS
	};


	// Public methods
	public:

	Mixer_Settings ( );

	void
	load ( );

	void
	write ( );


	// Public attributes

	unsigned int view_type;

	unsigned int show_tray_icon;

	unsigned int wheel_degrees;

	bool show_view_selection;

	bool tray_on_minimize;

	bool tray_on_close;

	bool is_tray_minimized;


	QSnd::Simple_Mixer_Settings smixer;

	QSnd::Alsa_Config_View_Settings alsa_cfg;

	Mini_Mixer_Settings mini;
};


#endif
