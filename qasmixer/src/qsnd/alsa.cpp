//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2009
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "alsa.hpp"

#include <iostream>


namespace QSnd
{


void
print_alsa_error (
	const std::string & prefix,
	int err_n )
{
	std::cerr << "[EE] ";
	std::cerr << prefix << ": ";
	std::cerr << snd_strerror ( err_n ) << "\n";
}


int
acquire_alsa_default_card ( )
{
	int res ( -1 );
	int err;

	snd_ctl_t * snd_ctl_p;
	err = snd_ctl_open ( &snd_ctl_p, "default", SND_CTL_NONBLOCK );
	if ( err < 0 ) {
		const std::string es ( "snd_ctl_open ( default )" );
		print_alsa_error ( es, err );
		return res;
	}

	snd_ctl_card_info_t * snd_card_info_p ( 0 );
	snd_ctl_card_info_alloca ( &snd_card_info_p );

	err = snd_ctl_card_info ( snd_ctl_p, snd_card_info_p );
	if ( err < 0 ) {
		const std::string es ( "snd_ctl_card_info ( default )" );
		print_alsa_error ( es, err );
		return res;
	}

	res = snd_ctl_card_info_get_card ( snd_card_info_p );

	err = snd_ctl_close ( snd_ctl_p );
	if ( err < 0 ) {
		const std::string es ( "snd_ctl_close ( default )" );
		print_alsa_error ( es, err );
	}

	return res;
}


QString
snd_error_qstring (
	int err_n )
{
	return QString::fromLocal8Bit ( snd_strerror ( err_n ) );
}


} // End of namespace

