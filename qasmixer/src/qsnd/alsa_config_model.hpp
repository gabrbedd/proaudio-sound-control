//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_qsnd_alsa_config_model_hpp__
#define __INC_qsnd_alsa_config_model_hpp__

#include "wdg/static_tree_model.hpp"
#include "alsa.hpp"


namespace QSnd
{


///
/// @brief Alsa_Config_Model
///
class Alsa_Config_Model :
	public Wdg::Static_Tree_Model
{
	Q_OBJECT

	// Public methods
	public:

	Alsa_Config_Model ( );

	~Alsa_Config_Model ( );


	// Model methods

	QVariant
	headerData (
		int section,
		Qt::Orientation orientation,
		int role = Qt::DisplayRole ) const;

	Qt::ItemFlags
	flags (
		const QModelIndex & index_n ) const;

	QVariant
	data (
		const QModelIndex & index,
		int role = Qt::DisplayRole ) const;


	// Public slots
	public slots:

	void
	revert ( );


	// Protected methods
	protected:

	void
	clear_config ( );

	int
	add_children_recursively (
		Node * node_n,
		snd_config_t * cfg_n );

	snd_config_t *
	cfg_struct (
		const Node * node_n ) const;

	snd_config_t *
	cfg_struct (
		const QModelIndex & index_n ) const;

	int
	cfg_count_children (
		snd_config_t * cfg_n ) const;

	snd_config_t *
	cfg_child (
		snd_config_t * cfg_n,
		unsigned int index_n ) const;

	QString
	cfg_id_string (
		snd_config_t * cfg_n ) const;

	QString
	cfg_value_string (
		snd_config_t * cfg_n ) const;


	// Private attributes
	private:

	snd_config_t * _cfg_root;
};


} // End of namespace

#endif
