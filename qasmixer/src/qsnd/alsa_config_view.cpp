//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "alsa_config_view.hpp"

#include <QApplication>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include <iostream>


namespace QSnd
{


Alsa_Config_View::Alsa_Config_View (
	Alsa_Config_View_Settings & settings_n,
	QWidget * parent_n ) :
QWidget ( parent_n ),
_settings ( settings_n )
{
	_btn_expand.setText ( tr ( "&Expand" ) );
	_btn_collapse.setText ( tr ( "Co&llapse" ) );
	_btn_sort.setText ( tr ( "&Sort" ) );

	_expand_depth_label.setText ( tr ( "Depth:" ) );
	_expand_depth.setRange ( 0, 999 );
	_expand_depth.setValue ( 2 );

	connect ( &_btn_collapse, SIGNAL ( clicked() ),
		this, SLOT ( collapse_to_level() ) );

	connect ( &_btn_expand, SIGNAL ( clicked() ),
		this, SLOT ( expand_to_level() ) );

	connect ( &_btn_sort, SIGNAL ( toggled ( bool ) ),
		this, SLOT ( enable_sorting ( bool ) ) );

	_tree_view.setSelectionMode ( QAbstractItemView::ExtendedSelection );
	_tree_view.setModel ( &_sort_model );

	if ( _tree_view.selectionModel() != 0 ) {
		connect ( _tree_view.selectionModel(),
			SIGNAL ( selectionChanged  ( const QItemSelection &, const QItemSelection & ) ),
			this,
			SLOT ( items_selected ( const QItemSelection &, const QItemSelection & ) ) );
	}


	QHBoxLayout * lay_btn ( new QHBoxLayout );
	lay_btn->addWidget ( &_btn_collapse );
	lay_btn->addWidget ( &_btn_expand );
	lay_btn->addWidget ( &_expand_depth_label );
	lay_btn->addWidget ( &_expand_depth );
	lay_btn->addStretch ( 1 );
	lay_btn->addWidget ( &_btn_sort );

	QVBoxLayout * lay_vbox ( new QVBoxLayout );
	lay_vbox->setContentsMargins ( 0, 0, 0, 0 );
	lay_vbox->addWidget ( &_tree_view );
	lay_vbox->addLayout ( lay_btn );

	setLayout ( lay_vbox );

	enable_sorting ( _settings.sorted );
}


Alsa_Config_View::~Alsa_Config_View ( )
{
}


void
Alsa_Config_View::set_model (
	QAbstractItemModel * model_n )
{
	_sort_model.setSourceModel ( model_n );
}


void
Alsa_Config_View::enable_sorting (
	bool flag_n )
{
	if ( flag_n ) {
		_sort_model.setDynamicSortFilter ( true );
		_sort_model.sort ( 0 );
	} else {
		_sort_model.setDynamicSortFilter ( false );
		_sort_model.revert();
	}

	_btn_sort.setChecked ( flag_n );
	_settings.sorted = flag_n;

	update_button_state();
}


void
Alsa_Config_View::reload_config ( )
{
	if ( model() != 0 ) {
		model()->revert();
	}
}


void
Alsa_Config_View::expand_to_level (
	bool expanded_n )
{
	int depth ( _expand_depth.value() );
	if ( ( _tree_view.selectionModel() != 0 ) && ( depth >= 0 ) ) {
		QModelIndexList indices ( _tree_view.selectionModel()->selectedRows() );
		for ( int ii=0; ii < indices.size(); ++ii ) {
			const QModelIndex & idx ( indices[ii] );
			if ( idx.isValid() ) {
				_tree_view.set_expanded_recursive ( idx, depth, expanded_n );
			}
		}
	}
}


void
Alsa_Config_View::expand_to_level ( )
{
	expand_to_level ( true );
}


void
Alsa_Config_View::collapse_to_level ( )
{
	expand_to_level ( false );
}


void
Alsa_Config_View::items_selected (
	const QItemSelection &,
	const QItemSelection & )
{
	update_button_state();
}


void
Alsa_Config_View::update_button_state ( )
{
	bool is_enabled ( false );
	if ( _tree_view.selectionModel() != 0 ) {
		is_enabled = _tree_view.selectionModel()->hasSelection();
	}

	_btn_expand.setEnabled ( is_enabled );
	_btn_collapse.setEnabled ( is_enabled );
	_expand_depth.setEnabled ( is_enabled );
}


} // End of namespace
