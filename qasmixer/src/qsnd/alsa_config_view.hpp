//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_alsa_config_view_hpp__
#define __INC_alsa_config_view_hpp__

#include "alsa_config_view_settings.hpp"
#include "wdg/tree_view_kv.hpp"

#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QSpinBox>

#include <QSortFilterProxyModel>


namespace QSnd
{


class Alsa_Config_View :
	public QWidget
{
	Q_OBJECT

	// Public methods
	public:

	Alsa_Config_View (
		Alsa_Config_View_Settings & settings_n,
		QWidget * parent_n = 0 );

	~Alsa_Config_View ( );


	QAbstractItemModel *
	model ( ) const;

	void
	set_model (
		QAbstractItemModel * model_n );


	// Public slots
	public slots:

	void
	reload_config ( );

	void
	expand_to_level (
		bool expanded_n );

	void
	expand_to_level ( );

	void
	collapse_to_level ( );

	void
	enable_sorting (
		bool flag_n );


	// Protected slots
	protected slots:

	void
	items_selected (
		const QItemSelection & sel0_n,
		const QItemSelection & sel1_n );


	// Protected methods
	protected:

	void
	update_button_state ( );


	// Private attributes
	private:

	Alsa_Config_View_Settings & _settings;

	QSortFilterProxyModel _sort_model;

	Wdg::Tree_View_KV _tree_view;

	QPushButton _btn_expand;
	QPushButton _btn_collapse;
	QCheckBox _btn_sort;

	QLabel _expand_depth_label;
	QSpinBox _expand_depth;
};


inline
QAbstractItemModel *
Alsa_Config_View::model ( ) const
{
	return _sort_model.sourceModel();
}


} // End of namespace


#endif
