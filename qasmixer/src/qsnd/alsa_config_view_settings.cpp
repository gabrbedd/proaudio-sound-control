//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "alsa_config_view_settings.hpp"


namespace QSnd
{


Alsa_Config_View_Settings::Alsa_Config_View_Settings ( )
{
	sorted = true;
}


bool
Alsa_Config_View_Settings::operator == (
	const Alsa_Config_View_Settings & cfg_n ) const
{
	return ( sorted == cfg_n.sorted );
}


bool
Alsa_Config_View_Settings::operator != (
	const Alsa_Config_View_Settings & cfg_n ) const
{
	return !operator== ( cfg_n );
}


} // End of namespace
