//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_alsa_config_view_settings_hpp__
#define __INC_alsa_config_view_settings_hpp__


namespace QSnd
{


///
/// @brief Alsa_Config_View_Settings
///
class Alsa_Config_View_Settings
{
	// Public methods
	public:

	Alsa_Config_View_Settings ( );

	bool
	operator == (
		const Alsa_Config_View_Settings & cfg_n ) const;

	bool
	operator != (
		const Alsa_Config_View_Settings & cfg_n ) const;


	// Public attributes

	bool sorted;
};


} // End of namespace


#endif
