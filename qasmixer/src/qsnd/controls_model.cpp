//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "controls_model.hpp"

#include <QFont>
#include <iostream>
#include "alsa.hpp"


namespace QSnd
{


Controls_Model::Controls_Model (
	QAbstractItemModel & config_model_n ) :
_default_card_idx ( -1 ),
_config_model ( config_model_n )
{
	QStandardItem * item_root ( invisibleRootItem() );

	QFont fnt;
	fnt.setWeight ( QFont::Bold );

	_item_cards = new QStandardItem;
	_item_cards->setText ( tr ( "Cards" ) );
	_item_cards->setFont ( fnt );
	_item_cards->setEditable ( false );
	_item_cards->setSelectable ( false );
	item_root->appendRow ( _item_cards );

	_item_plugs = new QStandardItem;
	_item_plugs->setText ( tr ( "Plugins" ) );
	_item_plugs->setFont ( fnt );
	_item_plugs->setEditable ( false );
	_item_plugs->setSelectable ( false );
	item_root->appendRow ( _item_plugs );

	connect ( &_config_model, SIGNAL ( modelAboutToBeReset() ),
		this, SLOT ( reset_begin() ) );

	connect ( &_config_model, SIGNAL ( modelReset() ),
		this, SLOT ( reset_finish() ) );

	revert();
}


Controls_Model::~Controls_Model ( )
{
	clear_cards();
	clear_plugins();
}


const QSnd::Snd_Card_Info *
Controls_Model::card_info (
	int index_n ) const
{
	if ( index_n < num_cards() ) {
		return _card_infos[index_n];
	}
	return 0;
}


const QSnd::Snd_Control_Address *
Controls_Model::ctl_info (
	const QModelIndex & idx_n ) const
{
	const QSnd::Snd_Control_Address * res ( 0 );
	if ( idx_n.parent() == _item_cards->index() ) {
		if ( idx_n.row() < _controls_cards.size() ) {
			res = &_controls_cards[ idx_n.row() ];
		}
	} else if ( idx_n.parent() == _item_plugs->index() ) {
		if ( idx_n.row() < _controls_plugs.size() ) {
			res = &_controls_plugs[ idx_n.row() ];
		}
	}

	return res;
}


QModelIndex
Controls_Model::ctl_info_index (
	const QSnd::Snd_Control_Address & ctl_info_n ) const
{
	QModelIndex res;

	// Search in cards
	for ( int ii=0; ii < _controls_cards.size(); ++ii ) {
		if ( _controls_cards[ii] == ctl_info_n ) {
			res = _item_cards->child ( ii )->index();
			break;
		};
	}

	// Search in plugins
	if ( !res.isValid() ) {
		for ( int ii=0; ii < _controls_plugs.size(); ++ii ) {
			if ( _controls_plugs[ii] == ctl_info_n ) {
				res = _item_plugs->child ( ii )->index();
				break;
			};
		}
	}

	return res;
}


void
Controls_Model::revert ( )
{
	reset_begin();
	reset_finish();
}


void
Controls_Model::reset_begin ( )
{
	beginResetModel();

	clear_cards();
	clear_plugins();
}


void
Controls_Model::reset_finish ( )
{
	load_cards();
	load_plugins();

	endResetModel();
}


void
Controls_Model::clear_cards ( )
{
	_item_cards->removeRows ( 0, _item_cards->rowCount() );

	if ( _card_infos.size() > 0 ) {
		for ( int ii=0; ii < _card_infos.size(); ++ii ) {
			delete _card_infos[ii];
		}
		_card_infos.clear();
	}
	_controls_cards.clear();

	_default_index = QModelIndex();
	_default_ctl.clear();
	_default_card_idx = -1;
}


void
Controls_Model::clear_plugins ( )
{
	_item_plugs->removeRows ( 0, _item_plugs->rowCount() );
	_controls_plugs.clear();
}


void
Controls_Model::load_cards ( )
{
	_default_card_idx = qMax ( 0, acquire_alsa_default_card() );

	int card_idx = -1;
	while ( ( snd_card_next ( &card_idx ) == 0 ) && ( card_idx >= 0 ) ) {
		QSnd::Snd_Card_Info * card ( new QSnd::Snd_Card_Info );
		card->acquire_info ( card_idx );
		_card_infos.push_back ( card );
	}


	// Setup control elements
	_controls_cards.clear();

	for ( int ii=0; ii < _card_infos.size(); ++ii ) {
		_controls_cards.append ( Snd_Control_Address() );
		Snd_Control_Address & ctl ( _controls_cards[ii] );
		const Snd_Card_Info * cinfo ( _card_infos[ii] );

		Snd_Control_Address_Arg argm ( "CARD", "string" );
		argm.arg_value = QString ( "%1" ).arg ( cinfo->card_index() );

		ctl.set_ctl_name ( "hw" );
		ctl.append_arg ( argm );

		if ( cinfo->card_index() == _default_card_idx ) {
			_default_ctl = ctl;
		}
	}


	// Create standard items
	if ( _controls_cards.size() > 0 ) {
		const QString ttip_emask ( "<div><b>%1:</b> %2</div>" );
		const QString ttip_def_card ( tr ( "Default card" ) );
		QString valc;

		for ( int ii=0; ii < _controls_cards.size(); ++ii ) {
			const Snd_Card_Info * cinfo ( _card_infos[ii] );

			QStandardItem * sitem ( new QStandardItem );
			sitem->setText ( cinfo->card_name() );
			sitem->setEditable ( false );

			if ( cinfo->card_index() == _default_card_idx ) {
				QFont fnt;
				fnt.setStyle ( QFont::StyleItalic );
				sitem->setFont ( fnt );
			}

			{
				QString ttip;
				valc = ttip_emask;
				valc = valc.arg ( tr ( "Name" ) );
				valc = valc.arg ( cinfo->card_name() );
				ttip.append ( valc );

				valc = ttip_emask;
				valc = valc.arg ( tr ( "Id" ) );
				valc = valc.arg ( cinfo->card_id() );
				ttip.append ( valc );

				valc = ttip_emask;
				valc = valc.arg ( tr ( "Mixer name" ) );
				valc = valc.arg ( cinfo->card_mixer_name() );
				ttip.append ( valc );

				if ( cinfo->card_index() == _default_card_idx ) {
					ttip += "<div><i>";
					ttip += ttip_def_card;
					ttip += "</i></div>";
				}
				sitem->setToolTip ( ttip );
			}

			_item_cards->appendRow ( sitem );

			if ( cinfo->card_index() == _default_card_idx ) {
				_default_index = sitem->index();
			}
		}
	}
}


void
Controls_Model::load_plugins ( )
{
	QModelIndex idx_ctl;
	for ( int ii=0; ii < _config_model.rowCount(); ++ii ) {
		QModelIndex idx ( _config_model.index ( ii, 0 ) );
		if ( _config_model.data ( idx ) == "ctl" ) {
			idx_ctl = idx;
			break;
		}
	}

	if ( idx_ctl.isValid() ) {

		// Create control items
		for ( int ii=0; ii < _config_model.rowCount ( idx_ctl ); ++ii ) {
			QModelIndex idx ( _config_model.index ( ii, 0, idx_ctl ) );
			if ( _config_model.rowCount ( idx ) > 0 ) {
				QString str ( _config_model.data ( idx ).toString() );
				Snd_Control_Address qctli ( str );
				_controls_plugs.append ( qctli );
			}
		}

		// Create standard items
		if ( _controls_plugs.size() > 0 ) {
			for ( int ii=0; ii < _controls_plugs.size(); ++ii ) {
				QStandardItem * sitem ( new QStandardItem );
				sitem->setText ( _controls_plugs[ii].ctl_name() );
				sitem->setEditable ( false );

				_item_plugs->appendRow ( sitem );
			}
		}

	}

}


} // End of namespace

