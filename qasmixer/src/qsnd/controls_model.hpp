//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_snd_controls_model_hpp__
#define __INC_snd_controls_model_hpp__

#include <QList>
#include <QAbstractItemModel>
#include <QStandardItemModel>

#include "snd_card_info.hpp"
#include "snd_control_address.hpp"


namespace QSnd
{


///
/// @brief Controls_Model
///
class Controls_Model :
	public QStandardItemModel
{
	Q_OBJECT;

	// Public methods
	public:

	Controls_Model (
		QAbstractItemModel & config_model_n );

	~Controls_Model ( );


	// Base items access

	const QStandardItem *
	item_cards ( ) const;

	const QStandardItem *
	item_plugs ( ) const;


	// Cards access

	int
	num_cards ( ) const;

	const QSnd::Snd_Card_Info *
	card_info (
		int index_n ) const;


	// Controls access

	int
	num_controls ( ) const;

	const QSnd::Snd_Control_Address *
	ctl_info (
		const QModelIndex & idx_n ) const;

	QModelIndex
	ctl_info_index (
		const QSnd::Snd_Control_Address & ctl_info_n ) const;


	// Default values

	const QSnd::Snd_Control_Address &
	default_ctl ( ) const;

	QModelIndex
	default_index ( ) const;


	// Public slots
	public slots:

	void
	revert ( );


	// Protected slots
	protected slots:

	void
	reset_begin ( );

	void
	reset_finish ( );


	// Protected methods
	protected:

	void
	clear_cards ( );

	void
	clear_plugins ( );

	void
	load_cards ( );

	void
	load_plugins ( );


	// Private attributes
	private:

	QStandardItem * _item_cards;

	QStandardItem * _item_plugs;

	QList < QSnd::Snd_Card_Info * > _card_infos;

	QList < QSnd::Snd_Control_Address > _controls_cards;

	QList < QSnd::Snd_Control_Address > _controls_plugs;


	QModelIndex _default_index;

	QSnd::Snd_Control_Address _default_ctl;

	int _default_card_idx;


	QAbstractItemModel & _config_model;
};


inline
const QStandardItem *
Controls_Model::item_cards ( ) const
{
	return _item_cards;
}


inline
const QStandardItem *
Controls_Model::item_plugs ( ) const
{
	return _item_plugs;
}


inline
int
Controls_Model::num_cards ( ) const
{
	return _card_infos.size();
}


inline
QModelIndex
Controls_Model::default_index ( ) const
{
	return _default_index;
}


inline
const QSnd::Snd_Control_Address &
Controls_Model::default_ctl ( ) const
{
	return _default_ctl;
}


inline
int
Controls_Model::num_controls ( ) const
{
	return _controls_plugs.size();
}


} // End of namespace


#endif


