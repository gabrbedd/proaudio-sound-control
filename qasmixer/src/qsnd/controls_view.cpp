//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "controls_view.hpp"
#include <iostream>


namespace QSnd
{


//
// Controls_View
//
Controls_View::Controls_View (
	QWidget * parent ) :
QTreeView ( parent ),
_show_rows_min ( 3 ),
_show_rows_avrg ( 10 ),
_min_chars_vis ( 16 )
{
	setHeaderHidden ( true );
	setRootIsDecorated ( false );
}


Controls_View::~Controls_View ( )
{
}


QSize
Controls_View::minimumSizeHint ( ) const
{
	ensurePolished();

	QSize res;
	int & ww ( res.rwidth() );
	int & hh ( res.rheight() );

	QSize vsb_msh ( verticalScrollBar()->minimumSizeHint() );
	QSize vsb_sh ( verticalScrollBar()->sizeHint() );

	// Width
	{
		ww = fontMetrics().averageCharWidth() * _min_chars_vis;
		ww = qMax ( 16, ww );
		ww += indentation();
		if ( vsb_msh.width() > 0 ) {
			ww += vsb_msh.width();
		} else if ( vsb_sh.width() > 0 ) {
			ww += vsb_sh.width();
		}
	}


	// Height
	{
		int rh0 ( sizeHintForRow ( 0 ) );
		rh0 = qMax ( rh0,
			itemDelegate()->sizeHint ( viewOptions(), QModelIndex() ).height() );
		hh = rh0 * _show_rows_min;
		hh = qMax ( vsb_msh.height(), hh );
	}

	QMargins mg ( contentsMargins() );
	ww += mg.left() + mg.right();
	hh += mg.top() + mg.bottom();

	return res;
}


QSize
Controls_View::sizeHint ( ) const
{
	ensurePolished();

	QSize res;
	int & ww ( res.rwidth() );
	int & hh ( res.rheight() );

	QSize vsb_sh ( verticalScrollBar()->sizeHint() );

	// Width
	{
		ww = fontMetrics().averageCharWidth() * _min_chars_vis;
		ww += indentation();
		if ( vsb_sh.width() > 0 ) {
			ww += vsb_sh.width();
		}
	}

	// Height
	{

		int rh0 ( sizeHintForRow ( 0 ) );
		rh0 = qMax ( rh0,
			itemDelegate()->sizeHint ( viewOptions(), QModelIndex() ).height() );
		hh = rh0 * _show_rows_avrg;
		hh = qMax ( vsb_sh.height(), hh );
	}

	QMargins mg ( contentsMargins() );
	ww += mg.left() + mg.right();
	hh += mg.top() + mg.bottom();

	return res;
}


int
Controls_View::sizeHintForColumn (
	int column ) const
{
	int ww ( 16 );
	ww = qMax ( fontMetrics().averageCharWidth() * _min_chars_vis, ww );
	ww = qMax ( QTreeView::sizeHintForColumn ( column ), ww );
	return ww;
}


void
Controls_View::currentChanged (
	const QModelIndex & current,
	const QModelIndex & previous )
{
	QTreeView::currentChanged ( current, previous );
	emit activated ( current );
}


} // End of namespace

