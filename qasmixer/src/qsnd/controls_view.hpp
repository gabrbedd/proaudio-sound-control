//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_controls_view_hpp__
#define __INC_controls_view_hpp__

#include <QTreeView>
#include <QScrollBar>


namespace QSnd
{


///
/// @brief Controls_View
///
class Controls_View :
	public QTreeView
{
	Q_OBJECT

	// Public methods
	public:

	Controls_View (
		QWidget * parent = 0 );

	~Controls_View ( );


	QSize
	minimumSizeHint ( ) const;

	QSize
	sizeHint ( ) const;

	int
	sizeHintForColumn (
		int column ) const;


	// Protected methods
	protected:

	void
	currentChanged (
		const QModelIndex & current,
		const QModelIndex & previous );


	// Private attributes
	private:

	int _show_rows_min;
	int _show_rows_avrg;
	int _min_chars_vis;
};


} // End of namespace


#endif
