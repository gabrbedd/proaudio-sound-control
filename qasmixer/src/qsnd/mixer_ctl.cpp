//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mixer_ctl.hpp"

#include "mixer_ctl_proxy_switch.hpp"
#include "mixer_ctl_proxy_integer.hpp"
#include "mixer_ctl_proxy_enum.hpp"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>

#include <QCheckBox>
#include <QPushButton>
#include <QComboBox>

#include "wdg/equal_columns_layout.hpp"
#include "wdg/covered_spinbox.hpp"
#include "wdg/label_width.hpp"
#include "wdg/ds_slider.hpp"
#include "wdg/ds_slider_painter_basic.hpp"
#include "wdg/ds_check_button.hpp"
#include "wdg/ds_check_button_painter_basic.hpp"

#include <QCoreApplication>
#include <QKeyEvent>
#include <iostream>


namespace QSnd
{


//
// Mixer_CTL
//
Mixer_CTL::Mixer_CTL (
	QWidget * parent_n ) :
QWidget ( parent_n ),
_mixer_style ( 0 ),
_snd_elem_group ( 0 ),
_elem_idx ( 0 ),
_wheel_degrees ( 0 ),
_editor_pad ( 0 ),
_slider_hub ( new Wdg::Painter::DS_Slider_Painter_Basic, 3 ),
_check_button_hub ( new Wdg::Painter::DS_Check_Button_Painter_Basic, 2 )
{
	setContentsMargins ( 0, 0, 0, 0 );

	_str_bool_yes = tr ( "yes" );
	_str_bool_no = tr ( "no" );

	_str_range = tr ( "Range:" );
	_str_unsupported = tr ( "Elements of the type %1 are not supported" );

	{
		const QString dmask ( "<div>%1</div>\n<div>(%2)</div>" );
		_ttip_name_lbl_mask = dmask.arg ( tr ( "Element name" ) );
	}


	_ttip_grid_lbl_elem = tr ( "Element index" );
	_ttip_grid_lbl_channel = tr ( "Channel index" );
	{
		const QString dmask ( "<div>%1</div>" );
		_ttip_grid_widget += dmask.arg ( tr ( "El. index: %1" ) );
		_ttip_grid_widget += "\n";
		_ttip_grid_widget += dmask.arg ( tr ( "Channel: %2" ) );
	}


	// Pad widget layout

	{
		QVBoxLayout * lay_pad_wdg ( new QVBoxLayout );
		lay_pad_wdg->setContentsMargins ( 0, 0, 0, 0 );
		_pad_wdg.setLayout ( lay_pad_wdg );
	}


	// Info widget 1 layout

	{
		QHBoxLayout * lay_elem_name ( new QHBoxLayout );
		lay_elem_name->setContentsMargins ( 0, 0, 0, 0 );
		lay_elem_name->addWidget ( &_info_lbl_name );
		lay_elem_name->addStretch();

		QVBoxLayout * lay_info_wdg ( new QVBoxLayout );
		lay_info_wdg->setContentsMargins ( 0, 0, 0, 0 );
		lay_info_wdg->addLayout ( lay_elem_name );
		_info_wdg_1.setLayout ( lay_info_wdg );
	}


	// Info widget 2 layout

	{

		QHBoxLayout * lay_info_items_1 ( new QHBoxLayout );
		lay_info_items_1->setContentsMargins ( 0, 0, 0, 0 );

		{
			QGridLayout * lay_items ( new QGridLayout );
			lay_items->setContentsMargins ( 0, 0, 0, 0 );
			lay_items->addWidget ( &_info_lbl_index.name,  0, 0 );
			lay_items->addWidget ( &_info_lbl_index.value, 0, 1 );
			lay_items->addWidget ( &_info_lbl_dev.name,    0, 2 );
			lay_items->addWidget ( &_info_lbl_dev.value,   0, 3 );

			lay_info_items_1->addLayout ( lay_items );
			lay_info_items_1->addStretch();
		}

		QHBoxLayout * lay_info_items_2 ( new QHBoxLayout );
		lay_info_items_2->setContentsMargins ( 0, 0, 0, 0 );

		{
			QGridLayout * lay_items ( new QGridLayout );
			lay_items->setContentsMargins ( 0, 0, 0, 0 );

			lay_items->addWidget ( &_info_lbl_active.name,    0, 0 );
			lay_items->addWidget ( &_info_lbl_active.value,   0, 1 );
			lay_items->addWidget ( &_info_lbl_readable.name,  0, 2 );
			lay_items->addWidget ( &_info_lbl_readable.value, 0, 3 );
			lay_items->addWidget ( &_info_lbl_count.name,     0, 4 );
			lay_items->addWidget ( &_info_lbl_count.value,    0, 5 );
			lay_items->addWidget ( &_info_lbl_volatile.name,  1, 0 );
			lay_items->addWidget ( &_info_lbl_volatile.value, 1, 1 );
			lay_items->addWidget ( &_info_lbl_writable.name,  1, 2 );
			lay_items->addWidget ( &_info_lbl_writable.value, 1, 3 );
			lay_items->addWidget ( &_info_lbl_numid.name,     1, 4 );
			lay_items->addWidget ( &_info_lbl_numid.value,    1, 5 );

			lay_info_items_2->addLayout ( lay_items );
			lay_info_items_2->addStretch();
		}

		QVBoxLayout * lay_info_wdg ( new QVBoxLayout );
		lay_info_wdg->setContentsMargins ( 0, 0, 0, 0 );
		lay_info_wdg->addLayout ( lay_info_items_1 );
		lay_info_wdg->addLayout ( lay_info_items_2 );
		_info_wdg_2.setLayout ( lay_info_wdg );
	}


	// Item name labels
	_info_lbl_index.name.setText ( tr ( "Index:" ) );
	_info_lbl_dev.name.setText ( tr ( "Device:" ) );

	_info_lbl_active.name.setText ( tr ( "Active:" ) );
	_info_lbl_volatile.name.setText ( tr ( "Volatile:" ) );
	_info_lbl_readable.name.setText ( tr ( "Readable:" ) );
	_info_lbl_writable.name.setText ( tr ( "Writable:" ) );

	_info_lbl_count.name.setText ( tr ( "Channels:" ) );
	_info_lbl_numid.name.setText ( tr ( "Num. id:" ) );


	// Item name tooltips

	_info_lbl_index.name.setToolTip ( tr ( "Element index" ) );
	_info_lbl_dev.name.setToolTip ( tr ( "Device" ) );

	_info_lbl_active.name.setToolTip ( tr ( "Is active" ) );
	_info_lbl_volatile.name.setToolTip ( tr ( "Is volatile" ) );
	_info_lbl_readable.name.setToolTip ( tr ( "Is readable" ) );
	_info_lbl_writable.name.setToolTip ( tr ( "Is writable" ) );

	_info_lbl_count.name.setToolTip ( tr ( "Channel count" ) );
	_info_lbl_numid.name.setToolTip ( tr ( "Numeric Id" ) );


	// Item values

	_info_dev_mask = "%1,%2";
	_info_lbl_index.value.set_min_text ( "1000" );
	_info_lbl_dev.value.set_min_text ( "999,99" );

	_info_lbl_active.value.set_min_text ( "AAAA" );
	_info_lbl_volatile.value.set_min_text ( "AAAA" );
	_info_lbl_readable.value.set_min_text ( "AAAA" );
	_info_lbl_writable.value.set_min_text ( "AAAA" );

	_info_lbl_count.value.set_min_text ( "1000" );
	_info_lbl_numid.value.set_min_text ( "1000" );

	// Item value tooltips

	{
		QString ttip_dev ( _info_dev_mask );
		ttip_dev = ttip_dev.arg ( tr ( "Device" ) );
		ttip_dev = ttip_dev.arg ( tr ( "Subdevice" ) );

		_info_lbl_index.value.setToolTip ( _info_lbl_index.name.toolTip() );
		_info_lbl_dev.value.setToolTip ( ttip_dev );

		_info_lbl_count.value.setToolTip ( _info_lbl_count.name.toolTip() );
		_info_lbl_numid.value.setToolTip ( _info_lbl_numid.name.toolTip() );
	}



	// Item alignment
	{
		const Qt::Alignment align_cc ( Qt::AlignHCenter | Qt::AlignVCenter );
		const Qt::Alignment align_lc ( Qt::AlignLeft | Qt::AlignVCenter );
		_info_lbl_index.value.setAlignment ( align_cc );
		_info_lbl_dev.value.setAlignment ( align_cc );
		_info_lbl_readable.value.setAlignment ( align_lc );
		_info_lbl_writable.value.setAlignment ( align_lc );
		_info_lbl_volatile.value.setAlignment ( align_lc );
		_info_lbl_active.value.setAlignment ( align_lc );
		_info_lbl_count.value.setAlignment ( align_lc );
		_info_lbl_numid.value.setAlignment ( align_lc );
	}

	// Set final layout
	{
		unsigned int vspace ( vspacer_height() );
		QVBoxLayout * lay_vbox ( new QVBoxLayout );
		lay_vbox->addWidget ( &_info_wdg_1, 0 );
		lay_vbox->addSpacing ( vspace );
		lay_vbox->addWidget ( &_pad_wdg, 1 );
		lay_vbox->addSpacing ( vspace );
		lay_vbox->addWidget ( &_info_wdg_2, 0 );
		setLayout ( lay_vbox );
	}

	update_info();
}


Mixer_CTL::~Mixer_CTL ( )
{
	clear();
}


void
Mixer_CTL::set_mixer_style (
	Mixer_Style * mstyle_n )
{
	_mixer_style = mstyle_n;
}


void
Mixer_CTL::set_wheel_degrees (
	unsigned int degrees_n )
{
	if ( _wheel_degrees != degrees_n ) {

		_wheel_degrees = degrees_n;

		for ( int ii=0; ii < _input_widgets.size(); ++ii ) {
			Wdg::DS_Slider * slider (
				dynamic_cast < Wdg::DS_Slider * > ( _input_widgets[ii] ) );
			if ( slider != 0 ) {
				slider->set_wheel_degrees ( _wheel_degrees );
			}
		}
	}
}


void
Mixer_CTL::clear ( )
{
	_input_widgets.clear();
	_secondary_widgets.clear();

	if ( _editor_pad != 0 ) {
		delete _editor_pad;
		_editor_pad = 0;
	}

	if ( _proxies.size() > 0 ) {
		for ( int ii=0; ii < _proxies.size(); ++ii ) {
			delete _proxies[ii];
		}
		_proxies.clear();
	}

	_snd_elem_group = 0;
}


void
Mixer_CTL::set_snd_elem_group (
	Snd_Mixer_CTL_Elem_Group * elem_group_n,
	unsigned int index_n )
{
	if ( ( snd_elem_group() == elem_group_n ) &&
		( _elem_idx == index_n ) )
	{
		return;
	}

	clear();

	_snd_elem_group = elem_group_n;
	_elem_idx = index_n;

	if ( snd_elem_group() != 0 ) {
		setup_widgets();
		update_proxies_values();
	}

	update_info();
}


void
Mixer_CTL::update_proxies_values ( )
{
	for ( int ii=0; ii < _proxies.size(); ++ii ) {
		_proxies[ii]->update_value();
	}
}


void
Mixer_CTL::setup_widgets ( )
{
	if ( snd_elem_group() == 0 ) {
		return;
	}
	if ( snd_elem_group()->num_elems() == 0 ) {
		return;
	}

	if ( elem_idx() < snd_elem_group()->num_elems() ) {
		Snd_Mixer_CTL_Elem * elem ( snd_elem_group()->elem ( elem_idx() ) );
		if ( elem->is_boolean() ) {
			setup_boolean();
		} else if ( elem->is_integer() ) {
			setup_integer();
		} else if ( elem->is_enumerated() ) {
			setup_enumerated();
		} else {
			setup_unsupported();
		}
	} else {
		Snd_Mixer_CTL_Elem * elem ( snd_elem_group()->elem ( 0 ) );
		if ( elem->is_boolean() ) {
			setup_boolean_grid();
		} else if ( elem->is_integer() ) {
			setup_integer_grid();
		} else if ( elem->is_enumerated() ) {
			setup_enumerated_grid();
		} else {
			setup_unsupported();
		}
	}
}


void
Mixer_CTL::setup_boolean ( )
{
	Snd_Mixer_CTL_Elem * elem ( snd_elem_group()->elem ( elem_idx() ) );

	const unsigned int num_channels ( elem->count() );
	for ( unsigned int ii=0; ii < num_channels; ++ii ) {
		Mixer_CTL_Proxy_Switch * mcps ( new Mixer_CTL_Proxy_Switch );
		mcps->set_snd_elem ( elem );
		mcps->set_elem_idx ( ii );
		_proxies.append ( mcps );
	}

	// Set joined on demand
	{
		bool joined ( true );
		bool value = elem->switch_state ( 0 );
		for ( unsigned int ii=1; ii < num_channels; ++ii ) {
			if ( elem->switch_state ( ii ) != value ) {
				joined = false;
				break;
			}
		}
		for ( unsigned int ii=0; ii < num_channels; ++ii ) {
			_proxies[ii]->set_joined ( joined );
		}
	}

	const bool is_enabled (
		elem->is_writable() );

	QHBoxLayout * lay_checks ( new QHBoxLayout );
	lay_checks->setContentsMargins ( 0, 0, 0, 0 );

	{
		QList < QWidget * > widgets;

		for ( unsigned int ii=0; ii < num_channels; ++ii ) {

			Mixer_CTL_Proxy_Switch * mcps (
				static_cast < Mixer_CTL_Proxy_Switch * > ( _proxies[ii] ) );

			Wdg::DS_Check_Button * wdg_sw (
				new Wdg::DS_Check_Button ( 0, &_check_button_hub ) );

			wdg_sw->installEventFilter ( mcps );
			wdg_sw->setEnabled ( is_enabled );

			connect ( mcps, SIGNAL ( sig_switch_state_changed ( bool ) ),
				wdg_sw, SLOT ( setChecked ( bool ) ) );

			connect ( wdg_sw, SIGNAL ( toggled ( bool ) ),
				mcps, SLOT ( set_switch_state ( bool ) ) );

			widgets.append ( wdg_sw );

			_input_widgets.append ( wdg_sw );
			if ( ii > 0 ) {
				_secondary_widgets.append ( wdg_sw );
			}
		}

		QLayout * lay_grid ( create_channel_grid ( widgets, false ) );
		lay_checks->addLayout ( lay_grid, 0 );
		lay_checks->addStretch ( 1 );
	}

	// Mass checking buttons

	QVBoxLayout * lay_extra ( new QVBoxLayout );
	lay_extra->setContentsMargins ( 0, 0, 0, 0 );

	// Toggle all button
	{
		QPushButton * btn_toggle_all ( new QPushButton );

		btn_toggle_all->setText ( tr ( "Toggle all" ) );
		btn_toggle_all->setEnabled ( is_enabled );

		connect ( btn_toggle_all, SIGNAL ( clicked ( bool ) ),
			this, SLOT ( invert_all_switches() ) );

		QHBoxLayout * lay_hor ( new QHBoxLayout );
		lay_hor->setContentsMargins ( 0, 0, 0, 0 );
		lay_hor->addWidget ( btn_toggle_all, 0 );

		// Joined button
		if ( num_channels > 1 ) {
			QWidget * btn_joined ( create_joined_button ( is_enabled ) );
			lay_hor->addSpacing ( fontMetrics().averageCharWidth()*2 );
			lay_hor->addWidget ( btn_joined, 0 );
		}

		lay_hor->addStretch ( 1 );
		lay_extra->addLayout ( lay_hor );
	}


	// Pad layout
	_editor_pad = new QWidget;
	{
		QVBoxLayout * lay_pad ( new QVBoxLayout );
		lay_pad->setContentsMargins ( 0, 0, 0, 0 );
		lay_pad->addLayout ( lay_checks, 0 );
		lay_pad->addSpacing ( fontMetrics().height() );
		lay_pad->addLayout ( lay_extra, 0 );
		lay_pad->addStretch ( 1 );
		_editor_pad->setLayout ( lay_pad );
	}

	_pad_wdg.layout()->addWidget ( _editor_pad );

	update_enabled_state();
}



void
Mixer_CTL::setup_integer ( )
{
	Snd_Mixer_CTL_Elem * elem ( snd_elem_group()->elem ( elem_idx() ) );

	// Create proxies
	const unsigned int num_channels ( elem->count() );
	for ( unsigned int ii=0; ii < num_channels; ++ii ) {
		Mixer_CTL_Proxy_Integer * mcpi ( new Mixer_CTL_Proxy_Integer );
		mcpi->set_snd_elem ( elem );
		mcpi->set_elem_idx ( ii );
		_proxies.append ( mcpi );
	}

	// Set joined on demand
	{
		bool joined ( true );
		long value = elem->integer_value ( 0 );
		for ( unsigned int ii=1; ii < num_channels; ++ii ) {
			if ( elem->integer_value ( ii ) != value ) {
				joined = false;
				break;
			}
		}
		for ( unsigned int ii=0; ii < num_channels; ++ii ) {
			_proxies[ii]->set_joined ( joined );
		}
	}

	const bool is_enabled (
		elem->is_writable() );

	//
	// Sliders area
	//

	Wdg::Equal_Columns_Layout * lay_sl_pad (
		new Wdg::Equal_Columns_Layout );
	lay_sl_pad->setContentsMargins ( 0, 0, 0, 0 );

	// Slider widgets
	{
		// Pixmaps buffers
		const Qt::Alignment align_cc ( Qt::AlignHCenter | Qt::AlignVCenter );

		QString val ( "%1" );
		for ( unsigned int ii=0; ii < num_channels; ++ii ) {
			QLabel * wdg_lbl ( new QLabel );
			Wdg::DS_Slider * wdg_sl ( new Wdg::DS_Slider ( 0, &_slider_hub ) );

			Mixer_CTL_Proxy_Integer * mcpi (
				static_cast < Mixer_CTL_Proxy_Integer * > ( _proxies[ii] ) );

			// Label

			wdg_lbl->setText ( val.arg ( ii ) );
			wdg_lbl->setToolTip ( _ttip_grid_lbl_channel );
			wdg_lbl->setAlignment ( align_cc );


			// Slider

			//wdg_sl->set_pixmaps_buffers ( sl_buff );
			wdg_sl->set_wheel_degrees ( _wheel_degrees );
			if ( mixer_style() != 0 ) {
				wdg_sl->setPalette ( mixer_style()->palette_play() );
			}
			wdg_sl->set_maximum_index ( mcpi->integer_index_max() );
			wdg_sl->setEnabled ( is_enabled );
			wdg_sl->installEventFilter ( mcpi );

			connect ( mcpi, SIGNAL ( sig_integer_index_changed ( unsigned long ) ),
				wdg_sl, SLOT ( set_current_index ( unsigned long ) ) );

			connect ( wdg_sl, SIGNAL ( sig_current_index_changed ( unsigned long ) ),
				mcpi, SLOT ( set_integer_index ( unsigned long ) ) );

			lay_sl_pad->add_group_widget ( wdg_lbl, ii, 0, 0 );
			lay_sl_pad->add_group_widget ( wdg_sl, ii, 0, 1 );

			_input_widgets.append ( wdg_sl );
			if ( ii > 0 ) {
				_secondary_widgets.append ( wdg_lbl );
				_secondary_widgets.append ( wdg_sl );
			}
		}

	}

	QWidget * sliders_area = new QWidget;
	sliders_area->setLayout ( lay_sl_pad );


	//
	// Manual slider value input area
	//

	QHBoxLayout * lay_inputs ( new QHBoxLayout );
	lay_inputs->setContentsMargins ( 0, 0, 0, 0 );

	{
		QList < QWidget * > wdg_items;

		const Qt::Alignment align_cc ( Qt::AlignHCenter | Qt::AlignVCenter );

		for ( unsigned int ii=0; ii < num_channels; ++ii ) {

			Mixer_CTL_Proxy_Integer * mcpi (
				static_cast < Mixer_CTL_Proxy_Integer * > ( _proxies[ii] ) );

			Wdg::Covered_Spinbox * wdg_ipt ( new Wdg::Covered_Spinbox );

			wdg_ipt->set_range ( elem->integer_min(), elem->integer_max() );
			wdg_ipt->setEnabled ( is_enabled );
			wdg_ipt->spinbox()->installEventFilter ( mcpi );

			connect ( mcpi, SIGNAL ( sig_integer_value_changed ( int ) ),
				wdg_ipt, SLOT ( set_value ( int ) ) );

			connect ( wdg_ipt, SIGNAL ( sig_value_changed ( int ) ),
				mcpi, SLOT ( set_integer_value ( int ) ) );

			wdg_items.append ( wdg_ipt );

			_input_widgets.append ( wdg_ipt );
			if ( ii > 0 ) {
				_secondary_widgets.append ( wdg_ipt );
			}
		}

		QGridLayout * lay_grid ( create_channel_grid ( wdg_items, true ) );
		lay_inputs->addLayout ( lay_grid, 0 );
		lay_inputs->addStretch ( 1 );
	}

	// Extra buttons/labels layout
	QHBoxLayout * lay_extra ( new QHBoxLayout );
	lay_extra->setContentsMargins ( 0, 0, 0, 0 );

	// Range label
	{
		lay_extra->addLayout (
			create_range_label (
				elem->integer_min(),
				elem->integer_max() ) );
	}

	// Joined button
	if ( num_channels > 1 ) {
		QWidget * btn_joined ( create_joined_button ( is_enabled ) );
		lay_extra->addSpacing ( fontMetrics().averageCharWidth()*2 );
		lay_extra->addWidget ( btn_joined );
	}

	lay_extra->addStretch ( 1 );

	// Pad layout
	_editor_pad = new QWidget;
	{
		unsigned int vspace ( vspacer_height() );
		QVBoxLayout * lay_pad ( new QVBoxLayout );
		lay_pad->setContentsMargins ( 0, 0, 0, 0 );
		lay_pad->addWidget ( sliders_area, 1 );
		lay_pad->addSpacing ( vspace );
		lay_pad->addLayout ( lay_inputs, 0 );
		lay_pad->addLayout ( lay_extra, 0 );
		_editor_pad->setLayout ( lay_pad );
	}

	_pad_wdg.layout()->addWidget ( _editor_pad );

	update_enabled_state();
}


void
Mixer_CTL::setup_enumerated ( )
{
	Snd_Mixer_CTL_Elem * elem ( snd_elem_group()->elem ( elem_idx() ) );

	// Create proxies
	const unsigned int num_channels ( elem->count() );
	for ( unsigned int ii=0; ii < num_channels; ++ii ) {
		Mixer_CTL_Proxy_Enum * mcpe (
			new Mixer_CTL_Proxy_Enum );
		mcpe->set_snd_elem ( elem );
		mcpe->set_elem_idx ( ii );
		_proxies.append ( mcpe );
	}

	// Set joined on demand
	{
		bool joined ( true );
		unsigned int value = elem->enum_index ( 0 );
		for ( unsigned int ii=1; ii < num_channels; ++ii ) {
			if ( elem->enum_index ( ii ) != value ) {
				joined = false;
				break;
			}
		}
		for ( unsigned int ii=0; ii < num_channels; ++ii ) {
			_proxies[ii]->set_joined ( joined );
		}
	}

	const bool is_enabled (
		elem->is_writable() );


	//
	// ComboBox area
	//

	QHBoxLayout * lay_items ( new QHBoxLayout );
	lay_items->setContentsMargins ( 0, 0, 0, 0 );

	{
		QList < QWidget * > wdg_items;
		QStringList item_names;

		{
			unsigned int num_items ( elem->enum_num_items() );
			for ( unsigned int ii=0; ii < num_items; ++ii ) {
				item_names.append ( elem->enum_item_display_name ( ii ) );
			}
		}

		for ( unsigned int ii=0; ii < num_channels; ++ii ) {

			Mixer_CTL_Proxy_Enum * mcpe (
				static_cast < Mixer_CTL_Proxy_Enum * > ( _proxies[ii] ) );

			QComboBox * wdg_sel ( new QComboBox );

			wdg_sel->setEnabled ( is_enabled );
			wdg_sel->addItems ( item_names );
			wdg_sel->installEventFilter ( mcpe );

			connect ( mcpe, SIGNAL ( sig_enum_index_changed ( int ) ),
				wdg_sel, SLOT ( setCurrentIndex ( int ) ) );

			connect ( wdg_sel, SIGNAL ( currentIndexChanged ( int ) ),
				mcpe, SLOT ( set_enum_index ( int ) ) );

			wdg_items.append ( wdg_sel );

			_input_widgets.append ( wdg_sel );
			if ( ii > 0 ) {
				_secondary_widgets.append ( wdg_sel );
			}
		}

		QLayout * lay_grid ( create_channel_grid ( wdg_items, false ) );
		lay_items->addLayout ( lay_grid, 0 );
		lay_items->addStretch ( 1 );
	}

	// Extra buttons/labels layout
	QHBoxLayout * lay_extra ( new QHBoxLayout );
	lay_extra->setContentsMargins ( 0, 0, 0, 0 );

	// Joined button
	if ( num_channels > 1 ) {
		QWidget * btn_joined ( create_joined_button ( is_enabled ) );
		lay_extra->addWidget ( btn_joined );
	}

	lay_extra->addStretch ( 1 );


	// Pad layout
	_editor_pad = new QWidget;
	{
		QVBoxLayout * lay_pad ( new QVBoxLayout );
		lay_pad->setContentsMargins ( 0, 0, 0, 0 );
		lay_pad->addLayout ( lay_items, 0 );
		lay_pad->addSpacing ( fontMetrics().height() );
		lay_pad->addLayout ( lay_extra, 0 );
		lay_pad->addStretch ( 1 );
		_editor_pad->setLayout ( lay_pad );
	}

	_pad_wdg.layout()->addWidget ( _editor_pad );

	update_enabled_state();
}


void
Mixer_CTL::setup_boolean_grid ( )
{
	// Create proxies
	const unsigned int num_elems ( snd_elem_group()->num_elems() );
	for ( unsigned int eii=0; eii < num_elems; ++eii ) {
		Snd_Mixer_CTL_Elem * elem ( snd_elem_group()->elem ( eii ) );
		unsigned int num_channels ( elem->count() );
		for ( unsigned int ii=0; ii < num_channels; ++ii ) {
			Mixer_CTL_Proxy_Switch * mcps ( new Mixer_CTL_Proxy_Switch );
			mcps->set_snd_elem ( elem );
			mcps->set_elem_idx ( ii );
			_proxies.append ( mcps );
		}
	}

	QVBoxLayout * lay_switches_pad ( new QVBoxLayout );
	lay_switches_pad->setContentsMargins ( 0, 0, 0, 0 );

	{
		QLabel * lbl_elems ( create_grid_head_label ( num_elems ) );
		lay_switches_pad->addWidget ( lbl_elems );
	}

	{
		QGridLayout * lay_grid ( new QGridLayout );
		lay_grid->setContentsMargins ( 0, 0, 0, 0 );

		const Qt::Alignment align_rc ( Qt::AlignRight | Qt::AlignVCenter );
		const Qt::Alignment align_cc ( Qt::AlignHCenter | Qt::AlignVCenter );
		const QString val ( "%1" );

		unsigned int channel_count ( 0 );
		unsigned int proxy_idx ( 0 );
		unsigned int num_elems ( snd_elem_group()->num_elems() );
		for ( unsigned int eii=0; eii < num_elems; ++eii ) {
			Snd_Mixer_CTL_Elem * elem ( snd_elem_group()->elem ( eii ) );

			// Element index label
			{
				Wdg::Label_Width * wdg_lbl ( new Wdg::Label_Width );
				wdg_lbl->setAlignment ( align_cc );
				wdg_lbl->set_min_text ( "999" );
				wdg_lbl->setText ( val.arg ( elem->elem_index() ) );
				wdg_lbl->setToolTip ( _ttip_grid_lbl_elem );
				lay_grid->addWidget ( wdg_lbl, 0, 1 + eii, align_cc );
			}

			const bool is_enabled ( elem->is_writable() );
			const unsigned int num_channels ( elem->count() );
			if ( channel_count < num_channels ) {
				channel_count = num_channels;
			}
			for ( unsigned int ii=0; ii < num_channels; ++ii ) {

				Mixer_CTL_Proxy_Switch * mcps (
					static_cast < Mixer_CTL_Proxy_Switch * > (
						_proxies[proxy_idx] ) );

				// Switch widget

				Wdg::DS_Check_Button * wdg_sw (
					new Wdg::DS_Check_Button ( 0, &_check_button_hub ) );

				wdg_sw->installEventFilter ( mcps );
				wdg_sw->setEnabled ( is_enabled );
				wdg_sw->setToolTip ( _ttip_grid_widget.arg ( eii ).arg ( ii ) );

				connect ( mcps, SIGNAL ( sig_switch_state_changed ( bool ) ),
					wdg_sw, SLOT ( setChecked ( bool ) ) );

				connect ( wdg_sw, SIGNAL ( toggled ( bool ) ),
					mcps, SLOT ( set_switch_state ( bool ) ) );

				lay_grid->addWidget ( wdg_sw, 1 + ii, 1 + eii, align_cc );

				++proxy_idx;
			}
		}

		// Channel labels

		for ( unsigned int ii=0; ii < channel_count; ++ii ) {
			QLabel * wdg_lbl ( new QLabel );
			wdg_lbl->setAlignment ( align_rc );
			wdg_lbl->setText ( val.arg ( ii ) );
			wdg_lbl->setToolTip ( _ttip_grid_lbl_channel );
			lay_grid->addWidget ( wdg_lbl, 1 + ii, 0 );
		}

		// Wrap into stretching layout
		{
			QHBoxLayout * lay_switches_grid ( new QHBoxLayout );
			lay_switches_grid->setContentsMargins ( 0, 0, 0, 0 );
			lay_switches_grid->addLayout ( lay_grid );
			lay_switches_grid->addStretch ( 1 );

			lay_switches_pad->addLayout ( lay_switches_grid );
			lay_switches_pad->addStretch ( 1 );
		}
	}


	// Scroll area
	QScrollArea * scroll_area ( new QScrollArea );
	{
		QWidget * switches_pad ( new QWidget );
		switches_pad->setLayout ( lay_switches_pad );

		scroll_area->setFrameStyle ( QFrame::NoFrame );
		scroll_area->setWidget ( switches_pad );
	}


	// Pad layout
	_editor_pad = new QWidget;
	{
		QVBoxLayout * lay_pad ( new QVBoxLayout );
		lay_pad->setContentsMargins ( 0, 0, 0, 0 );
		lay_pad->addWidget ( scroll_area );
		_editor_pad->setLayout ( lay_pad );
	}

	_pad_wdg.layout()->addWidget ( _editor_pad );
}


void
Mixer_CTL::setup_integer_grid ( )
{
	// Create proxies
	const unsigned int num_elems ( snd_elem_group()->num_elems() );
	for ( unsigned int eii=0; eii < num_elems; ++eii ) {
		Snd_Mixer_CTL_Elem * elem ( snd_elem_group()->elem ( eii ) );
		unsigned int num_channels ( elem->count() );
		for ( unsigned int ii=0; ii < num_channels; ++ii ) {
			Mixer_CTL_Proxy_Integer * mcps ( new Mixer_CTL_Proxy_Integer );
			mcps->set_snd_elem ( elem );
			mcps->set_elem_idx ( ii );
			_proxies.append ( mcps );
		}
	}

	QVBoxLayout * lay_sliders_pad ( new QVBoxLayout );
	lay_sliders_pad->setContentsMargins ( 0, 0, 0, 0 );

	{
		QGridLayout * lay_grid ( new QGridLayout );
		lay_grid->setContentsMargins ( 0, 0, 0, 0 );

		const Qt::Alignment align_rc ( Qt::AlignRight | Qt::AlignVCenter );
		const Qt::Alignment align_cc ( Qt::AlignHCenter | Qt::AlignVCenter );
		const QString val ( "%1" );

		unsigned int channel_count ( 0 );
		unsigned int proxy_idx ( 0 );
		unsigned int num_elems ( snd_elem_group()->num_elems() );
		for ( unsigned int eii=0; eii < num_elems; ++eii ) {
			Snd_Mixer_CTL_Elem * elem ( snd_elem_group()->elem ( eii ) );

			// Element index label
			{
				QLabel * wdg_lbl ( new QLabel );
				wdg_lbl->setAlignment ( align_cc );
				wdg_lbl->setText ( val.arg ( elem->elem_index() ) );
				wdg_lbl->setToolTip ( _ttip_grid_lbl_elem );
				{
					QFont fnt ( wdg_lbl->font() );
					fnt.setBold ( true );
					wdg_lbl->setFont ( fnt );
				}
				lay_grid->addWidget ( wdg_lbl, 0, 1 + eii );
			}

			const bool is_enabled ( elem->is_writable() );
			const unsigned int num_channels ( elem->count() );
			if ( channel_count < num_channels ) {
				channel_count = num_channels;
			}
			for ( unsigned int ii=0; ii < num_channels; ++ii ) {

				Mixer_CTL_Proxy_Integer * mcpi (
					static_cast < Mixer_CTL_Proxy_Integer * > (
						_proxies[proxy_idx] ) );

				// Switch widget

				Wdg::Covered_Spinbox * wdg_ipt ( new Wdg::Covered_Spinbox );

				wdg_ipt->set_range ( elem->integer_min(), elem->integer_max() );
				wdg_ipt->setToolTip ( _ttip_grid_widget.arg ( eii ).arg ( ii ) );
				wdg_ipt->setEnabled ( is_enabled );
				wdg_ipt->spinbox()->installEventFilter ( mcpi );

				connect ( mcpi, SIGNAL ( sig_integer_value_changed ( int ) ),
					wdg_ipt, SLOT ( set_value ( int ) ) );

				connect ( wdg_ipt, SIGNAL ( sig_value_changed ( int ) ),
					mcpi, SLOT ( set_integer_value ( int ) ) );

				lay_grid->addWidget ( wdg_ipt, 1 + ii, 1 + eii );

				++proxy_idx;
			}
		}

		// Channel labels
		for ( unsigned int ii=0; ii < channel_count; ++ii ) {
			QLabel * wdg_lbl ( new QLabel );
			wdg_lbl->setAlignment ( align_rc );
			wdg_lbl->setText ( val.arg ( ii ) );
			wdg_lbl->setToolTip ( _ttip_grid_lbl_channel );
			{
				QFont fnt ( wdg_lbl->font() );
				fnt.setBold ( true );
				wdg_lbl->setFont ( fnt );
			}
			lay_grid->addWidget ( wdg_lbl, 1 + ii, 0 );
		}

		// Wrap into stretching layout
		{
			QHBoxLayout * lay_switches_grid ( new QHBoxLayout );
			lay_switches_grid->setContentsMargins ( 0, 0, 0, 0 );
			lay_switches_grid->addLayout ( lay_grid );
			lay_switches_grid->addStretch ( 1 );

			lay_sliders_pad->addLayout ( lay_switches_grid );
			lay_sliders_pad->addStretch ( 1 );
		}
	}


	// Scroll area
	QScrollArea * scroll_area ( new QScrollArea );
	{
		QWidget * sliders_pad ( new QWidget );
		sliders_pad->setLayout ( lay_sliders_pad );

		scroll_area->setFrameStyle ( QFrame::NoFrame );
		scroll_area->setWidget ( sliders_pad );
	}

	QHBoxLayout * lay_extra ( new QHBoxLayout );
	lay_extra->setContentsMargins ( 0, 0, 0, 0 );

	// Range label
	{
		Snd_Mixer_CTL_Elem * elem ( snd_elem_group()->elem ( 0 ) );
		lay_extra->addLayout (
			create_range_label (
				elem->integer_min(),
				elem->integer_max() ) );
	}
	lay_extra->addStretch ( 1 );


	// Pad layout
	_editor_pad = new QWidget;
	{
		QLabel * lbl_elems ( create_grid_head_label ( num_elems ) );

		QVBoxLayout * lay_pad ( new QVBoxLayout );
		lay_pad->setContentsMargins ( 0, 0, 0, 0 );
		lay_pad->addWidget ( lbl_elems );
		lay_pad->addWidget ( scroll_area );
		lay_pad->addLayout ( lay_extra );
		_editor_pad->setLayout ( lay_pad );
	}

	_pad_wdg.layout()->addWidget ( _editor_pad );
}


void
Mixer_CTL::setup_enumerated_grid ( )
{
	// Create proxies
	const unsigned int num_elems ( snd_elem_group()->num_elems() );
	for ( unsigned int eii=0; eii < num_elems; ++eii ) {
		Snd_Mixer_CTL_Elem * elem ( snd_elem_group()->elem ( eii ) );
		unsigned int num_channels ( elem->count() );
		for ( unsigned int ii=0; ii < num_channels; ++ii ) {
			Mixer_CTL_Proxy_Enum * mcpe ( new Mixer_CTL_Proxy_Enum );
			mcpe->set_snd_elem ( elem );
			mcpe->set_elem_idx ( ii );
			_proxies.append ( mcpe );
		}
	}

	QVBoxLayout * lay_enums_pad ( new QVBoxLayout );
	lay_enums_pad->setContentsMargins ( 0, 0, 0, 0 );

	{
		QLabel * lbl_elems ( create_grid_head_label ( num_elems ) );
		lay_enums_pad->addWidget ( lbl_elems );
	}

	{
		QGridLayout * lay_grid ( new QGridLayout );
		lay_grid->setContentsMargins ( 0, 0, 0, 0 );

		QStringList item_names;

		const Qt::Alignment align_rc ( Qt::AlignRight | Qt::AlignVCenter );
		const Qt::Alignment align_cc ( Qt::AlignHCenter | Qt::AlignVCenter );
		const QString val ( "%1" );

		unsigned int channel_count ( 0 );
		unsigned int proxy_idx ( 0 );
		unsigned int num_elems ( snd_elem_group()->num_elems() );
		for ( unsigned int eii=0; eii < num_elems; ++eii ) {
			Snd_Mixer_CTL_Elem * elem ( snd_elem_group()->elem ( eii ) );

			// Acquire item names
			{
				item_names.clear();
				unsigned int num_items ( elem->enum_num_items() );
				for ( unsigned int ii=0; ii < num_items; ++ii ) {
					item_names.append ( elem->enum_item_display_name ( ii ) );
				}
			}

			// Element index label
			{
				Wdg::Label_Width * wdg_lbl ( new Wdg::Label_Width );
				wdg_lbl->setAlignment ( align_cc );
				wdg_lbl->set_min_text ( "999" );
				wdg_lbl->setText ( val.arg ( elem->elem_index() ) );
				wdg_lbl->setToolTip ( _ttip_grid_lbl_elem );
				lay_grid->addWidget ( wdg_lbl, 0, 1 + eii, align_cc );
			}

			const bool is_enabled ( elem->is_writable() );
			const unsigned int num_channels ( elem->count() );
			if ( channel_count < num_channels ) {
				channel_count = num_channels;
			}
			for ( unsigned int ii=0; ii < num_channels; ++ii ) {

				Mixer_CTL_Proxy_Enum * mcpe (
					static_cast < Mixer_CTL_Proxy_Enum * > (
						_proxies[proxy_idx] ) );

				// Selection widget

				QComboBox * wdg_sel ( new QComboBox );
				wdg_sel->setEnabled ( is_enabled );
				wdg_sel->setToolTip ( _ttip_grid_widget.arg ( eii ).arg ( ii ) );
				wdg_sel->installEventFilter ( mcpe );
				wdg_sel->addItems ( item_names );

				connect ( mcpe, SIGNAL ( sig_enum_index_changed ( int ) ),
					wdg_sel, SLOT ( setCurrentIndex ( int ) ) );

				connect ( wdg_sel, SIGNAL ( currentIndexChanged ( int ) ),
					mcpe, SLOT ( set_enum_index ( int ) ) );

				lay_grid->addWidget ( wdg_sel, 1 + ii, 1 + eii, align_cc );

				++proxy_idx;
			}
		}

		// Channel labels

		for ( unsigned int ii=0; ii < channel_count; ++ii ) {
			QLabel * wdg_lbl ( new QLabel );
			wdg_lbl->setAlignment ( align_rc );
			wdg_lbl->setText ( val.arg ( ii ) );
			wdg_lbl->setToolTip ( _ttip_grid_lbl_channel );
			lay_grid->addWidget ( wdg_lbl, 1 + ii, 0 );
		}

		// Wrap into stretching layout
		{
			QHBoxLayout * lay_enums_grid ( new QHBoxLayout );
			lay_enums_grid->setContentsMargins ( 0, 0, 0, 0 );
			lay_enums_grid->addLayout ( lay_grid );
			lay_enums_grid->addStretch ( 1 );

			lay_enums_pad->addLayout ( lay_enums_grid );
			lay_enums_pad->addStretch ( 1 );
		}
	}


	// Scroll area
	QScrollArea * scroll_area ( new QScrollArea );
	{
		QWidget * enums_pad ( new QWidget );
		enums_pad->setLayout ( lay_enums_pad );

		scroll_area->setFrameStyle ( QFrame::NoFrame );
		scroll_area->setWidget ( enums_pad );
	}


	// Pad layout
	_editor_pad = new QWidget;
	{
		QVBoxLayout * lay_pad ( new QVBoxLayout );
		lay_pad->setContentsMargins ( 0, 0, 0, 0 );
		lay_pad->addWidget ( scroll_area );
		_editor_pad->setLayout ( lay_pad );
	}

	_pad_wdg.layout()->addWidget ( _editor_pad );
}


void
Mixer_CTL::setup_unsupported ( )
{
	// Pad layout
	_editor_pad = new QWidget;
	{
		QLabel * lbl_head ( new QLabel );
		{
			Snd_Mixer_CTL_Elem * elem ( snd_elem_group()->elem ( 0 ) );

			QString val (  elem->elem_type_display_name() );
			val = _str_unsupported.arg ( val );
			val = QString ( "<h4>%1</h4>" ).arg ( val );
			lbl_head->setText ( val );
		}

		unsigned int vspace ( vspacer_height() );
		QVBoxLayout * lay_pad ( new QVBoxLayout );
		lay_pad->setContentsMargins ( 0, 0, 0, 0 );
		lay_pad->addSpacing ( vspace );
		lay_pad->addWidget ( lbl_head, 0 );
		lay_pad->addSpacing ( vspace );
		lay_pad->addStretch ( 1 );

		_editor_pad->setLayout ( lay_pad );
	}

	_pad_wdg.layout()->addWidget ( _editor_pad );
}



unsigned int
Mixer_CTL::vspacer_height ( ) const
{
	return qMax ( 0, fontMetrics().height() / 2 );
}


QLayout *
Mixer_CTL::create_range_label (
	int min_n,
	int max_n )
{
	QHBoxLayout * lay_res ( new QHBoxLayout );
	lay_res->setContentsMargins ( 0, 0, 0, 0 );

	QLabel * lbl_range ( new QLabel );
	Wdg::Label_Width * lbl_range_val ( new Wdg::Label_Width );
	lbl_range->setText ( _str_range );

	{
		const QString range_mask ( tr ( "%1 - %2" ) );
		QString valc ( range_mask );
		valc = valc.arg ( min_n );
		valc = valc.arg ( max_n );

		lbl_range_val->set_min_text ( range_mask.arg ( 0 ).arg ( 99999 ) );
		lbl_range_val->setText ( valc );
	}

	lay_res->addWidget ( lbl_range );
	lay_res->addWidget ( lbl_range_val );

	return lay_res;
}


QWidget *
Mixer_CTL::create_joined_button (
	bool enabled_n )
{
	QCheckBox * btn_joined ( new QCheckBox );

	btn_joined->setEnabled ( enabled_n );
	btn_joined->setText ( tr ( "Joined changing" ) );
	if ( _proxies.size() > 0 ) {
		btn_joined->setChecked ( _proxies[0]->is_joined() );
	}

	connect ( btn_joined, SIGNAL ( toggled ( bool ) ),
		this, SLOT ( joined_checked ( bool ) ) );

	return btn_joined;
}


QLabel *
Mixer_CTL::create_grid_head_label (
	unsigned int num_n )
{
	QLabel * lbl_elems ( new QLabel );
	lbl_elems->setText ( tr ( "%n elements", "", num_n ) );
	return lbl_elems;
}


QGridLayout *
Mixer_CTL::create_channel_grid (
	const QList < QWidget * > & items_n,
	bool bold_labels_n )
{
	QGridLayout * lay_grid ( new QGridLayout() );
	lay_grid->setContentsMargins ( 0, 0, 0, 0 );

	if ( items_n.size() == 0 ) {
		return lay_grid;
	}

	const Qt::Alignment align_cc ( Qt::AlignHCenter | Qt::AlignVCenter );

	unsigned int num ( items_n.size() );
	unsigned int lay_cols ( 1 );
	for ( unsigned int ii=1; ii <= num; ii*=2 ) {
		if ( ii*ii >= num ) {
			lay_cols = ii;
			break;
		}
	}

	const QString val ( "%1" );
	for ( unsigned int ii=0; ii < num; ++ii ) {
		QWidget * wdg_input ( items_n[ii] );

		Wdg::Label_Width * wdg_label ( new Wdg::Label_Width );
		wdg_label->set_min_text ( val.arg ( 999 ) );
		wdg_label->setText ( val.arg ( ii ) );
		wdg_label->setToolTip ( _ttip_grid_lbl_channel );
		wdg_label->setAlignment ( align_cc );
		wdg_label->setEnabled ( wdg_input->isEnabled() );

		if ( bold_labels_n ) {
			QFont fnt ( wdg_label->font() );
			fnt.setBold ( true );
			wdg_label->setFont ( fnt );
		}

		_input_widgets.append ( wdg_input );
		if ( ii > 0 ) {
			_secondary_widgets.append ( wdg_label );
		}

		unsigned int lay_row = ( ii / lay_cols );
		unsigned int lay_col = ( ii % lay_cols );
		lay_grid->addWidget ( wdg_label, lay_row * 2, lay_col, align_cc );
		lay_grid->addWidget ( wdg_input, lay_row * 2 + 1, lay_col, align_cc );
	}

	return lay_grid;
}


void
Mixer_CTL::set_all_switches_checked (
	bool checked_n )
{
	if ( snd_elem_group() != 0 ) {
		if ( elem_idx() < snd_elem_group()->num_elems() ) {
			snd_elem_group()->elem ( elem_idx() )->set_switch_all ( checked_n );
		}
	}
}


void
Mixer_CTL::check_all_switches ( )
{
	set_all_switches_checked ( true );
}


void
Mixer_CTL::uncheck_all_switches ( )
{
	set_all_switches_checked ( false );
}


void
Mixer_CTL::invert_all_switches ( )
{
	if ( snd_elem_group() != 0 ) {
		if ( elem_idx() < snd_elem_group()->num_elems() ) {
			snd_elem_group()->elem ( elem_idx() )->invert_switch_all();
		}
	}
}


void
Mixer_CTL::joined_checked (
	bool checked_n )
{
	for ( int ii=0; ii < _proxies.size(); ++ii ) {
		_proxies[ii]->set_joined ( checked_n );
	}

	update_enabled_state();

	if ( checked_n && ( snd_elem_group() != 0 )  ) {
		if ( elem_idx() < snd_elem_group()->num_elems() ) {
			snd_elem_group()->elem ( elem_idx() )->level_values();
		}
	}
}


void
Mixer_CTL::update_enabled_state ( )
{
	if ( snd_elem_group() == 0 ) {
		return;
	}
	if ( elem_idx() >= snd_elem_group()->num_elems() ) {
		return;
	}

	const Snd_Mixer_CTL_Elem * elem ( snd_elem_group()->elem ( elem_idx() ) );

	const bool enabled ( !_proxies[0]->is_joined() && elem->is_writable() );
	const unsigned int num_wdg ( _secondary_widgets.size() );
	for ( unsigned int ii=0; ii < num_wdg; ++ii ) {
		_secondary_widgets[ii]->setEnabled ( enabled );
	}
}


void
Mixer_CTL::update_info ( )
{
	QFont name_fnt ( font() );
	QString name_val;
	QString name_ttip;

	_info_wdg_1.setEnabled ( ( snd_elem_group() != 0 ) );
	_info_wdg_2.setEnabled ( ( snd_elem_group() != 0 ) );

	// Clear
	{
		QString clear_txt ( "" );
		_info_lbl_name.setText ( clear_txt );
		_info_lbl_index.value.setText ( clear_txt );
		_info_lbl_dev.value.setText ( clear_txt );

		_info_lbl_readable.value.setText ( clear_txt );
		_info_lbl_writable.value.setText ( clear_txt );
		_info_lbl_volatile.value.setText ( clear_txt );
		_info_lbl_active.value.setText ( clear_txt );

		_info_lbl_count.value.setText ( clear_txt );
		_info_lbl_numid.value.setText ( clear_txt );
	}

	if ( snd_elem_group() == 0 ) {

		name_fnt.setItalic ( true );
		name_val = tr ( "No element selected" );

	} else {

		const Snd_Mixer_CTL_Elem * elem_first ( snd_elem_group()->elem ( 0 ) );

		const QString dname ( elem_first->display_name() );

		name_val = "<h3>%1</h3>";
		name_val = name_val.arg ( dname );

		// Tooltip
		name_ttip = _ttip_name_lbl_mask.arg ( elem_first->elem_name() );

		if ( elem_idx() < snd_elem_group()->num_elems() ) {

			const QString val ( "%1" );
			QString valc;

			Snd_Mixer_CTL_Elem * elem ( snd_elem_group()->elem ( elem_idx() ) );

			valc = val.arg ( elem->elem_index() );
			_info_lbl_index.value.setText ( valc );

			valc = _info_dev_mask;
			valc = valc.arg ( elem->device() );
			valc = valc.arg ( elem->subdevice() );
			_info_lbl_dev.value.setText ( valc );


			valc = elem->is_readable() ? _str_bool_yes : _str_bool_no;
			_info_lbl_readable.value.setText ( valc );

			valc = elem->is_writable() ? _str_bool_yes : _str_bool_no;
			_info_lbl_writable.value.setText ( valc );

			valc = elem->is_volatile() ? _str_bool_yes : _str_bool_no;
			_info_lbl_volatile.value.setText ( valc );

			valc = elem->is_active() ? _str_bool_yes : _str_bool_no;
			_info_lbl_active.value.setText ( valc );

			valc = val.arg ( elem->count(), 3 );
			_info_lbl_count.value.setText ( valc );

			valc = val.arg ( elem->elem_numid(), 3 );
			_info_lbl_numid.value.setText ( valc );

		}

	}

	_info_lbl_name.setText ( name_val );
	_info_lbl_name.setToolTip ( name_ttip );
	_info_lbl_name.setFont ( name_fnt );
}


} // End of namespace
