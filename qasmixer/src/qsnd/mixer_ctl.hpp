//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_mixer_ctl_hpp__
#define __INC_mixer_ctl_hpp__

#include <QString>
#include <QAction>
#include <QMenu>
#include <QTimer>
#include <QFrame>
#include <QLabel>
#include <QGridLayout>
#include <utility>

#include "wdg/label_width.hpp"
#include "wdg/scroll_area_horizontal.hpp"
#include "wdg/shared_pixmaps_set_hub.hpp"
#include "qsnd/snd_mixer_ctl_elem.hpp"
#include "qsnd/mixer_style.hpp"
#include "qsnd/mixer_ctl_proxy.hpp"


namespace QSnd
{


///
/// @brief Mixer_CTL
///
class Mixer_CTL :
	public QWidget
{
	Q_OBJECT

	// Public typedefs
	public:

	struct Label_Pair {
		Wdg::Label_Width name;
		Wdg::Label_Width value;
	};


	// Public methods
	public:

	Mixer_CTL (
		QWidget * parent_n = 0 );

	~Mixer_CTL ( );


	// Element

	Snd_Mixer_CTL_Elem_Group *
	snd_elem_group ( ) const;

	void
	set_snd_elem_group (
		Snd_Mixer_CTL_Elem_Group * elem_group_n,
		unsigned int index_n = 0 );

	unsigned int
	elem_idx ( ) const;


	// Pixmaps hub

	Mixer_Style *
	mixer_style ( ) const;

	void
	set_mixer_style (
		Mixer_Style * mstyle_n );


	// Wheel degrees

	unsigned int
	wheel_degrees ( ) const;

	void
	set_wheel_degrees (
		unsigned int degrees_n );


	// Protected slots
	protected slots:

	void
	update_proxies_values ( );

	void
	set_all_switches_checked (
		bool checked_n );

	void
	check_all_switches ( );

	void
	uncheck_all_switches ( );

	void
	invert_all_switches ( );


	void
	joined_checked (
		bool checked_n );


	// Protected methods
	protected:

	void
	clear ( );

	void
	update_info ( );

	unsigned int
	vspacer_height ( ) const;


	void
	setup_widgets ( );

	void
	setup_boolean ( );

	void
	setup_integer ( );

	void
	setup_enumerated ( );

	void
	setup_boolean_grid ( );

	void
	setup_integer_grid ( );

	void
	setup_enumerated_grid ( );

	void
	setup_unsupported ( );



	// Widget / Layout creators

	QLayout *
	create_range_label (
		int min_n,
		int max_n );

	QWidget *
	create_joined_button (
		bool enabled_n );

	QLabel *
	create_grid_head_label (
		unsigned int num_n );

	QGridLayout *
	create_channel_grid (
		const QList < QWidget * > & items_n,
		bool bold_labels_n = false );


	void
	update_enabled_state ( );


	// Private attributes
	private:

	Mixer_Style * _mixer_style;


	// Group and element selection

	Snd_Mixer_CTL_Elem_Group * _snd_elem_group;

	unsigned int _elem_idx;


	QList < Mixer_CTL_Proxy * > _proxies;

	QList < QWidget * > _input_widgets;
	QList < QWidget * > _secondary_widgets;


	unsigned int _wheel_degrees;


	// Edit pad widget

	QWidget _pad_wdg;

	// Info widget

	QFrame _info_wdg_1;
	QFrame _info_wdg_2;

	QWidget * _editor_pad;

	QLabel _info_lbl_name;

	Label_Pair _info_lbl_index;
	Label_Pair _info_lbl_dev;

	Label_Pair _info_lbl_active;
	Label_Pair _info_lbl_volatile;
	Label_Pair _info_lbl_readable;
	Label_Pair _info_lbl_writable;

	Label_Pair _info_lbl_count;
	Label_Pair _info_lbl_numid;

	QString _info_dev_mask;
	QString _str_bool_yes;
	QString _str_bool_no;
	QString _str_range;
	QString _str_unsupported;

	QString _ttip_name_lbl_mask;

	QString _ttip_grid_lbl_elem;
	QString _ttip_grid_lbl_channel;
	QString _ttip_grid_widget;


	Wdg::Shared_Pixmaps_Set_Hub _slider_hub;
	Wdg::Shared_Pixmaps_Set_Hub _check_button_hub;
};


inline
Mixer_Style *
Mixer_CTL::mixer_style ( ) const
{
	return _mixer_style;
}


inline
Snd_Mixer_CTL_Elem_Group *
Mixer_CTL::snd_elem_group ( ) const
{
	return _snd_elem_group;
}


inline
unsigned int
Mixer_CTL::elem_idx ( ) const
{
	return _elem_idx;
}


inline
unsigned int
Mixer_CTL::wheel_degrees ( ) const
{
	return _wheel_degrees;
}


} // End of namespace


#endif
