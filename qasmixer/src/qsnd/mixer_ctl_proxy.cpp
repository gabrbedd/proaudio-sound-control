//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "mixer_ctl_proxy.hpp"

#include <QApplication>
#include <QFocusEvent>


namespace QSnd
{


//
// Mixer_CTL_Proxy
//
Mixer_CTL_Proxy::Mixer_CTL_Proxy ( ) :
_snd_elem ( 0 ),
_elem_idx ( 0 ),
_is_joined ( false ),
_has_focus ( false )
{
}


Mixer_CTL_Proxy::~Mixer_CTL_Proxy ( )
{
}


void
Mixer_CTL_Proxy::set_snd_elem (
	Snd_Mixer_CTL_Elem * elem_n )
{
	if ( _snd_elem != 0 ) {
		disconnect ( _snd_elem, 0, this, 0 );
	}

	_snd_elem = elem_n;

	if ( _snd_elem != 0 ) {
		connect ( _snd_elem, SIGNAL ( sig_values_changed() ),
			this, SLOT ( update_value() ) );
	}
}


void
Mixer_CTL_Proxy::set_elem_idx (
	unsigned int idx_n )
{
	_elem_idx = idx_n;
}


void
Mixer_CTL_Proxy::set_joined (
	bool flag_n )
{
	_is_joined = flag_n;
}


void
Mixer_CTL_Proxy::update_value ( )
{
	// Dummy implementation
}


bool
Mixer_CTL_Proxy::joined_by_key ( ) const
{
	bool res ( true );
	if ( ( QApplication::keyboardModifiers() & Qt::ControlModifier ) == 0 ) {
		res = false;
	}
	res = ( res && has_focus() );
	return res;
}


bool
Mixer_CTL_Proxy::eventFilter (
	QObject * obj_n,
	QEvent * event_n )
{
	bool res ( QObject::eventFilter ( obj_n, event_n ) );

	if ( !res ) {
		QFocusEvent * fev ( dynamic_cast < QFocusEvent * > ( event_n ) );
		if ( fev != 0 ) {
			_has_focus = fev->gotFocus();
		}
	}

	return res;
}


} // End of namespace
