//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_mixer_ctl_proxy_hpp__
#define __INC_mixer_ctl_proxy_hpp__

#include <QObject>
#include "snd_mixer_ctl_elem.hpp"


namespace QSnd
{


///
/// @brief Mixer_CTL_Proxy
///
class Mixer_CTL_Proxy :
	public QObject
{
	Q_OBJECT


	// Public methods
	public:

	Mixer_CTL_Proxy ( );

	virtual
	~Mixer_CTL_Proxy ( );


	// Snd element

	Snd_Mixer_CTL_Elem *
	snd_elem ( ) const;

	void
	set_snd_elem (
		Snd_Mixer_CTL_Elem * elem_n );


	// Snd element index

	unsigned int
	elem_idx ( ) const;

	void
	set_elem_idx (
		unsigned int idx_n );


	// Joined

	bool
	is_joined ( ) const;

	void
	set_joined (
		bool flag_n );

	bool
	joined_by_key ( ) const;


	// Focus

	bool
	has_focus ( ) const;



	// Public slots
	public:

	virtual
	void
	update_value ( );


	// Protected methods
	protected:

	bool
	eventFilter (
		QObject * obj_n,
		QEvent * event_n );


	// Private attributes
	private:

	Snd_Mixer_CTL_Elem * _snd_elem;

	unsigned int _elem_idx;

	bool _is_joined;

	bool _has_focus;
};


inline
Snd_Mixer_CTL_Elem *
Mixer_CTL_Proxy::snd_elem ( ) const
{
	return _snd_elem;
}


inline
unsigned int
Mixer_CTL_Proxy::elem_idx ( ) const
{
	return _elem_idx;
}



inline
bool
Mixer_CTL_Proxy::is_joined ( ) const
{
	return _is_joined;
}


inline
bool
Mixer_CTL_Proxy::has_focus ( ) const
{
	return _has_focus;
}


} // End of namespace


#endif
