//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "mixer_ctl_proxy_enum.hpp"

#include <iostream>


namespace QSnd
{


//
// Mixer_CTL_Proxy_Enum
//
Mixer_CTL_Proxy_Enum::Mixer_CTL_Proxy_Enum ( ) :
_enum_index ( 0 ),
_updating_state ( false )
{
}


Mixer_CTL_Proxy_Enum::~Mixer_CTL_Proxy_Enum ( )
{
}


void
Mixer_CTL_Proxy_Enum::set_enum_index (
	unsigned int index_n )
{
	if ( enum_index() != index_n ) {
		_enum_index = index_n;
		this->enum_index_changed();
		emit sig_enum_index_changed ( enum_index() );
		emit sig_enum_index_changed ( static_cast < int > ( enum_index() ) );
	}
}

void
Mixer_CTL_Proxy_Enum::set_enum_index (
	int index_n )
{
	if ( index_n >= 0 ) {
		set_enum_index ( static_cast < unsigned int > ( index_n ) );
	}
}


void
Mixer_CTL_Proxy_Enum::enum_index_changed ( )
{
	if ( ( snd_elem() != 0 ) && !_updating_state ) {
		if ( is_joined() || joined_by_key() ) {
			snd_elem()->set_enum_index_all ( enum_index() );
		} else {
			snd_elem()->set_enum_index ( elem_idx(), enum_index() );
		}
	}
}


void
Mixer_CTL_Proxy_Enum::update_value ( )
{
	if ( ( snd_elem() != 0 ) && !_updating_state ) {
		_updating_state = true;

		set_enum_index ( snd_elem()->enum_index ( elem_idx() ) );

		_updating_state = false;
	}
}


} // End of namespace
