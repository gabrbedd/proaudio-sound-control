//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_mixer_ctl_proxy_enum_hpp__
#define __INC_mixer_ctl_proxy_enum_hpp__

#include <QObject>
#include "mixer_ctl_proxy.hpp"


namespace QSnd
{


///
/// @brief Mixer_CTL_Proxy_Enum
///
class Mixer_CTL_Proxy_Enum :
	public Mixer_CTL_Proxy
{
	Q_OBJECT

	// Public methods
	public:

	Mixer_CTL_Proxy_Enum ( );

	~Mixer_CTL_Proxy_Enum ( );


	unsigned int
	enum_index ( ) const;

	unsigned int
	enum_num_items ( ) const;

	const char *
	enum_item_name (
		unsigned int index_n );


	// Signals
	signals:

	void
	sig_enum_index_changed (
		unsigned int index_n );

	void
	sig_enum_index_changed (
		int index_n );


	// Public slots
	public slots:

	void
	set_enum_index (
		unsigned int index_n );

	void
	set_enum_index (
		int index_n );

	void
	update_value ( );


	// Protected methods
	protected:

	void
	enum_index_changed ( );


	// Private attributes
	private:

	unsigned int _enum_index;

	bool _updating_state;
};


inline
unsigned int
Mixer_CTL_Proxy_Enum::enum_index ( ) const
{
	return _enum_index;
}


inline
unsigned int
Mixer_CTL_Proxy_Enum::enum_num_items ( ) const
{
	return snd_elem()->enum_num_items();
}


inline
const char *
Mixer_CTL_Proxy_Enum::enum_item_name (
	unsigned int index_n )
{
	return snd_elem()->enum_item_name ( index_n );
}


} // End of namespace


#endif
