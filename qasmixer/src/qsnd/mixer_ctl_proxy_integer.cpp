//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "mixer_ctl_proxy_integer.hpp"

#include <iostream>


namespace QSnd
{


//
// Mixer_CTL_Proxy_Integer
//
Mixer_CTL_Proxy_Integer::Mixer_CTL_Proxy_Integer ( ) :
_integer_value ( 0 ),
_integer_index ( 0 ),
_updating_state ( false )
{
}


Mixer_CTL_Proxy_Integer::~Mixer_CTL_Proxy_Integer ( )
{
}


void
Mixer_CTL_Proxy_Integer::set_integer_value (
	long value_n )
{
	if ( integer_value() != value_n ) {
		_integer_value = value_n;
		this->integer_value_changed();
		emit sig_integer_value_changed ( integer_value() );
		emit sig_integer_value_changed ( int ( integer_value() ) );
	}
}


void
Mixer_CTL_Proxy_Integer::set_integer_value (
	int value_n )
{
	set_integer_value ( long ( value_n ) );
}


void
Mixer_CTL_Proxy_Integer::set_integer_index (
	unsigned long idx_n )
{
	if ( integer_index() != idx_n ) {
		_integer_index = idx_n;
		this->integer_index_changed();
		emit sig_integer_index_changed ( integer_index() );
	}
}


void
Mixer_CTL_Proxy_Integer::integer_value_changed ( )
{
	//std::cout << "Mixer_CTL_Proxy_Integer::integer_value_changed " << integer_value() << "\n";

	if ( snd_elem() != 0 ) {
		unsigned long idx (
			Wdg::integer_distance ( integer_min(), integer_value() ) );
		set_integer_index ( idx );
	}

	if ( ( snd_elem() != 0 ) && !_updating_state ) {
		if ( is_joined() || joined_by_key() ) {
			snd_elem()->set_integer_all ( integer_value() );
		} else {
			snd_elem()->set_integer ( elem_idx(), integer_value() );
		}
	}
}


void
Mixer_CTL_Proxy_Integer::integer_index_changed ( )
{
	//std::cout << "Mixer_CTL_Proxy_Integer::integer_index_changed " << integer_index() << "\n";

	if ( snd_elem() != 0 ) {
		long int_val ( integer_min() );
		int_val += integer_index();
		set_integer_value ( int_val );
	}
}


void
Mixer_CTL_Proxy_Integer::update_value ( )
{
	if ( ( snd_elem() != 0 ) && !_updating_state ) {
		_updating_state = true;

		set_integer_value ( snd_elem()->integer_value ( elem_idx() ) );

		_updating_state = false;
	}
}


} // End of namespace
