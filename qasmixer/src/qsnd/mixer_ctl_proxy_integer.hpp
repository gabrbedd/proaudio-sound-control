//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_mixer_ctl_proxy_integer_hpp__
#define __INC_mixer_ctl_proxy_integer_hpp__

#include <QObject>
#include "mixer_ctl_proxy.hpp"
#include "wdg/uint_mapper.hpp"


namespace QSnd
{


///
/// @brief Mixer_CTL_Proxy_Integer
///
class Mixer_CTL_Proxy_Integer :
	public Mixer_CTL_Proxy
{
	Q_OBJECT

	// Public methods
	public:

	Mixer_CTL_Proxy_Integer ( );

	~Mixer_CTL_Proxy_Integer ( );


	long
	integer_min ( ) const;

	long
	integer_max ( ) const;

	long
	integer_value ( ) const;


	unsigned long
	integer_index ( );

	unsigned long
	integer_index_max ( );


	// Signals
	signals:

	void
	sig_integer_value_changed (
		long value_n );

	void
	sig_integer_value_changed (
		int value_n );

	void
	sig_integer_index_changed (
		unsigned long value_n );


	// Public slots
	public slots:

	void
	set_integer_value (
		long value_n );

	void
	set_integer_value (
		int value_n );

	void
	set_integer_index (
		unsigned long idx_n );


	void
	update_value ( );


	// Protected methods
	protected:

	void
	integer_value_changed ( );

	void
	integer_index_changed ( );


	// Private attributes
	private:

	long _integer_value;

	unsigned long _integer_index;

	bool _updating_state;
};


inline
long
Mixer_CTL_Proxy_Integer::integer_min ( ) const
{
	return snd_elem()->integer_min();
}


inline
long
Mixer_CTL_Proxy_Integer::integer_max ( ) const
{
	return snd_elem()->integer_max();
}


inline
long
Mixer_CTL_Proxy_Integer::integer_value ( ) const
{
	return _integer_value;
}


inline
unsigned long
Mixer_CTL_Proxy_Integer::integer_index ( )
{
	return _integer_index;
}


inline
unsigned long
Mixer_CTL_Proxy_Integer::integer_index_max ( )
{
	return Wdg::integer_distance ( integer_min(), integer_max() );
}


} // End of namespace


#endif
