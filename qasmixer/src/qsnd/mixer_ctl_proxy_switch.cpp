//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "mixer_ctl_proxy_switch.hpp"

#include <iostream>


namespace QSnd
{


//
// Mixer_CTL_Proxy_Switch
//
Mixer_CTL_Proxy_Switch::Mixer_CTL_Proxy_Switch ( ) :
_switch_state ( false ),
_updating_state ( false )
{
}


Mixer_CTL_Proxy_Switch::~Mixer_CTL_Proxy_Switch ( )
{
}


void
Mixer_CTL_Proxy_Switch::set_switch_state (
	bool state_n )
{
	if ( switch_state() != state_n ) {
		_switch_state = state_n;
		this->switch_state_changed();
		emit sig_switch_state_changed ( switch_state() );
	}
}


void
Mixer_CTL_Proxy_Switch::switch_state_changed ( )
{
	if ( ( snd_elem() != 0 ) && !_updating_state ) {
		if ( is_joined() || joined_by_key() ) {
			snd_elem()->set_switch_all ( switch_state() );
		} else {
			snd_elem()->set_switch_state ( elem_idx(), switch_state() );
		}
	}
}


void
Mixer_CTL_Proxy_Switch::update_value ( )
{
	if ( ( snd_elem() != 0 ) && !_updating_state ) {
		_updating_state = true;

		set_switch_state ( snd_elem()->switch_state ( elem_idx() ) );

		_updating_state = false;
	}
}


} // End of namespace
