//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_mixer_ctl_proxy_switch_hpp__
#define __INC_mixer_ctl_proxy_switch_hpp__

#include <QObject>
#include "mixer_ctl_proxy.hpp"


namespace QSnd
{


///
/// @brief Mixer_CTL_Proxy_Switch
///
class Mixer_CTL_Proxy_Switch :
	public Mixer_CTL_Proxy
{
	Q_OBJECT

	// Public methods
	public:

	Mixer_CTL_Proxy_Switch ( );

	~Mixer_CTL_Proxy_Switch ( );


	bool
	switch_state ( ) const;


	// Signals
	signals:

	void
	sig_switch_state_changed (
		bool state_n );


	// Public slots
	public slots:

	void
	set_switch_state (
		bool state_n );

	void
	update_value ( );


	// Protected methods
	protected:

	void
	switch_state_changed ( );


	// Private attributes
	private:

	bool _switch_state;

	bool _updating_state;
};


inline
bool
Mixer_CTL_Proxy_Switch::switch_state ( ) const
{
	return _switch_state;
}


} // End of namespace


#endif
