//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_mixer_ctl_table_model_hpp__
#define __INC_mixer_ctl_table_model_hpp__

#include <QAbstractTableModel>
#include <QList>
#include <QStringList>

#include "snd_mixer_ctl.hpp"


namespace QSnd
{


///
/// @brief Mixer_CTL_Table_Model
///
class Mixer_CTL_Table_Model :
	public QAbstractTableModel
{
	Q_OBJECT;

	// Public methods
	public:

	Mixer_CTL_Table_Model ( );

	~Mixer_CTL_Table_Model ( );


	// Mixer class

	Snd_Mixer_CTL *
	snd_mixer ( ) const;

	void
	set_snd_mixer (
		Snd_Mixer_CTL * snd_mixer_n );


	// Interface type index

	unsigned int
	iface_type_idx ( ) const;

	void
	set_iface_type_idx (
		unsigned int type_n );


	Snd_Mixer_CTL_Elem *
	elem (
		const QModelIndex & idx_n ) const;



	int
	rowCount (
		const QModelIndex & parent_n = QModelIndex() ) const;

	int
	columnCount (
		const QModelIndex & parent_n = QModelIndex() ) const;

	QVariant
	data (
		const QModelIndex & index_n,
		int role_n = Qt::DisplayRole ) const;

	QVariant
	headerData (
		int section_n,
		Qt::Orientation orientation_n,
		int role_n = Qt::DisplayRole ) const;


	// Protected methods
	protected:

	void
	clear ( );

	void
	load ( );


	// Private attributes
	private:

	Snd_Mixer_CTL * _snd_mixer;

	QList < Snd_Mixer_CTL_Elem * > _elems;

	unsigned int _iface_type_idx;

	QString _str_dev_mask;
	QString _str_is_readable[2];
	QString _str_is_writable[2];
	QString _str_is_active[2];
	QString _str_is_volatile[2];

	QString _ttip_dev;
	QString _ttip_is_readable[2];
	QString _ttip_is_writable[2];
	QString _ttip_is_active[2];
	QString _ttip_is_volatile[2];

	QString _ttip_name_mask;

	QString _col_names[10];
	QString _col_ttips[10];

	const unsigned int _num_columns;
	const Qt::Alignment _align_cc;
	const Qt::Alignment _align_lc;
};


inline
Snd_Mixer_CTL *
Mixer_CTL_Table_Model::snd_mixer ( ) const
{
	return _snd_mixer;
}


inline
unsigned int
Mixer_CTL_Table_Model::iface_type_idx ( ) const
{
	return _iface_type_idx;
}


} // End of namespace


#endif
