//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "mixer_ctl_tree_model.hpp"

#include <QFont>
#include <iostream>


namespace QSnd
{


Mixer_CTL_Tree_Model::Mixer_CTL_Tree_Model ( ) :
_base_item ( 0 ),
_snd_mixer ( 0 ),
_iface_type_idx ( 0 )
{
	_base_item = new QStandardItem;
	_base_item->setText ( tr ( "Elements" ) );
	_base_item->setSelectable ( true );
	_base_item->setEditable ( false );
	invisibleRootItem()->appendRow ( _base_item );
}


Mixer_CTL_Tree_Model::~Mixer_CTL_Tree_Model ( )
{
	set_snd_mixer ( 0 );
}


void
Mixer_CTL_Tree_Model::elem_group (
	const QModelIndex & idx_n,
	Snd_Mixer_CTL_Elem_Group * * grp_n,
	unsigned int * index_n ) const
{
	if ( grp_n == 0  ) {
		return;
	}

	if ( !idx_n.isValid() ||
		( idx_n.column() != 0 ) ||
		( idx_n.row() < 0 ) )
	{
		return;
	}

	if ( idx_n == base_item()->index() ) {
		return;
	}

	Snd_Mixer_CTL_Elem_Group * grp ( 0 );
	unsigned int elem_index ( 0 );

	{
		const QModelIndex idx_base ( base_item()->index() );
		const QModelIndex idx_par ( idx_n.parent() );

		if ( idx_par == idx_base ) {

			// First level item
			if ( idx_n.row() < _groups.size() ) {
				grp = _groups[idx_n.row()];
				if ( grp->num_elems() > 1 ) {
					elem_index = grp->num_elems();
				}
			}

		} else {

			const QModelIndex idx_ppar ( idx_par.parent() );
			if ( idx_ppar == idx_base ) {

				// Second level item
				if ( idx_par.row() < _groups.size() ) {
					grp = _groups[idx_par.row()];
					if ( idx_n.row() < (int)grp->num_elems() ) {
						elem_index = idx_n.row();
					} else {
						grp = 0;
					}
				}
			}
		}

	}

	if ( grp != 0 ) {
		*grp_n = grp;
		if ( index_n != 0 ) {
			*index_n = elem_index;
		}
	}
}


QModelIndex
Mixer_CTL_Tree_Model::elem_index (
	Snd_Mixer_CTL_Elem * elem_n ) const
{
	QModelIndex res;

	if ( ( elem_n != 0 ) && ( _groups.size() > 0 ) ) {
		bool found ( false );
		unsigned int gidx ( 0 );
		unsigned int eidx ( 0 );

		const unsigned int num_groups ( _groups.size() );
		for ( unsigned int gii=0; gii < num_groups; ++gii ) {
			Snd_Mixer_CTL_Elem_Group * grp ( _groups[gii] );
			for ( unsigned int eii=0; eii < grp->num_elems(); ++eii ) {
				if ( grp->elem ( eii ) == elem_n ) {
					gidx = gii;
					eidx = eii;
					found = true;
					break;
				}
			}
		}

		if ( found ) {
			Snd_Mixer_CTL_Elem_Group * grp ( _groups[gidx] );
			res = index ( gidx, 0, base_item()->index() );
			if ( grp->num_elems() > 1 ) {
				res = index ( eidx, 0, res );
			}
		}
	}

	return res;
}


void
Mixer_CTL_Tree_Model::set_snd_mixer (
	Snd_Mixer_CTL * snd_mixer_n )
{
	if ( _snd_mixer != snd_mixer_n ) {
		clear();

		_snd_mixer = snd_mixer_n;

		load();
	}
}


void
Mixer_CTL_Tree_Model::set_iface_type_idx (
	unsigned int type_n )
{
	if ( _iface_type_idx != type_n ) {
		clear();

		_iface_type_idx = type_n;

		load();
	}
}


void
Mixer_CTL_Tree_Model::clear ( )
{
	_base_item->removeRows ( 0, _base_item->rowCount() );

	for ( int ii=0; ii < _groups.size(); ++ii ) {
		delete _groups[ii];
	}
	_groups.clear();
}


void
Mixer_CTL_Tree_Model::load ( )
{
	if ( snd_mixer() == 0 ) {
		return;
	}

	if ( snd_mixer()->iface_type_count ( iface_type_idx() ) == 0 ) {
		return;
	}

	// Find groups

	QString grp_elem_name;
	QString elem_name;

	Snd_Mixer_CTL_Elem_Group * grp ( 0 );
	Snd_Mixer_CTL_Elem * grp_elem ( 0 );

	const unsigned int num_elems ( snd_mixer()->num_elems() );
	for ( unsigned int ii=0; ii < num_elems; ++ii ) {

		Snd_Mixer_CTL_Elem * elem ( snd_mixer()->elem ( ii ) );
		if ( elem->iface_type_idx() != iface_type_idx() ) {
			continue;
		}

		elem_name = elem->elem_name();

		// Create a new group or append to the current one
		bool new_grp ( false );
		if ( grp_elem == 0 ) {
			new_grp = true;
		} else {
			if ( ( grp_elem_name != elem_name ) ||
				( elem->elem_type()  != grp_elem->elem_type()  ) ||
				( elem->iface()      != grp_elem->iface()      ) ||
				( elem->elem_index() == grp_elem->elem_index() ) )
			{
				new_grp = true;
			}
		}

		// Create a new group
		if ( new_grp ) {
			grp_elem = elem;
			grp_elem_name = elem->elem_name();

			grp = new Snd_Mixer_CTL_Elem_Group;
			_groups.append ( grp );
		}
		grp->append_elem ( elem );
	}

	//
	// Create model items
	//
	const QString val ( "%1" );
	const QString ttip_suff ( "(%1)" );
	const QString dmask ( "<div>%1</div>" );

	QString ename;
	QString iname;
	QString ittip;

	for ( int ii=0; ii < _groups.size(); ++ii ) {
		const Snd_Mixer_CTL_Elem_Group * grp ( _groups[ii] );
		const Snd_Mixer_CTL_Elem * elem_first ( grp->elem ( 0 ) );
		unsigned int num_elm ( grp->num_elems() );

		QStandardItem * pitem ( new QStandardItem );
		pitem->setSelectable ( true );
		pitem->setEditable ( false );

		ename = elem_first->elem_name();
		iname = elem_first->display_name();
		ittip = dmask.arg ( iname );

		// Append untranslated real name to tooltip
		if ( iname != ename ) {
			ittip.append ( "\n" );
			ittip.append ( dmask.arg ( ttip_suff.arg ( ename ) ) );
		}

		if ( num_elm > 1 ) {
			for ( unsigned int jj=0; jj < num_elm; ++jj ) {
				const Snd_Mixer_CTL_Elem * elem ( grp->elem ( jj ) );
				QStandardItem * item ( new QStandardItem );
				item->setText ( val.arg ( elem->elem_index() ) );
				item->setSelectable ( true );
				item->setEditable ( false );
				pitem->appendRow ( item );
			}
		}

		pitem->setText ( iname );
		pitem->setToolTip ( ittip );

		_base_item->appendRow ( pitem );
	}

}


} // End of namespace
