//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_snd_mixer_ctl_model_hpp__
#define __INC_snd_mixer_ctl_model_hpp__

#include <QAbstractItemModel>
#include <QStandardItemModel>
#include <QList>
#include <QStringList>

#include "snd_mixer_ctl.hpp"


namespace QSnd
{


///
/// @brief Mixer_CTL_Tree_Model
///
class Mixer_CTL_Tree_Model :
	public QStandardItemModel
{
	Q_OBJECT;

	// Public methods
	public:

	Mixer_CTL_Tree_Model ( );

	~Mixer_CTL_Tree_Model ( );


	// Mixer class

	Snd_Mixer_CTL *
	snd_mixer ( ) const;

	void
	set_snd_mixer (
		Snd_Mixer_CTL * snd_mixer_n );


	// Interface type index

	unsigned int
	iface_type_idx ( ) const;

	void
	set_iface_type_idx (
		unsigned int type_n );


	void
	elem_group (
		const QModelIndex & idx_n,
		Snd_Mixer_CTL_Elem_Group * * grp_n,
		unsigned int * index_n ) const;


	QModelIndex
	elem_index (
		Snd_Mixer_CTL_Elem * elem_n ) const;


	// Base item

	QStandardItem *
	base_item ( ) const;


	// Protected methods
	protected:

	void
	clear ( );

	void
	load ( );


	// Private attributes
	private:

	QStandardItem * _base_item;

	Snd_Mixer_CTL * _snd_mixer;

	QList < Snd_Mixer_CTL_Elem_Group * > _groups;

	unsigned int _iface_type_idx;
};


inline
Snd_Mixer_CTL *
Mixer_CTL_Tree_Model::snd_mixer ( ) const
{
	return _snd_mixer;
}


inline
unsigned int
Mixer_CTL_Tree_Model::iface_type_idx ( ) const
{
	return _iface_type_idx;
}


inline
QStandardItem *
Mixer_CTL_Tree_Model::base_item ( ) const
{
	return _base_item;
}


} // End of namespace


#endif
