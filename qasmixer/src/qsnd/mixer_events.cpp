//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "mixer_events.hpp"


namespace QSnd
{


QEvent::Type event_type_update_values_request;
QEvent::Type event_type_update_values;

QEvent::Type event_type_reload_request;
QEvent::Type event_type_reload;

QEvent::Type event_type_separation_request;
QEvent::Type event_type_separation;

QEvent::Type event_type_values_changed;
QEvent::Type event_type_refresh_data;


void
init_mixer_events ( )
{
	event_type_update_values_request =
		static_cast < QEvent::Type > ( QEvent::registerEventType() );
	event_type_update_values =
		static_cast < QEvent::Type > ( QEvent::registerEventType() );

	event_type_reload_request =
		static_cast < QEvent::Type > ( QEvent::registerEventType() );
	event_type_reload =
		static_cast < QEvent::Type > ( QEvent::registerEventType() );

	event_type_separation_request =
		static_cast < QEvent::Type > ( QEvent::registerEventType() );
	event_type_separation =
		static_cast < QEvent::Type > ( QEvent::registerEventType() );

	event_type_values_changed =
		static_cast < QEvent::Type > ( QEvent::registerEventType() );
	event_type_refresh_data =
		static_cast < QEvent::Type > ( QEvent::registerEventType() );
}


} // End of namespace

