//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_qsnd_mixer_events_hpp__
#define __INC_qsnd_mixer_events_hpp__

#include <QEvent>


namespace QSnd
{


extern QEvent::Type event_type_update_values_request;
extern QEvent::Type event_type_update_values;

extern QEvent::Type event_type_reload_request;
extern QEvent::Type event_type_reload;

extern QEvent::Type event_type_separation_request;
extern QEvent::Type event_type_separation;

extern QEvent::Type event_type_values_changed;
extern QEvent::Type event_type_refresh_data;


void
init_mixer_events ( );


} // End of namespace


#endif
