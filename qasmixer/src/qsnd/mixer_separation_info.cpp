//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "mixer_separation_info.hpp"


namespace QSnd
{


Mixer_Separation_Info::Mixer_Separation_Info ( ) :
_requested ( false ),
_do_it ( false )
{
}


} // End of namespace

