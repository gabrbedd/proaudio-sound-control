//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mixer_sliders.hpp"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>

#include <QCoreApplication>
#include <QKeyEvent>
#include <QFocusEvent>
#include <wdg/pass_events.hpp>

#include <iostream>


namespace QSnd
{


Mixer_Sliders::Mixer_Sliders (
	QWidget * parent_n ) :
QWidget ( parent_n ),
_status_bar ( _sliders_pad ),
_separation_requested ( false ),
_cmenu ( this ),
_act_toggle_joined ( this ),
_act_level_volumes ( this ),
_act_separator_channels ( this ),
_act_toggle_mute ( this ),
_wheel_degrees ( 0 ),
_mixer_simple ( 0 ),
_mixer_style ( 0 )
{
	_sliders_area.setFrameStyle ( QFrame::NoFrame );
	_sliders_area.set_widget ( &_sliders_pad );

	_status_bar.setFrameStyle ( QFrame::NoFrame );

	// Sliders pad
	sliders_pad()->installEventFilter ( this );

	connect ( sliders_pad(), SIGNAL ( sig_focus_changed() ),
		this, SLOT ( update_focus_proxies() ) );


	// Actions
	_act_toggle_joined.setShortcut ( QKeySequence ( tr ( "c" ) ) );
	_act_toggle_joined.setIcon ( QIcon::fromTheme ( "object-flip-horizontal" ) );
	_act_toggle_joined.setIconVisibleInMenu ( true );

	_act_level_volumes.setShortcut ( QKeySequence ( tr ( "l" ) ) );
	_act_level_volumes.setIcon ( QIcon::fromTheme ( "object-flip-vertical" ) );
	_act_level_volumes.setIconVisibleInMenu ( true );

	_act_toggle_mute.setShortcut ( QKeySequence ( tr ( "m" ) ) );
	_act_toggle_mute.setIconVisibleInMenu ( true );

	_act_separator_channels.setSeparator ( true );

	_act_level_volumes.setText ( tr ( "&Level channels" ) );

	_act_str_toggle_joined[0] = tr ( "Split &channels" );
	_act_str_toggle_joined[1] = tr ( "Join &channels" );
	_act_str_mute[0] = tr ( "&Mute" );
	_act_str_mute[1] = tr ( "&Mute all" );
	_act_str_unmute[0] = tr ( "Un&mute" );
	_act_str_unmute[1] = tr ( "Un&mute all" );
	_act_str_toggle_mute = tr ( "Toggle &mutes" );

	_icon_vol_high = QIcon::fromTheme ( "audio-volume-high" );
	_icon_vol_med = QIcon::fromTheme ( "audio-volume-medium" );
	_icon_muted = QIcon::fromTheme ( "audio-volume-muted" );

	connect ( &_act_toggle_joined, SIGNAL ( triggered ( bool ) ),
		this, SLOT ( action_toggle_joined() ) );

	connect ( &_act_level_volumes, SIGNAL ( triggered ( bool ) ),
		this, SLOT ( action_level_volumes() ) );

	connect ( &_act_toggle_mute, SIGNAL ( triggered ( bool ) ),
		this, SLOT ( action_toggle_mute() ) );


	// Context menu
	_cmenu.addAction ( &_act_toggle_joined );
	_cmenu.addAction ( &_act_level_volumes );
	_cmenu.addAction ( &_act_separator_channels );
	_cmenu.addAction ( &_act_toggle_mute );

	connect ( &_cmenu, SIGNAL ( aboutToHide() ),
		this, SLOT ( context_menu_cleanup_behind() ) );


	// Proxies vars
	_ttip_slider[0] = tr ( "Playback slider" );
	_ttip_slider[1] = tr ( "Capture slider" );
	_ttip_switch[0] = tr ( "Playback switch" );
	_ttip_switch[1] = tr ( "Capture switch" );


	// Layout
	QVBoxLayout * lay_vbox ( new QVBoxLayout() );
	lay_vbox->addWidget ( &_sliders_area, 1 );
	lay_vbox->addWidget ( &_status_bar, 0 );
	setLayout ( lay_vbox );

	// Adjust margins and spacings
	{
		int space ( qMax ( fontMetrics().height(), lay_vbox->spacing() ) );
		lay_vbox->setSpacing ( space );
	}

	_status_bar.setVisible ( _settings.show_slider_status_bar );
}


Mixer_Sliders::~Mixer_Sliders ( )
{
	set_mixer_simple ( 0 );
}


void
Mixer_Sliders::set_mixer_style (
	const Mixer_Style * mstyle_n )
{
	if ( _mixer_style != mstyle_n ) {
		_mixer_style = mstyle_n;
	}
}


void
Mixer_Sliders::set_mixer_simple (
	Snd_Mixer_Simple * mixer_n  )
{
	//std::cout << "Mixer_Sliders::set_mixer_simple " << mixer_n  << "\n";

	if ( _mixer_simple == mixer_n ) {
		return;
	}

	_cmenu.close();

	if ( _mixer_simple != 0 ) {
		disconnect ( _mixer_simple, 0, this, 0 );
		disconnect ( _mixer_simple, 0, &_status_bar, 0 );

		show_visible_proxies_sets ( false );
		clear_proxies_groups();
	}

	_mixer_simple = mixer_n;

	if ( _mixer_simple != 0 ) {
		create_proxies_groups();

		rebuild_visible_proxies_list();

		show_visible_proxies_sets ( true );
	}
}


void
Mixer_Sliders::set_mixer_settings (
	const Simple_Mixer_Settings & cfg_n  )
{
	//std::cout << "Mixer_Sliders::set_mixer_settings " << "\n";

	if ( _settings != cfg_n ) {

		_cmenu.close();

		show_visible_proxies_sets ( false );

		_settings = cfg_n;

		rebuild_visible_proxies_list();

		show_visible_proxies_sets ( true );
		_status_bar.setVisible ( _settings.show_slider_status_bar );
	}
}


void
Mixer_Sliders::set_wheel_degrees (
	unsigned int degrees_n )
{
	if ( _wheel_degrees != degrees_n ) {
		_wheel_degrees = degrees_n;
		sliders_pad()->set_wheel_degrees ( _wheel_degrees );
	}
}


void
Mixer_Sliders::clear_proxies_groups ( )
{
	for ( int ii=0; ii < _proxies_groups.size(); ++ii ) {
		delete _proxies_groups[ii];
	}
	_proxies_groups.clear();
	_proxies_groups_visible.clear();
}


void
Mixer_Sliders::create_proxies_groups ( )
{
	if ( _mixer_simple == 0 ) {
		return;
	}

	for ( unsigned int ii=0; ii < _mixer_simple->num_elems(); ++ii ) {
		Snd_Mixer_Simple_Elem * qsme ( _mixer_simple->elem ( ii ) );

		for ( unsigned int snd_dir = 0; snd_dir < 2; ++snd_dir ) {
			if ( qsme->has_volume ( snd_dir ) ) {

				Mixer_Sliders_Proxies_Group * mspg (
					new Mixer_Sliders_Proxies_Group ( this ) );

				mspg->set_snd_dir ( snd_dir );
				mspg->set_mixer_simple_elem ( qsme );
				mspg->set_group_name ( qsme->display_name() );

				if ( _mixer_style != 0 ) {
					mspg->set_palette ( _mixer_style->palette ( snd_dir ) );
				}

				if ( mspg->should_be_separated() ) {
					setup_proxies_group_separate ( mspg );
				} else {
					setup_proxies_group_joined ( mspg );
				}

				if ( mspg->num_columns() > 0 ) {
					_proxies_groups.append ( mspg );
				} else {
					delete mspg;
				}
			}
		}

	}

}


void
Mixer_Sliders::setup_proxies_group_joined (
	Mixer_Sliders_Proxies_Group * mspg_n )
{
	mspg_n->clear_columns();
	mspg_n->set_is_joined ( true );

	{
		QString ttip;
		ttip = mspg_n->group_name();
		mspg_n->set_tool_tip ( ttip );
	}

	create_proxies_group ( mspg_n, 0 );
	mspg_n->update_mixer_values();
}


void
Mixer_Sliders::setup_proxies_group_separate (
	Mixer_Sliders_Proxies_Group * mspg_n )
{
	const unsigned int num_channels (
		mspg_n->mixer_simple_elem()->num_channels ( mspg_n->snd_dir() ) );

	mspg_n->clear_columns();
	mspg_n->set_is_joined ( false );

	{
		QString ttip;
		ttip = mspg_n->group_name();
		mspg_n->set_tool_tip ( ttip );
	}

	for ( unsigned int ii=0; ii < num_channels; ++ii ) {
		create_proxies_group ( mspg_n, ii );
	}
	mspg_n->update_mixer_values();
}


bool
Mixer_Sliders::create_proxies_group (
	Mixer_Sliders_Proxies_Group * mspg_n,
	unsigned int channel_idx_n )
{
	bool res ( false );

	Snd_Mixer_Simple_Elem * qsme ( mspg_n->mixer_simple_elem() );
	const unsigned int snd_dir ( mspg_n->snd_dir() );

	Mixer_Sliders_Proxies_Column * mspc (
		new Mixer_Sliders_Proxies_Column ( mspg_n ) );

	// Item name
	QString iname;
	if ( mspg_n->is_joined() ) {
		iname = mspg_n->group_name();
	} else {
		iname = tr ( "%1 (%2)" );
		iname = iname.arg ( QCoreApplication::translate (
			"ALSA::Channel_Name",
			qsme->channel_name ( snd_dir, channel_idx_n ) ) );
		iname = iname.arg ( qsme->channel ( snd_dir, channel_idx_n ) );
	}

	// Tool tip
	QString ttip_name;
	QString ttip_channel;
	ttip_name += "<div><b>";
	ttip_name += mspg_n->group_name();
	ttip_name += "</b></div>\n";

	if ( !mspg_n->is_joined() ) {
		ttip_channel += "<div>";
		ttip_channel += iname;
		ttip_channel += "</div>\n";
	}

	// Create volume proxy
	if ( qsme->has_volume ( snd_dir ) &&
		( channel_idx_n < qsme->num_volume_channels ( snd_dir ) ) )
	{
		Mixer_Sliders_Proxy_Slider * msps (
			new Mixer_Sliders_Proxy_Slider ( mspc ) );

		msps->set_snd_dir ( snd_dir );
		msps->set_channel_idx ( channel_idx_n );
		msps->set_is_joined ( mspg_n->is_joined() );
		msps->set_mixer_simple_elem ( qsme );

		msps->set_item_name ( iname );
		msps->set_group_name ( mspg_n->group_name() );

		{
			QString ttip ( ttip_name );
			{
				QString ttip_type;
				ttip_type += "<div>";
				ttip_type += _ttip_slider[snd_dir];
				ttip_type += "</div>";
				ttip += ttip_type;
			}
			ttip += ttip_channel;
			msps->set_tool_tip ( ttip );
		}

		// Proxy Style
		Wdg::Sliders_Pad_Proxy_Style * sinfo (
			new Wdg::Sliders_Pad_Proxy_Style ( snd_dir ) );

		if ( msps->has_dB() ) {
			if ( ( msps->dB_max() > 0 ) && ( msps->dB_min() < 0 ) ) {
				sinfo->slider_has_minimum = true;
				sinfo->slider_minimum_idx = msps->ask_dB_vol_nearest ( 0 );
			} else {
				if ( msps->dB_max() <= 0 ) {
					sinfo->slider_has_minimum = true;
					sinfo->slider_minimum_idx = msps->slider_index_max();
				} else if ( msps->dB_min() >= 0 ) {
					sinfo->slider_has_minimum = true;
					sinfo->slider_minimum_idx = 0;
				}
			}
		}

		if ( _mixer_style != 0 ) {
			sinfo->palette = _mixer_style->palette ( snd_dir );
		}
		msps->set_style_info ( sinfo );
	}


	// Create switch proxy
	if ( qsme->has_switch ( snd_dir ) &&
		( channel_idx_n < qsme->num_switch_channels ( snd_dir ) ) )
	{
		Mixer_Sliders_Proxy_Switch * msps (
			new Mixer_Sliders_Proxy_Switch ( mspc ) );

		msps->set_snd_dir ( snd_dir );
		msps->set_channel_idx ( channel_idx_n );
		msps->set_is_joined ( mspg_n->is_joined() );
		msps->set_mixer_simple_elem ( qsme );

		msps->set_style_info (
			new Wdg::Sliders_Pad_Proxy_Style ( snd_dir ) );

		msps->set_item_name ( iname );
		msps->set_group_name ( mspg_n->group_name() );

		{
			QString ttip ( ttip_name );
			{
				QString ttip_type;
				ttip_type += "<div>";
				ttip_type += _ttip_switch[snd_dir];
				ttip_type += "</div>";
				ttip += ttip_type;
			}

			ttip += ttip_channel;
			msps->set_tool_tip ( ttip );
		}

		// Proxy Style
		Wdg::Sliders_Pad_Proxy_Style * sinfo (
			new Wdg::Sliders_Pad_Proxy_Style ( snd_dir ) );
		if ( _mixer_style != 0 ) {
			sinfo->palette = _mixer_style->palette ( snd_dir );
		}
		msps->set_style_info ( sinfo );
	}

	if ( mspc->has_slider() || mspc->has_switch() ) {
		mspg_n->append_column ( mspc );
	} else {
		delete mspc;
		res = true;
	}

	return res;
}


bool
Mixer_Sliders::should_be_visible (
	const Mixer_Sliders_Proxies_Group * mspg_n ) const
{
	const Snd_Mixer_Simple_Elem * qsme ( mspg_n->mixer_simple_elem() );
	unsigned int snd_dir ( mspg_n->snd_dir() % 2 );
	return ( qsme->is_active() && _settings.show_stream[snd_dir] );
}


void
Mixer_Sliders::rebuild_visible_proxies_list ( )
{
	_proxies_groups_visible.clear();

	for ( int ii=0; ii < _proxies_groups.size(); ++ii ) {
		Mixer_Sliders_Proxies_Group * mspg ( _proxies_groups[ii] );
		mspg->set_is_visible ( should_be_visible ( mspg ) );
		if ( mspg->is_visible() ) {
			if ( mspg->needs_separation() ) {
				separate_proxies_group ( mspg );
			}
			_proxies_groups_visible.append ( mspg );
		}
	}
}


void
Mixer_Sliders::action_level_volumes ( )
{
	//std::cout << "Mixer_Sliders::action_level_volumes" << "\n";

	Mixer_Sliders_Proxies_Group * mspg ( _act_proxies_group );

	if ( mspg != 0 ) {

		Mixer_Sliders_Proxies_Column * mspc ( 0 );

		{
			unsigned int col_idx ( 0 );
			if ( _act_proxy_column < mspg->num_columns() ) {
				col_idx = _act_proxy_column;
			}

			mspc = dynamic_cast < Mixer_Sliders_Proxies_Column * > (
				mspg->column ( col_idx ) );
		}

		Mixer_Sliders_Proxy_Slider * msps ( 0 );
		if ( mspc != 0 ) {
			msps = dynamic_cast < Mixer_Sliders_Proxy_Slider * > ( mspc->slider_proxy() );
		}

		if ( msps != 0 ) {
			mspg->mixer_simple_elem()->set_volume_all (
				mspg->snd_dir(), msps->volume_value() );
		} else {
			mspg->mixer_simple_elem()->level_volumes ( mspg->snd_dir() );
		}
	}

}


void
Mixer_Sliders::action_toggle_joined ( )
{
	//::std::cout << "Mixer_Sliders::action_toggle_joined" << "\n";

	Mixer_Sliders_Proxies_Group * mspg ( _act_proxies_group );
	if ( mspg != 0 ) {
		toggle_joined_separated ( mspg );
	}
}


void
Mixer_Sliders::action_toggle_mute ( )
{
	//::std::cout << "Mixer_Sliders::action_toggle_mute" << "\n";

	Mixer_Sliders_Proxies_Group * mspg ( _act_proxies_group );
	if ( mspg != 0 ) {
		mspg->mixer_simple_elem()->invert_switches ( mspg->snd_dir() );
	}
}


void
Mixer_Sliders::update_focus_proxies ( )
{
	_focus_proxies_group = 0;
	_focus_proxy_column = 0;
	if ( sliders_pad()->focus_info().has_focus ) {
		// Find focus proxies_group
		const int idx ( sliders_pad()->focus_info().group_idx );
		if ( idx < _proxies_groups_visible.size() ) {
			_focus_proxies_group = proxies_group_visible ( idx );
			_focus_proxy_column = sliders_pad()->focus_info().column_idx;
		}
	}

	if ( _focus_proxies_group != 0 ) {
		_act_proxies_group = _focus_proxies_group;
		_act_proxy_column = _focus_proxy_column;
	}
}


void
Mixer_Sliders::acquire_ui_state (
	Mixer_State & state_n )
{
	if ( _focus_proxies_group != 0 ) {
		Mixer_Sliders_Proxies_Group * mspg ( _focus_proxies_group );
		state_n.focus_proxy.set_group_name ( mspg->group_name() );
		state_n.focus_proxy.set_snd_dir ( mspg->snd_dir() );
		state_n.focus_proxy.set_proxy_idx ( mspg->focus_column() );
	}

	if ( _act_proxies_group != 0 ) {
		Mixer_Sliders_Proxies_Group * mspg ( _act_proxies_group );
		state_n.action_proxy.set_group_name ( mspg->group_name() );
		state_n.action_proxy.set_snd_dir ( mspg->snd_dir() );
		state_n.action_proxy.set_proxy_idx ( mspg->focus_column() );
	}
}


void
Mixer_Sliders::restore_ui_state (
	const Mixer_State & state_n )
{
	{
		Mixer_Sliders_Proxies_Group * mspg (
			find_visible_proxy ( state_n.action_proxy ) );
		_act_proxies_group = mspg;
		_act_proxy_column = state_n.action_proxy.proxy_idx();
	}

	// Setting the focus may override _act_proxies_group
	// so set it afterwards
	{
		Mixer_Sliders_Proxies_Group * mspg (
			find_visible_proxy ( state_n.focus_proxy ) );
		if ( mspg != 0 ) {
			sliders_pad()->set_focus_proxy (
				mspg->group_index(),
				state_n.focus_proxy.proxy_idx() );
		}
	}

	if ( _cmenu.isVisible() ) {
		context_menu_update();
	}
}


Mixer_Sliders_Proxies_Group *
Mixer_Sliders::find_visible_proxy (
	const Mixer_State_Proxy & prox_id_n )
{
	Mixer_Sliders_Proxies_Group * mspg ( 0 );

	if ( !prox_id_n.is_clear() ) {
		for ( int ii=0; ii < _proxies_groups_visible.size(); ++ii ) {
			Mixer_Sliders_Proxies_Group * mspg_cur (
				proxies_group_visible ( ii ) );
			if (
				( mspg_cur->group_name() == prox_id_n.group_name() ) &&
				( mspg_cur->snd_dir() == prox_id_n.snd_dir() ) )
			{
				mspg = mspg_cur;
				break;
			}
		}
	}

	return mspg;
}


void
Mixer_Sliders::toggle_joined_separated (
	Mixer_Sliders_Proxies_Group * mspg_n )
{
	if ( mspg_n != 0 ) {

		if ( !mspg_n->can_be_separated() ) {
			return;
		}

		Mixer_State ui_state;
		acquire_ui_state ( ui_state );
		show_visible_proxies_sets ( false );

		if ( mspg_n->is_joined() ) {
			separate_proxies_group ( mspg_n );
		} else {
			join_proxies_group ( mspg_n );
		}

		show_visible_proxies_sets ( true );
		restore_ui_state ( ui_state );
	}
}


void
Mixer_Sliders::join_proxies_group (
	Mixer_Sliders_Proxies_Group * mspg_n )
{
	if ( !mspg_n->is_joined() ) {
		Snd_Mixer_Simple_Elem * qsme ( mspg_n->mixer_simple_elem() );
		qsme->level_volumes ( mspg_n->snd_dir() );
		qsme->level_switches ( mspg_n->snd_dir() );

		setup_proxies_group_joined ( mspg_n );
		if ( _cmenu.isVisible() ) {
			context_menu_update();
		}
	}
}


void
Mixer_Sliders::separate_proxies_group (
	Mixer_Sliders_Proxies_Group * mspg_n )
{
	if ( mspg_n->can_be_separated() ) {
		setup_proxies_group_separate ( mspg_n );
		if ( _cmenu.isVisible() ) {
			context_menu_update();
		}
	}
}


void
Mixer_Sliders::show_visible_proxies_sets (
	bool flag_n )
{
	if ( flag_n ) {
		if ( _proxies_groups_visible.size() > 0 ) {
			sliders_pad()->set_proxies_groups ( &_proxies_groups_visible );
			sliders_pad()->set_wheel_degrees ( _wheel_degrees );
			_sliders_area.set_widget ( sliders_pad() );

			sliders_pad()->setAutoFillBackground ( false );
			sliders_pad()->show();
		}
		updateGeometry();
	} else {
		sliders_pad()->hide();
		_sliders_area.take_widget();
		sliders_pad()->set_proxies_groups ( 0 );
	}
}


void
Mixer_Sliders::separate_where_requested ( )
{
	//::std::cout << "Mixer_Sliders::separate_where_requested" << "\n";

	bool visible_request ( false );
	for ( int ii=0; ii < _proxies_groups_visible.size(); ++ii ) {
		Mixer_Sliders_Proxies_Group * mspg ( proxies_group_visible ( ii ) );
		if ( mspg->separation_request() ) {
			visible_request = true;
			break;
		}
	}

	if ( visible_request ) {
		Mixer_State ui_state;
		acquire_ui_state ( ui_state );
		show_visible_proxies_sets ( false );

		for ( int ii=0; ii < _proxies_groups_visible.size(); ++ii ) {
			Mixer_Sliders_Proxies_Group * mspg ( proxies_group_visible ( ii ) );
			if ( mspg->separation_request() ) {
				mspg->set_separation_request ( false );
				separate_proxies_group ( mspg );
			}
		}

		show_visible_proxies_sets ( true );
		restore_ui_state ( ui_state );
	}

	//::std::cout << "Mixer_Sliders::separate_where_requested done" << "\n";
}


void
Mixer_Sliders::context_menu_start (
	const QPoint & pos_n )
{
	if ( _cmenu.isVisible() ) {
		return;
	}

	if ( !sliders_pad()->focus_info().has_focus ) {
		return;
	}

	if ( _act_proxies_group != 0 ) {
		if ( context_menu_update() > 0 ) {
			_act_proxies_group->set_notify_value_change ( true );

			_cmenu.setTitle ( _act_proxies_group->group_name() );
			_cmenu.popup ( pos_n );
		}
	}
}


void
Mixer_Sliders::context_menu_cleanup_behind ( )
{
	//::std::cout << "Mixer_Sliders::context_cleanup_behind\n";
	if ( _act_proxies_group != 0 ) {
		_act_proxies_group->set_notify_value_change ( false );
	}
}


unsigned int
Mixer_Sliders::context_menu_update ( )
{
	//::std::cout << "Mixer_Sliders::context_menu_update\n";

	unsigned int act_vis ( 0 );

	Mixer_Sliders_Proxies_Group * mspg ( _act_proxies_group );

	if ( mspg == 0 ) {
		_cmenu.close();
		return act_vis;
	}

	const Snd_Mixer_Simple_Elem * qsme ( mspg->mixer_simple_elem() );
	const unsigned int snd_dir ( mspg->snd_dir() );


	// Update split/join and level channels actions
	{
		const bool vis_joined ( mspg->can_be_separated() );

		_act_toggle_joined.setVisible ( vis_joined );
		if ( vis_joined ) {
			++act_vis;

			unsigned int idx ( mspg->is_joined() ? 0 : 1 );
			_act_toggle_joined.setText ( _act_str_toggle_joined[idx] );
		}

		const bool vis_level (
			( mspg->num_sliders() > 1 ) &&
			!qsme->volumes_equal ( snd_dir ) );

		_act_level_volumes.setVisible ( vis_level );
		if ( vis_level ) {
			++act_vis;
		}

	}

	// Update mute / unmute channels actions
	{
		unsigned int num_sw ( mspg->num_switches() );

		if ( num_sw > 0 ) {
			++act_vis;

			QString * act_txt = &_act_str_toggle_mute;
			QIcon * act_icon = &_icon_vol_med;
			const bool is_on ( qsme->switch_state ( snd_dir, 0 ) );

			if ( is_on ) {
				act_icon = &_icon_muted;
			} else {
				act_icon = &_icon_vol_high;
			}

			if ( num_sw == 1 ) {
				if ( is_on ) {
					act_txt = &_act_str_mute[0];
				} else {
					act_txt = &_act_str_unmute[0];
				}
			} else {
				if ( qsme->switches_equal ( snd_dir ) ) {
					if ( is_on ) {
						act_txt = &_act_str_mute[1];
					} else {
						act_txt = &_act_str_unmute[1];
					}
				}
			}

			_act_toggle_mute.setText ( *act_txt );
			_act_toggle_mute.setIcon ( *act_icon );
		}

		_act_toggle_mute.setVisible ( num_sw > 0 );
	}

	if ( act_vis == 0 ) {
		_cmenu.close();
	}

	return act_vis;
}


bool
Mixer_Sliders::eventFilter (
	QObject * watched_n,
	QEvent * event_n )
{
	bool res ( false );

	if ( watched_n == sliders_pad() ) {

		// This event filter is installed on the sliders pad

		QKeyEvent * ev_key (
			dynamic_cast < QKeyEvent * > ( event_n ) );
		QContextMenuEvent * ev_cmenu (
			dynamic_cast < QContextMenuEvent * > ( event_n ) );

		if ( ev_key != 0 ) {
			if ( ev_key->type() == QEvent::KeyPress ) {
				res = true;
				// Trigger actions
				const QKeySequence key_seq ( ev_key->key() );
				if ( _act_toggle_joined.shortcut() == key_seq ) {
					_act_toggle_joined.trigger();
				} else if ( _act_level_volumes.shortcut() == key_seq ) {
					_act_level_volumes.trigger();
				} else if ( _act_toggle_mute.shortcut() == key_seq ) {
					_act_toggle_mute.trigger();
				} else {
					res = false;
				}
			}
		} else if ( ev_cmenu != 0 ) {
			// Start context menu
			context_menu_start ( ev_cmenu->globalPos() );
			res = true;
		}
	}

	return res;
}



bool
Mixer_Sliders::event (
	QEvent * event_n )
{
	bool res ( true );

	if ( event_n->type() == event_type_separation_request ) {

		if ( !_separation_requested ) {
			_separation_requested = true;
			QCoreApplication::postEvent (
				this, new QEvent ( event_type_separation ) );
		}

	} else if ( event_n->type() == event_type_separation ) {

		_separation_requested = false;
		separate_where_requested();

	} else if ( event_n->type() == event_type_values_changed ) {

		context_menu_update();

	} else {

		res = QWidget::event ( event_n );

	}

	return res;
}


} // End of namespace

