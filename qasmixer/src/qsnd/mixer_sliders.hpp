//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_mixer_sliders_hpp__
#define __INC_mixer_sliders_hpp__

#include <QString>
#include <QAction>
#include <QMenu>
#include <QTimer>

#include "simple_mixer_settings.hpp"
#include "snd_mixer_simple.hpp"
#include "wdg/scroll_area_horizontal.hpp"

#include "mixer_style.hpp"
#include "mixer_ui_state.hpp"
#include "mixer_sliders_proxies_group.hpp"
#include "mixer_sliders_status_bar.hpp"


namespace QSnd
{


///
/// @brief Mixer_Sliders
///
class Mixer_Sliders :
	public QWidget
{
	Q_OBJECT

	// Public methods
	public:

	Mixer_Sliders (
		QWidget * parent_n = 0 );

	~Mixer_Sliders ( );


	Wdg::Sliders_Pad *
	sliders_pad ( );


	// Simple mixer

	Snd_Mixer_Simple *
	mixer_simple ( ) const;

	void
	set_mixer_simple (
		Snd_Mixer_Simple * mixer_n  );


	// Mixer config

	const Simple_Mixer_Settings &
	mixer_settings ( );

	void
	set_mixer_settings (
		const Simple_Mixer_Settings & cfg_n );


	// Wheel degrees

	unsigned int
	wheel_degrees ( ) const;

	void
	set_wheel_degrees (
		unsigned int degrees_n );


	// Style palette

	const Mixer_Style *
	mixer_style ( ) const;

	void
	set_mixer_style (
		const Mixer_Style * mstyle_n );


	/// @brief Number of visualized proxies groups
	unsigned int
	num_visible ( ) const;


	// Event handling

	bool
	event (
		QEvent * event_n );


	// Protected slots
	protected slots:

	void
	action_toggle_joined ( );

	void
	action_level_volumes ( );

	void
	action_toggle_mute ( );

	void
	update_focus_proxies ( );

	void
	context_menu_cleanup_behind ( );


	// Protected methods
	protected:


	// Proxy group creation / deletion

	void
	clear_proxies_groups ( );

	void
	create_proxies_groups ( );

	void
	setup_proxies_group_joined (
		Mixer_Sliders_Proxies_Group * mspg_n );

	void
	setup_proxies_group_separate (
		Mixer_Sliders_Proxies_Group * mspg_n );

	bool
	create_proxies_group (
		Mixer_Sliders_Proxies_Group * mspg_n,
		unsigned int channel_idx_n );


	// Proxy group manipulation

	bool
	should_be_visible (
		const Mixer_Sliders_Proxies_Group * mspg_n ) const;

	void
	toggle_joined_separated (
		Mixer_Sliders_Proxies_Group * mspg_n );

	void
	join_proxies_group (
		Mixer_Sliders_Proxies_Group * mspg_n );

	void
	separate_proxies_group (
		Mixer_Sliders_Proxies_Group * mspg_n );


	// Proxy group showing hiding

	void
	show_visible_proxies_sets (
		bool flag_n );


	// Proxy group updating

	void
	separate_where_requested ( );

	void
	rebuild_visible_proxies_list ( );


	// Focus proxy

	void
	acquire_ui_state (
		Mixer_State & state_n );

	void
	restore_ui_state (
		const Mixer_State & state_n );

	Mixer_Sliders_Proxies_Group *
	find_visible_proxy (
		const Mixer_State_Proxy & prox_id_n );


	// Context menu

	void
	context_menu_start (
		const QPoint & pos_n );

	/// @return The number of visible actions
	unsigned int
	context_menu_update ( );


	// Event callbacks

	bool
	eventFilter (
		QObject * watched,
		QEvent * event );


	/// @brief Convenience method that does a static cast
	///
	const Mixer_Sliders_Proxies_Group *
	proxies_group_visible (
		unsigned int idx_n ) const;


	/// @brief Convenience method that does a static cast
	///
	Mixer_Sliders_Proxies_Group *
	proxies_group_visible (
		unsigned int idx_n );


	// Private attributes
	private:

	Mixer_Sliders_Proxies_Group_List _proxies_groups;

	Wdg::Sliders_Pad_Proxies_Group_List _proxies_groups_visible;


	Wdg::Scroll_Area_Horizontal _sliders_area;

	Wdg::Sliders_Pad _sliders_pad;


	Mixer_Sliders_Status_Bar _status_bar;


	bool _separation_requested;


	QPointer < Mixer_Sliders_Proxies_Group > _focus_proxies_group;
	unsigned int _focus_proxy_column;

	QPointer < Mixer_Sliders_Proxies_Group > _act_proxies_group;
	unsigned int _act_proxy_column;

	// Context menu

	QMenu _cmenu;

	QAction _act_toggle_joined;
	QAction _act_level_volumes;
	QAction _act_separator_channels;
	QAction _act_toggle_mute;

	QString _act_str_toggle_joined[2];
	QString _act_str_mute[2];
	QString _act_str_unmute[2];
	QString _act_str_toggle_mute;

	QIcon _icon_vol_high;
	QIcon _icon_vol_med;
	QIcon _icon_muted;

	Simple_Mixer_Settings _settings;

	unsigned int _wheel_degrees;


	Snd_Mixer_Simple * _mixer_simple;

	const Mixer_Style * _mixer_style;


	QString _ttip_slider[2];
	QString _ttip_switch[2];
};


inline
Snd_Mixer_Simple *
Mixer_Sliders::mixer_simple ( ) const
{
	return _mixer_simple;
}


inline
const Mixer_Style *
Mixer_Sliders::mixer_style ( ) const
{
	return _mixer_style;
}


inline
const Simple_Mixer_Settings &
Mixer_Sliders::mixer_settings ( )
{
	return _settings;
}


inline
unsigned int
Mixer_Sliders::wheel_degrees ( ) const
{
	return _wheel_degrees;
}


inline
unsigned int
Mixer_Sliders::num_visible ( ) const
{
	return _proxies_groups_visible.size();
}


inline
Wdg::Sliders_Pad *
Mixer_Sliders::sliders_pad ( )
{
	return &_sliders_pad;
}


inline
Mixer_Sliders_Proxies_Group *
Mixer_Sliders::proxies_group_visible (
	unsigned int idx_n )
{
	return static_cast < Mixer_Sliders_Proxies_Group * > ( _proxies_groups_visible[idx_n] );
}


inline
const Mixer_Sliders_Proxies_Group *
Mixer_Sliders::proxies_group_visible (
	unsigned int idx_n ) const
{
	return static_cast < const Mixer_Sliders_Proxies_Group * > ( _proxies_groups_visible[idx_n] );
}


} // End of namespace


#endif
