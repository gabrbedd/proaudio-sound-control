//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "mixer_sliders_proxies_column.hpp"

#include "wdg/pass_events.hpp"
#include <QCoreApplication>
#include <iostream>


namespace QSnd
{


//
// Mixer_Sliders_Proxies_Column
//

Mixer_Sliders_Proxies_Column::Mixer_Sliders_Proxies_Column (
	QObject * parent_n ) :
Wdg::Sliders_Pad_Proxies_Column ( parent_n )
{
}


Mixer_Sliders_Proxies_Column::~Mixer_Sliders_Proxies_Column ( )
{
}


void
Mixer_Sliders_Proxies_Column::update_mixer_values ( )
{
	if ( slider_proxy() != 0 ) {
		static_cast < Mixer_Sliders_Proxy_Slider * > (
			slider_proxy() )->update_mixer_values();
	}
	if ( switch_proxy() != 0 ) {
		static_cast < Mixer_Sliders_Proxy_Switch * > (
			switch_proxy() )->update_mixer_values();
	}
}


bool
Mixer_Sliders_Proxies_Column::event (
	QEvent * event_n )
{
	Wdg::Key_Pass_Event * ev_kp (
		dynamic_cast < Wdg::Key_Pass_Event * > ( event_n ) );
	if ( ev_kp != 0 ) {
		if ( parent() != 0 ) {
			Wdg::Key_Pass_Event ev_pass ( *ev_kp, column_index() );
			QCoreApplication::sendEvent ( parent(), &ev_pass );
		}
		return true;
	}

	return Sliders_Pad_Proxies_Column::event ( event_n );
}


} // End of namespace
