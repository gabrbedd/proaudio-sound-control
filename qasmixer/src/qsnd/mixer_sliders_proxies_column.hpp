//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_mixer_sliders_proxies_column_hpp__
#define __INC_mixer_sliders_proxies_column_hpp__

#include "wdg/sliders_pad_proxies_column.hpp"
#include "mixer_sliders_proxy_slider.hpp"
#include "mixer_sliders_proxy_switch.hpp"


namespace QSnd
{


///
/// @brief Mixer_Sliders_Proxies_Column
///
class Mixer_Sliders_Proxies_Column :
	public Wdg::Sliders_Pad_Proxies_Column
{
	Q_OBJECT

	// Public methods
	public:

	Mixer_Sliders_Proxies_Column (
		QObject * parent_n = 0 );

	~Mixer_Sliders_Proxies_Column ( );


	void
	update_mixer_values ( );


	// Protected methods
	protected:

	bool
	event (
		QEvent * event_n );

};


} // End of namespace


#endif
