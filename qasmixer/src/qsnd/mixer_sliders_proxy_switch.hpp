//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_mixer_sliders_proxy_switch_hpp__
#define __INC_mixer_sliders_proxy_switch_hpp__

#include "wdg/sliders_pad_proxy_switch.hpp"
#include "snd_mixer_simple_elem.hpp"


namespace QSnd
{


///
/// @brief Mixer_Sliders_Proxy_Switch
///
class Mixer_Sliders_Proxy_Switch :
	public Wdg::Sliders_Pad_Proxy_Switch
{
	Q_OBJECT

	// Public methods
	public:

	Mixer_Sliders_Proxy_Switch (
		QObject * parent_n = 0 );


	Snd_Mixer_Simple_Elem *
	mixer_simple_elem ( ) const;

	void
	set_mixer_simple_elem (
		Snd_Mixer_Simple_Elem * selem_n );


	unsigned char
	snd_dir ( ) const;

	void
	set_snd_dir (
		unsigned char dir_n );


	unsigned int
	channel_idx ( ) const;

	void
	set_channel_idx (
		unsigned int idx_n );


	bool
	is_joined ( ) const;

	void
	set_is_joined (
		bool flag_n );


	// Public slots
	public slots:

	void
	update_mixer_values ( );


	// Protected methods
	protected:

	void
	switch_state_changed ( );


	// Private attributes
	private:

	Snd_Mixer_Simple_Elem * _mixer_simple_elem;

	unsigned int _channel_idx;

	unsigned char _snd_dir;

	bool _is_joined;

	bool _alsa_updating;
};


inline
Snd_Mixer_Simple_Elem *
Mixer_Sliders_Proxy_Switch::mixer_simple_elem ( ) const
{
	return _mixer_simple_elem;
}


inline
unsigned char
Mixer_Sliders_Proxy_Switch::snd_dir ( ) const
{
	return _snd_dir;
}


inline
unsigned int
Mixer_Sliders_Proxy_Switch::channel_idx ( ) const
{
	return _channel_idx;
}


inline
bool
Mixer_Sliders_Proxy_Switch::is_joined ( ) const
{
	return _is_joined;
}


} // End of namespace


#endif


