//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "mixer_sliders_status_bar.hpp"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFontMetrics>

#include <QApplication>

#include <iostream>


namespace QSnd
{


//
// Mixer_Sliders_Status_Bar
//
Mixer_Sliders_Status_Bar::Mixer_Sliders_Status_Bar (
	Wdg::Sliders_Pad & pad_n,
	QWidget * parent_n ) :
QFrame ( parent_n ),
_sliders_pad ( pad_n ),
_proxy_slider ( 0 ),
_volume_title ( this ),
_volume_value ( this ),
_volume_range ( this ),
_db_title ( this ),
_db_value ( this ),
_db_range ( this ),
_elem_name ( this )
{
	QFont fnt_title ( _volume_title.font() );
	fnt_title.setWeight ( QFont::Bold );

	const Qt::Alignment align_lc ( Qt::AlignLeft | Qt::AlignVCenter );
	const Qt::Alignment align_rc ( Qt::AlignRight | Qt::AlignVCenter );
	const Qt::Alignment align_cc ( Qt::AlignHCenter | Qt::AlignVCenter );


	_range_mask = "[%1; %2]";


	//
	// Volume
	//

	// Volume label
	_volume_title.setFont ( fnt_title );
	_volume_title.setText ( tr ( "Vol.:" ) );
	_volume_title.setToolTip ( tr ( "Volume" ) );

	{
		QSizePolicy policy ( _volume_title.sizePolicy() );
		policy.setHorizontalPolicy ( QSizePolicy::Fixed );
		_volume_title.setSizePolicy ( policy );
	}

	_volume_value.spinbox()->setSingleStep ( 1 );
	_volume_value.spinbox()->setKeyboardTracking ( false );


	// Volume value
	connect ( _volume_value.spinbox(), SIGNAL ( valueChanged ( int ) ),
		this, SLOT ( volume_value_changed ( int ) ) );

	_volume_value.setToolTip ( tr ( "Current volume" ) );

	{
		QSizePolicy policy ( _volume_value.sizePolicy() );
		policy.setHorizontalPolicy ( QSizePolicy::Fixed );
		_volume_value.setSizePolicy ( policy );
	}


	// Volume range
	_volume_range.setAlignment ( align_lc );
	_volume_range.setToolTip ( tr ( "Volume range" ) );
	_volume_range.setTextInteractionFlags ( Qt::TextSelectableByMouse );

	{
		QSizePolicy policy ( _volume_range.sizePolicy() );
		policy.setHorizontalPolicy ( QSizePolicy::Fixed );
		_volume_range.setSizePolicy ( policy );
	}

	{
		QString len_txt ( _range_mask );
		len_txt = len_txt.arg ( 0 );
		len_txt = len_txt.arg ( 255 );
		_volume_range.set_min_text ( len_txt );
	}


	//
	// Decibel
	//


	// Decibel label
	_db_title.setFont ( fnt_title );
	_db_title.setText ( tr ( "dB:" ) );
	_db_title.setToolTip ( tr ( "Decibel" ) );

	{
		QSizePolicy policy ( _db_title.sizePolicy() );
		policy.setHorizontalPolicy ( QSizePolicy::Fixed );
		_db_title.setSizePolicy ( policy );
	}

	// Decibel value
	_db_value.spinbox()->setButtonSymbols ( QAbstractSpinBox::NoButtons );
	_db_value.spinbox()->setSingleStep ( 0 );
	_db_value.spinbox()->setKeyboardTracking ( false );
	_db_value.set_field_width ( 0 );
	_db_value.set_format ( 'f' );
	_db_value.set_precision ( 2 );

	connect ( _db_value.spinbox(), SIGNAL ( valueChanged ( double ) ),
		this, SLOT ( db_value_changed ( double ) ) );

	_db_value.setToolTip ( tr ( "Current Decibel value" ) );

	{
		QSizePolicy policy ( _db_value.sizePolicy() );
		policy.setHorizontalPolicy ( QSizePolicy::Fixed );
		_db_value.setSizePolicy ( policy );
	}


	// Decibel limit
	_db_range.setAlignment ( align_lc );
	_db_range.setToolTip ( tr ( "Decibel range" ) );
	_db_range.setTextInteractionFlags ( Qt::TextSelectableByMouse );

	{
		QSizePolicy policy ( _db_range.sizePolicy() );
		policy.setHorizontalPolicy ( QSizePolicy::Fixed );
		_db_range.setSizePolicy ( policy );
	}

	{
		QString len_txt ( _range_mask );
		len_txt = len_txt.arg ( -99.99, 0, 'f', 2, '0' );
		len_txt = len_txt.arg ( 0.0, 0, 'f', 2, '0' );
		_db_range.set_min_text ( len_txt );
	}


	//
	// Element name
	//
	_elem_name_title.setFont ( fnt_title );
	_elem_name_title.setText ( tr ( "Elem.:" ) );
	_elem_name_title.setToolTip ( tr ( "Element name" ) );
	_elem_name.setToolTip ( tr ( "Element name" ) );


	//
	// Sliders pad event connection
	//

	connect ( &_sliders_pad, SIGNAL ( sig_focus_changed() ),
		this, SLOT ( slider_focus_changed() ) );


	//
	// Layout
	//

	{
		int hspace ( fontMetrics().averageCharWidth() * 2 );
		QBoxLayout * lay_hbox ( new QHBoxLayout() );
		lay_hbox->setContentsMargins ( 0, 0, 0, 0 );

		lay_hbox->addWidget ( &_volume_title );
		lay_hbox->addWidget ( &_volume_value );
		lay_hbox->addWidget ( &_volume_range );

		lay_hbox->addSpacing ( hspace );

		lay_hbox->addWidget ( &_db_title );
		lay_hbox->addWidget ( &_db_value );
		lay_hbox->addWidget ( &_db_range );

		lay_hbox->addSpacing ( hspace );

		lay_hbox->addWidget ( &_elem_name_title );
		lay_hbox->addWidget ( &_elem_name );
		lay_hbox->addStretch ( 0 );

		setLayout ( lay_hbox );
	}

	setup_values();
	update_values();
}


Mixer_Sliders_Status_Bar::~Mixer_Sliders_Status_Bar ( )
{
}


void
Mixer_Sliders_Status_Bar::slider_focus_changed ( )
{
	//std::cout << "Mixer_Sliders_Status_Bar::slider_focus_changed" << "\n";

	Mixer_Sliders_Proxy_Slider * proxy_new ( _proxy_slider );

	if ( _sliders_pad.focus_info().has_focus ) {

		unsigned int group_idx ( _sliders_pad.focus_info().group_idx );
		if ( group_idx < _sliders_pad.num_proxies_groups() ) {
			Wdg::Sliders_Pad_Proxies_Group * pgroup (
				_sliders_pad.proxies_group ( group_idx ) );

			unsigned int wdg_idx ( _sliders_pad.focus_info().column_idx );
			if ( wdg_idx < pgroup->num_sliders() ) {
				proxy_new = dynamic_cast < Mixer_Sliders_Proxy_Slider * > (
					pgroup->column ( wdg_idx )->slider_proxy() );
			}
		}
	}

	set_slider_proxy ( proxy_new );
}


void
Mixer_Sliders_Status_Bar::proxy_destroyed ( )
{
	set_slider_proxy ( 0 );
}


void
Mixer_Sliders_Status_Bar::set_slider_proxy (
	Mixer_Sliders_Proxy_Slider * proxy_n )
{
	//std::cout << "Mixer_Sliders_Status_Bar::set_slider_proxy " << proxy_n  << "\n";

	if ( _proxy_slider != proxy_n ) {

		if ( _proxy_slider != 0 ) {
			if ( _proxy_slider->mixer_simple_elem() != 0 ) {
				disconnect ( _proxy_slider->mixer_simple_elem(), 0, this, 0 );
			}

			disconnect ( _proxy_slider, 0, this, 0 );
		}

		_proxy_slider = proxy_n;

		setup_values();

		if ( _proxy_slider != 0 ) {

			if ( _proxy_slider->mixer_simple_elem() != 0 ) {
				connect ( _proxy_slider->mixer_simple_elem(), SIGNAL ( sig_values_changed() ),
					this, SLOT ( update_values() ) );
			}

			connect ( _proxy_slider, SIGNAL ( destroyed (  QObject * ) ),
				this, SLOT ( proxy_destroyed() ) );

			update_values();
		}
	}
}


void
Mixer_Sliders_Status_Bar::volume_value_changed (
	int value_n )
{
	if ( _proxy_slider != 0 ) {
		_proxy_slider->set_volume_value ( value_n );
	}
}


void
Mixer_Sliders_Status_Bar::db_value_changed (
	double value_n )
{
	if ( _proxy_slider != 0 ) {
		const long dB_long ( value_n * 100.0 );
		const long vol_near ( _proxy_slider->ask_dB_vol_nearest ( dB_long ) );
		const long dB_near = _proxy_slider->ask_vol_dB ( vol_near );
		if ( dB_long != dB_near ) {
			_db_value.blockSignals ( true );
			_db_value.set_value ( dB_near / 100.0 );
			_db_value.blockSignals ( false );
		}
		_proxy_slider->set_volume_value ( vol_near );
	}
}


void
Mixer_Sliders_Status_Bar::mixer_values_changed ( )
{
	update_values();
}


void
Mixer_Sliders_Status_Bar::update_values ( )
{
	if ( _proxy_slider != 0 ) {

		QString txt ( "%1" );
		{
			const long val ( _proxy_slider->volume_value() );
			_volume_value.blockSignals ( true );
			_volume_value.set_value ( val );
			_volume_value.blockSignals ( false );
		}

		if ( _proxy_slider->has_dB() ) {
			const double val ( _proxy_slider->dB_value() / 100.0 );
			_db_value.blockSignals ( true );
			_db_value.set_value ( val );
			_db_value.blockSignals ( false );
		}

	}
}


void
Mixer_Sliders_Status_Bar::setup_values ( )
{
	QString txt_special;
	QString txt_range;

	long vol_min;
	long vol_max;

	bool has_vol ( false );
	bool has_db ( false );

	if ( _proxy_slider != 0 ) {
		has_vol = true;
		has_db = _proxy_slider->has_dB();
		txt_special = _proxy_slider->group_name();
		if ( txt_special != _proxy_slider->item_name() ) {
			txt_special += " - ";
			txt_special += _proxy_slider->item_name();
		}
	}

	// Element name
	_elem_name.setText ( txt_special );

	// Volume
	//_volume_title.setEnabled ( has_vol );
	_volume_value.setEnabled ( has_vol );
	_volume_range.setEnabled ( has_vol );

	if ( has_vol ) {
		vol_min = _proxy_slider->volume_min();
		vol_max = _proxy_slider->volume_max();

		txt_special = QString();
		txt_range = _range_mask.arg ( vol_min ).arg ( vol_max );

		_volume_value.update_label();
	} else {
		vol_min = 0;
		vol_max = 0;

		txt_special = "-";
		txt_range = _range_mask.arg ( "-" ).arg ( "-" );

		_volume_value.label()->setText ( txt_special );
	}

	_volume_value.switch_to_label();

	_volume_value.spinbox()->blockSignals ( true );
	_volume_value.spinbox()->setSpecialValueText ( txt_special );
	_volume_value.set_range ( vol_min, vol_max );
	_volume_value.spinbox()->setValue ( vol_min ); // To show the special text on demand
	_volume_value.spinbox()->blockSignals ( false );

	_volume_range.setText ( txt_range );


	// Decibel

	_db_title.setEnabled ( has_db );
	_db_value.setEnabled ( has_db );
	_db_range.setEnabled ( has_db );

	if ( has_db ) {
		vol_min = _proxy_slider->dB_min();
		vol_max = _proxy_slider->dB_max();

		txt_special = QString();
		txt_range = _range_mask;
		txt_range = txt_range.arg ( vol_min / 100.0, 0, 'f', 2 );
		txt_range = txt_range.arg ( vol_max / 100.0, 0, 'f', 2 );

		_db_value.update_label();
	} else {
		vol_min = -9900;
		vol_max = 0;

		txt_special = "-";
		txt_range = _range_mask.arg ( "-" ).arg ( "-" );

		_db_value.label()->setText ( txt_special );
	}

	_db_value.switch_to_label();

	_db_value.spinbox()->blockSignals ( true );
	_db_value.spinbox()->setSpecialValueText ( txt_special );
	_db_value.set_range ( vol_min / 100.0, vol_max / 100.0 );
	_db_value.spinbox()->setValue ( vol_min / 100.0 ); // To show the special text on demand
	_db_value.spinbox()->blockSignals ( false );

	_db_range.setText ( txt_range );

	updateGeometry();
}


} // End of namespace

