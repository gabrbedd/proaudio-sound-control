//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_mixer_sliders_status_bar_hpp__
#define __INC_mixer_sliders_status_bar_hpp__

#include <QWidget>
#include <QFrame>
#include <QLabel>
#include <QComboBox>
#include <QCheckBox>
#include <QRadioButton>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QStackedLayout>

#include <QPointer>

#include "wdg/label_elide.hpp"
#include "wdg/label_width.hpp"
#include "wdg/covered_spinbox.hpp"
#include "mixer_sliders_proxies_group.hpp"
#include "wdg/sliders_pad.hpp"


namespace QSnd
{


///
/// @brief Mixer_Sliders_Status_Bar
///
class Mixer_Sliders_Status_Bar :
	public QFrame
{
	Q_OBJECT

	// Public methods
	public:

	Mixer_Sliders_Status_Bar (
		Wdg::Sliders_Pad & pad_n,
		QWidget * parent_n = 0 );

	~Mixer_Sliders_Status_Bar ( );


	// Public slots
	public slots:

	void
	mixer_values_changed ( );


	// Protected slots
	protected slots:

	void
	slider_focus_changed ( );

	void
	proxy_destroyed ( );


	void
	update_values ( );


	void
	volume_value_changed (
		int value_n );

	void
	db_value_changed (
		double value_n );


	// Protected methods
	protected:

	void
	setup_values ( );

	void
	set_slider_proxy (
		Mixer_Sliders_Proxy_Slider * proxy_n );



	// Private attributes
	private:

	Wdg::Sliders_Pad & _sliders_pad;

	Mixer_Sliders_Proxy_Slider * _proxy_slider;

	QString _range_mask;

	// Widgets

	QLabel _volume_title;
	Wdg::Covered_Spinbox _volume_value;
	Wdg::Label_Width _volume_range;

	QLabel _db_title;
	Wdg::Covered_Spinbox_Double _db_value;
	Wdg::Label_Width _db_range;

	Wdg::Label_Elide _elem_name_title;
	Wdg::Label_Elide _elem_name;
};


} // End of namespace


#endif
