//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "mixer_style.hpp"

#include "wdg/ds_slider_painter_basic.hpp"
#include "wdg/ds_check_button_painter_basic.hpp"
#include <QApplication>
#include <QPalette>


namespace QSnd
{


Mixer_Style::Mixer_Style ( )
{
	//
	// Palettes
	//

	const QPalette::ColorGroup grp_act ( QPalette::Active );
	const QPalette::ColorGroup grp_iact ( QPalette::Inactive );
	QColor col;

	// Playback palette
	{
		QPalette & pal ( _palettes[0] );
		pal = QApplication::palette();

		col = QColor ( 15, 15, 242 );
		pal.setColor ( grp_act, QPalette::Window, col );
		pal.setColor ( grp_iact, QPalette::Window, col );

		col = QColor ( 0, 0, 60 );
		pal.setColor ( grp_act, QPalette::WindowText, col );
		pal.setColor ( grp_iact, QPalette::WindowText, col );

		col = QColor ( 255, 255, 180 );
		pal.setColor ( grp_act, QPalette::Light, col );
		pal.setColor ( grp_iact, QPalette::Light, col );
	}

	// Capture palette
	{
		QPalette & pal ( _palettes[1] );
		pal = palette_play();

		col = QColor ( 225, 15, 15 );
		pal.setColor ( grp_act, QPalette::Window, col );
		pal.setColor ( grp_iact, QPalette::Window, col );

		col = QColor ( 80, 0, 0 );
		pal.setColor ( grp_act, QPalette::WindowText, col );
		pal.setColor ( grp_iact, QPalette::WindowText, col );
	}
}


Mixer_Style::~Mixer_Style ( )
{
}


} // End of namespace

