//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_wdg_mixer_style_hpp__
#define __INC_wdg_mixer_style_hpp__

#include <QPalette>


namespace QSnd
{


class Mixer_Style
{
	// Public methods
	public:

	Mixer_Style ( );

	~Mixer_Style ( );

	const QPalette &
	palette (
		unsigned int idx_n ) const;

	const QPalette &
	palette_play ( ) const;

	const QPalette &
	palette_cap ( ) const;


	// Private attributes
	private:

	QPalette _palettes[2];
};


inline
const QPalette &
Mixer_Style::palette (
	unsigned int idx_n ) const
{
	return _palettes[idx_n%2];
}


inline
const QPalette &
Mixer_Style::palette_play ( ) const
{
	return _palettes[0];
}


inline
const QPalette &
Mixer_Style::palette_cap ( ) const
{
	return _palettes[1];
}


} // End of namespace


#endif
