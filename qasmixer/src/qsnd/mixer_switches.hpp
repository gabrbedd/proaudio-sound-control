//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_mixer_switches_hpp__
#define __INC_mixer_switches_hpp__

#include "snd_mixer_simple.hpp"
#include "wdg/switches_area.hpp"
#include "wdg/switches_pad.hpp"
#include "simple_mixer_settings.hpp"
#include "mixer_ui_state.hpp"
#include "mixer_switches_proxies_group.hpp"

#include <QWidget>
#include <QMenu>
#include <QTimer>
#include <QPointer>


namespace QSnd
{


///
/// @brief Mixer_Switches
///
class Mixer_Switches :
	public QWidget
{
	Q_OBJECT

	// Public methods
	public:

	Mixer_Switches (
		QWidget * parent_n = 0 );

	~Mixer_Switches ( );


	// Size hints

	QSize
	minimumSizeHint ( ) const;

	QSize
	sizeHint ( ) const;


	// Mixer simple

	Snd_Mixer_Simple *
	mixer_simple ( ) const;

	void
	set_mixer_simple (
		Snd_Mixer_Simple * mixer_n );


	// Mixer settings

	const Simple_Mixer_Settings &
	mixer_settings ( ) const;

	void
	set_mixer_settings (
		const Simple_Mixer_Settings & cfg_n );



	unsigned int
	num_visible ( ) const;


	// Event handling

	bool
	event (
		QEvent * event_n );


	// Protected slots
	protected slots:

	void
	action_toggle_joined ( );

	void
	update_focus_proxies ( );

	void
	context_menu_cleanup_behind ( );


	// Protected methods
	protected:

	// Proxy group creation / deletion

	void
	clear_proxies_groups ( );

	void
	create_proxies_groups ( );

	void
	setup_proxies_group_joined (
		Mixer_Switches_Proxies_Group * mspg_n );

	void
	setup_proxies_group_separate (
		Mixer_Switches_Proxies_Group * mspg_n );

	Wdg::Switches_Pad_Proxy *
	create_proxy (
		Mixer_Switches_Proxies_Group * mspg_n,
		int channel_idx_n );


	// Proxy group manipulation

	bool
	should_be_visible (
		const Mixer_Switches_Proxies_Group * mspg_n ) const;

	void
	toggle_joined_separated (
		Mixer_Switches_Proxies_Group * mspg_n );

	void
	join_proxies_group (
		Mixer_Switches_Proxies_Group * mspg_n );

	void
	separate_proxies_group (
		Mixer_Switches_Proxies_Group * mspg_n );



	// Proxy group showing hiding

	void
	show_visible_proxies_sets (
		bool flag_n );


	// Proxy group updating

	void
	separate_where_requested ( );

	void
	rebuild_visible_proxies_list ( );


	// Focus proxy

	void
	acquire_ui_state (
		Mixer_State & state_n );

	void
	restore_ui_state (
		const Mixer_State & state_n );

	Mixer_Switches_Proxies_Group *
	find_visible_proxy (
		const Mixer_State_Proxy & prox_id_n );


	// Context menu

	void
	context_menu_start (
		const QPoint & pos_n );

	unsigned int
	context_menu_update ( );


	// Event callbacks

	bool
	eventFilter (
		QObject * watched,
		QEvent * event );


	/// @brief Convenience method that does a type cast
	const Mixer_Switches_Proxies_Group *
	proxies_group_visible (
		unsigned int idx_n ) const;

	/// @brief Convenience method that does a type cast
	Mixer_Switches_Proxies_Group *
	proxies_group_visible (
		unsigned int idx_n );


	/// @brief Switches pad getter
	const Wdg::Switches_Pad *
	switches_pad ( ) const;

	/// @brief Switches pad getter
	Wdg::Switches_Pad *
	switches_pad ( );


	// Private attributes
	private:

	Mixer_Switches_Proxy_Group_List _proxies_groups;

	Wdg::Switches_Pad_Proxies_Group_List _proxies_groups_visible;


	Wdg::Switches_Area _switches_area;

	Wdg::Switches_Pad _switches_pad;


	// Mixer objects

	Simple_Mixer_Settings _settings;

	Snd_Mixer_Simple * _mixer;


	bool _separation_requested;


	// Focus proxies

	QPointer < Mixer_Switches_Proxies_Group > _focus_proxies_group;
	unsigned int _focus_proxy_column;

	QPointer < Mixer_Switches_Proxies_Group > _act_proxies_group;
	unsigned int _act_proxy_column;


	// Context menu

	QMenu _cmenu;

	QAction _act_toggle_joined;

	QString _act_str_toggle_joined[2];
};


inline
Snd_Mixer_Simple *
Mixer_Switches::mixer_simple ( ) const
{
	return _mixer;
}


inline
const Simple_Mixer_Settings &
Mixer_Switches::mixer_settings ( ) const
{
	return _settings;
}


inline
const Mixer_Switches_Proxies_Group *
Mixer_Switches::proxies_group_visible (
	unsigned int idx_n ) const
{
	return static_cast < const Mixer_Switches_Proxies_Group * > (
		_proxies_groups_visible[idx_n] );
}


inline
Mixer_Switches_Proxies_Group *
Mixer_Switches::proxies_group_visible (
	unsigned int idx_n )
{
	return static_cast < Mixer_Switches_Proxies_Group * > (
		_proxies_groups_visible[idx_n] );
}


inline
unsigned int
Mixer_Switches::num_visible ( ) const
{
	return _proxies_groups_visible.size();
}


inline
const Wdg::Switches_Pad *
Mixer_Switches::switches_pad ( ) const
{
	return &_switches_pad;
}


inline
Wdg::Switches_Pad *
Mixer_Switches::switches_pad ( )
{
	return &_switches_pad;
}


} // End of namespace


#endif
