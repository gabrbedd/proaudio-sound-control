//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mixer_ui_state.hpp"


namespace QSnd
{


Mixer_State_Proxy::Mixer_State_Proxy ( )
{
	clear();
}


void
Mixer_State_Proxy::clear ( )
{
	_group_name.clear();
	_snd_dir = 0;
	_proxy_idx = 0;
}


} // End of namespace
