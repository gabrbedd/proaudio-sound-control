//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_mixer_ui_state_hpp__
#define __INC_mixer_ui_state_hpp__

#include <QString>


namespace QSnd
{


class Mixer_State_Proxy
{
	// Public methods
	public:

	Mixer_State_Proxy ( );

	void
	clear ( );

	bool
	is_clear ( ) const;


	const QString &
	group_name ( ) const;

	void
	set_group_name (
		const QString & name_n );


	unsigned char
	snd_dir ( ) const;

	void
	set_snd_dir (
		unsigned char dir_n );


	unsigned char
	proxy_idx ( ) const;

	void
	set_proxy_idx (
		unsigned char idx_n );



	// Private attributes
	private:

	QString _group_name;

	unsigned char _snd_dir;

	unsigned char _proxy_idx;
};


inline
bool
Mixer_State_Proxy::is_clear ( ) const
{
	return _group_name.isEmpty();
}


inline
const QString &
Mixer_State_Proxy::group_name ( ) const
{
	return _group_name;
}


inline
void
Mixer_State_Proxy::set_group_name (
	const QString & name_n )
{
	_group_name = name_n;
}


inline
unsigned char
Mixer_State_Proxy::snd_dir ( ) const
{
	return _snd_dir;
}


inline
void
Mixer_State_Proxy::set_snd_dir (
	unsigned char dir_n )
{
	_snd_dir = dir_n;
}


inline
unsigned char
Mixer_State_Proxy::proxy_idx ( ) const
{
	return _proxy_idx;
}


inline
void
Mixer_State_Proxy::set_proxy_idx (
	unsigned char idx_n )
{
	_proxy_idx = idx_n;
}



class Mixer_State
{
	// Public methods
	public:

	Mixer_State_Proxy focus_proxy;
	Mixer_State_Proxy action_proxy;
};


} // End of namespace


#endif
