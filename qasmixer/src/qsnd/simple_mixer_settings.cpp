//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "simple_mixer_settings.hpp"


namespace QSnd
{


Simple_Mixer_Settings::Simple_Mixer_Settings ( )
{
	show_slider_status_bar = true;
	show_stream[0] = true;
	show_stream[1] = true;
}


bool
Simple_Mixer_Settings::operator == (
	const Simple_Mixer_Settings & cfg_n ) const
{
	return
		( show_slider_status_bar == cfg_n.show_slider_status_bar ) &&
		( show_stream[0] == cfg_n.show_stream[0] ) &&
		( show_stream[1] == cfg_n.show_stream[1] );
}


bool
Simple_Mixer_Settings::operator != (
	const Simple_Mixer_Settings & cfg_n ) const
{
	return ! ( operator== ( cfg_n ) );
}


} // End of namespace
