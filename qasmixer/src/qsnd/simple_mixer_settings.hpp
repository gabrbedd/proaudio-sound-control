//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_simple_mixer_settings_hpp__
#define __INC_simple_mixer_settings_hpp__


namespace QSnd
{


///
/// @brief Simple_Mixer_Settings
///
class Simple_Mixer_Settings
{
	// Public methods
	public:

	Simple_Mixer_Settings ( );

	bool
	operator == (
		const Simple_Mixer_Settings & cfg_n ) const;

	bool
	operator != (
		const Simple_Mixer_Settings & cfg_n ) const;


	// Public attributes

	bool show_slider_status_bar;

	bool show_stream[2];
};


} // End of namespace


#endif
