//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_amix_qsnd_card_info_hpp__
#define __INC_amix_qsnd_card_info_hpp__

#include <QString>
#include "alsa.hpp"


namespace QSnd
{


///
/// @brief Snd_Card_Info
///
class Snd_Card_Info
{
	// Public methods
	public:

	Snd_Card_Info ( );

	void
	clear ( );

	bool
	is_clear ( ) const;


	int
	acquire_info (
		const int hw_idx_n );

	int
	acquire_info (
		const QString & dev_str_n );

	int
	acquire_info (
		snd_hctl_t * snd_hctl_n );


	int
	card_index ( ) const;

	const QString &
	card_id ( ) const;

	const QString &
	card_driver ( ) const;

	const QString &
	card_name ( ) const;

	const QString &
	card_long_name ( ) const;

	const QString &
	card_mixer_name ( ) const;

	const QString &
	card_components ( ) const;


	// Private attributes
	private:

	int _card_index;

	QString _strings[6];
};


inline
int
Snd_Card_Info::card_index ( ) const
{
	return _card_index;
}


inline
const QString &
Snd_Card_Info::card_id ( ) const
{
	return _strings[0];
}


inline
const QString &
Snd_Card_Info::card_driver ( ) const
{
	return _strings[1];
}


inline
const QString &
Snd_Card_Info::card_name ( ) const
{
	return _strings[2];
}


inline
const QString &
Snd_Card_Info::card_long_name ( ) const
{
	return _strings[3];
}


inline
const QString &
Snd_Card_Info::card_mixer_name ( ) const
{
	return _strings[4];
}


inline
const QString &
Snd_Card_Info::card_components ( ) const
{
	return _strings[5];
}


} // End of namespace


#endif
