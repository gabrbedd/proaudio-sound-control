//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "snd_control_address.hpp"


namespace QSnd
{


//
// Snd_Control_Address_Arg
//

Snd_Control_Address_Arg::Snd_Control_Address_Arg (
	const QString & name_n,
	const QString & value_n,
	const QString & type_n ) :
arg_name ( name_n ),
arg_value ( value_n ),
arg_type ( type_n )
{
}


void
Snd_Control_Address_Arg::clear ( )
{
	arg_name.clear();
	arg_value.clear();
	arg_type = "string";
}


Snd_Control_Address_Arg &
Snd_Control_Address_Arg::operator = (
	const Snd_Control_Address_Arg & cinfo_n )
{
	arg_name = cinfo_n.arg_name;
	arg_value = cinfo_n.arg_value;
	arg_type = cinfo_n.arg_type;

	return *this;
}


//
// Snd_Control_Address
//

Snd_Control_Address::Snd_Control_Address (
	const QString & ctl_id_n )
{
	if ( !ctl_id_n.isEmpty() ) {
		set_ctl_id ( ctl_id_n );
	}
}


void
Snd_Control_Address::clear ( )
{
	_ctl_name.clear();
	_arguments.clear();
	_ctl_id.clear();
}


void
Snd_Control_Address::set_ctl_name (
	const QString & name_n )
{
	_ctl_name = name_n;
	update_ctl_id();
}


void
Snd_Control_Address::set_ctl_id (
	const QString & id_n )
{
	clear();

	QString pstr ( id_n );

	int idx;
	idx = pstr.indexOf ( ':' );
	if ( idx < 0 ) {
		_ctl_name = id_n;
	} else if ( idx > 1 ) {
		_ctl_name = pstr.left ( idx );
		pstr = pstr.mid ( idx + 1 );

		QString arg_str;
		while ( !pstr.isEmpty() ) {
			idx = pstr.indexOf ( ',' );
			if ( idx < 0 ) {
				arg_str = pstr;
				pstr.clear();
			} else {
				arg_str = pstr.left ( idx );
				pstr = pstr.mid ( idx + 1 );
			}

			Snd_Control_Address_Arg argm;

			idx = arg_str.indexOf ( '=' );
			if ( idx < 0 ) {
				argm.arg_value = arg_str;
			} else {
				argm.arg_name = arg_str.left ( idx );
				argm.arg_value = arg_str.mid ( idx + 1 );
			}

			if ( !argm.arg_value.isEmpty() ) {
				_arguments.append ( argm );
			}
		}
	}

	update_ctl_id();
}


void
Snd_Control_Address::append_arg (
	const Snd_Control_Address_Arg & arg_n )
{
	_arguments.append ( arg_n );
	update_ctl_id();
}


void
Snd_Control_Address::set_arg_value (
	int idx_n,
	const QString & value_n )
{
	_arguments[idx_n].arg_value = value_n;
}


void
Snd_Control_Address::set_arg_value (
	const QString & arg_name_n,
	const QString & value_n )
{
	for ( int ii=0; ii < _arguments.size(); ++ii ) {
		Snd_Control_Address_Arg & argm ( _arguments[ii] );
		if ( argm.arg_name == arg_name_n ) {
			argm.arg_value = value_n;
			break;
		}
	}
}


void
Snd_Control_Address::update_ctl_id ( )
{
	_ctl_id = _ctl_name;
	if ( _arguments.size() > 0 ) {
		_ctl_id += ":";
		for ( int ii=0; ii < _arguments.size(); ++ii ) {
			const Snd_Control_Address_Arg & argm ( arg ( ii ) );
			if ( ii > 0 ) {
				_ctl_id += ",";
			}

			if ( !argm.arg_name.isEmpty() ) {
				_ctl_id += argm.arg_name;
				_ctl_id += "=";
			}
			_ctl_id += argm.arg_value;
		}
	}
}


Snd_Control_Address &
Snd_Control_Address::operator = (
	const Snd_Control_Address & cinfo_n )
{
	_ctl_name = cinfo_n.ctl_name();
	_arguments.clear();
	for ( int ii=0; ii < cinfo_n.num_args(); ++ii ) {
		_arguments.append ( cinfo_n.arg ( ii ) );
	}
	update_ctl_id();
	return *this;
}


bool
Snd_Control_Address::operator == (
	const Snd_Control_Address & cinfo_n ) const
{
	bool res = ( _ctl_name == cinfo_n.ctl_name() );
	res = res && ( num_args() == cinfo_n.num_args() );
	if ( res ) {
		for ( int ii=0; ii < cinfo_n.num_args(); ++ii ) {
			Snd_Control_Address_Arg arg1 ( arg ( ii ) );
			Snd_Control_Address_Arg arg2 ( cinfo_n.arg ( ii ) );
			if ( arg1.arg_value == arg2.arg_value ) {
				if ( ( !arg1.arg_name.isEmpty() ) &&
				     ( !arg2.arg_name.isEmpty() ) )
				{
					if ( arg1.arg_name != arg2.arg_name ) {
						res = false;
						break;
					}
				}
			} else {
				res = false;
				break;
			}
		}
	}
	return res;
}


bool
Snd_Control_Address::operator != (
	const Snd_Control_Address & cinfo_n ) const
{
	return !operator== ( cinfo_n );
}


} // End of namespace

