//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_snd_control_address_hpp__
#define __INC_snd_control_address_hpp__

#include <QString>
#include <QList>


namespace QSnd
{


///
/// @brief Snd_Control_Address_Arg
///
class Snd_Control_Address_Arg
{
	// Public methods
	public:

	Snd_Control_Address_Arg (
		const QString & name_n = QString(),
		const QString & value_n = QString(),
		const QString & type_n = QString ( "string" ) );

	void
	clear ( );


	// Public attributes

	QString arg_name;
	QString arg_value;

	QString arg_type;

	Snd_Control_Address_Arg &
	operator = (
		const Snd_Control_Address_Arg & cinfo_n );
};


///
/// @brief Snd_Control_Address
///
class Snd_Control_Address
{
	// Public methods
	public:

	Snd_Control_Address (
		const QString & ctl_id_n = QString() );

	void
	clear ( );

	bool
	is_clear ( ) const;


	// Control name

	const QString &
	ctl_name ( ) const;

	void
	set_ctl_name (
		const QString & name_n );


	// Control id string

	const QString &
	ctl_id ( ) const;

	void
	set_ctl_id (
		const QString & id_n );


	// Control arguments

	int
	num_args ( ) const;

	const Snd_Control_Address_Arg &
	arg (
		int idx_n ) const;

	void
	append_arg (
		const Snd_Control_Address_Arg & arg_n );

	void
	set_arg_value (
		int idx_n,
		const QString & value_n );

	void
	set_arg_value (
		const QString & arg_name_n,
		const QString & value_n );






	// Operators

	Snd_Control_Address &
	operator = (
		const Snd_Control_Address & cinfo_n );

	bool
	operator == (
		const Snd_Control_Address & cinfo_n ) const;

	bool
	operator != (
		const Snd_Control_Address & cinfo_n ) const;


	// Protected methods
	protected:

	void
	update_ctl_id ( );


	// Private attributes
	private:

	QString _ctl_name;

	QList < Snd_Control_Address_Arg > _arguments;

	QString _ctl_id;
};


inline
bool
Snd_Control_Address::is_clear ( ) const
{
	return _ctl_name.isEmpty();
}


inline
const QString &
Snd_Control_Address::ctl_name ( ) const
{
	return _ctl_name;
}


inline
int
Snd_Control_Address::num_args ( ) const
{
	return _arguments.size();
}


inline
const Snd_Control_Address_Arg &
Snd_Control_Address::arg (
	int idx_n ) const
{
	return _arguments[idx_n];
}


inline
const QString &
Snd_Control_Address::ctl_id ( ) const
{
	return _ctl_id;
}


} // End of namespace


#endif


