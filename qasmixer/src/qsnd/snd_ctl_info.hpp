//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_snd_ctl_info_hpp__
#define __INC_snd_ctl_info_hpp__

#include "alsa.hpp"
#include "snd_pcm_device_info.hpp"
#include "snd_card_info.hpp"

#include <QObject>
#include <QString>
#include <QList>


namespace QSnd
{


///
/// @brief Snd_CTL_Info
///
class Snd_CTL_Info :
	public QObject
{
	Q_OBJECT

	// Public methods
	public:

	Snd_CTL_Info ( );

	~Snd_CTL_Info ( );


	// Open / close

	int
	open (
		const QString & plug_str_n );

	bool
	is_open ( ) const;

	void
	close ( );

	const QString &
	ctl_name ( ) const;


	// Error strings / codes

	const QString &
	err_func ( ) const;

	const QString &
	err_message ( ) const;


	// Card info

	const Snd_Card_Info *
	card_info ( ) const;


	// Device info

	unsigned int
	num_devices ( ) const;

	const Snd_PCM_Device_Info *
	device_info (
		unsigned int idx_n ) const;


	void
	request_reload ( );


	bool
	event (
		QEvent * event_n );


	// Alsa callbacks

	static
	int
	alsa_callback_hctl_info (
		snd_hctl_t * snd_hctl_n,
		unsigned int mask_n,
		snd_hctl_elem_t * );


	// Signals
	signals:

	void
	sig_reload_request ( );


	// Protected methods
	protected:

	int
	acquire_devices_info ( );


	// Private attributes
	private:

	QString _ctl_name;

	Snd_Card_Info _card_info;

	QList < Snd_PCM_Device_Info * > _devices_info;

	snd_hctl_t * _snd_hctl_handle;

	bool _reload_requested;


	QString _err_func;

	QString _err_message;
};


inline
const QString &
Snd_CTL_Info::ctl_name ( ) const
{
	return _ctl_name;
}


inline
const Snd_Card_Info *
Snd_CTL_Info::card_info ( ) const
{
	return &_card_info;
}


inline
bool
Snd_CTL_Info::is_open ( ) const
{
	return ( _snd_hctl_handle != 0 );
}


inline
const QString &
Snd_CTL_Info::err_func ( ) const
{
	return _err_func;
}


inline
const QString &
Snd_CTL_Info::err_message ( ) const
{
	return _err_message;
}


inline
unsigned int
Snd_CTL_Info::num_devices ( ) const
{
	return _devices_info.size();
}


inline
const Snd_PCM_Device_Info *
Snd_CTL_Info::device_info (
	unsigned int idx_n ) const
{
	return _devices_info[idx_n];
}


} // End of namespace

#endif
