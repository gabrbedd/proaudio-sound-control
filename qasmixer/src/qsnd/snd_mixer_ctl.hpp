//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_snd_mixer_ctl_hpp__
#define __INC_snd_mixer_ctl_hpp__

#include <QObject>
#include <QList>
#include <QSocketNotifier>

#include "alsa.hpp"
#include "snd_mixer_ctl_data.hpp"
#include "snd_mixer_ctl_elem.hpp"
#include "mixer_events.hpp"


namespace QSnd
{


///
/// @brief Snd_Mixer_CTL
///
/// Brings Qt and ALSA objects together but without
/// any GUI objects
///
class Snd_Mixer_CTL :
    public QObject
{
	Q_OBJECT

	// Public methods
	public:

	Snd_Mixer_CTL (
		QObject * parent_n = 0 );

	~Snd_Mixer_CTL ( );


	const Snd_Mixer_CTL_Data &
	data ( ) const;

	snd_hctl_t *
	snd_hctl ( );


	// Init / close

	/// @return On an error a negative error code
	int
	open (
		const QString & dev_name_n );

	void
	close ( );

	bool
	is_open ( ) const;


	// Error strings / codes

	const QString &
	err_func ( ) const;

	const QString &
	err_message ( ) const;


	// Elements

	unsigned int
	num_elems ( ) const;

	const Snd_Mixer_CTL_Elem *
	elem (
		unsigned int idx_n ) const;

	Snd_Mixer_CTL_Elem *
	elem (
		unsigned int idx_n );


	// Interface types

	unsigned int
	iface_type_count (
		unsigned int type_idx_n ) const;

	unsigned int
	iface_types_avail ( ) const;

	unsigned int
	iface_avail_type (
		unsigned int type_idx_n ) const;


	// Alsa callbacks

	static
	int
	alsa_callback_hctl (
		snd_hctl_t * snd_hctl_n,
		unsigned int mask_n,
		snd_hctl_elem_t * elem_n );


	// Signals
	signals:

	void
	sig_mixer_reload_request ( );


	// Public slots
	public slots:

	void
	request_reload ( );


	// Protected slots
	protected slots:

	void
	socket_event (
		int socket_id );


	// Protected methods
	protected:

	int
	create_mixer_elems ( );


	// Socket notifiers

	int
	create_socket_notifiers ( );

	void
	set_socked_notifiers_enabled (
		bool flag_n );


	void
	signalize_all_changes ( );


	bool
	event (
		QEvent * event_n );


	// Private attributes
	private:

	Snd_Mixer_CTL_Data _data;

	snd_hctl_t * _snd_hctl;

	QList < Snd_Mixer_CTL_Elem * > _mixer_elems;

	std::vector < pollfd > _pollfds;

	QList < QSocketNotifier * > _socket_notifiers;

	unsigned int _iface_num_types;

	unsigned int _iface_avail_types[7];

	unsigned int _iface_type_count[7];

	bool _update_requested;

	bool _reload_requested;


	QString _err_func;

	QString _err_message;
};


inline
const Snd_Mixer_CTL_Data &
Snd_Mixer_CTL::data ( ) const
{
	return _data;
}


inline
snd_hctl_t *
Snd_Mixer_CTL::snd_hctl ( )
{
	return _snd_hctl;
}


inline
bool
Snd_Mixer_CTL::is_open ( ) const
{
	return ( _snd_hctl != 0 );
}


inline
const QString &
Snd_Mixer_CTL::err_func ( ) const
{
	return _err_func;
}


inline
const QString &
Snd_Mixer_CTL::err_message ( ) const
{
	return _err_message;
}


// Elements

inline
unsigned int
Snd_Mixer_CTL::num_elems ( ) const
{
	return _mixer_elems.size();
}


inline
const Snd_Mixer_CTL_Elem *
Snd_Mixer_CTL::elem (
	unsigned int idx_n ) const
{
	return _mixer_elems[idx_n];
}


inline
Snd_Mixer_CTL_Elem *
Snd_Mixer_CTL::elem (
	unsigned int idx_n )
{
	return _mixer_elems[idx_n];
}


// Interface types


inline
unsigned int
Snd_Mixer_CTL::iface_type_count (
	unsigned int type_idx_n ) const
{
	return _iface_type_count[type_idx_n];
}


inline
unsigned int
Snd_Mixer_CTL::iface_types_avail ( ) const
{
	return _iface_num_types;
}


inline
unsigned int
Snd_Mixer_CTL::iface_avail_type (
	unsigned int type_idx_n ) const
{
	return _iface_avail_types[type_idx_n];
}


} // End of namespace


#endif
