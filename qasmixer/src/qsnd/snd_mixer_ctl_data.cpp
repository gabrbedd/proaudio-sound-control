//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "snd_mixer_ctl_data.hpp"

#include <iostream>
#include <QEvent>
#include <QCoreApplication>


namespace QSnd
{


//
// Snd_Mixer_CTL_Data
//
Snd_Mixer_CTL_Data::Snd_Mixer_CTL_Data ( )
{

	//
	// Element type names
	//

	for ( unsigned int ii=0; ii < num_iface_types(); ++ii ) {
		const char * if_name (
			snd_ctl_elem_type_name ( element_idx_type ( ii ) ) );
		_etype_names[ii] = if_name;
		_etype_display_names[ii] = QCoreApplication::translate (
			"ALSA::CTL_Elem_Type_Name", if_name );

		// Fix untranslated names
		if ( _etype_names[ii] == _etype_display_names[ii] ) {
			QString & idn ( _etype_display_names[ii] );
			if ( idn == "NONE" ) {
				idn = "None";
			} else if ( idn == "BOOLEAN" ) {
				idn = "Boolean";
			} else if ( idn == "INTEGER" ) {
				idn = "Integer";
			} else if ( idn == "ENUMERATED" ) {
				idn = "Enumerated";
			} else if ( idn == "BYTES" ) {
				idn = "Bytes";
			} else if ( idn == "IEC958" ) {
				idn = "IEC958";
			} else if ( idn == "INTEGER64" ) {
				idn = "Integer64";
			}
		}
	}

	{
		const unsigned int idx ( num_element_types() );
		_etype_names[idx] = QCoreApplication::translate (
			"ALSA::CTL_Elem_Type_Name", "Unknown" );
		_etype_display_names[idx] = _etype_names[idx];
	}



	//
	// Interface names
	//

	for ( unsigned int ii=0; ii < num_iface_types(); ++ii ) {
		const char * if_name = snd_ctl_elem_iface_name ( iface_idx_type ( ii ) );
		_iface_names[ii] = if_name;
		_iface_display_names[ii] = QCoreApplication::translate (
			"ALSA::CTL_Elem_IFace_Name", if_name );

		// Fix untranslated names
		if ( _iface_names[ii] == _iface_display_names[ii] ) {
			QString & idn ( _iface_display_names[ii] );
			if ( idn == "CARD" ) {
				idn = "Card";
			} else if ( idn == "HWDEP" ) {
				idn = "Hw dep.";
			} else if ( idn == "MIXER" ) {
				idn = "Mixer";
			} else if ( idn == "PCM" ) {
				idn = "PCM";
			} else if ( idn == "RAWMIDI" ) {
				idn = "Raw midi";
			} else if ( idn == "TIMER" ) {
				idn = "Timer";
			} else if ( idn == "SEQUENCER" ) {
				idn = "Sequencer";
			}
		}
	}

	{
		const unsigned int idx ( num_iface_types() );
		_iface_names[idx] = QCoreApplication::translate (
			"ALSA::CTL_Elem_IFace_Name", "Unknown" );
		_iface_display_names[idx] = _iface_names[idx];
	}

}


const QString &
Snd_Mixer_CTL_Data::elem_type_name (
	snd_ctl_elem_type_t type_n ) const
{
	return elem_type_name ( element_type_idx ( type_n ) );
}


const QString &
Snd_Mixer_CTL_Data::elem_type_display_name (
	snd_ctl_elem_type_t type_n ) const
{
	return elem_type_display_name ( element_type_idx ( type_n ) );
}


const QString &
Snd_Mixer_CTL_Data::iface_name (
	snd_ctl_elem_iface_t type_n ) const
{
	return iface_name ( iface_type_idx ( type_n ) );
}


const QString &
Snd_Mixer_CTL_Data::iface_display_name (
	snd_ctl_elem_iface_t type_n ) const
{
	return iface_display_name ( iface_type_idx ( type_n ) );
}


} // End of namespace
