//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_snd_mixer_simple_hpp__
#define __INC_snd_mixer_simple_hpp__

#include <vector>
#include <QObject>
#include <QList>
#include <QSocketNotifier>

#include "snd_mixer_simple_elem.hpp"
#include "mixer_events.hpp"


namespace QSnd
{


///
/// @brief Snd_Mixer_Simple
///
/// Brings Qt and ALSA objects together but without
/// any GUI objects
///
class Snd_Mixer_Simple :
    public QObject
{
	Q_OBJECT

	// Public methods
	public:

	Snd_Mixer_Simple (
		QObject * parent_n = 0 );

	~Snd_Mixer_Simple ( );


	snd_hctl_t *
	snd_hctl ( );

	snd_mixer_t *
	snd_mixer ( );


	// Elements

	unsigned int
	num_elems ( ) const;

	const Snd_Mixer_Simple_Elem *
	elem (
		unsigned int idx_n ) const;

	Snd_Mixer_Simple_Elem *
	elem (
		unsigned int idx_n );


	// open / close

	/// Opens a simple mixer
	/// @return On an error a negative error code
	int
	open (
		const QString & dev_name_n );

	void
	close ( );

	bool
	is_open ( ) const;


	// Error strings / codes

	const QString &
	err_func ( ) const;

	const QString &
	err_message ( ) const;


	// Event handling

	bool
	event (
		QEvent * event_n );


	// Alsa callbacks

	static
	int
	alsa_callback_mixer (
		snd_mixer_t * snd_mixer_n,
		unsigned int mask_n,
		snd_mixer_elem_t * );


	// Signals
	signals:

	void
	sig_mixer_reload_request ( );


	// Public slots
	public slots:

	void
	request_reload ( );


	// Protected slots
	protected slots:

	void
	socket_event (
		int socket_id );


	// Protected methods
	protected:

	// Mixer elements

	void
	append_mixer_elem (
		Snd_Mixer_Simple_Elem * mx_elem_n );

	void
	clear_mixer_elems ( );

	virtual
	int
	create_mixer_elems ( );

	Snd_Mixer_Simple_Elem *
	create_mixer_elem (
		snd_mixer_elem_t * elem_n );

	/// @brief Filters elements from beeing used
	/// @return 1 if the element didn't pass the filter 0 otherwise
	int
	filter_mixer_elem (
		snd_mixer_elem_t * elem_n );


	// Socket notifiers

	int
	create_socket_notifiers ( );

	void
	set_socked_notifiers_enabled (
		bool flag_n );


	void
	signalize_all_changes ( );


	// Private attributes
	private:

	snd_hctl_t * _snd_hctl;

	snd_mixer_t * _snd_mixer;

	QList < Snd_Mixer_Simple_Elem * > _mixer_elems;

	std::vector < pollfd > _pollfds;

	QList < QSocketNotifier * > _socket_notifiers;

	bool _update_requested;

	bool _reload_requested;


	QString _err_func;

	QString _err_message;
};


inline
bool
Snd_Mixer_Simple::is_open ( ) const
{
	return ( _snd_mixer != 0 );
}


inline
const QString &
Snd_Mixer_Simple::err_func ( ) const
{
	return _err_func;
}


inline
const QString &
Snd_Mixer_Simple::err_message ( ) const
{
	return _err_message;
}


inline
snd_hctl_t *
Snd_Mixer_Simple::snd_hctl ( )
{
	return _snd_hctl;
}


inline
snd_mixer_t *
Snd_Mixer_Simple::snd_mixer ( )
{
	return _snd_mixer;
}



// Elements

inline
unsigned int
Snd_Mixer_Simple::num_elems ( ) const
{
	return _mixer_elems.size();
}


inline
const Snd_Mixer_Simple_Elem *
Snd_Mixer_Simple::elem (
	unsigned int idx_n ) const
{
	return _mixer_elems[idx_n];
}


inline
Snd_Mixer_Simple_Elem *
Snd_Mixer_Simple::elem (
	unsigned int idx_n )
{
	return _mixer_elems[idx_n];
}


inline
void
Snd_Mixer_Simple::append_mixer_elem (
	Snd_Mixer_Simple_Elem * mx_elem_n )
{
	if ( mx_elem_n != 0 ) {
		_mixer_elems.append ( mx_elem_n );
	}
}


} // End of namespace


#endif
