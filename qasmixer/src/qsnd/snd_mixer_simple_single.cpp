//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "snd_mixer_simple_single.hpp"

#include <iostream>
#include <QEvent>
#include <QCoreApplication>


namespace QSnd
{


//
// Snd_Mixer_Simple
//
Snd_Mixer_Simple_Single::Snd_Mixer_Simple_Single (
	QObject * parent_n ) :
Snd_Mixer_Simple ( parent_n ),
_elem_name ( "Master" )
{
}


Snd_Mixer_Simple_Single::~Snd_Mixer_Simple_Single ( )
{
}


void
Snd_Mixer_Simple_Single::set_elem_name (
	const QString & name_n )
{
	if ( name_n != _elem_name ) {
		clear_mixer_elems();
		_elem_name = name_n;
		create_mixer_elems();
	}
}


int
Snd_Mixer_Simple_Single::create_mixer_elems ( )
{
	if ( !is_open() ) {
		return -1;
	}

	unsigned int idx ( 0 );

	// Find element index
	{
		unsigned int ii ( 0 );
		snd_mixer_elem_t * elem = snd_mixer_first_elem ( snd_mixer() );
		while ( elem != 0 )	{
			if ( _elem_name == snd_mixer_selem_get_name ( elem ) ) {
				idx = ii;
				break;
			}
			++ii;
			elem = snd_mixer_elem_next ( elem );
		}
	}

	Snd_Mixer_Simple_Elem * mx_elem ( 0 );

	// Create mixer element
	{
		unsigned int ii ( 0 );
		snd_mixer_elem_t * elem = snd_mixer_first_elem ( snd_mixer() );
		while ( elem != 0 )	{
			if ( ii == idx ) {
				mx_elem = create_mixer_elem ( elem );
				break;
			}
			++ii;
			elem = snd_mixer_elem_next ( elem );
		}
	}

	if ( mx_elem != 0 ) {
		append_mixer_elem ( mx_elem );
	} else {
		return -1;
	}

	return 0;
}


} // End of namespace

