//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_snd_mixer_simple_single_hpp__
#define __INC_snd_mixer_simple_single_hpp__

#include "snd_mixer_simple.hpp"


namespace QSnd
{


///
/// @brief Snd_Mixer_Simple_Single
///
///
class Snd_Mixer_Simple_Single :
    public Snd_Mixer_Simple
{
	Q_OBJECT

	// Public methods
	public:

	Snd_Mixer_Simple_Single (
		QObject * parent_n = 0 );

	~Snd_Mixer_Simple_Single ( );


	void
	set_elem_name (
		const QString & name_n );


	// Protected methods
	protected:

	virtual
	int
	create_mixer_elems ( );


	// Private attributes
	private:

	QString _elem_name;
};


} // End of namespace


#endif
