//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "snd_pcm_device_info.hpp"

#include <iostream>

namespace QSnd
{


//
// Snd_PCM_Subdevice_Info
//

Snd_PCM_Subdevice_Info::Snd_PCM_Subdevice_Info (
	snd_pcm_info_t * pcm_info_n )
{
	acquire_info ( pcm_info_n );
}


int
Snd_PCM_Subdevice_Info::acquire_info (
	snd_pcm_info_t * pcm_info_n )
{
	if ( pcm_info_n != 0 ) {
		_dev_index = snd_pcm_info_get_subdevice ( pcm_info_n );
		_dev_name = snd_pcm_info_get_subdevice_name ( pcm_info_n );
	} else {
		_dev_index = 0;
		_dev_name.clear();
	}

	return 0;
}



//
// Snd_PCM_Subdevices_Info
//


Snd_PCM_Subdevices_Info::Snd_PCM_Subdevices_Info ( ) :
_stream_dir ( 0 ),
_num_sdevs ( 0 ),
_num_sdevs_avail ( 0 )
{
}


Snd_PCM_Subdevices_Info::~Snd_PCM_Subdevices_Info ( )
{
	clear();
}


void
Snd_PCM_Subdevices_Info::clear ( )
{
	_stream_dir = 0;
	_num_sdevs = 0;
	_num_sdevs_avail = 0;

	if ( _sdevs_info.size() > 0 ) {
		for ( int ii = 0; ii < _sdevs_info.size(); ++ii ) {
			delete _sdevs_info[ii];
		}
		_sdevs_info.clear();
	}
}


int
Snd_PCM_Subdevices_Info::acquire_subdevices (
	snd_ctl_t * snd_ctl_handle_n,
	int dev_idx_n,
	unsigned int stream_dir_n )
{
	int err ( 0 );

	clear();
	_stream_dir = stream_dir_n;

	snd_pcm_stream_t pcm_stream ( SND_PCM_STREAM_PLAYBACK );
	if ( _stream_dir > 0 ) {
		pcm_stream = SND_PCM_STREAM_CAPTURE;
	}

	snd_pcm_info_t * pcm_info;
	snd_pcm_info_alloca ( &pcm_info );

	snd_pcm_info_set_device ( pcm_info, dev_idx_n );
	snd_pcm_info_set_subdevice ( pcm_info, 0 );
	snd_pcm_info_set_stream ( pcm_info, pcm_stream );

	err = snd_ctl_pcm_info ( snd_ctl_handle_n, pcm_info );
	if ( err >= 0 ) {

		_num_sdevs = snd_pcm_info_get_subdevices_count ( pcm_info );
		_num_sdevs_avail = snd_pcm_info_get_subdevices_avail ( pcm_info );

		// Read subdevices info
		for ( unsigned int ii=0; ii < _num_sdevs; ++ii ) {

			snd_pcm_info_set_device ( pcm_info, dev_idx_n );
			snd_pcm_info_set_subdevice ( pcm_info, ii );
			snd_pcm_info_set_stream ( pcm_info, pcm_stream );

			Snd_PCM_Subdevice_Info * sdev_info ( new Snd_PCM_Subdevice_Info );
			err = snd_ctl_pcm_info ( snd_ctl_handle_n, pcm_info );
			if ( err >= 0 ) {
				sdev_info->acquire_info ( pcm_info );
			}
			_sdevs_info.append ( sdev_info );
		}
	}

	return err;
}



//
// Snd_PCM_Device_Info
//

Snd_PCM_Device_Info::Snd_PCM_Device_Info ( )
{
	clear();
}


void
Snd_PCM_Device_Info::clear ( )
{
	_dev_index = -1;

	_dev_id.clear();
	_dev_name.clear();

	_sdevs_info[0].clear();
	_sdevs_info[1].clear();
}


int
Snd_PCM_Device_Info::acquire_device_info (
	snd_ctl_t * snd_ctl_handle_n,
	int device_idx_n )
{
	int err ( 0 );

	clear();

	if ( snd_ctl_handle_n == 0 ) {
		return -1;
	}

	_dev_index = device_idx_n;

	snd_pcm_info_t * pcm_info;
	snd_pcm_info_alloca ( &pcm_info );

	// Read Playback info
	snd_pcm_info_set_device ( pcm_info, _dev_index );
	snd_pcm_info_set_subdevice ( pcm_info, 0 );
	snd_pcm_info_set_stream ( pcm_info, SND_PCM_STREAM_PLAYBACK );

	err = snd_ctl_pcm_info ( snd_ctl_handle_n, pcm_info );
	if ( err < 0 ) {
		snd_pcm_info_set_device ( pcm_info, _dev_index );
		snd_pcm_info_set_subdevice ( pcm_info, 0 );
		snd_pcm_info_set_stream ( pcm_info, SND_PCM_STREAM_CAPTURE );
	}

	err = snd_ctl_pcm_info ( snd_ctl_handle_n, pcm_info );
	if ( err >= 0 ) {

		_dev_id = snd_pcm_info_get_id ( pcm_info );
		_dev_name = snd_pcm_info_get_name ( pcm_info );

		for ( unsigned int ii=0; ii < 2; ++ii ) {
			_sdevs_info[ii].acquire_subdevices ( snd_ctl_handle_n, _dev_index, ii );
		}
	}

	if ( err < 0 ) {
		clear();
	}

	return err;
}


} // End of namespace
