//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_snd_pcm_device_info_hpp__
#define __INC_snd_pcm_device_info_hpp__

#include "alsa.hpp"
#include <QString>
#include <QList>

namespace QSnd
{


///
/// @brief Snd_PCM_Subdevice_Info
///
class Snd_PCM_Subdevice_Info
{
	// Public methods
	public:

	Snd_PCM_Subdevice_Info (
		snd_pcm_info_t * pcm_info_n = 0 );

	int
	dev_idx ( ) const;

	const QString &
	dev_name ( ) const;


	int
	acquire_info (
		snd_pcm_info_t * pcm_info_n );


	// Private attributes
	private:

	int _dev_index;

	QString _dev_name;
};


inline
int
Snd_PCM_Subdevice_Info::dev_idx ( ) const
{
	return _dev_index;
}


inline
const QString &
Snd_PCM_Subdevice_Info::dev_name ( ) const
{
	return _dev_name;
}


///
/// @brief Snd_PCM_Subdevices_Info
///
class Snd_PCM_Subdevices_Info
{
	// Public methods
	public:

	Snd_PCM_Subdevices_Info ( );

	~Snd_PCM_Subdevices_Info ( );

	unsigned int
	stream_dir ( ) const;


	unsigned int
	num_subdevices ( ) const;

	unsigned int
	num_subdevices_avail ( ) const;

	const Snd_PCM_Subdevice_Info *
	subdevice_info (
		unsigned int idx_n ) const;


	void
	clear ( );

	int
	acquire_subdevices (
		snd_ctl_t * snd_ctl_handle_n,
		int dev_idx_n,
		unsigned int stream_dir_n );


	// Private attributes
	private:

	unsigned int _stream_dir;

	unsigned int _num_sdevs;
	unsigned int _num_sdevs_avail;

	QList < Snd_PCM_Subdevice_Info * > _sdevs_info;
};


inline
unsigned int
Snd_PCM_Subdevices_Info::stream_dir ( ) const
{
	return _stream_dir;
}


inline
unsigned int
Snd_PCM_Subdevices_Info::num_subdevices ( ) const
{
	return _num_sdevs;
}


inline
unsigned int
Snd_PCM_Subdevices_Info::num_subdevices_avail ( ) const
{
	return _num_sdevs_avail;
}


inline
const Snd_PCM_Subdevice_Info *
Snd_PCM_Subdevices_Info::subdevice_info (
	unsigned int idx_n ) const
{
	return _sdevs_info[idx_n];
}



///
/// @brief Snd_PCM_Device_Info
///
class Snd_PCM_Device_Info
{
	// Public methods
	public:

	Snd_PCM_Device_Info ( );


	int
	dev_index ( ) const;

	const QString &
	dev_id ( ) const;

	const QString &
	dev_name ( ) const;


	const Snd_PCM_Subdevices_Info &
	subdevices_info (
		unsigned int idx_n ) const;


	void
	clear ( );

	int
	acquire_device_info (
		snd_ctl_t * snd_ctl_handle,
		int device_idx_n );


	// Private attributes
	private:

	int _dev_index;

	QString _dev_id;
	QString _dev_name;

	Snd_PCM_Subdevices_Info _sdevs_info[2];
};


inline
int
Snd_PCM_Device_Info::dev_index ( ) const
{
	return _dev_index;
}


inline
const QString &
Snd_PCM_Device_Info::dev_id ( ) const
{
	return _dev_id;
}


inline
const QString &
Snd_PCM_Device_Info::dev_name ( ) const
{
	return _dev_name;
}


inline
const Snd_PCM_Subdevices_Info &
Snd_PCM_Device_Info::subdevices_info (
	unsigned int stream_dir_n ) const
{
	return _sdevs_info[stream_dir_n];
}


} // End of namespace

#endif
