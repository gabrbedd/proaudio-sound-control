//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "side_interface.hpp"

#include <iostream>


Side_Interface::Side_Interface (
	QSnd::Mixer_Style & mixer_style_n,
	QWidget * parent_n ) :
QFrame ( parent_n ),
_mixer_style ( mixer_style_n ),
_controls_model ( _config_model )
{
	// Control selection tree view
	{
		_control_selection.setHorizontalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );

		connect ( &_control_selection, SIGNAL ( activated ( const QModelIndex & ) ),
			this, SLOT ( control_selected ( const QModelIndex & ) ) );

		_control_selection.setModel ( &_controls_model );

		connect ( &_controls_model, SIGNAL ( modelReset() ),
			this, SLOT ( expand_control_groups() ) );

		connect ( &_controls_model, SIGNAL ( modelReset() ),
			this, SLOT ( restore_selected_ctl() ) );

		expand_control_groups();
	}


	const QString hmask ( "<h3>%1</h3>" );

	// Stream direction selection
	{
		_wdg_stream_dir.hide();

		_sdir_label.setText ( hmask.arg ( tr ( "Show stream" ) ) );

		_stream_check[0].setText ( tr ( "&Playback" ) );
		_stream_check[0].setToolTip ( tr ( "Show playback elements" ) );

		_stream_check[1].setText ( tr ( "&Capture" ) );
		_stream_check[1].setToolTip ( tr ( "Show capture elements" ) );

		QVBoxLayout * lay_sdir ( new QVBoxLayout );
		lay_sdir->setContentsMargins ( 0, 0, 0, 0 );
		lay_sdir->addWidget ( &_sdir_label );
		lay_sdir->addWidget ( &_stream_check[0] );
		lay_sdir->addWidget ( &_stream_check[1] );
		_wdg_stream_dir.setLayout ( lay_sdir );
	}


	// Interface selection
	{
		_wdg_interface.hide();

		_iface_label.setText ( hmask.arg ( tr ( "Interface" ) ) );
		_iface_label.hide();

		_iface_lay_buttons = new QVBoxLayout;
		_iface_lay_buttons->setContentsMargins ( 0, 0, 0, 0 );

		QVBoxLayout * lay_iface ( new QVBoxLayout );
		lay_iface->setContentsMargins ( 0, 0, 0, 0 );
		lay_iface->addWidget ( &_iface_label );
		lay_iface->addLayout ( _iface_lay_buttons );
		_wdg_interface.setLayout ( lay_iface );
	}


	// View selection
	{
		_view_label.setText ( hmask.arg ( tr ( "View type" ) ) );

		QVBoxLayout * lay_view ( new QVBoxLayout );
		lay_view->setContentsMargins ( 0, 0, 0, 0 );
		lay_view->addWidget ( &_view_label );
		lay_view->addWidget ( &_view_selection );
		_wdg_view.setLayout ( lay_view );
	}


	// Top widget
	{
		QVBoxLayout * lay_top = new QVBoxLayout();
		lay_top->addWidget ( &_control_selection );
		_wdg_top.setLayout ( lay_top );
	}


	// Bottom widget
	{
		QVBoxLayout * lay_bottom ( new QVBoxLayout() );
		lay_bottom->addWidget ( &_wdg_stream_dir );
		lay_bottom->addWidget ( &_wdg_interface );
		lay_bottom->addStretch();
		_wdg_bottom.setLayout ( lay_bottom );
	}


	// Splitter
	_splitter.setOrientation ( Qt::Vertical );
	_splitter.addWidget ( &_wdg_top );
	_splitter.addWidget ( &_wdg_bottom );
	_splitter.setCollapsible ( 0, false );
	_splitter.setCollapsible ( 1, false );
	_splitter.setStretchFactor ( 0, 0 );
	_splitter.setStretchFactor ( 1, 1 );

	{
		QVBoxLayout * lay_vbox ( new QVBoxLayout() );
		lay_vbox->addWidget ( &_wdg_view );
		lay_vbox->addWidget ( &_splitter );
		setLayout ( lay_vbox );
	}

	// Adjust contents margins
	{
		QMargins mgs ( _wdg_top.layout()->contentsMargins() );
		mgs.setTop ( 0 );
		mgs.setLeft ( 0 );
		mgs.setRight ( 0 );
		_wdg_top.layout()->setContentsMargins ( mgs );
	}

	{
		QMargins mgs ( _wdg_bottom.layout()->contentsMargins() );
		mgs.setLeft ( 0 );
		mgs.setRight ( 0 );
		mgs.setBottom ( 0 );
		_wdg_bottom.layout()->setContentsMargins ( mgs );
	}

	{
		//QMargins mgs ( layout()->contentsMargins() );
		//mgs.setTop ( 0 );
		//mgs.setBottom ( 0 );
		//layout()->setContentsMargins ( mgs );
	}
}


Side_Interface::~Side_Interface ( )
{
}


void
Side_Interface::update_selected_ctl (
	const QSnd::Snd_Control_Address & ctl_n )
{
	QModelIndex midx (
		_controls_model.ctl_info_index ( ctl_n ) );

	_control_selection.blockSignals ( true );
	_control_selection.setCurrentIndex ( midx );
	_control_selection.blockSignals ( false );

	if ( midx.isValid() ) {
		_selected_ctl = *_controls_model.ctl_info ( midx );
	} else {
		_selected_ctl.clear();
	}
}


void
Side_Interface::set_interface_names (
	const QStringList & ifaces_n )
{
	QList < QAbstractButton * > lst ( _iface_buttons.buttons() );

	for ( int ii=0; ii < lst.size(); ++ii ) {
		_iface_buttons.removeButton ( lst[ii] );
		delete lst[ii];
	}

	for ( int ii=0; ii < ifaces_n.size(); ++ii ) {
		QRadioButton * btn ( new QRadioButton ( ifaces_n[ii] ) );

		_iface_buttons.addButton ( btn, ii );
		_iface_lay_buttons->addWidget ( btn );

		if ( ifaces_n.size() < 2 ) {
			btn->setEnabled ( false );
		}
		btn->show();
	}

	_iface_label.setVisible ( ifaces_n.size() > 0 );
}


void
Side_Interface::select_interface (
	unsigned int idx_n )
{
	QAbstractButton * btn ( _iface_buttons.button ( idx_n ) );
	if ( btn != 0 ) {
		btn->setChecked ( true );
	}
}


void
Side_Interface::control_selected (
	const QModelIndex & idx_n )
{
	const QSnd::Snd_Control_Address * ctl_info ( 0 );

	//std::cout << "Side_Interface::control_selected " << "\n";

	if ( idx_n.isValid() && ( _control_selection.model() != 0 ) ) {
		ctl_info = _controls_model.ctl_info ( idx_n );
	}

	if ( ctl_info != 0 ) {
		if ( _selected_ctl != (*ctl_info) ) {
			_selected_ctl = *ctl_info;
			_current_ctl = _selected_ctl;
			emit sig_control_changed();
		}
	}

}


void
Side_Interface::expand_control_groups ( )
{
	_control_selection.setExpanded ( _controls_model.item_cards()->index(), true );
	_control_selection.setExpanded ( _controls_model.item_plugs()->index(), true );
}


void
Side_Interface::refresh_data ( )
{
	//std::cout << "Side_Interface::refresh_data" << "\n";

	_config_model.revert();
}


void
Side_Interface::restore_selected_ctl ( )
{
	QModelIndex midx (
		_controls_model.ctl_info_index ( _selected_ctl ) );
	_control_selection.setCurrentIndex ( midx );
}

