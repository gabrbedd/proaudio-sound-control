//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_side_interface_hpp__
#define __INC_side_interface_hpp__

#include <QFrame>
#include <QLabel>
#include <QComboBox>
#include <QCheckBox>
#include <QAbstractButton>
#include <QRadioButton>
#include <QButtonGroup>
#include <QSplitter>
#include <QVBoxLayout>

#include "qsnd/alsa_config_model.hpp"
#include "qsnd/controls_model.hpp"
#include "qsnd/controls_view.hpp"
#include "qsnd/mixer_style.hpp"


class Side_Interface :
	public QFrame
{
	Q_OBJECT

	// Public methods
	public:

	Side_Interface (
		QSnd::Mixer_Style & mixer_style_n,
		QWidget * parent_n = 0 );

	~Side_Interface ( );


	QSnd::Mixer_Style &
	mixer_style ( );


	QComboBox *
	view_selection ( );

	QAbstractButton *
	stream_check (
		unsigned int idx_n );

	QButtonGroup *
	iface_buttons ( );


	QWidget *
	wdg_view ( );

	QWidget *
	wdg_stream_dir ( );

	QWidget *
	wdg_interface ( );


	const QSnd::Alsa_Config_Model &
	config_model ( ) const;

	QSnd::Alsa_Config_Model &
	config_model ( );


	const QSnd::Snd_Control_Address &
	default_ctl ( ) const;

	const QSnd::Snd_Control_Address &
	selected_ctl ( ) const;

	void
	update_selected_ctl (
		const QSnd::Snd_Control_Address & ctl_n );


	void
	set_interface_names (
		const QStringList & ifaces_n );

	void
	select_interface (
		unsigned int idx_n );


	// Signals
	signals:

	void
	sig_control_changed ( );


	// Public slots
	public slots:

	void
	refresh_data ( );


	// Protected slots
	protected slots:

	void
	control_selected (
		const QModelIndex & idx_n );

	void
	expand_control_groups ( );

	void
	restore_selected_ctl ( );


	// Private attributes
	private:

	QSnd::Mixer_Style & _mixer_style;


	QSnd::Alsa_Config_Model _config_model;

	QSnd::Controls_Model _controls_model;

	QSnd::Snd_Control_Address _selected_ctl;

	QSnd::Snd_Control_Address _current_ctl;


	// Widgets

	QSplitter _splitter;

	QWidget _wdg_top;

	QWidget _wdg_bottom;


	QWidget _wdg_view;

	QLabel _view_label;

	QComboBox _view_selection;


	// Control selection

	QSnd::Controls_View _control_selection;


	// Stream direction

	QWidget _wdg_stream_dir;

	QLabel _sdir_label;

	QCheckBox _stream_check[2];


	// Interface selection

	QWidget _wdg_interface;

	QLabel _iface_label;

	QVBoxLayout * _iface_lay_buttons;

	QButtonGroup _iface_buttons;
};


inline
QSnd::Mixer_Style &
Side_Interface::mixer_style ( )
{
	return _mixer_style;
}


inline
QComboBox *
Side_Interface::view_selection ( )
{
	return &_view_selection;
}


inline
QAbstractButton *
Side_Interface::stream_check (
	unsigned int idx_n )
{
	return &_stream_check[idx_n];
}


// Group widgets

inline
QWidget *
Side_Interface::wdg_view ( )
{
	return &_wdg_view;
}

inline
QWidget *
Side_Interface::wdg_stream_dir ( )
{
	return &_wdg_stream_dir;
}


inline
QWidget *
Side_Interface::wdg_interface ( )
{
	return &_wdg_interface;
}


inline
const QSnd::Alsa_Config_Model &
Side_Interface::config_model ( ) const
{
	return _config_model;
}


inline
QSnd::Alsa_Config_Model &
Side_Interface::config_model ( )
{
	return _config_model;
}


inline
const QSnd::Snd_Control_Address &
Side_Interface::default_ctl ( ) const
{
	return _controls_model.default_ctl();
}


inline
const QSnd::Snd_Control_Address &
Side_Interface::selected_ctl ( ) const
{
	return _selected_ctl;
}


inline
QButtonGroup *
Side_Interface::iface_buttons ( )
{
	return &_iface_buttons;
}


#endif
