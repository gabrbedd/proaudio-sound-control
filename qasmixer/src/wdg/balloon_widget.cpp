//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "balloon_widget.hpp"

#include <QVBoxLayout>
#include <QPainter>
#include <iostream>


namespace Wdg
{


Balloon_Widget::Balloon_Widget (
	QWidget * parent_n ) :
QWidget ( parent_n, Qt::ToolTip ),
_update_pxmap ( true )
{
	hide();

	setFocusPolicy ( Qt::NoFocus );

	//setStyleSheet("background:transparent;");
	//setAttribute(Qt::WA_TranslucentBackground);
	//setWindowFlags(Qt::FramelessWindowHint);

	_close_timer.setInterval ( 6000 );
	_close_timer.setSingleShot ( true );

	connect ( &_close_timer, SIGNAL ( timeout() ),
		this, SLOT ( close() ) );

	{
		int mg_h ( fontMetrics().height() );
		int mg_v ( mg_h * 3 / 4 );
		setContentsMargins ( mg_h, mg_v, mg_h, mg_v );
	}

	{
		QVBoxLayout * lay_vbox ( new QVBoxLayout );
		lay_vbox->setContentsMargins ( 0, 0, 0, 0 );
		setLayout ( lay_vbox );
	}
}


void
Balloon_Widget::add_widget (
	QWidget * wdg_n )
{
	layout()->addWidget ( wdg_n );
}


unsigned int
Balloon_Widget::duration_ms ( ) const
{
	return qMax ( 0, _close_timer.interval() );
}


void
Balloon_Widget::set_duration_ms (
	unsigned int ms_n )
{
	_close_timer.setInterval ( ms_n );
}


void
Balloon_Widget::set_tray_icon_geometry (
	const QRect & geom_n )
{
	if ( _tray_icon_geom != geom_n ) {
		_tray_icon_geom = geom_n;
		update_geometry();
	}
}


void
Balloon_Widget::update_geometry ( )
{
	if ( !_tray_icon_geom.isValid() ) {
		return;
	}

	const QPoint ti_tl ( _tray_icon_geom.topLeft() );

	QRect srect ( _desktop.screenGeometry ( ti_tl ) );
	QRect arect ( _desktop.availableGeometry ( ti_tl ) );

	QSize nsize ( minimumSizeHint() );
	QPoint npos;

	unsigned int mg_hor ( fontMetrics().averageCharWidth() );
	unsigned int mg_vert ( fontMetrics().height() / 2 );


	// Find corner
	if ( ti_tl.x() < ( srect.left() + srect.width() - ti_tl.x() ) ) {
		// Left
		npos.setX ( arect.left() + mg_hor );
	} else {
		// Right
		npos.setX ( arect.left() + arect.width() - nsize.width() - mg_hor );
	}

	// Find corner
	if ( ti_tl.y() < ( srect.top() + srect.height() - ti_tl.y() ) ) {
		// Top
		npos.setY ( arect.top() + mg_vert);
	} else {
		// Bottom
		npos.setY ( arect.top() + arect.height() - nsize.height() - mg_vert );
	}

	resize ( nsize );
	move ( npos );
}


void
Balloon_Widget::start_show ( )
{
	if ( _tray_icon_geom.isValid() ) {
		show();
		_close_timer.start();
	}
}


void
Balloon_Widget::resizeEvent (
	QResizeEvent * event_n )
{
	QWidget::resizeEvent ( event_n );
	_update_pxmap = true;
}


void
Balloon_Widget::paintEvent (
	QPaintEvent * )
{
	if ( _update_pxmap ) {
		_update_pxmap = false;

		_pxmap = QPixmap ( size() );
		_pxmap.fill ( Qt::transparent );
		QPainter pnt ( &_pxmap );
		pnt.setRenderHints (
			QPainter::Antialiasing | QPainter::SmoothPixmapTransform );

		int border ( 1 );
		int rad ( 2 );
		{
			QPen pen ( palette().color ( QPalette::WindowText ), border );
			pnt.setPen ( pen );
			QBrush brush ( palette().color ( QPalette::Window ) );
			pnt.setBrush ( brush );
		}

		QRectF ref ( rect() );
		const double adj ( border / 2.0 );
		ref.adjust ( adj, adj, -adj, -adj );
		pnt.drawRoundedRect ( ref, rad, rad );
	}

	{
		QPainter pnt ( this );
		pnt.drawPixmap ( QPoint ( 0, 0 ), _pxmap );
	}
}


} // End of namespace
