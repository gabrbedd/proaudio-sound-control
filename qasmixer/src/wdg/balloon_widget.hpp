//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_balloon_widget_hpp__
#define __INC_balloon_widget_hpp__

#include <QFrame>
#include <QDesktopWidget>
#include <QTimer>


namespace Wdg
{


///
/// @brief Balloon_Widget
///
class Balloon_Widget :
	public QWidget
{
	Q_OBJECT

	// Public methods
	public:

	Balloon_Widget (
		QWidget * parent_n = 0 );


	unsigned int
	duration_ms ( ) const;

	void
	set_duration_ms (
		unsigned int ms_n );


	const QRect &
	tray_icon_geometry ( ) const;

	void
	set_tray_icon_geometry (
		const QRect & geom_n );

	void
	add_widget (
		QWidget * wdg_n );


	// Public slots
	public slots:

	void
	start_show ( );


	// Protected methods
	protected:

	void
	resizeEvent (
		QResizeEvent * event_n );

	void
	paintEvent (
		QPaintEvent * event_n );


	void
	update_geometry ( );


	// Private attributes
	private:

	QPixmap _pxmap;

	bool _update_pxmap;

	QRect _tray_icon_geom;

	QDesktopWidget _desktop;

	QTimer _close_timer;
};



inline
const QRect &
Balloon_Widget::tray_icon_geometry ( ) const
{
	return _tray_icon_geom;
}


} // End of namespace


#endif
