//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_qwidget_painter_methods_hpp__
#define __INC_qwidget_painter_methods_hpp__

#include <QColor>

namespace Wdg
{
namespace Painter
{


QColor
col_mix (
	const QColor & col_1,
	const QColor & col_2,
	int w_1,
	int w_2 );


} // End of namespace
} // End of namespace


#endif
