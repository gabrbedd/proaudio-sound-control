//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "covered_input.hpp"

#include <QHBoxLayout>
#include <QEvent>
#include <QFocusEvent>
#include <iostream>


namespace Wdg
{


Covered_Input::Covered_Input (
	QWidget * parent_n ) :
QWidget ( parent_n ),
_label ( this ),
_input ( 0 )
{
	_label.setFocusPolicy ( Qt::StrongFocus );
	_label.setAlignment ( Qt::AlignRight | Qt::AlignVCenter );
	_label.installEventFilter ( this );

	//_label.setBackgroundRole ( QPalette::Base );
	//_label.setAutoFillBackground ( true );

	// Put label in extra widget to center align the right aligned label
	QWidget * wdg_lcon ( new QWidget ( this ) );
	wdg_lcon->installEventFilter ( this );
	{
		QHBoxLayout * lay_vbox ( new QHBoxLayout );
		lay_vbox->setContentsMargins ( 0, 0, 0, 0 );
		lay_vbox->addStretch ( 1 );
		lay_vbox->addWidget ( &_label );
		lay_vbox->addStretch ( 1 );

		wdg_lcon->setLayout ( lay_vbox );
	}

	_lay_stack.setContentsMargins ( 0, 0, 0, 0 );
	_lay_stack.addWidget ( wdg_lcon );

	setLayout ( &_lay_stack );
}


void
Covered_Input::set_input (
	QWidget * wdg_n )
{
	if ( _input != 0 ) {
		_input->removeEventFilter ( this );
	}

	_input = wdg_n;

	if ( _input != 0 ) {
		_lay_stack.addWidget ( _input );
		_input->installEventFilter ( this );
	}
}


void
Covered_Input::switch_to_label ( )
{
	_lay_stack.setCurrentIndex ( 0 );
}


void
Covered_Input::switch_to_input ( )
{
	if ( input() != 0 ) {
		_lay_stack.setCurrentIndex ( 1 );
	}
}


void
Covered_Input::switch_to_input (
	bool show_n )
{
	if ( show_n ) {
		switch_to_input();
	} else {
		switch_to_label();
	}
}


bool
Covered_Input::event (
	QEvent * event_n )
{
	if ( isEnabled() ) {
		if ( event_n->type() == QEvent::Enter ) {
			switch_to_input();
		} else if ( event_n->type() == QEvent::Leave ) {
			if ( input() != 0 ) {
				if ( !input()->hasFocus() ) {
					switch_to_label();
				}
			}
		}
	} else {
		if ( event_n->type() == QEvent::EnabledChange ) {
			switch_to_label();
		}
	}

	return QWidget::event ( event_n );
}


bool
Covered_Input::eventFilter (
	QObject * watched,
	QEvent * event_n )
{
	if ( ( watched == 0 ) || ( event_n == 0 ) ) {
		return false;
	}
	if ( watched == label() ) {
		if ( event_n->type() == QEvent::FocusIn ) {
			//std::cout << "Focus in\n";
			if ( input() != 0 ) {
				switch_to_input();
				input()->setFocus();
				return true;
			}
		}
	} else if ( watched == input() ) {
		if ( event_n->type() == QEvent::FocusOut ) {
			//std::cout << "Focus out\n";
			if ( !input()->underMouse() ) {
				switch_to_label();
			}
		}
	}
	return false;
}


} // End of namespace

