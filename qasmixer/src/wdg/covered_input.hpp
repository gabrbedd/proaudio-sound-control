//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_covered_input_hpp__
#define __INC_covered_input_hpp__

#include <QWidget>
#include <QStackedLayout>
#include "label_width.hpp"


namespace Wdg
{


class Covered_Input :
	public QWidget
{
	Q_OBJECT

	// Public methods
	public:

	Covered_Input (
		QWidget * parent_n = 0 );


	Label_Width *
	label ( );


	QWidget *
	input ( );

	void
	set_input (
		QWidget * wdg_n );


	bool
	input_visible ( ) const;


	// Public slots
	public slots:

	void
	switch_to_label ( );

	void
	switch_to_input ( );

	void
	switch_to_input (
		bool show_n );


	// Protected methods
	protected:

	bool
	event (
		QEvent * event_n );

	bool
	eventFilter (
		QObject * watched,
		QEvent * event_n );


	// Private attributes
	private:

	QStackedLayout _lay_stack;

	Label_Width _label;

	QWidget * _input;
};


inline
Label_Width *
Covered_Input::label ( )
{
	return &_label;
}


inline
QWidget *
Covered_Input::input ( )
{
	return _input;
}


inline
bool
Covered_Input::input_visible ( ) const
{
	return ( _lay_stack.currentIndex() > 0 );
}


} // End of namespace

#endif
