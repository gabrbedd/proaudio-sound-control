//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "covered_spinbox.hpp"


namespace Wdg
{


//
// Covered_Spinbox
//
Covered_Spinbox::Covered_Spinbox (
	QWidget * parent_n ) :
Covered_Input ( parent_n ),
_lbl_mask ( "%1" )
{
	_value = _spinbox.value();
	set_input ( spinbox() );
	connect ( &_spinbox, SIGNAL ( valueChanged ( int ) ),
		this, SLOT ( set_value ( int ) ) );
	update_label();
}


void
Covered_Spinbox::set_value (
	int value_n )
{
	if ( value() != value_n ) {
		_value = value_n;
		update_label();
		_spinbox.setValue ( value() );
		emit sig_value_changed ( value() );
	}
}


void
Covered_Spinbox::set_range (
	int min_n,
	int max_n )
{
	spinbox()->setRange ( min_n, max_n );

	const QFontMetrics fmet ( label()->fontMetrics() );

	const QString str_min (	_lbl_mask.arg ( min_n ) );
	const QString str_max (	_lbl_mask.arg ( max_n ) );

	const QString * str ( 0 );
	int lmin = fmet.width ( str_min );
	int lmax = fmet.width ( str_max );

	if ( lmin > lmax ) {
		str = &str_min;
	} else {
		str = &str_max;
	}
	label()->set_min_text ( *str );

	updateGeometry();
}


void
Covered_Spinbox::set_label_mask (
	const QString & mask_n )
{
	_lbl_mask = mask_n;
	update_label();
}


void
Covered_Spinbox::update_label ( )
{
	label()->setText ( _lbl_mask.arg ( value() ) );
}


//
// Covered_Spinbox_Double
//
Covered_Spinbox_Double::Covered_Spinbox_Double (
	QWidget * parent_n ) :
Covered_Input ( parent_n ),
_lbl_mask ( "%1" ),
_field_width ( 0 ),
_format ( 'g' ),
_precision ( -1 )
{
	_value = _spinbox.value();
	set_input ( spinbox() );
	connect ( &_spinbox, SIGNAL ( valueChanged ( double ) ),
		this, SLOT ( set_value ( double ) ) );
	update_label();
}


void
Covered_Spinbox_Double::set_value (
	double value_n )
{
	if ( value() != value_n ) {
		_value = value_n;
		update_label();
		_spinbox.setValue ( value() );
		emit sig_value_changed ( value() );
	}
}


void
Covered_Spinbox_Double::set_range (
	double min_n,
	double max_n )
{
	_spinbox.setRange ( min_n, max_n );

	const QFontMetrics fmet ( label()->fontMetrics() );

	const QString str_min (
		_lbl_mask.arg ( min_n, _field_width, _format, _precision ) );
	const QString str_max (
		_lbl_mask.arg ( max_n, _field_width, _format, _precision ) );

	const QString * str ( 0 );
	int lmin = fmet.width ( str_min );
	int lmax = fmet.width ( str_max );

	if ( lmin > lmax ) {
		str = &str_min;
	} else {
		str = &str_max;
	}
	label()->set_min_text ( *str );

	updateGeometry();
}


void
Covered_Spinbox_Double::set_label_mask (
	const QString & mask_n )
{
	_lbl_mask = mask_n;
	update_label();
}


void
Covered_Spinbox_Double::set_field_width (
	int val_n )
{
	if ( _field_width != val_n ) {
		_field_width = val_n;
		update_label();
	}
}


void
Covered_Spinbox_Double::set_format (
	char val_n )
{
	if ( _format != val_n ) {
		_format = val_n;
		update_label();
	}
}


void
Covered_Spinbox_Double::set_precision (
	int val_n )
{
	if ( _precision != val_n ) {
		_precision = val_n;
		update_label();
	}
}


void
Covered_Spinbox_Double::update_label ( )
{
	QString val (
		_lbl_mask.arg ( value(), _field_width, _format, _precision ) );
	label()->setText ( val );
}


} // End of namespace

