//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_covered_spinbox_hpp__
#define __INC_covered_spinbox_hpp__

#include <QSpinBox>

#include "covered_input.hpp"


namespace Wdg
{


///
/// @brief Covered_Spinbox
///
class Covered_Spinbox :
	public Covered_Input
{
	Q_OBJECT

	// Public methods
	public:

	Covered_Spinbox (
		QWidget * parent_n = 0 );


	QSpinBox *
	spinbox ( );


	// Value

	int
	value ( ) const;

	void
	set_range (
		int min_n,
		int max_n );


	// Label mask

	const QString &
	label_mask ( ) const;

	void
	set_label_mask (
		const QString & mask_n );


	// Signals
	signals:

	void
	sig_value_changed (
		int value_n );


	// Public slots
	public slots:

	void
	set_value (
		int value_n );

	void
	update_label ( );


	// Private attributes
	private:

	QSpinBox _spinbox;

	int _value;

	QString _lbl_mask;
};


inline
QSpinBox *
Covered_Spinbox::spinbox ( )
{
	return &_spinbox;
}


inline
int
Covered_Spinbox::value ( ) const
{
	return _value;
}


inline
const QString &
Covered_Spinbox::label_mask ( ) const
{
	return _lbl_mask;
}



///
/// @brief Covered_Spinbox_Double
///
class Covered_Spinbox_Double :
	public Covered_Input
{
	Q_OBJECT

	// Public methods
	public:

	Covered_Spinbox_Double (
		QWidget * parent_n = 0 );


	QDoubleSpinBox *
	spinbox ( );


	// Value

	double
	value ( ) const;

	void
	set_range (
		double min_n,
		double max_n );


	// Label formatting

	const QString &
	label_mask ( ) const;

	void
	set_label_mask (
		const QString & mask_n );


	int
	field_width () const;

	void
	set_field_width (
		int val_n );


	char
	format ( ) const;

	void
	set_format (
		char val_n );


	int
	precision ( );

	void
	set_precision (
		int val_n );


	// Signals
	signals:

	void
	sig_value_changed (
		double value_n );


	// Public slots
	public slots:

	void
	set_value (
		double value_n );

	void
	update_label ( );


	// Private attributes
	private:

	QDoubleSpinBox _spinbox;

	double _value;

	QString _lbl_mask;

	int _field_width;
	char _format;
	int _precision;
};


inline
QDoubleSpinBox *
Covered_Spinbox_Double::spinbox ( )
{
	return &_spinbox;
}


inline
double
Covered_Spinbox_Double::value ( ) const
{
	return _value;
}


inline
const QString &
Covered_Spinbox_Double::label_mask ( ) const
{
	return _lbl_mask;
}


inline
int
Covered_Spinbox_Double::field_width () const
{
	return _field_width;
}


inline
char
Covered_Spinbox_Double::format ( ) const
{
	return _format;
}


inline
int
Covered_Spinbox_Double::precision ( )
{
	return _precision;
}


} // End of namespace

#endif
