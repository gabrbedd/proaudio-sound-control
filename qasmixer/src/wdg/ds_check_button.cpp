//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "ds_check_button.hpp"

#include <QEvent>
#include <QPainter>

#include <iostream>

namespace Wdg
{


//
// DS_Check_Button
//
DS_Check_Button::DS_Check_Button (
	QWidget * parent,
	Shared_Pixmaps_Set_Hub * hub_n ) :
QAbstractButton ( parent ),
_pxm_bg ( 0 ),
_pxm_handle ( 0 ),
_update_pixmaps ( true ),
_hub ( hub_n )
{
	setCheckable ( true );
	{
		QSizePolicy policy ( sizePolicy() );
		policy.setHorizontalPolicy ( QSizePolicy::Preferred );
		policy.setVerticalPolicy ( QSizePolicy::Preferred );
		setSizePolicy ( policy );
	}
}


DS_Check_Button::~DS_Check_Button ( )
{
	set_hub ( 0 );
}


QSize
DS_Check_Button::sizeHint ( ) const
{
	ensurePolished();
	const QFontMetrics fmet ( fontMetrics() );
	return QSize ( fmet.height(), fmet.height() );
}


QSize
DS_Check_Button::minimumSizeHint ( ) const
{
	ensurePolished();
	const QFontMetrics fmet ( fontMetrics() );
	return QSize ( fmet.height(), fmet.height() );
}


void
DS_Check_Button::set_hub (
	Shared_Pixmaps_Set_Hub * hub_n )
{
	if ( _hub != 0 ) {
		_hub->return_pixmaps ( _pxm_bg );
		_hub->return_pixmaps ( _pxm_handle );
	}

	_hub = hub_n;

	if ( _hub != 0 ) {

	}

	_update_pixmaps = true;
	update();
}


void
DS_Check_Button::set_pixmaps_meta_bg (
	const DS_Check_Button_Pixmaps_Meta_Bg & cfg_n )
{
	_pxm_meta_bg = cfg_n;

	_update_pixmaps = true;
	update();
}


void
DS_Check_Button::set_pixmaps_meta_handle (
	const DS_Check_Button_Pixmaps_Meta_Handle & cfg_n )
{
	_pxm_meta_handle = cfg_n;

	_update_pixmaps = true;
	update();
}



// Events


void
DS_Check_Button::changeEvent (
	QEvent * event_n )
{
	QWidget::changeEvent ( event_n );

	if ( ( event_n->type() == QEvent::ActivationChange ) ||
		( event_n->type() == QEvent::EnabledChange ) ||
		( event_n->type() == QEvent::StyleChange ) ||
		( event_n->type() == QEvent::PaletteChange ) ||
		( event_n->type() == QEvent::LayoutDirectionChange ) )
	{
		_update_pixmaps = true;
		update();
	}
}


void
DS_Check_Button::enterEvent (
	QEvent * event_n )
{
	QAbstractButton::enterEvent ( event_n );
	update();
}


void
DS_Check_Button::leaveEvent (
	QEvent * event_n )
{
	QAbstractButton::leaveEvent ( event_n );
	update();
}


void
DS_Check_Button::resizeEvent (
	QResizeEvent * event_n )
{
	QAbstractButton::resizeEvent ( event_n );
	QRect ren ( contentsRect() );
	if ( ren.width() != ren.height() ) {
		const int delta ( ren.width() - ren.height() );
		if ( delta > 0 ) {
			ren.setWidth ( ren.height() );
			ren.moveLeft ( ren.left() + delta / 2 );
		} else {
			ren.setHeight ( ren.width() );
			ren.moveTop ( ren.top() - delta / 2 );
		}
	}

	_pxmap_rect = ren;
	_update_pixmaps = true;
}


void
DS_Check_Button::paintEvent (
	QPaintEvent * )
{
	if ( _update_pixmaps ) {
		_update_pixmaps = false;
		update_pixmaps();
	}

	QPainter painter ( this );

	// Draw background
	if ( _pxm_bg != 0 ) {
		int pm_idx ( 0 );
		if ( hasFocus() ) {
			pm_idx = 1;
		}
		if ( underMouse() ) {
			pm_idx += 2;
		}
		const QPixmap & pxmap ( _pxm_bg->pixmap ( pm_idx ) );
		painter.drawPixmap ( _pxmap_rect.topLeft(), pxmap );
	}

	// Draw handle
	if ( _pxm_handle != 0 ) {
		if ( isChecked() || isDown() ) {
			int pm_idx ( 0 );
			if ( hasFocus() ) {
				pm_idx = 1;
			}
			if ( isDown() ) {
				pm_idx += 4;
			} else {
				if ( underMouse() ) {
					pm_idx += 2;
				}
			}

			const QPixmap & pxmap ( _pxm_handle->pixmap ( pm_idx ) );
			painter.drawPixmap ( _pxmap_rect.topLeft(), pxmap );
		}
	}
}


void
DS_Check_Button::update_pixmaps ( )
{
	if ( _hub != 0 ) {
		_pxm_meta_bg.size = _pxmap_rect.size();
		_pxm_meta_handle.size = _pxmap_rect.size();

		_pxm_meta_bg.palette = palette();
		_pxm_meta_handle.palette = palette();

		_hub->update_pixmaps ( _pxm_bg, _pxm_meta_bg );
		_hub->update_pixmaps ( _pxm_handle, _pxm_meta_handle );
	}
}


} // End of namespace
