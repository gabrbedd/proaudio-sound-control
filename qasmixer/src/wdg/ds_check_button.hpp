//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_ds_check_button_hpp__
#define __INC_ds_check_button_hpp__


#include <QAbstractButton>

#include "shared_pixmaps_set_hub.hpp"
#include "ds_check_button_pixmaps_meta.hpp"


namespace Wdg
{


class DS_Check_Button :
	public QAbstractButton
{
	Q_OBJECT

	// Public methods
	public:

	DS_Check_Button (
		QWidget * parent = 0,
		Shared_Pixmaps_Set_Hub * hub_n = 0 );

	~DS_Check_Button ( );


	QSize
	sizeHint ( ) const;

	QSize
	minimumSizeHint ( ) const;


	// Hub

	Shared_Pixmaps_Set_Hub *
	hub ( ) const;

	void
	set_hub (
		Shared_Pixmaps_Set_Hub * hub_n );


	// Pixmaps config

	const DS_Check_Button_Pixmaps_Meta_Bg &
	pixmaps_meta_bg ( ) const;

	void
	set_pixmaps_meta_bg (
		const DS_Check_Button_Pixmaps_Meta_Bg & cfg_n );


	const DS_Check_Button_Pixmaps_Meta_Handle &
	pixmaps_meta_handle ( ) const;

	void
	set_pixmaps_meta_handle (
		const DS_Check_Button_Pixmaps_Meta_Handle & cfg_n );


	// Protected methods
	protected:

	void
	changeEvent (
		QEvent * event_n );

	void
	enterEvent (
		QEvent * );

	void
	leaveEvent (
		QEvent * );

	void
	resizeEvent (
		QResizeEvent * event );

	void
	paintEvent (
		QPaintEvent * event );


	void
	update_pixmaps ( );


	// Private attributes
	private:

	Shared_Pixmaps_Set * _pxm_bg;
	Shared_Pixmaps_Set * _pxm_handle;

	QRect _pxmap_rect;

	bool _update_pixmaps;

	DS_Check_Button_Pixmaps_Meta_Bg _pxm_meta_bg;
	DS_Check_Button_Pixmaps_Meta_Handle _pxm_meta_handle;

	Shared_Pixmaps_Set_Hub * _hub;
};


inline
Shared_Pixmaps_Set_Hub *
DS_Check_Button::hub ( ) const
{
	return _hub;
}


} // End of namespace

#endif


