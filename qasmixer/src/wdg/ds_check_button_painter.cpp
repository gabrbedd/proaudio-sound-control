//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "ds_check_button_painter.hpp"


namespace Wdg
{
namespace Painter
{


DS_Check_Button_Painter::DS_Check_Button_Painter ( ) :
_pm_meta ( 0 )
{
}


int
DS_Check_Button_Painter::paint_pixmaps (
	Shared_Pixmaps_Set * spms_n,
	unsigned int image_type_n )
{
	int res ( -1 );
	if ( spms_n != 0  ) {
		_pm_meta = spms_n->meta();
		if ( _pm_meta != 0 ) {
			set_current_palette ( _pm_meta->palette );

			if ( image_type_n == 0 ) {
				spms_n->set_num_pixmaps ( 4 );
				res = this->paint_bg ( spms_n );

			} else if ( image_type_n == 1 ) {
				spms_n->set_num_pixmaps ( 6 );
				res = this->paint_handle ( spms_n );

			}
		}
	}
	return res;
}



} // End of namespace
} // End of namespace
