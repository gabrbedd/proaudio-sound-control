//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_ds_check_button_painter_hpp__
#define __INC_ds_check_button_painter_hpp__

#include "ds_check_button_pixmaps_meta.hpp"
#include "pixmaps_painter.hpp"


namespace Wdg
{
namespace Painter
{



///
/// @brief DS_Check_Button_Painter
///
class DS_Check_Button_Painter :
	public Pixmaps_Painter
{

	// Public methods
	public:

	DS_Check_Button_Painter ( );

	const Shared_Pixmaps_Set_Meta *
	pm_meta ( ) const;


	int
	paint_pixmaps (
		Shared_Pixmaps_Set * spms_n,
		unsigned int image_type_n );



	// Protected methods
	protected:

	///
	/// @brief Paints the background
	///
	/// 0 - idle
	/// 1 - focus
	/// 2 - hover idle
	/// 3 - hover focus
	///
	virtual
	int
	paint_bg (
		Shared_Pixmaps_Set * spms_n ) = 0;


	///
	/// @brief Paints the handle
	///
	/// 0 - idle
	/// 1 - focus
	/// 2 - hover idle
	/// 3 - hover focus
	/// 4 - half checked idle
	/// 5 - half checked focus
	///
	virtual
	int
	paint_handle (
		Shared_Pixmaps_Set * spms_n ) = 0;


	bool
	has_focus ( ) const;

	bool
	mouse_over ( ) const;

	bool
	half_checked ( ) const;

	void
	set_focus_mover (
		bool has_focus_n,
		bool mouse_over_n );

	void
	set_half_checked (
		bool half_checked_n );





	// Private attributes
	private:

	const Shared_Pixmaps_Set_Meta * _pm_meta;

	bool _has_focus;
	bool _mouse_over;
	bool _half_checked;
};


inline
const Shared_Pixmaps_Set_Meta *
DS_Check_Button_Painter::pm_meta ( ) const
{
	return _pm_meta;
}


inline
bool
DS_Check_Button_Painter::has_focus ( ) const
{
	return _has_focus;
}


inline
bool
DS_Check_Button_Painter::mouse_over ( ) const
{
	return _mouse_over;
}


inline
bool
DS_Check_Button_Painter::half_checked ( ) const
{
	return _half_checked;
}


inline
void
DS_Check_Button_Painter::set_focus_mover (
	bool has_focus_n,
	bool mouse_over_n )
{
	_has_focus = has_focus_n;
	_mouse_over = mouse_over_n;
}


inline
void
DS_Check_Button_Painter::set_half_checked (
	bool half_checked_n )
{
	_half_checked = half_checked_n;
}



} // End of namespace
} // End of namespace


#endif


