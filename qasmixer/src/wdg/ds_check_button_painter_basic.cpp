//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ds_check_button_painter_basic.hpp"

#define _USE_MATH_DEFINES
#include <cmath>
#include "color_methods.hpp"

#include <iostream>


namespace Wdg
{
namespace Painter
{


DS_Check_Button_Painter_Basic::DS_Check_Button_Painter_Basic ( )
{
}


int
DS_Check_Button_Painter_Basic::paint_bg (
	Shared_Pixmaps_Set * spms_n )
{
	if ( spms_n->pixmap_size().isValid() ) {
		set_is ( spms_n->pixmap_size() );

		_min_len = qMin ( is().width(), is().height() );
		_max_len = qMax ( is().width(), is().height() );
		_border_width = qMax ( _min_len / 16, 1 );
		_gap_width = qMax ( _min_len / 8, 2 );

		{
			const QColor col_bg ( cpal().color ( QPalette::Button ) );
			const QColor col_fg ( cpal().color ( QPalette::ButtonText ) );
			_col_border = Wdg::Painter::col_mix ( col_bg, col_fg, 1, 1 );
		}

		{
			const QColor col_bg ( cpal().color ( QPalette::Button ) );
			const QColor col_fg ( cpal().color ( QPalette::ButtonText ) );
			_col_focus = Wdg::Painter::col_mix ( col_bg, col_fg, 1, 2 );
		}

		set_focus_mover ( false, false );
		paint_bg_type ( spms_n, 0 );

		set_focus_mover ( true, false );
		paint_bg_type ( spms_n, 1 );

		set_focus_mover ( false, true );
		paint_bg_type ( spms_n, 2 );

		set_focus_mover ( true, true );
		paint_bg_type ( spms_n, 3 );

		return 0;
	}
	return -1;
}


void
DS_Check_Button_Painter_Basic::paint_bg_type (
	Shared_Pixmaps_Set * spms_n,
	int pixmap_idx_n )
{
	QImage img ( is(), QImage::Format_ARGB32 );
	img.fill ( 0 );

	{
		QPainter pnt ( &img );
		pnt.setRenderHints (
			QPainter::Antialiasing |
			QPainter::TextAntialiasing |
			QPainter::SmoothPixmapTransform );

		set_painter ( &pnt );

		// Paint
		paint_bg_area();
		paint_bg_deco();
		paint_bg_border();
	}

	spms_n->pixmap_ref ( pixmap_idx_n ) = QPixmap::fromImage ( img );
}


void
DS_Check_Button_Painter_Basic::paint_bg_area ( )
{
	// Area
	{
		QRadialGradient grad;
		{
			const qreal rad ( _min_len / 2.0 - _border_width );
			const QPointF center ( is().width() / 2.0, is().height() / 2.0 );

			grad.setRadius ( rad );
			grad.setCenter ( center );
			grad.setFocalPoint ( center );

			// Color mixing
			int w_dk ( 40 );
			if ( mouse_over() ) {
				w_dk = 45;
			}

			QColor col_dk ( cpal().color ( QPalette::Window ) );
			QColor col_win ( cpal().color ( QPalette::Button ) );

			QColor col_grad_base ( col_win );
			QColor col_grad_dk (
				Wdg::Painter::col_mix ( col_dk, col_win, w_dk, 255 - w_dk ) );

			grad.setColorAt ( 0, col_grad_base );
			grad.setColorAt ( 1, col_grad_dk );
		}

		QRectF arect ( irectf() );
		qreal adj ( _border_width / 2.0 );
		arect.adjust ( adj, adj, -adj, -adj );

		painter()->setPen ( Qt::NoPen );
		painter()->setBrush ( grad );
		painter()->drawEllipse ( arect );
	}
}


void
DS_Check_Button_Painter_Basic::paint_bg_deco ( )
{
	// Mouse over
	if ( mouse_over() ) {
		paint_highlight();
	}
}


void
DS_Check_Button_Painter_Basic::paint_bg_border ( )
{
	// Border
	{
		QPen pen ( _col_border );
		pen.setWidth ( _border_width );

		painter()->setPen ( pen );
		painter()->setBrush ( Qt::NoBrush );
	}

	{
		QRectF brect ( irectf() );
		const qreal adj ( _border_width / 2.0 );
		brect.adjust ( adj, adj, -adj, -adj );

		painter()->drawEllipse ( brect );
	}

	if ( has_focus() ) {
		paint_focus_path();
	}
}


int
DS_Check_Button_Painter_Basic::paint_handle (
	Shared_Pixmaps_Set * spms_n )
{
	if ( spms_n->pixmap_size().isValid() ) {
		set_is ( spms_n->pixmap_size() );

		set_half_checked ( false );
		set_focus_mover ( false, false );
		paint_handle_type ( spms_n, 0 );

		set_focus_mover ( true, false );
		paint_handle_type ( spms_n, 1 );

		set_focus_mover ( false, true );
		paint_handle_type ( spms_n, 2 );

		set_focus_mover ( true, true );
		paint_handle_type ( spms_n, 3 );


		set_half_checked ( true );
		set_focus_mover ( false, false );
		paint_handle_type ( spms_n, 4 );

		set_focus_mover ( true, false );
		paint_handle_type ( spms_n, 5 );

		return 0;
	}
	return -1;
}



void
DS_Check_Button_Painter_Basic::paint_handle_type (
	Shared_Pixmaps_Set * spms_n,
	int img_idx_n )
{
	QImage img ( is(), QImage::Format_ARGB32 );
	img.fill ( 0 );

	QPainter pnt ( &img );
	pnt.setRenderHints (
		QPainter::Antialiasing |
		QPainter::TextAntialiasing |
		QPainter::SmoothPixmapTransform );

	set_painter ( &pnt );

	paint_handle_area();
	paint_handle_deco();

	spms_n->pixmap_ref ( img_idx_n ) = QPixmap::fromImage ( img );
}


void
DS_Check_Button_Painter_Basic::paint_handle_area ( )
{
	QRectF btn_rect ( irectf() );
	{
		const qreal adj ( _border_width + _gap_width );
		btn_rect.adjust ( adj, adj, -adj, -adj );
	}

	QColor col_dk ( cpal().color ( QPalette::WindowText ) );
	QColor col_dka ( col_dk );
	col_dka.setAlpha ( 64 );

	painter()->setPen ( Qt::NoPen );
	if ( half_checked() ) {
		QRadialGradient grad;
		const QPointF center ( btn_rect.center() );

		const qreal rad ( qMin ( btn_rect.width(), btn_rect.height() ) / 2.0 );
		grad.setRadius ( rad );
		grad.setCenter ( center );
		grad.setFocalPoint ( center );

		grad.setColorAt ( 0.0, col_dk );
		grad.setColorAt ( 0.2, col_dk );
		grad.setColorAt ( 1.0, col_dka );

		painter()->setBrush ( grad );
	} else {
		painter()->setBrush ( col_dk );
	}

	painter()->drawEllipse ( btn_rect );
}


void
DS_Check_Button_Painter_Basic::paint_handle_deco ( )
{
	if ( mouse_over() ) {
		paint_highlight();
	}
}


void
DS_Check_Button_Painter_Basic::paint_highlight ( )
{
	QRectF brect ( irectf() );
	const qreal adj ( _border_width );
	brect.adjust ( adj, adj, -adj, -adj );

	QRadialGradient grad;
	{
		const qreal diam0 ( _min_len - _border_width );
		const qreal diam1 ( _min_len - _border_width - _gap_width );
		const qreal diam2 ( qMax ( ( diam1 - _gap_width * 6.0 ), _gap_width * 3.0 ) );

		//std::cout << "_min_len   " << _min_len   << "\n";
		//std::cout << "_gap_width " << _gap_width << "\n";
		//std::cout << "diam0 " << diam0 << "\n";
		//std::cout << "diam1 " << diam1 << "\n";
		//std::cout << "diam2 " << diam2 << "\n";

		const QPointF center ( is().width() / 2.0, is().height() / 2.0 );

		grad.setRadius ( diam0 / 2.0 );
		grad.setCenter ( center );
		grad.setFocalPoint ( center );

		QColor col_full ( cpal().color ( QPalette::Highlight ) );
		QColor col_alpha ( col_full );

		col_full.setAlpha ( 180 );
		col_alpha.setAlpha ( 0 );

		qreal w1 = diam2 / diam0;
		qreal w2 = diam1 / diam0;

		grad.setColorAt ( 0, col_alpha );
		grad.setColorAt ( w1, col_alpha );
		grad.setColorAt ( w2, col_full );
		grad.setColorAt ( 1, col_full );
	}

	painter()->setPen ( Qt::NoPen );
	painter()->setBrush ( grad );
	painter()->drawEllipse ( brect );
}


void
DS_Check_Button_Painter_Basic::paint_focus_path ( )
{
	QPainterPath ppath;

	{
		const QPointF center ( irectf().center() );
		const qreal big_rad ( _min_len / 2.0 - _border_width );

		const qreal rscale ( M_PI / 180.0 );
		const qreal ang_delta ( 90.0 * rscale );
		qreal angle ( ( 90.0 / 2.0 ) * rscale );

		qreal crad ( _min_len / 2.0 - big_rad*std::cos ( angle ) );
		crad *= 2.0 / 3.0;

		for ( unsigned int ii=0; ii < 4; ++ii ) {
			QPointF ccen ( std::cos ( angle ), std::sin ( angle ) );
			ccen *= big_rad;
			ccen += center;
			ppath.addEllipse ( ccen, crad, crad );
			angle += ang_delta;
		}
	}

	// Subtract inner circle
	{
		QPainterPath ppathc;

		QRectF brect ( irectf() );
		const qreal adj ( _border_width / 2.0 );
		brect.adjust ( adj, adj, -adj, -adj );

		ppathc.addEllipse ( brect );
		ppath = ppath.subtracted ( ppathc );
	}

	painter()->setPen ( Qt::NoPen );
	painter()->setBrush ( _col_focus );
	painter()->drawPath ( ppath );
}


} // End of namespace
} // End of namespace
