//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_ds_check_button_painter_box_hpp__
#define __INC_ds_check_button_painter_box_hpp__

#include "ds_check_button_painter.hpp"
#include "primitive_painter.hpp"


namespace Wdg
{
namespace Painter
{


class DS_Check_Button_Painter_Basic :
	public DS_Check_Button_Painter
{
	// Public methods
	public:

	DS_Check_Button_Painter_Basic ( );


	// Protected methods
	protected:

	void
	paint_highlight ( );

	void
	paint_focus_path ( );


	// Background

	int
	paint_bg (
		Shared_Pixmaps_Set * spms_n );

	void
	paint_bg_type (
		Shared_Pixmaps_Set * spms_n,
		int pixmap_idx_n );

	void
	paint_bg_area ( );

	void
	paint_bg_deco ( );

	void
	paint_bg_border ( );


	// Handle

	int
	paint_handle (
		Shared_Pixmaps_Set * spms_n );

	void
	paint_handle_type (
		Shared_Pixmaps_Set * spms_n,
		int img_idx_n );

	void
	paint_handle_area ( );

	void
	paint_handle_deco ( );


	// Private attributes
	private:

	int _max_len;
	int _min_len;
	int _border_width;
	int _gap_width;

	QColor _col_border;
	QColor _col_focus;
};



} // End of namespace
} // End of namespace


#endif
