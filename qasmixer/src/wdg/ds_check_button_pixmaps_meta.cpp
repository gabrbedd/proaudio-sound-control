//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "ds_check_button_pixmaps_meta.hpp"


namespace Wdg
{


//
// DS_Check_Button_Pixmaps_Meta_Bg
//

DS_Check_Button_Pixmaps_Meta_Bg::DS_Check_Button_Pixmaps_Meta_Bg ( ) :
Shared_Pixmaps_Set_Meta ( QSize(), 0 )
{
}

Shared_Pixmaps_Set_Meta *
DS_Check_Button_Pixmaps_Meta_Bg::new_clone ( ) const
{
	return new DS_Check_Button_Pixmaps_Meta_Bg ( *this );
}


//
// DS_Check_Button_Pixmaps_Meta_Handle
//

DS_Check_Button_Pixmaps_Meta_Handle::DS_Check_Button_Pixmaps_Meta_Handle ( ) :
Shared_Pixmaps_Set_Meta ( QSize(), 1 )
{
}

Shared_Pixmaps_Set_Meta *
DS_Check_Button_Pixmaps_Meta_Handle::new_clone ( ) const
{
	return new DS_Check_Button_Pixmaps_Meta_Handle ( *this );
}


} // End of namespace
