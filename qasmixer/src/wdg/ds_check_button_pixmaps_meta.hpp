//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_ds_check_button_pixmaps_meta_hpp__
#define __INC_ds_check_button_pixmaps_meta_hpp__


#include "shared_pixmaps_set.hpp"
#include "shared_pixmaps_set_buffer.hpp"


namespace Wdg
{


//
// DS_Check_Button_Pixmaps_Meta_Bg
//

class DS_Check_Button_Pixmaps_Meta_Bg :
	public Shared_Pixmaps_Set_Meta
{
	public:

	DS_Check_Button_Pixmaps_Meta_Bg ( );

	Shared_Pixmaps_Set_Meta *
	new_clone ( ) const;
};


//
// DS_Check_Button_Pixmaps_Meta_Handle
//

class DS_Check_Button_Pixmaps_Meta_Handle :
	public Shared_Pixmaps_Set_Meta
{
	public:

	DS_Check_Button_Pixmaps_Meta_Handle ( );

	Shared_Pixmaps_Set_Meta *
	new_clone ( ) const;
};


} // End of namespace

#endif


