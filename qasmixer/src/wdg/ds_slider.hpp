//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_ds_slider_hpp__
#define __INC_ds_slider_hpp__

#include <QAbstractSlider>
#include <QTime>
#include <QTimer>
#include <QPen>
#include <QBrush>

#include "shared_pixmaps_set_hub.hpp"
#include "ds_slider_pixmaps_meta.hpp"
#include "uint_mapper.hpp"
#include "cubic_curve.hpp"


namespace Wdg
{



///
/// @brief Dynamic size slider
///
class DS_Slider :
	public QWidget
{
	Q_OBJECT

	// Public methods
	public:

	DS_Slider (
		QWidget * parent_n = 0,
		Shared_Pixmaps_Set_Hub * hub_n = 0 );

	~DS_Slider ( );


	// Size hints

	QSize
	sizeHint ( ) const;

	QSize
	minimumSizeHint ( ) const;


	// Index

	unsigned long
	current_index ( ) const;

	unsigned long
	maximum_index ( ) const;


	bool
	slider_down ( ) const;


	// Mouse wheel

	void
	set_wheel_degrees (
		unsigned int degrees_n );

	unsigned int
	wheel_degrees ( ) const;


	// Hub

	Shared_Pixmaps_Set_Hub *
	hub ( ) const;

	void
	set_hub (
		Shared_Pixmaps_Set_Hub * hub_n );


	// Pixmap meta bg

	const DS_Slider_Pixmaps_Meta_Bg &
	pixmaps_meta_bg ( ) const;

	void
	set_pixmaps_meta_bg (
		const DS_Slider_Pixmaps_Meta_Bg & cfg_n );


	// Pixmap meta handle

	const DS_Slider_Pixmaps_Meta_Handle &
	pixmaps_meta_handle ( ) const;

	void
	set_pixmaps_meta_handle (
		const DS_Slider_Pixmaps_Meta_Handle & cfg_n );


	// Pixmap meta marker

	const DS_Slider_Pixmaps_Meta_Marker &
	pixmaps_meta_marker ( ) const;

	void
	set_pixmaps_meta_marker (
		const DS_Slider_Pixmaps_Meta_Marker & cfg_n );


	// Public slots
	public slots:

	void
	set_current_index (
		unsigned long idx_n );

	void
	adjust_current_index (
		long idx_delta_n );

	void
	set_maximum_index (
		unsigned long idx_n );


	// Signals
	signals:

	void
	sig_current_index_changed (
		unsigned long idx_n );

	void
	sig_maximum_index_changed (
		unsigned long idx_n );


	// Protected methods
	protected:

	void
	set_slider_down (
		bool down_n );

	virtual
	void
	current_index_changed ( );

	virtual
	void
	maximum_index_changed ( );


	// Mapping: Pixel <=> Value

	unsigned long
	map_handle_pos_to_index (
		int handle_pos_n );

	unsigned int
	map_index_to_handle_pos (
		unsigned long idx_n );


	unsigned int
	map_tick_pos_to_handle_pos (
		int tick_pos_n );

	unsigned int
	map_handle_pos_to_tick_pos (
		int handle_pos_n );


	unsigned long
	map_tick_pos_to_index (
		int tick_pos_n );

	unsigned int
	map_index_to_tick_pos (
		unsigned long idx_n );


	void
	clear_index_mappers ( );

	void
	update_index_mappers ( );


	// Cursor mode

	bool
	update_cursor_mode ( );


	// Handle position

	/// @return true if the handle position was changed
	bool
	set_handle_pos (
		unsigned int pos_n );


	void
	update_value_from_handle_pos ( );

	void
	finish_handle_manipulation ( );


	// Change events

	void
	changeEvent (
		QEvent * event_n );


	// Focus events

	void
	focusInEvent (
		QFocusEvent * event_n );

	void
	focusOutEvent (
		QFocusEvent * event_n );


	// Enter events

	void
	enterEvent (
		QEvent * event_n );

	void
	leaveEvent (
		QEvent * event_n );


	// Mouse events

	void
	mousePressEvent (
		QMouseEvent * event_n );

	void
	mouseReleaseEvent (
		QMouseEvent * event_n );

	void
	mouseMoveEvent (
		QMouseEvent * event_n );

	void
	wheelEvent (
		QWheelEvent * event_n );


	// Keyboard events

	void
	keyPressEvent (
		QKeyEvent * event_n );

	void
	keyReleaseEvent (
		QKeyEvent * event_n );


	// Window events

	void
	resizeEvent (
		QResizeEvent * event_n );

	void
	paintEvent (
		QPaintEvent * event_n );


	/// @brief Fetches new pixmaps from the image buffers
	void
	update_pixmaps ( );

	void
	update_painter_utils ( );



	// Private slots
	private slots:

	void
	anim_tick ( );


	// Private methods
	private:

	void
	anim_stop ( );

	void
	anim_snap_start ( );

	bool
	anim_snap_tick (
		unsigned int msec_n );



	// Private attributes
	private:

	unsigned long _current_index;
	unsigned long _maximum_index;

	unsigned int _wheel_degrees;

	long _step_single;
	long _step_page;


	// Pixmap sets
	Shared_Pixmaps_Set * _pxm_bg;
	Shared_Pixmaps_Set * _pxm_handle;
	Shared_Pixmaps_Set * _pxm_marker;

	QRect _handle_rect;
	QPen _snap_pen;
	QBrush _snap_brush;

	unsigned int _handle_pos;
	unsigned int _handle_pos_index;
	unsigned int _handle_pos_hint;
	unsigned int _handle_pos_max;
	int _marker_offset[2];
	int _tick_min;
	int _tick_max;
	QPoint _mouse_last;

	UInt_Mapper * _map_hp_idx;
	UInt_Mapper * _map_idx_hp;

	bool _slider_down;
	bool _update_pixmaps;
	bool _cursor_select_allowed;
	bool _cursor_over_handle;
	bool _cursor_value_hinting;
	bool _anim_run_snap;

	// Animation
	QTimer _anim_timer;

	QTime _anim_snap_time;
	unsigned int _anim_snap_msec_max;
	Cubic_Curve _cubic_curve;

	// Graphics creation
	const unsigned int _handle_min_height;
	const unsigned int _handle_max_height;
	QSize _marker_size;

	// Pixmap buffers
	DS_Slider_Pixmaps_Meta_Bg _pxm_meta_bg;
	DS_Slider_Pixmaps_Meta_Handle _pxm_meta_handle;
	DS_Slider_Pixmaps_Meta_Marker _pxm_meta_marker;

	Shared_Pixmaps_Set_Hub * _hub;
};


inline
unsigned long
DS_Slider::current_index ( ) const
{
	return _current_index;
}


inline
unsigned long
DS_Slider::maximum_index ( ) const
{
	return _maximum_index;
}


inline
bool
DS_Slider::slider_down ( ) const
{
	return _slider_down;
}


inline
unsigned int
DS_Slider::wheel_degrees ( ) const
{
	return _wheel_degrees;
}


inline
Shared_Pixmaps_Set_Hub *
DS_Slider::hub ( ) const
{
	return _hub;
}


inline
const DS_Slider_Pixmaps_Meta_Bg &
DS_Slider::pixmaps_meta_bg ( ) const
{
	return _pxm_meta_bg;
}


inline
const DS_Slider_Pixmaps_Meta_Handle &
DS_Slider::pixmaps_meta_handle ( ) const
{
	return _pxm_meta_handle;
}


inline
const DS_Slider_Pixmaps_Meta_Marker &
DS_Slider::pixmaps_meta_marker ( ) const
{
	return _pxm_meta_marker;
}


} // End of namespace


#endif
