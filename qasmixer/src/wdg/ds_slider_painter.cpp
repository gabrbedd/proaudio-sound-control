//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "ds_slider_painter.hpp"

#include <iostream>

namespace Wdg
{
namespace Painter
{


DS_Slider_Painter::DS_Slider_Painter ( ):
_tick_min_dist ( 8 ),
_pm_meta ( 0 ),
_pm_meta_handle ( 0 ),
_pm_meta_bg ( 0 ),
_pm_meta_marker ( 0 )
{
}


int
DS_Slider_Painter::paint_pixmaps (
	Shared_Pixmaps_Set * spms_n,
	unsigned int image_type_n )
{
	int res ( -1 );

	if ( spms_n != 0 ) {
		_pm_meta = spms_n->meta();
		if ( _pm_meta != 0 ) {
			set_current_palette ( _pm_meta->palette );

			if ( image_type_n == 0 ) {

				// Paint background images
				_pm_meta_bg = dynamic_cast < const DS_Slider_Pixmaps_Meta_Bg * > ( _pm_meta );
				if ( _pm_meta_bg != 0 ) {
					spms_n->set_num_pixmaps ( 6 );
					res = this->paint_bg ( spms_n );
				}

			} else if ( image_type_n == 1 ) {

				// Paint handle images
				_pm_meta_handle = dynamic_cast < const DS_Slider_Pixmaps_Meta_Handle * > ( _pm_meta );
				spms_n->set_num_pixmaps ( 5 );
				res = this->paint_handle ( spms_n );

			} else if ( image_type_n == 2 ) {

				// Paint marker images
				_pm_meta_marker = dynamic_cast < const DS_Slider_Pixmaps_Meta_Marker * > ( _pm_meta );
				spms_n->set_num_pixmaps ( 2 );
				res = this->paint_marker ( spms_n );
			}
		}
	}

	return res;
}


} // End of namespace
} // End of namespace
