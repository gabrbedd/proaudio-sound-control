//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_ds_slider_painter_hpp__
#define __INC_ds_slider_painter_hpp__

#include "ds_slider_pixmaps_meta.hpp"
#include "pixmaps_painter.hpp"


namespace Wdg
{
namespace Painter
{


///
/// @brief DS_Slider_Painter
///
class DS_Slider_Painter :
	public Pixmaps_Painter
{
	// Public methods
	public:

	DS_Slider_Painter ( );

	int
	tick_min_dist ( ) const;

	void
	set_tick_min_dist (
		int delta_n );


	const Shared_Pixmaps_Set_Meta *
	pm_meta ( ) const;

	const DS_Slider_Pixmaps_Meta_Handle *
	pm_meta_handle ( ) const;

	const DS_Slider_Pixmaps_Meta_Bg *
	pm_meta_bg ( ) const;

	const DS_Slider_Pixmaps_Meta_Marker *
	pm_meta_marker ( ) const;


	int
	paint_pixmaps (
		Shared_Pixmaps_Set * spms_n,
		unsigned int image_type_n );


	// Protected methods
	protected:

	///
	/// @brief Paints background images
	///
	/// 0 - idle
	/// 1 - focus
	/// 2 - weak focus
	///
	virtual
	int
	paint_bg (
		Shared_Pixmaps_Set * spms_n ) = 0;


	///
	/// @brief Paints handle images
	///
	/// 0 - idle
	/// 1 - focus
	/// 2 - idle + hover
	/// 3 - focus + hover
	/// 4 - isDown()
	///
	virtual
	int
	paint_handle (
		Shared_Pixmaps_Set * spms_n ) = 0;


	///
	/// @brief Paints marker images
	///
	/// 0 - Current value marker
	/// 1 - Value hint marker
	///
	virtual
	int
	paint_marker (
		Shared_Pixmaps_Set * spms_n ) = 0;


	bool
	has_focus ( ) const;

	bool
	has_weak_focus ( ) const;

	bool
	mouse_over ( ) const;

	bool
	is_down ( ) const;


	void
	set_has_focus (
		bool flag_n );

	void
	set_has_weak_focus (
		bool flag_n );

	void
	set_mouse_over (
		bool flag_n );

	void
	set_is_down (
		bool flag_n );


	// Private attributes
	private:

	int _tick_min_dist;

	const Shared_Pixmaps_Set_Meta * _pm_meta;

	const DS_Slider_Pixmaps_Meta_Handle * _pm_meta_handle;

	const DS_Slider_Pixmaps_Meta_Bg * _pm_meta_bg;

	const DS_Slider_Pixmaps_Meta_Marker * _pm_meta_marker;


	bool _has_focus;

	bool _has_weak_focus;

	bool _mouse_over;

	bool _is_down;
};


inline
int
DS_Slider_Painter::tick_min_dist ( ) const
{
	return _tick_min_dist;
}


inline
void
DS_Slider_Painter::set_tick_min_dist (
	int delta_n )
{
	_tick_min_dist = delta_n;
}


inline
const Shared_Pixmaps_Set_Meta *
DS_Slider_Painter::pm_meta ( ) const
{
	return _pm_meta;
}


inline
const DS_Slider_Pixmaps_Meta_Handle *
DS_Slider_Painter::pm_meta_handle ( ) const
{
	return _pm_meta_handle;
}


inline
const DS_Slider_Pixmaps_Meta_Bg *
DS_Slider_Painter::pm_meta_bg ( ) const
{
	return _pm_meta_bg;
}


inline
const DS_Slider_Pixmaps_Meta_Marker *
DS_Slider_Painter::pm_meta_marker ( ) const
{
	return _pm_meta_marker;
}


inline
bool
DS_Slider_Painter::has_focus ( ) const
{
	return _has_focus;
}


inline
bool
DS_Slider_Painter::has_weak_focus ( ) const
{
	return _has_weak_focus;
}


inline
bool
DS_Slider_Painter::mouse_over ( ) const
{
	return _mouse_over;
}


inline
bool
DS_Slider_Painter::is_down ( ) const
{
	return _is_down;
}


inline
void
DS_Slider_Painter::set_has_focus (
	bool flag_n )
{
	_has_focus = flag_n;
}


inline
void
DS_Slider_Painter::set_has_weak_focus (
	bool flag_n )
{
	_has_weak_focus = flag_n;
}


inline
void
DS_Slider_Painter::set_mouse_over (
	bool flag_n )
{
	_mouse_over = flag_n;
}


inline
void
DS_Slider_Painter::set_is_down (
	bool is_down_n )
{
	_is_down = is_down_n;
}




} // End of namespace
} // End of namespace


#endif


