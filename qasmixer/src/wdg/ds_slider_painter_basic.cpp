//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ds_slider_painter_basic.hpp"

#include "color_methods.hpp"
#include "uint_mapper.hpp"

#include <cmath>
#include <iostream>


namespace Wdg
{
namespace Painter
{


DS_Slider_Painter_Basic::DS_Slider_Painter_Basic ( )
{
}


int
DS_Slider_Painter_Basic::paint_bg (
	Shared_Pixmaps_Set * spms_n )
{
	int res ( -1 );
	if ( pm_meta_bg()->size.isValid() ) {
		set_is ( pm_meta_bg()->size );

		_bw = is().width() / 24;
		_bw = qMax ( 2, _bw );

		_ew = qMax ( 1, ( _bw / 4 ) );
		_bevel = _bw;


		calc_ticks();
		paint_bg_base();

		unsigned int idx ( 0 );
		for ( unsigned int ii=0; ii < 2; ++ii ) {

			set_is_down ( ii > 0 );

			set_has_focus ( false );
			set_has_weak_focus ( false );
			paint_bg_type ( spms_n, idx++ );

			set_has_focus ( true );
			set_has_weak_focus ( false );
			paint_bg_type ( spms_n, idx++ );

			set_has_focus ( true );
			set_has_weak_focus ( true );
			paint_bg_type ( spms_n, idx++ );
		}

		// Clear base image
		_img_base = QImage();
		res = 0;
	}
	return res;
}


void
DS_Slider_Painter_Basic::calc_ticks ( )
{
	int in_width ( is().width() - 2*_bw );
	_tick_width[0] = in_width * 1 / 2;
	_tick_width[1] = in_width * 1 / 4;
	_tick_width[2] = in_width * 1 / 16;

	_tick_width[0] = std::max ( _tick_width[0], 6 );
	_tick_width[1] = std::max ( _tick_width[1], 4 );
	_tick_width[2] = std::max ( _tick_width[2], 2 );

	if ( ( is().width() % 2 ) == 0 ) {
		for ( int ii=0; ii < 3; ++ii ) {
			if ( ( _tick_width[ii] % 2 ) != 0 ) {
				++( _tick_width[ii] );
			}
		}
	} else {
		for ( int ii=0; ii < 3; ++ii ) {
			if ( ( _tick_width[ii] % 2 ) == 0 ) {
				++( _tick_width[ii] );
			}
		}
	}
}


void
DS_Slider_Painter_Basic::paint_bg_base ( )
{
	_img_base = QImage ( is(), QImage::Format_ARGB32 );
	_img_base.fill ( 0 );

	QPainter pnt ( &_img_base );
	pnt.setRenderHints (
		QPainter::Antialiasing |
		QPainter::TextAntialiasing |
		QPainter::SmoothPixmapTransform );

	set_painter ( &pnt );

	paint_bg_area();
	paint_bg_frame();
}


void
DS_Slider_Painter_Basic::paint_bg_type (
	Shared_Pixmaps_Set * spms_n,
	int pixmap_idx_n )
{
	QImage img ( _img_base );

	{
		QPainter pnt ( &img );
		pnt.setRenderHints (
			QPainter::Antialiasing |
			QPainter::TextAntialiasing |
			QPainter::SmoothPixmapTransform );

		set_painter ( &pnt );

		paint_bg_area_deco();
		paint_bg_ticks();
		paint_bg_frame_deco();
	}

	spms_n->pixmap_ref ( pixmap_idx_n ) = QPixmap::fromImage ( img );
}


void
DS_Slider_Painter_Basic::paint_bg_area ( )
{
	Primitive_Painter spainter ( painter() );
	const QRect img_rect ( QPoint ( 0, 0 ), is() );

	QColor col_li ( cpal().color ( QPalette::Button ) );
	QColor col_mid ( cpal().color ( QPalette::Button ) );
	QColor col_dk ( cpal().color ( QPalette::Mid ) );

	col_li.setAlpha ( 55 );
	col_dk.setAlpha ( 190 );

	// Inner area
	const QRectF re_in (
		QPoint ( _bw, _bw ),
		QSize ( is().width() - 2*_bw, is().height() - 2*_bw ) );

	const qreal x_start = _bw;
	const qreal x_end = is().width() - _bw;
	const qreal x_mid = x_start + re_in.width() / 3.0;


	QPainterPath path_area;
	{
		const qreal bev ( _bevel + ( _bw + 1.5*_ew ) * ( sqrt ( 2.0 ) - 2.0 ) );
		spainter.setup_bevel_rect ( path_area, re_in,  bev );
	}

	painter()->setPen ( Qt::NoPen );
	painter()->setBrush ( col_mid );
	painter()->drawPath ( path_area );

	// 3D fake gradient
	{
		QLinearGradient gr ( x_start, 0.0, x_end, 0.0 );
		gr.setColorAt ( 0.0, col_li );
		gr.setColorAt ( x_mid / ( x_end - x_start ), col_mid );
		gr.setColorAt ( 1.0, col_dk );

		painter()->setBrush ( gr );
		painter()->drawPath ( path_area );
	}

	// Sound color overlay
	{
		QColor col_snd_beg ( cpal().color ( QPalette::Window ) );
		QColor col_snd_end ( col_snd_beg );
		col_snd_beg.setAlpha ( 30 );
		col_snd_end.setAlpha ( 0 );

		QLinearGradient gr ( x_start, 0.0, x_start + re_in.width() / 7.0, 0.0 );
		gr.setColorAt ( 0, col_snd_beg );
		gr.setColorAt ( 1, col_snd_end );

		painter()->setBrush ( gr );
		painter()->drawPath ( path_area );
	}
}


void
DS_Slider_Painter_Basic::paint_bg_area_deco ( )
{
	const int range_max_idx ( pm_meta_bg()->ticks_range_max_idx );
	int y_min;
	int y_bottom;
	int y_top;

	bool has_minimum ( pm_meta_bg()->bg_show_image );
	bool has_zero_split ( false );

	y_bottom = is().height() - 1;
	y_bottom -= pm_meta_bg()->ticks_range_start;
	y_top = y_bottom - range_max_idx;
	y_min = y_bottom;

	if ( has_minimum ) {
		Wdg::UInt_Mapper_Auto mapper ( range_max_idx, pm_meta_bg()->ticks_max_idx );
		y_min -= mapper.v2_to_v1 ( pm_meta_bg()->bg_tick_min_idx );
	}
	has_zero_split = ( ( y_min > y_top ) && ( y_min < y_bottom ) );

	const qreal w_tick ( ( _tick_width[0] + _tick_width[1] ) / 2.0 );
	const qreal w_out ( w_tick * 2.0 / 3.0 );
	const qreal w_neck ( 1 );

	qreal x_out_l ( ( is().width() - w_tick ) / 2.0 );
	qreal x_neck_l ( x_out_l );
	qreal x_out_r;
	qreal x_neck_r;

	x_neck_l = std::floor ( x_neck_l );
	x_neck_r = x_neck_l + w_neck;

	x_out_l = std::floor ( x_out_l );
	x_out_r = x_out_l + w_out;

	unsigned int num_pts ( 0 );
	QPointF pts[6];

	if ( has_minimum ) {
		if ( has_zero_split ) {
			qreal x_out_rt ( x_out_r );
			qreal x_out_rb ( x_out_r );

			const int dt ( y_min - y_top );
			const int db ( y_bottom - y_min );
			if ( dt > db ) {
				int ww = ( db * w_out ) / dt;
				x_out_rb = x_out_l + ww;
			} else {
				int ww = ( dt * w_out ) / db;
				x_out_rt = x_out_l + ww;
			}

			num_pts = 6;
			pts[0] = QPointF ( x_out_l,  y_top + 0.5 );
			pts[1] = QPointF ( x_out_rt, y_top + 0.5 );
			pts[2] = QPointF ( x_neck_r, y_min + 0.5 );
			pts[3] = QPointF ( x_out_rb, y_bottom + 0.5 );
			pts[4] = QPointF ( x_out_l,  y_bottom + 0.5 );
			pts[5] = QPointF ( x_neck_l, y_min + 0.5 );
		} else {
			qreal x_rt;
			qreal x_rb;
			if ( y_min < y_bottom ) {
				x_rt = x_neck_r;
				x_rb = x_out_r;
			} else {
				x_rt = x_out_r;
				x_rb = x_neck_r;
			}

			num_pts = 4;
			pts[0] = QPointF ( x_neck_l, y_top + 0.5 );
			pts[1] = QPointF ( x_rt,     y_top + 0.5 );
			pts[2] = QPointF ( x_rb,     y_bottom + 0.5 );
			pts[3] = QPointF ( x_out_l,  y_bottom + 0.5 );
		}
	} else {
		num_pts = 4;
		x_out_r = x_out_l + w_out / 2.0;
		pts[0] = QPointF ( x_out_l,  y_top + 0.5 );
		pts[1] = QPointF ( x_out_r, y_top + 0.5 );
		pts[2] = QPointF ( x_out_r, y_bottom + 0.5 );
		pts[3] = QPointF ( x_out_l,  y_bottom + 0.5 );
	}

	{
		QColor col_fill ( cpal().color ( QPalette::Window ) );
		QColor col_pen ( cpal().color ( QPalette::WindowText ) );
		if ( is_down() ) {
			col_fill.setAlpha ( 40 );
			col_pen.setAlpha ( 40 );
		} else {
			col_fill.setAlpha ( 16 );
			col_pen.setAlpha ( 18 );
		}

		painter()->setBrush ( col_fill );
		painter()->setPen ( col_pen );
	}

	painter()->drawPolygon ( pts, num_pts );
}


void
DS_Slider_Painter_Basic::paint_bg_frame ( )
{
	Primitive_Painter spainter ( painter() );

	QColor fcol;
	{
		QColor col_btn ( cpal().color ( QPalette::Button ) );
		QColor col_mid ( cpal().color ( QPalette::Mid ) );
		if ( col_mid == col_btn ) {
			col_mid = cpal().color ( QPalette::WindowText );
			fcol = col_mix ( col_btn, col_mid, 1, 1 );
		} else {
			fcol = col_mix ( col_btn, col_mid, 1, 2 );
		}
		//fcol = col_mid;
	}

	QRect re_img ( QPoint ( 0, 0 ), is() );
	spainter.draw_bevel_raised_frame (
		re_img, _bw, _ew, _bevel, fcol );
}


void
DS_Slider_Painter_Basic::paint_bg_frame_deco ( )
{
	Primitive_Painter spainter ( painter() );

	const QColor col_norm ( cpal().color ( QPalette::Button ) );
	const QColor col_text ( cpal().color ( QPalette::ButtonText ) );
	const QColor col_text2 ( cpal().color ( QPalette::WindowText ) );
	const QColor col_high ( cpal().color ( QPalette::Highlight ) );

	if ( has_focus() ) {
		QColor bcol;
		if ( has_weak_focus() ) {
			bcol = col_high;
		} else {
			bcol = Wdg::Painter::col_mix ( col_text, col_text2, 2, 1 );
			bcol = Wdg::Painter::col_mix ( bcol, col_norm, 3, 1 );
		}

		QRectF re_in ( QPoint ( 0, 0 ), is() );

		int frw ( qMax ( 1, _bw - 2*_ew ) );
		qreal bev ( _bevel );

		bool shrink ( true );
		if ( frw < 2 ) {
			if ( !has_weak_focus() ) {
				if ( _bw > 2 ) {
					frw = qMin ( 2, _bw );
				} else {
					//shrink = false;
				}
			}
		}

		if ( shrink ) {
			re_in.adjust ( _ew, _ew, -_ew, -_ew );
			bev += _ew * ( sqrt ( 2.0 ) - 2.0 );
		}

		painter()->setPen ( Qt::NoPen );
		painter()->setBrush ( bcol );
		spainter.draw_bevel_frame (
			re_in, frw, bev );
	}
}


void
DS_Slider_Painter_Basic::paint_bg_ticks ( )
{
	const int range_max_idx ( pm_meta_bg()->ticks_range_max_idx );
	unsigned int ticks_max_idx;
	int idx_delta ( 1 );
	int y_off;

	y_off = is().height() - 1;
	y_off -= pm_meta_bg()->ticks_range_start;

	const int y_bottom = y_off;
	const int y_top    = y_off - range_max_idx;

	// Setup main tick position mapper
	ticks_max_idx = pm_meta_bg()->ticks_max_idx;
	if ( ticks_max_idx > 0 ) {
		if ( ( range_max_idx / ticks_max_idx ) < 2 ) {
			ticks_max_idx = range_max_idx / tick_min_dist();
		}
	}

	Wdg::UInt_Mapper_Up mapper ( ticks_max_idx, range_max_idx );

	if ( ticks_max_idx > 0 ) {
		const int tmd ( mapper.min_dist() );
		while ( ( idx_delta * tmd ) < tick_min_dist() ) {
			++idx_delta;
		}
	}


	// Paint
	painter()->setPen ( Qt::NoPen );

	{
		QColor tick_col ( cpal().color ( QPalette::WindowText ) );
		tick_col = col_mix ( tick_col, cpal().color ( QPalette::Button ), 3, 1 );

		const int idx_start = idx_delta;
		for ( unsigned int ii=idx_start; ii < ticks_max_idx; ii += idx_delta ) {
			int yy = y_bottom - mapper.map ( ii );
			if ( ( yy - y_top ) < tick_min_dist() ) {
				break;
			}
			paint_bg_tick ( yy, _tick_width[1], tick_col );
		}
	}


	{
		QColor tick_col ( cpal().color ( QPalette::WindowText ) );
		tick_col = col_mix ( tick_col, cpal().color ( QPalette::Button ), 4, 1 );

		// Bottom and top ticks
		if ( ticks_max_idx > 0 ) {
			paint_bg_tick ( y_top, _tick_width[0], tick_col );
		}
		paint_bg_tick ( y_bottom, _tick_width[0], tick_col );
	}
}


void
DS_Slider_Painter_Basic::paint_bg_tick (
	qreal tick_pos_n,
	qreal tick_width_n,
	const QColor & col_n )
{
	const qreal tick_start = ( is().width() - tick_width_n ) / 2.0;

	QColor col ( col_n );

	{
		col.setAlpha ( 32 );
		QPolygonF po;
		qreal xl ( tick_start - 0.5 );
		qreal xr ( tick_start + tick_width_n + 0.5 );
		po << QPointF ( xl, tick_pos_n - 1 );
		po << QPointF ( xr, tick_pos_n - 1 );
		po << QPointF ( xr + 1, tick_pos_n );
		po << QPointF ( xr + 1, tick_pos_n + 1 );
		po << QPointF ( xr, tick_pos_n + 2 );
		po << QPointF ( xl, tick_pos_n + 2 );
		po << QPointF ( xl - 1, tick_pos_n + 1 );
		po << QPointF ( xl - 1, tick_pos_n );

		painter()->setBrush ( col );
		painter()->drawPolygon ( po );
	}

	{
		col.setAlpha ( 200 );
		painter()->setBrush ( col );
		QRectF rect ( tick_start, tick_pos_n, tick_width_n, 1.0 );
		painter()->drawRect ( rect );
	}
}


int
DS_Slider_Painter_Basic::paint_handle (
	Shared_Pixmaps_Set * spms_n )
{
	if ( spms_n->pixmap_size().isValid() ) {

		set_is ( spms_n->pixmap_size() );

		_bw = is().width() / 24;
		_bw = qMax ( 2, _bw );

		_ew = qMax ( 1, ( _bw / 4 ) );
		_bevel = _bw;

		set_has_focus ( false );
		set_mouse_over ( false );
		set_is_down ( false );
		paint_handle_type ( spms_n, 0 );

		set_has_focus ( true );
		paint_handle_type ( spms_n, 1 );

		set_has_focus ( false );
		set_mouse_over ( true );
		paint_handle_type ( spms_n, 2 );

		set_has_focus ( true );
		paint_handle_type ( spms_n, 3 );

		set_is_down ( true );
		paint_handle_type ( spms_n, 4 );

		return 0;
	}
	return 1;
}


void
DS_Slider_Painter_Basic::paint_handle_type (
	Shared_Pixmaps_Set * spms_n,
	int pixmap_idx_n )
{
	QImage img ( is(), QImage::Format_ARGB32 );
	img.fill ( 0 );

	QPainter pnt ( &img );

	pnt.setRenderHints (
		QPainter::Antialiasing |
		QPainter::TextAntialiasing |
		QPainter::SmoothPixmapTransform );

	set_painter ( &pnt );

	paint_handle_area() ;
	paint_handle_frame();
	paint_handle_items();

	spms_n->pixmap_ref ( pixmap_idx_n ) = QPixmap::fromImage ( img );
}


void
DS_Slider_Painter_Basic::paint_handle_area ( )
{
	Primitive_Painter spainter ( painter() );

	const QRectF img_rect ( QPointF ( 0, 0 ), is() );
	const int iw ( is().width() - 2*_bw );
	const int ih ( is().height() - 2*_bw );

	QPainterPath path_all;
	spainter.setup_bevel_rect ( path_all, img_rect, _bevel );

	painter()->setPen ( Qt::NoPen );

	// Background
	{
		const qreal grw ( iw / 3.0 );
		const qreal grwn ( grw / qreal ( iw ) );

		QColor col_base ( cpal().color ( QPalette::Window ) );

		if ( cpal().currentColorGroup() == QPalette::Disabled ) {
			QColor col_btn ( cpal().color ( QPalette::Button ) );
			col_base = col_mix ( col_base, col_btn, 2, 1 );
		}

		QColor col_edge ( col_base );
		QColor col_center ( col_base );

		col_edge.setAlpha ( 170 );
		col_center.setAlpha ( 145 );

		// Draw gradient
		QLinearGradient gr ( _bw, 0.0, is().width() - _bw, 0.0 );
		gr.setColorAt ( 0.0, col_edge );
		gr.setColorAt ( grwn, col_center );
		gr.setColorAt ( 1.0 - grwn, col_center );
		gr.setColorAt ( 1.0, col_edge );

		painter()->setBrush ( gr );
		painter()->drawPath ( path_all );
	}


	// Highlight
	{
		QColor col_edge ( cpal().color ( QPalette::Light ) );
		QColor col_trans ( cpal().color ( QPalette::Light ) );
		col_edge.setAlpha ( 128 );
		col_trans.setAlpha ( 30 );

		const qreal hl_dy ( ih / 8.0 );
		QPointF p_top ( 0.0, _bw );
		QPointF p_bot ( 0.0, is().height() - _bw );

		QLinearGradient gr;
		gr.setSpread ( QGradient::ReflectSpread );

		if ( is_down() ) {
			p_bot.ry() -= hl_dy;
			gr.setStart ( p_bot );
			gr.setFinalStop ( p_top );
		} else {
			p_top.ry() += hl_dy;
			gr.setStart ( p_top );
			gr.setFinalStop ( p_bot );

			if ( mouse_over() ) {
				col_edge.setAlpha ( 150 );
			}
		}

		gr.setColorAt ( 0, col_edge );
		gr.setColorAt ( 1, col_trans );

		painter()->setBrush ( gr );
		painter()->drawPath ( path_all );
	}
}


void
DS_Slider_Painter_Basic::paint_handle_frame ( )
{
	Primitive_Painter spainter ( painter() );

	//const QColor col_btn ( cpal().color ( QPalette::Button ) );
	const QColor col_bg ( cpal().color ( QPalette::Window ) );
	const QColor col_fg ( cpal().color ( QPalette::WindowText ) );

	QColor col;
	col = Wdg::Painter::col_mix ( col_bg, col_fg, 1, 1 );

	const QRectF rect ( QPointF ( 0, 0 ), is() );
	spainter.draw_bevel_raised_frame (
		rect, _bw, _ew, _bevel,
		col );
}


void
DS_Slider_Painter_Basic::paint_handle_items ( )
{
	// Colors

	QColor col_bg ( cpal().color ( QPalette::Window ) );

	QColor col_light ( cpal().color ( QPalette::Light ) );
	col_light = Wdg::Painter::col_mix ( col_light, col_bg, 3, 2 );

	QColor col_dark ( cpal().color ( QPalette::WindowText ) );
	col_dark = Wdg::Painter::col_mix ( col_dark, col_bg, 5, 1 );


	// Variables
	const qreal center_v ( is().height() / 2.0 );
	const qreal height_sub ( _bevel + _bw * ( sqrt ( 2.0 ) - 1.0 ) );
	const qreal line_width_long ( 1.3333 ); // Width of the bright border
	const qreal line_width_small ( 1.25 ); // Width of the bright border
	const qreal line_width_fine ( 1.0 ); // Width of the bright border
	qreal tri_height_base;
	qreal tri_width;

	{
		const qreal in_w ( is().width() - 2*_bw );
		const qreal in_h ( is().height() - 2*_bw );
		const qreal tri_len_h ( in_w / 6.0f );
		const qreal tri_len_v ( in_h / 6.0f );
		const qreal tri_len ( qMin ( tri_len_v, tri_len_h ) );
		tri_height_base = tri_len;
		tri_width = tri_len;
		tri_width += 1;
	}

	if ( is_down() ) {
		const qreal down_scale ( 3.0 / 4.0 );
		tri_width *= down_scale;
	}
	// Round
	tri_width = std::floor ( tri_width + 0.5 );


	// Long path
	QPainterPath pp_long;
	{
		const qreal bb_w_loc ( line_width_long / 2.0 );

		const qreal xoff ( _bw );

		// Dark triangle
		{
			qreal pp_w ( tri_width - bb_w_loc );
			qreal pp_h ( is().height() / 2.0 - height_sub - bb_w_loc );

			const QPointF ptop ( xoff, center_v - pp_h );
			const QPointF pbot ( xoff, center_v + pp_h );
			const QPointF pmid_top ( xoff + pp_w, center_v - 0.5 );
			const QPointF pmid_bot ( xoff + pp_w, center_v + 0.5 );
			const qreal sl_dk_x1 ( pp_w / 2.25 );
			const qreal sl_dk_y1 ( pp_w / 2.5 );
			const qreal sl_dk_y2 ( pp_h / 4.0 );

			pp_long.moveTo ( QPointF ( 0.0, ptop.y() ) );
			pp_long.lineTo ( ptop );
			pp_long.cubicTo (
				QPointF ( ptop.x() + sl_dk_x1, ptop.y() + sl_dk_y1 ),
				QPointF ( pmid_top.x(), pmid_top.y() - sl_dk_y2 ),
				pmid_top );
			pp_long.lineTo ( pmid_bot );
			pp_long.cubicTo (
				QPointF ( pmid_bot.x(), pmid_bot.y() + sl_dk_y2 ),
				QPointF ( pbot.x() + sl_dk_x1, pbot.y() - sl_dk_y1 ),
				pbot );
			pp_long.lineTo ( QPointF ( 0.0, pbot.y() ) );
			pp_long.lineTo ( QPointF ( 0.0, ptop.y() ) );
		}
	}


	// Small triangular path
	QPolygonF pp_small;
	{
		qreal tri_height ( tri_height_base );
		tri_height = std::floor ( tri_height ) + 0.5;

		const qreal xoff ( _bw );
		const qreal pp_w ( tri_width );
		const qreal pp_h ( tri_height );

		pp_small << QPointF (    0, center_v - pp_h );
		pp_small << QPointF ( xoff, center_v - pp_h );
		pp_small << QPointF ( xoff, center_v - pp_h );
		pp_small << QPointF ( xoff + pp_w, center_v );
		pp_small << QPointF ( xoff, center_v + pp_h );
		pp_small << QPointF (    0, center_v + pp_h );
		pp_small << pp_small[0];
	}


	// Paint
	{	// Install clipping rectangle
		const QRect clip_rect (
			_bw, height_sub,
			is().width() - 2*_bw, is().height() - 2*height_sub );

		painter()->setClipRect ( clip_rect );
	}

	const QTransform trans_hmirror (
		-1.0, 0.0, 0.0,
		 0.0, 1.0, 0.0,
		 is().width(), 0.0, 1.0 );

	{	// Paint long triangle
		painter()->setBrush ( col_dark );
		painter()->setPen ( QPen ( col_light, line_width_long ) );

		painter()->drawPath ( pp_long );
		painter()->drawPath ( trans_hmirror.map ( pp_long ) );
	}

	{ 	// Paint small triangle
		painter()->setBrush ( col_dark );
		painter()->setPen ( QPen ( col_light, line_width_small ) );

		painter()->drawPolygon ( pp_small );
		painter()->drawPolygon ( trans_hmirror.map ( pp_small ) );
	}

	{	// Center line
		painter()->setPen ( Qt::NoPen );
		const qreal xoff ( _bw + tri_width );
		{
			painter()->setBrush ( col_light );
			const QRectF re_line (
				QPointF ( xoff, center_v - 0.5 - line_width_fine ),
				QSizeF ( is().width() - 2*xoff, 1.0 + line_width_fine*2  ) );
			painter()->drawRect ( re_line );
		}
		{
			painter()->setBrush ( col_dark );
			const QRectF re_line (
				QPointF ( _bw, center_v - 0.5f ),
				QSizeF ( is().width() - 2*_bw, 1.0 ) );
			painter()->drawRect ( re_line );
		}
	}

	painter()->setClipping ( false );
}


int
DS_Slider_Painter_Basic::paint_marker (
	Shared_Pixmaps_Set * spms_n )
{
	if ( spms_n->pixmap_size().isValid() ) {
		set_is ( spms_n->pixmap_size() );

		paint_marker_type ( spms_n, 0 );
		paint_marker_type ( spms_n, 1 );

		return 0;
	}
	return 1;
}


void
DS_Slider_Painter_Basic::paint_marker_type (
	Shared_Pixmaps_Set * spms_n,
	int pixmap_idx_n )
{
	QImage img ( is(), QImage::Format_ARGB32 );
	img.fill ( 0 );

	QPainter pnt ( &img );
	pnt.setRenderHints (
		QPainter::Antialiasing |
		QPainter::TextAntialiasing |
		QPainter::SmoothPixmapTransform );

	set_painter ( &pnt );

	switch ( pixmap_idx_n ) {
		case 0:
			paint_marker_current();
			break;
		case 1:
			paint_marker_hint();
			break;
		default:
			break;
	}

	spms_n->pixmap_ref ( pixmap_idx_n ) = QPixmap::fromImage ( img );
}



void
DS_Slider_Painter_Basic::paint_marker_current ( )
{
	int bevel ( is().width() / 3 );
	bevel = qMax ( bevel, 2 );

	{ // Background
		{
			QColor col ( cpal().color ( QPalette::WindowText ) );
			col.setAlpha ( 64 );
			painter()->setBrush ( col );
			painter()->setPen ( Qt::NoPen );
		}

		const qreal x0 ( 0.0 );
		const qreal x1 ( bevel );
		const qreal x2 ( is().width() - bevel );
		const qreal x3 ( is().width() );

		const qreal y0 ( 0.0 );
		const qreal y1 ( bevel );
		const qreal y2 ( is().height() - bevel );
		const qreal y3 ( is().height() );

		const int num_pts ( 9 );
		QPointF pts[num_pts] = {
			QPointF ( x0, y1 ),
			QPointF ( x1, y0 ),
			QPointF ( x2, y0 ),
			QPointF ( x3, y1 ),
			QPointF ( x3, y2 ),
			QPointF ( x2, y3 ),
			QPointF ( x1, y3 ),
			QPointF ( x0, y2 ),
			QPointF ( x0, y1 ),
		};

		painter()->drawPolygon ( pts, num_pts );
	}


	{ // Foreground
		{
			QColor col ( cpal().color ( QPalette::WindowText ) );
			painter()->setBrush ( col );
			painter()->setPen ( Qt::NoPen );
		}

		qreal delta ( is().width() / 6.0 );
		delta = qMax ( delta, 1.0 );
		qreal delta_sq = delta * ( ::std::sqrt ( 2.0 ) - 1.0 );

		const qreal x0 ( delta );
		const qreal x1 ( bevel + delta_sq );
		const qreal x2 ( is().width() - x1 );
		const qreal x3 ( is().width() - x0 );

		const qreal y0 ( delta );
		const qreal y1 ( bevel + delta_sq );
		const qreal y2 ( is().height() - y1 );
		const qreal y3 ( is().height() - y0 );

		const int num_pts ( 9 );
		QPointF pts[num_pts] = {
			QPointF ( x0, y1 ),
			QPointF ( x1, y0 ),
			QPointF ( x2, y0 ),
			QPointF ( x3, y1 ),
			QPointF ( x3, y2 ),
			QPointF ( x2, y3 ),
			QPointF ( x1, y3 ),
			QPointF ( x0, y2 ),
			QPointF ( x0, y1 ),
		};

		painter()->drawPolygon ( pts, num_pts );
	}
}


void
DS_Slider_Painter_Basic::paint_marker_hint ( )
{
	int bevel ( is().width() / 3 );
	bevel = qMax ( bevel, 2 );

	{
		{
			QColor col ( cpal().color ( QPalette::WindowText ) );
			col.setAlpha ( 100 );
			painter()->setBrush ( col );
			painter()->setPen ( Qt::NoPen );
		}

		const qreal x0 ( 0.0 );
		const qreal x1 ( bevel );
		const qreal x2 ( is().width() - bevel );
		const qreal x3 ( is().width() );

		const qreal y0 ( 0.0 );
		const qreal y1 ( bevel );
		const qreal y2 ( is().height() - bevel );
		const qreal y3 ( is().height() );


		const int num_pts ( 9 );
		QPointF pts[num_pts] = {
			QPointF ( x0, y1 ),
			QPointF ( x1, y0 ),
			QPointF ( x2, y0 ),
			QPointF ( x3, y1 ),
			QPointF ( x3, y2 ),
			QPointF ( x2, y3 ),
			QPointF ( x1, y3 ),
			QPointF ( x0, y2 ),
			QPointF ( x0, y1 ),
		};

		painter()->drawPolygon ( pts, num_pts );
	}
}


} // End of namespace
} // End of namespace

