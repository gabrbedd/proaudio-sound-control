//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_ds_slider_painter_basic_hpp__
#define __INC_ds_slider_painter_basic_hpp__

#include "ds_slider_painter.hpp"
#include "primitive_painter.hpp"


namespace Wdg
{
namespace Painter
{


class DS_Slider_Painter_Basic :
	public DS_Slider_Painter
{
	// Public methods
	public:

	DS_Slider_Painter_Basic ( );


	// Protected methods
	protected:

	// Background

	int
	paint_bg (
		Shared_Pixmaps_Set * spms_n );

	void
	paint_bg_base ( );

	void
	paint_bg_type (
		Shared_Pixmaps_Set * spms_n,
		int pixmap_idx_n );

	void
	paint_bg_area ( );

	void
	paint_bg_area_deco ( );

	void
	paint_bg_frame ( );

	void
	paint_bg_frame_deco ( );

	void
	paint_bg_ticks ( );

	void
	paint_bg_tick (
		qreal tick_pos_n,
		qreal tick_width_n,
		const QColor & col_n );


	void
	calc_ticks ( );


	// Handle

	int
	paint_handle (
		Shared_Pixmaps_Set * spms_n );

	void
	paint_handle_type (
		Shared_Pixmaps_Set * spms_n,
		int pixmap_idx_n );

	void
	paint_handle_area ( );

	void
	paint_handle_frame ( );

	void
	paint_handle_items ( );


	// Marker

	int
	paint_marker (
		Shared_Pixmaps_Set * spms_n );

	void
	paint_marker_type (
		Shared_Pixmaps_Set * spms_n,
		int img_idx_n );

	void
	paint_marker_current ( );

	void
	paint_marker_hint ( );


	// Private attributes
	private:

	int _ew; // Edge width
	int _bw; // Border width x,y

	int _bevel;

	int _tick_width[3];

	QImage _img_base;
};



} // End of namespace
} // End of namespace

#endif


