//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "ds_slider_pixmaps_meta.hpp"


namespace Wdg
{


//
// DS_Slider_Pixmaps_Meta_Bg
//

DS_Slider_Pixmaps_Meta_Bg::DS_Slider_Pixmaps_Meta_Bg ( ) :
Shared_Pixmaps_Set_Meta ( QSize(), 0 ),
ticks_max_idx ( 0 ),
ticks_range_max_idx ( 0 ),
ticks_range_start ( 0 ),
bg_show_image ( 0 ),
bg_tick_min_idx ( 0 )
{
}


bool
DS_Slider_Pixmaps_Meta_Bg::equal (
	const Shared_Pixmaps_Set_Meta & cfg_n ) const
{
	bool res ( false );

	const DS_Slider_Pixmaps_Meta_Bg * cfg_c (
		dynamic_cast < const DS_Slider_Pixmaps_Meta_Bg  * > ( &cfg_n ) );

	if ( cfg_c != 0 ) {
		res = Shared_Pixmaps_Set_Meta::equal ( cfg_n );
		res = res && ( cfg_c->ticks_max_idx == ticks_max_idx );
		res = res && ( cfg_c->ticks_range_max_idx == ticks_range_max_idx );
		res = res && ( cfg_c->ticks_range_start == ticks_range_start );
		res = res && ( cfg_c->bg_show_image == bg_show_image );
		res = res && ( cfg_c->bg_tick_min_idx == bg_tick_min_idx );
	}

	return res;
}


Shared_Pixmaps_Set_Meta *
DS_Slider_Pixmaps_Meta_Bg::new_clone ( ) const
{
	return new DS_Slider_Pixmaps_Meta_Bg ( *this );
}



//
// DS_Slider_Pixmaps_Meta_Handle
//

DS_Slider_Pixmaps_Meta_Handle::DS_Slider_Pixmaps_Meta_Handle ( ) :
Shared_Pixmaps_Set_Meta ( QSize(), 1 )
{
}

Shared_Pixmaps_Set_Meta *
DS_Slider_Pixmaps_Meta_Handle::new_clone ( ) const
{
	return new DS_Slider_Pixmaps_Meta_Handle ( *this );
}


//
// DS_Slider_Pixmaps_Meta_Marker
//

DS_Slider_Pixmaps_Meta_Marker::DS_Slider_Pixmaps_Meta_Marker ( ) :
Shared_Pixmaps_Set_Meta ( QSize(), 2 )
{
}

Shared_Pixmaps_Set_Meta *
DS_Slider_Pixmaps_Meta_Marker::new_clone ( ) const
{
	return new DS_Slider_Pixmaps_Meta_Marker ( *this );
}


} // End of namespace
