//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_ds_slider_pixmaps_meta_hpp__
#define __INC_ds_slider_pixmaps_meta_hpp__

#include "shared_pixmaps_set.hpp"
#include "shared_pixmaps_set_buffer.hpp"


namespace Wdg
{


///
/// @brief DS_Slider_Pixmaps_Meta_Bg
///

class DS_Slider_Pixmaps_Meta_Bg :
	public Shared_Pixmaps_Set_Meta
{
	// Public methods
	public:

	DS_Slider_Pixmaps_Meta_Bg ( );

	bool
	equal (
		const Shared_Pixmaps_Set_Meta & cfg_n ) const;

	Shared_Pixmaps_Set_Meta *
	new_clone ( ) const;


	// Public attributes
	unsigned int ticks_max_idx;
	int ticks_range_max_idx;
	int ticks_range_start;

	// Style
	int bg_show_image;
	unsigned int bg_tick_min_idx; // Which tick has the minimum width
};


class DS_Slider_Pixmaps_Meta_Handle :
	public Shared_Pixmaps_Set_Meta
{
	// Public methods
	public:

	DS_Slider_Pixmaps_Meta_Handle ( );

	Shared_Pixmaps_Set_Meta *
	new_clone ( ) const;
};


class DS_Slider_Pixmaps_Meta_Marker :
	public Shared_Pixmaps_Set_Meta
{
	// Public methods
	public:

	DS_Slider_Pixmaps_Meta_Marker ( );

	Shared_Pixmaps_Set_Meta *
	new_clone ( ) const;
};



} // End of namespace


#endif
