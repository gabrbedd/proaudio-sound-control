//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_pad_focus_info_hpp__
#define __INC_pad_focus_info_hpp__


namespace Wdg
{


class Pad_Focus_Info
{
	// Public methods
	public:

	Pad_Focus_Info ( );

	void
	clear ( );


	// Public attributes

	bool has_focus;

	unsigned int group_idx;

	unsigned int column_idx;
};


} // End of namespace


#endif



