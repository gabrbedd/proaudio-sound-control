//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "pass_events.hpp"

namespace Wdg
{


Focus_Pass_Event::Focus_Pass_Event (
	const QFocusEvent & event_n,
	unsigned int child_idx_n ) :
QFocusEvent ( event_n ),
_child_idx ( child_idx_n )
{
}


Key_Pass_Event::Key_Pass_Event (
	const QKeyEvent & event_n,
	unsigned int child_idx_n ) :
QKeyEvent ( event_n ),
_child_idx ( child_idx_n )
{
}


} // End of namespace
