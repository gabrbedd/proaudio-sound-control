//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_pass_events_hpp__
#define __INC_pass_events_hpp__

#include <QEvent>
#include <QFocusEvent>
#include <QKeyEvent>


namespace Wdg
{


///
/// @brief Focus_Pass_Event
///
class Focus_Pass_Event :
	public QFocusEvent
{
	// Public methods
	public:

	Focus_Pass_Event (
		const QFocusEvent & event_n,
		unsigned int child_idx_n );

	unsigned int
	child_idx ( ) const;


	// Private attributes
	private:

	unsigned int _child_idx;
};


inline
unsigned int
Focus_Pass_Event::child_idx ( ) const
{
	return _child_idx;
}



///
/// @brief Key_Pass_Event
///
class Key_Pass_Event :
	public QKeyEvent
{
	// Public methods
	public:

	Key_Pass_Event (
		const QKeyEvent & event_n,
		unsigned int child_idx_n );

	unsigned int
	child_idx ( ) const;


	// Private attributes
	private:

	unsigned int _child_idx;
};


inline
unsigned int
Key_Pass_Event::child_idx ( ) const
{
	return _child_idx;
}


} // End of namespace


#endif
