//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "pixmaps_painter.hpp"
#include <QApplication>


namespace Wdg
{
namespace Painter
{


Pixmaps_Painter::Pixmaps_Painter ( ) :
_current_palette ( QApplication::palette() )
{
}


Pixmaps_Painter::~Pixmaps_Painter ( )
{
	//std::cout << "~Pixmaps_Painter\n";
}


void
Pixmaps_Painter::set_current_palette (
	const QPalette & palette_n )
{
	_current_palette = palette_n;
}


} // End of namespace
} // End of namespace
