//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_wdg_pixmaps_painter_hpp__
#define __INC_wdg_pixmaps_painter_hpp__

#include "shared_pixmaps_set_painter.hpp"

#include <QPalette>
#include <QPainter>
#include <QList>


namespace Wdg
{
namespace Painter
{


class Pixmaps_Painter :
	public Shared_Pixmaps_Set_Painter
{
	// Public methods
	public:

	Pixmaps_Painter ( );

	~Pixmaps_Painter ( );


	// Current palette

	void
	set_current_palette (
		const QPalette & palette_n );

	const QPalette &
	cpal ( ) const;


	// Image size

	const QSize &
	is ( ) const;

	void
	set_is (
		const QSize & size_n );

	QRect
	irect ( ) const;

	QRectF
	irectf ( ) const;



	// Painter

	QPainter *
	painter ( ) const;

	void
	set_painter (
		QPainter * painter_n );


	// Private attributes
	private:

	QPainter * _painter;

	QPalette _current_palette;

	QSize _is; // Image size
};


inline
QPainter *
Pixmaps_Painter::painter ( ) const
{
	return _painter;
}


inline
void
Pixmaps_Painter::set_painter (
	QPainter * painter_n )
{
	_painter = painter_n;
}


inline
const QPalette &
Pixmaps_Painter::cpal ( ) const
{
	return _current_palette;
}


inline
const QSize &
Pixmaps_Painter::is ( ) const
{
	return _is;
}


inline
void
Pixmaps_Painter::set_is (
	const QSize & size_n )
{
	_is = size_n;
}


inline
QRect
Pixmaps_Painter::irect ( ) const
{
	return QRect ( QPoint ( 0, 0 ), _is );
}


inline
QRectF
Pixmaps_Painter::irectf ( ) const
{
	return QRect ( 0, 0, _is.width(), _is.height() );
}


} // End of namespace
} // End of namespace


#endif
