//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "primitive_painter.hpp"

#include <QPolygonF>
#include <QApplication>

#include <cmath>
#include <iostream>

#include "color_methods.hpp"


namespace Wdg
{
namespace Painter
{


Primitive_Painter::Primitive_Painter (
	QPainter * painter_n ) :
_painter ( painter_n )
{
}


void
Primitive_Painter::set_painter (
	QPainter * painter_n )
{
	_painter = painter_n;
}


QPalette
Primitive_Painter::palette ( ) const
{
	return QApplication::palette();
}


void
Primitive_Painter::draw_inner_border (
	const QRectF & rect_n,
	qreal border_width_n,
	qreal border_height_n )
{

	// Horizontal
	if ( border_height_n > 0 ) {
		painter()->drawRect ( QRectF (
			rect_n.left(), rect_n.top(),
			rect_n.width(), border_height_n ) );

		painter()->drawRect ( QRectF (
			rect_n.left(), rect_n.top() + rect_n.height() - border_height_n,
			rect_n.width(), border_height_n ) );
	}

	// Vertical
	if ( border_width_n > 0 ) {
		const qreal yy ( rect_n.top() + border_height_n );
		const qreal hh ( rect_n.height() - 2*border_height_n );
		painter()->drawRect ( QRectF (
			rect_n.left(), yy,
			border_width_n, hh ) );

		painter()->drawRect ( QRectF (
			rect_n.left() + rect_n.width() - border_width_n, yy,
			border_width_n, hh ) );
	}
}


void
Primitive_Painter::draw_inner_border (
	const QRectF & rect_n,
	qreal bw_n )
{
	draw_inner_border ( rect_n, bw_n, bw_n );
}


// Draw raised frame

void
Primitive_Painter::draw_raised_frame (
	const QRectF & re_out_n,
	qreal frame_width_n,
	qreal frame_height_n,
	qreal edge_width_n,
	qreal edge_height_n,
	const QColor & col_n,
	bool sunken_n )
{
	QColor col_light ( palette().color ( QPalette::Light ) );
	QColor col_shadow ( palette().color ( QPalette::Shadow ) );

	QColor col_mid ( col_n );
	QColor col_br ( Wdg::Painter::col_mix ( col_n, col_light, 5, 4 ) );
	QColor col_dk ( Wdg::Painter::col_mix ( col_n, col_shadow, 5, 4 ) );

	const qreal & fw ( frame_width_n );
	const qreal & fh ( frame_height_n );

	const qreal & ew ( edge_width_n );
	const qreal & eh ( edge_height_n );

	const qreal fw2 ( fw / 2.0 );
	const qreal fh2 ( fh / 2.0 );

	qreal xx[2] = { re_out_n.left(), re_out_n.left() };
	qreal yy[2] = { re_out_n.top(), re_out_n.top() };
	xx[1] += re_out_n.width();
	yy[1] += re_out_n.height();

	qreal xxi[2] = { xx[0] + fw, xx[1] - fw };
	qreal yyi[2] = { yy[0] + fh, yy[1] - fh };


	// Draw bright areas first
	painter()->setBrush ( col_br );
	draw_inner_border ( re_out_n, frame_width_n, frame_height_n );


	// Draw dark areas on top
	painter()->setBrush ( col_dk );
	if ( sunken_n ) {
		// Outer left-top + Inner right-bottom
		const unsigned int num_pts ( 7*2 );
		QPointF pts[num_pts] = {
			QPointF ( xx[0] + fw2, yy[1] - fh2 ),
			QPointF ( xx[0]      , yy[1]       ),
			QPointF ( xx[0]      , yy[0]       ),
			QPointF ( xx[1]      , yy[0]       ),
			QPointF ( xx[1] - fw2, yy[0] + fh2 ),
			QPointF ( xx[0] + fw2, yy[0] + fh2 ),
			QPointF ( xx[0] + fw2, yy[1] - fh2 ),

			QPointF ( xx[0] + fw2, yy[1] - fh2 ),
			QPointF ( xxi[0]     , yyi[1]      ),
			QPointF ( xxi[1]     , yyi[1]      ),
			QPointF ( xxi[1]     , yyi[0]      ),
			QPointF ( xx[1] - fw2, yy[0] + fh2 ),
			QPointF ( xx[1] - fw2, yy[1] - fh2 ),
			QPointF ( xx[0] + fw2, yy[1] - fh2 )
		};

		painter()->drawPolygon ( pts, num_pts );
	}


	if ( !sunken_n ) {
		// Inner left-top + Outer right-bottom
		const unsigned int num_pts ( 7*2 );
		QPointF pts[num_pts] = {
			QPointF ( xx[0] + fw2, yy[1] - fh2 ),
			QPointF ( xx[0] + fw2, yy[0] + fh2 ),
			QPointF ( xx[1] - fw2, yy[0] + fh2 ),
			QPointF ( xxi[1]     , yyi[0]      ),
			QPointF ( xxi[0]     , yyi[0]      ),
			QPointF ( xxi[0]     , yyi[1]      ),
			QPointF ( xx[0] + fw2, yy[1] - fh2 ),

			QPointF ( xx[0] + fw2, yy[1] - fh2 ),
			QPointF ( xx[1] - fw2, yy[1] - fh2 ),
			QPointF ( xx[1] - fw2, yy[0] + fh2 ),
			QPointF ( xx[1]      , yy[0]       ),
			QPointF ( xx[1]      , yy[1]       ),
			QPointF ( xx[0]      , yy[1]       ),
			QPointF ( xx[0] + fw2, yy[1] - fh2 )
		};

		painter()->drawPolygon ( pts, num_pts );
	}


	if ( ( ew > 0.0 ) && ( eh > 0.0 )
		&& ( 2*ew < fw ) && ( 2*eh < fh ) )
	{
		QRectF rect ( re_out_n );
		rect.adjust ( ew, eh, -ew, -eh );

		painter()->setBrush ( col_mid );
		draw_inner_border ( rect, fw - 2*ew, fh - 2*eh );
	}

}


void
Primitive_Painter::draw_raised_frame (
	const QRectF & re_out_n,
	qreal frame_width_n,
	qreal edge_width_n,
	const QColor & col_n,
	bool sunken_n )
{
	draw_raised_frame (
		re_out_n,
		frame_width_n, frame_width_n,
		edge_width_n, edge_width_n,
		col_n, sunken_n );
}


// Beveled drawing

void
Primitive_Painter::setup_bevel_rect (
	QPainterPath & path_n,
	const QRectF & rect_n,
	qreal bevel_px_n )
{
	const qreal & bev ( bevel_px_n );
	qreal xx[2] = { rect_n.left(), rect_n.left() };
	qreal yy[2] = { rect_n.top(), rect_n.top() };
	xx[1] += rect_n.width();
	yy[1] += rect_n.height();

	path_n.moveTo ( QPointF ( xx[0] + bev, yy[0]       ) );
	path_n.lineTo ( QPointF ( xx[1] - bev, yy[0]       ) );
	path_n.lineTo ( QPointF ( xx[1]      , yy[0] + bev ) );
	path_n.lineTo ( QPointF ( xx[1]      , yy[1] - bev ) );
	path_n.lineTo ( QPointF ( xx[1] - bev, yy[1]       ) );
	path_n.lineTo ( QPointF ( xx[0] + bev, yy[1]       ) );
	path_n.lineTo ( QPointF ( xx[0]      , yy[1] - bev ) );
	path_n.lineTo ( QPointF ( xx[0]      , yy[0] + bev ) );
	path_n.lineTo ( QPointF ( xx[0] + bev, yy[0]       ) );

	path_n.setFillRule ( Qt::OddEvenFill );
}


void
Primitive_Painter::setup_bevel_frame (
	QPainterPath & path_n,
	const QRectF & rect_n,
	qreal border_width_n,
	qreal bevel_px_n )
{
	qreal bev ( bevel_px_n );
	qreal xx[2] = { rect_n.left(), rect_n.left() };
	qreal yy[2] = { rect_n.top(), rect_n.top() };

	xx[1] += rect_n.width();
	yy[1] += rect_n.height();

	// Outer shape
	path_n.moveTo ( QPointF ( xx[0] + bev, yy[0]       ) );
	path_n.lineTo ( QPointF ( xx[1] - bev, yy[0]       ) );
	path_n.lineTo ( QPointF ( xx[1]      , yy[0] + bev ) );
	path_n.lineTo ( QPointF ( xx[1]      , yy[1] - bev ) );
	path_n.lineTo ( QPointF ( xx[1] - bev, yy[1]       ) );
	path_n.lineTo ( QPointF ( xx[0] + bev, yy[1]       ) );
	path_n.lineTo ( QPointF ( xx[0]      , yy[1] - bev ) );
	path_n.lineTo ( QPointF ( xx[0]      , yy[0] + bev ) );
	path_n.lineTo ( QPointF ( xx[0] + bev, yy[0]       ) );

	// Inner shape
	xx[0] += border_width_n;
	yy[0] += border_width_n;

	xx[1] -= border_width_n;
	yy[1] -= border_width_n;

	bev = bevel_px_n + border_width_n * ( sqrt ( 2.0 ) - 2.0 );
	bev = qMax ( qreal ( 0.0 ), bev );

	path_n.moveTo ( QPointF ( xx[0] + bev, yy[0]       ) );
	path_n.lineTo ( QPointF ( xx[1] - bev, yy[0]       ) );
	path_n.lineTo ( QPointF ( xx[1]      , yy[0] + bev ) );
	path_n.lineTo ( QPointF ( xx[1]      , yy[1] - bev ) );
	path_n.lineTo ( QPointF ( xx[1] - bev, yy[1]       ) );
	path_n.lineTo ( QPointF ( xx[0] + bev, yy[1]       ) );
	path_n.lineTo ( QPointF ( xx[0]      , yy[1] - bev ) );
	path_n.lineTo ( QPointF ( xx[0]      , yy[0] + bev ) );
	path_n.lineTo ( QPointF ( xx[0] + bev, yy[0]       ) );

	path_n.setFillRule ( Qt::OddEvenFill );
}


void
Primitive_Painter::draw_bevel_rect (
	const QRectF & rect_n,
	qreal dx_n )
{
	QPainterPath path;
	setup_bevel_rect ( path, rect_n, dx_n );
	painter()->drawPath ( path );
}


void
Primitive_Painter::draw_bevel_frame (
	const QRectF & rect_n,
	qreal border_width_n,
	qreal bevel_px_n )
{
	QPainterPath path;
	setup_bevel_frame ( path, rect_n, border_width_n, bevel_px_n );
	painter()->drawPath ( path );
}


void
Primitive_Painter::draw_bevel_raised_frame (
	const QRectF & re_out_n,
	qreal frame_width_n,
	qreal edge_width_n,
	qreal bevel_px_n,
	const QColor & col_n,
	bool sunken_n )
{
	QColor col_light ( palette().color ( QPalette::Light ) );
	QColor col_shadow ( palette().color ( QPalette::Shadow ) );

	const QColor & col_mid ( col_n );
	QColor col_lt (     Wdg::Painter::col_mix ( col_n, col_light, 5, 6 ) );
	QColor col_lt_mid ( Wdg::Painter::col_mix ( col_n, col_light, 5, 5 ) );
	QColor col_dk (     Wdg::Painter::col_mix ( col_n, col_shadow, 5, 4 ) );
	QColor col_dk_mid ( Wdg::Painter::col_mix ( col_n, col_shadow, 5, 3 ) );
	QColor col_mid_mid ( Wdg::Painter::col_mix ( col_lt_mid, col_dk_mid, 1, 1 ) );

	const qreal & fw ( frame_width_n );
	const qreal & ew ( edge_width_n );

	const qreal bev ( bevel_px_n );
	const qreal ll ( re_out_n.left() );
	const qreal rr ( re_out_n.left() + re_out_n.width() );
	const qreal tt ( re_out_n.top() );
	const qreal bb ( re_out_n.top() + re_out_n.height() );

	const qreal fw2 ( fw / 2.0 );
	const qreal px_mid ( bev + fw2 * ( sqrt ( 2.0 ) - 1.0 ) );
	const qreal px_cen ( bev + fw  * ( sqrt ( 2.0 ) - 1.0 ) );

	QPointF corner_tl_out[5] = {
		QPointF ( ll,          tt + bev    ),
		QPointF ( ll + bev,    tt          ),
		QPointF ( ll + px_mid, tt + fw2    ),
		QPointF ( ll + fw2,    tt + px_mid ),
		QPointF ( ll,          tt + bev    )
	};

	QPointF corner_tl_in[5] = {
		QPointF ( ll + fw2,    tt + px_mid ),
		QPointF ( ll + px_mid, tt + fw2    ),
		QPointF ( ll + px_cen, tt + fw     ),
		QPointF ( ll + fw,     tt + px_cen ),
		QPointF ( ll + fw2,    tt + px_mid )
	};

	QPointF edge_top_out[5] = {
		QPointF ( ll + bev, tt          ),
		QPointF ( rr - bev, tt          ),
		QPointF ( rr - px_mid, tt + fw2 ),
		QPointF ( ll + px_mid, tt + fw2 ),
		QPointF ( ll + bev, tt          )
	};

	QPointF edge_top_in[5] = {
		QPointF ( ll + px_mid, tt + fw2 ),
		QPointF ( rr - px_mid, tt + fw2 ),
		QPointF ( rr - px_cen, tt + fw  ),
		QPointF ( ll + px_cen, tt + fw  ),
		QPointF ( ll + px_mid, tt + fw2 )
	};


	QPointF corner_tr_out[5] = {
		QPointF ( rr - bev,    tt          ),
		QPointF ( rr,          tt + bev    ),
		QPointF ( rr - fw2,    tt + px_mid ),
		QPointF ( rr - px_mid, tt + fw2    ),
		QPointF ( rr - bev,    tt          )
	};

	QPointF corner_tr_in[5] = {
		QPointF ( rr - px_mid, tt + fw2    ),
		QPointF ( rr - fw2,    tt + px_mid ),
		QPointF ( rr - fw,     tt + px_cen ),
		QPointF ( rr - px_cen, tt + fw     ),
		QPointF ( rr - px_mid, tt + fw2    )
	};


	QPointF edge_right_out[5] = {
		QPointF ( rr      , tt + bev    ),
		QPointF ( rr      , bb - bev    ),
		QPointF ( rr - fw2, bb - px_mid ),
		QPointF ( rr - fw2, tt + px_mid ),
		QPointF ( rr      , tt + bev    )
	};

	QPointF edge_right_in[5] = {
		QPointF ( rr - fw2, tt + px_mid ),
		QPointF ( rr - fw2, bb - px_mid ),
		QPointF ( rr - fw,  bb - px_cen ),
		QPointF ( rr - fw,  tt + px_cen ),
		QPointF ( rr - fw2, tt + px_mid )
	};


	QPointF corner_br_out[5] = {
		QPointF ( rr,          bb - bev    ),
		QPointF ( rr - bev,    bb          ),
		QPointF ( rr - px_mid, bb - fw2    ),
		QPointF ( rr - fw2,    bb - px_mid ),
		QPointF ( rr,          bb - bev    )
	};

	QPointF corner_br_in[5] = {
		QPointF ( rr - fw2,    bb - px_mid ),
		QPointF ( rr - px_mid, bb - fw2    ),
		QPointF ( rr - px_cen, bb - fw     ),
		QPointF ( rr - fw,     bb - px_cen ),
		QPointF ( rr - fw2,    bb - px_mid )
	};


	QPointF edge_bottom_out[5] = {
		QPointF ( rr - bev,    bb       ),
		QPointF ( ll + bev,    bb       ),
		QPointF ( ll + px_mid, bb - fw2 ),
		QPointF ( rr - px_mid, bb - fw2 ),
		QPointF ( rr - bev,    bb       )
	};

	QPointF edge_bottom_in[5] = {
		QPointF ( rr - px_mid, bb - fw2 ),
		QPointF ( ll + px_mid, bb - fw2 ),
		QPointF ( ll + px_cen, bb - fw  ),
		QPointF ( rr - px_cen, bb - fw  ),
		QPointF ( rr - px_mid, bb - fw2 )
	};


	QPointF corner_bl_out[5] = {
		QPointF ( ll + bev,    bb          ),
		QPointF ( ll,          bb - bev    ),
		QPointF ( ll + fw2,    bb - px_mid ),
		QPointF ( ll + px_mid, bb - fw2    ),
		QPointF ( ll + bev,    bb          )
	};

	QPointF corner_bl_in[5] = {
		QPointF ( ll + px_mid, bb - fw2    ),
		QPointF ( ll + fw2,    bb - px_mid ),
		QPointF ( ll + fw,     bb - px_cen ),
		QPointF ( ll + px_cen, bb - fw     ),
		QPointF ( ll + px_mid, bb - fw2    )
	};


	QPointF edge_left_out[5] = {
		QPointF ( ll,       bb - bev    ),
		QPointF ( ll,       tt + bev    ),
		QPointF ( ll + fw2, tt + px_mid ),
		QPointF ( ll + fw2, bb - px_mid ),
		QPointF ( ll,       bb - bev    )
	};

	QPointF edge_left_in[5] = {
		QPointF ( ll + fw2, bb - px_mid ),
		QPointF ( ll + fw2, tt + px_mid ),
		QPointF ( ll + fw,  tt + px_cen ),
		QPointF ( ll + fw,  bb - px_cen ),
		QPointF ( ll + fw2, bb - px_mid ),
	};


	painter()->setPen ( Qt::NoPen );

	if ( !sunken_n ) {
		painter()->setBrush ( col_lt );
		painter()->drawPolygon ( corner_tl_out, 5 );
		painter()->drawPolygon ( corner_br_in, 5 );


		painter()->setBrush ( col_lt_mid );
		painter()->drawPolygon ( edge_top_out, 5 );
		painter()->drawPolygon ( edge_right_in, 5 );
		painter()->drawPolygon ( edge_bottom_in, 5 );
		painter()->drawPolygon ( edge_left_out, 5 );


		painter()->setBrush ( col_mid_mid );
		painter()->drawPolygon ( corner_tr_out, 5 );
		painter()->drawPolygon ( corner_tr_in, 5 );
		painter()->drawPolygon ( corner_bl_out, 5 );
		painter()->drawPolygon ( corner_bl_in, 5 );


		painter()->setBrush ( col_dk_mid );
		painter()->drawPolygon ( edge_top_in, 5 );
		painter()->drawPolygon ( edge_right_out, 5 );
		painter()->drawPolygon ( edge_bottom_out, 5 );
		painter()->drawPolygon ( edge_left_in, 5 );

		painter()->setBrush ( col_dk );
		painter()->drawPolygon ( corner_tl_in, 5 );
		painter()->drawPolygon ( corner_br_out, 5 );
	}


	// Center line
	const qreal ww ( fw - 2.0*ew );
	if ( ( ew > 0 ) && ( ww > 0 ) ) {
		painter()->setBrush ( col_mid );

		QRectF re_in ( re_out_n );
		re_in.adjust ( ew, ew, -ew, -ew );
		const qreal bev ( bevel_px_n + ew * ( sqrt ( 2.0 ) - 2.0 ) );
		draw_bevel_frame ( re_in, ww, bev );
	}

}


} // End of namespace
} // End of namespace
