//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_primitive_painter_hpp__
#define __INC_primitive_painter_hpp__

#include <QPalette>
#include <QPainter>


namespace Wdg
{
namespace Painter
{


class Primitive_Painter
{
	// Public methods
	public:

	Primitive_Painter (
		QPainter * painter_n = 0 );


	// Painter

	QPainter *
	painter ( ) const;

	void
	set_painter (
		QPainter * painter_n );


	// Palette

	QPalette
	palette ( ) const;


	// Draw inner border

	void
	draw_inner_border (
		const QRectF & rect_n,
		qreal border_width_n,
		qreal border_height_n );

	void
	draw_inner_border (
		const QRectF & rect_n,
		qreal border_width_n = 1 );


	// Draw raised frame

	void
	draw_raised_frame (
		const QRectF & re_out_n,
		qreal frame_width_n,
		qreal frame_height_n,
		qreal edge_width_n,
		qreal edge_height_n,
		const QColor & col_n,
		bool sunken_n = false );

	void
	draw_raised_frame (
		const QRectF & re_out_n,
		qreal frame_width_n,
		qreal edge_width_n,
		const QColor & col_n,
		bool sunken_n = false );


	// Beveled drawing

	void
	setup_bevel_rect (
		QPainterPath & path_n,
		const QRectF & rect_n,
		qreal bevel_px_n );

	void
	setup_bevel_frame (
		QPainterPath & path_n,
		const QRectF & rect_n,
		qreal border_width_n,
		qreal bevel_px_n );

	void
	draw_bevel_rect (
		const QRectF & rect_n,
		qreal bevel_px_n );

	void
	draw_bevel_frame (
		const QRectF & rect_n,
		qreal border_width_n,
		qreal bevel_px_n );

	void
	draw_bevel_raised_frame (
		const QRectF & re_out_n,
		qreal frame_width_n,
		qreal edge_width_n,
		qreal bevel_px_n,
		const QColor & col_n,
		bool sunken_n = false );


	// Private attributes
	private:

	QPainter * _painter;
};


inline
QPainter *
Primitive_Painter::painter ( ) const
{
	return _painter;
}


} // End of namespace
} // End of namespace


#endif
