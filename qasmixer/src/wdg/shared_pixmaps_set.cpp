//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "shared_pixmaps_set.hpp"

#include <iostream>


namespace Wdg
{


//
// Shared_Pixmaps_Set
//

Shared_Pixmaps_Set::Shared_Pixmaps_Set (
	Shared_Pixmaps_Set_Meta * cfg_n,
	int num_pixmaps_n ) :
_num_pixmaps ( 0 ),
_pixmaps ( 0 ),
_meta ( 0 ),
_num_users ( 1 )
{
	set_meta ( cfg_n );
	set_num_pixmaps ( num_pixmaps_n );
}


Shared_Pixmaps_Set::Shared_Pixmaps_Set (
	QSize size_n,
	int num_pixmaps_n ) :
_num_pixmaps ( 0 ),
_pixmaps ( 0 ),
_meta ( 0 ),
_num_users ( 0 )
{
	set_meta ( new Shared_Pixmaps_Set_Meta ( size_n ) );
	set_num_pixmaps ( num_pixmaps_n );
}


Shared_Pixmaps_Set::~Shared_Pixmaps_Set ( )
{
	clear_pixmaps();
	delete _meta;
}


void
Shared_Pixmaps_Set::clear_pixmaps ( )
{
	if ( _pixmaps != 0 ) {
		delete[] _pixmaps;
		_pixmaps = 0;
	}
	_num_pixmaps = 0;
}


void
Shared_Pixmaps_Set::set_num_pixmaps (
	unsigned int num_n )
{
	if ( num_n != num_pixmaps() ) {
		clear_pixmaps();
		if ( num_n > 0 ) {
			_pixmaps = new QPixmap[num_n];
			_num_pixmaps = num_n;

			if ( pixmap_size().isValid() ) {
				QPixmap pmap ( pixmap_size() );
				set_all_pixmaps ( pmap );
			}
		}
	}
}


void
Shared_Pixmaps_Set::set_meta (
	Shared_Pixmaps_Set_Meta * meta_n )
{
	if ( _meta != meta_n ) {
		if ( _meta != 0 ) {
			delete _meta;
		}
		_meta = meta_n;
	}

	if ( _meta == 0 ) {
		_meta = new Shared_Pixmaps_Set_Meta;
	}

	update_pixmap_sizes();
}


void
Shared_Pixmaps_Set::update_pixmap_sizes ( )
{
	if ( num_pixmaps() > 0 ) {
		QPixmap pmap;
		if ( pixmap_size().isValid() ) {
			pmap = QPixmap ( pixmap_size() );
		}
		set_all_pixmaps ( pmap );
	}
}


void
Shared_Pixmaps_Set::set_all_pixmaps (
	QPixmap & pman_n )
{
	for ( unsigned int ii=0; ii < num_pixmaps(); ++ii ) {
		_pixmaps[ii] = pman_n;
	}
}


} // End of namespace

