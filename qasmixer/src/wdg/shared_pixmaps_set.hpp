//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_wdg_shared_pixmaps_set_hpp__
#define __INC_wdg_shared_pixmaps_set_hpp__

#include <QPixmap>
#include <QTime>
#include "shared_pixmaps_set_meta.hpp"


namespace Wdg
{


///
/// @brief Shared_Pixmaps_Set
///
class Shared_Pixmaps_Set
{
	// Public methods
	public:

	Shared_Pixmaps_Set (
		Shared_Pixmaps_Set_Meta * cfg_n = 0,
		int num_pm_n = 0 );

	Shared_Pixmaps_Set (
		QSize size_n,
		int num_pm_n = 0 );

	virtual
	~Shared_Pixmaps_Set ( );


	void
	clear_pixmaps();


	// Pixmap size

	const QSize &
	pixmap_size ( ) const;

	int
	pixmap_width ( ) const;

	int
	pixmap_height ( ) const;


	// Number of pixmaps

	unsigned int
	num_pixmaps ( ) const;

	void
	set_num_pixmaps (
		unsigned int num_n );


	// Pixmap buffer

	const QPixmap &
	pixmap (
		int index_n ) const;

	QPixmap &
	pixmap_ref (
		int index_n );


	// Configuration

	const Shared_Pixmaps_Set_Meta *
	meta ( ) const;

	void
	set_meta (
		Shared_Pixmaps_Set_Meta * cfg_n );


	// Users

	unsigned int
	num_users ( ) const;

	void
	increment_num_users ( );

	void
	decrement_num_users ( );


	// Remove time

	const QTime &
	remove_time ( ) const;

	QTime &
	remove_time ( );


	// Private methods
	private:

	void
	update_pixmap_sizes ( );

	void
	set_all_pixmaps (
		QPixmap & pman_n );


	// Private attributes
	private:

	unsigned int _num_pixmaps;

	QPixmap * _pixmaps;

	Shared_Pixmaps_Set_Meta * _meta;


	unsigned int _num_users;

	QTime _remove_time;
};



inline
const QSize &
Shared_Pixmaps_Set::pixmap_size ( ) const
{
	return meta()->size;
}


inline
int
Shared_Pixmaps_Set::pixmap_width ( ) const
{
	return pixmap_size().width();
}


inline
int
Shared_Pixmaps_Set::pixmap_height ( ) const
{
	return pixmap_size().height();
}


inline
unsigned int
Shared_Pixmaps_Set::num_pixmaps ( ) const
{
	return _num_pixmaps;
}


inline
const QPixmap &
Shared_Pixmaps_Set::pixmap (
	int index_n ) const
{
	return _pixmaps[index_n];
}


inline
QPixmap &
Shared_Pixmaps_Set::pixmap_ref (
	int index_n )
{
	return _pixmaps[index_n];
}


inline
const Shared_Pixmaps_Set_Meta *
Shared_Pixmaps_Set::meta ( ) const
{
	return _meta;
}


inline
unsigned int
Shared_Pixmaps_Set::num_users ( ) const
{
	return _num_users;
}


inline
void
Shared_Pixmaps_Set::increment_num_users ( )
{
	++_num_users;
}


inline
void
Shared_Pixmaps_Set::decrement_num_users ( )
{
	if ( _num_users > 0 ) {
		--_num_users;
	}
}


inline
const QTime &
Shared_Pixmaps_Set::remove_time ( ) const
{
	return _remove_time;
}


inline
QTime &
Shared_Pixmaps_Set::remove_time ( )
{
	return _remove_time;
}


} // End of namespace


#endif
