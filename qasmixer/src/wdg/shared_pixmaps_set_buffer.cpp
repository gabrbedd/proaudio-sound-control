//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "shared_pixmaps_set_buffer.hpp"

#include <iostream>
#include <algorithm>
#include <cassert>


namespace Wdg
{


//
// Shared_Pixmaps_Set_Buffer
//
Shared_Pixmaps_Set_Buffer::Shared_Pixmaps_Set_Buffer ( )
{
	_remove_poll_timer.setInterval ( 1000 / 3 );
	connect ( &_remove_poll_timer, SIGNAL ( timeout() ),
		this, SLOT ( remove_poll() ) );
}


Shared_Pixmaps_Set_Buffer::~Shared_Pixmaps_Set_Buffer ( )
{
	//std::cout << "Shared_Pixmaps_Set_Buffer::~Shared_Pixmaps_Set_Buffer: List size " << _list.size() << "\n";
	clear();
}


void
Shared_Pixmaps_Set_Buffer::clear ( )
{
	if ( _list.size() > 0 ) {
		for ( int ii=0; ii < _list.size(); ++ii ) {
			//std::cout << "Users: " << _list[ii]->num_users() << "\n";
			delete _list[ii];
		}
		_list.clear();
	}
}


Shared_Pixmaps_Set *
Shared_Pixmaps_Set_Buffer::acquire_pset (
	const Shared_Pixmaps_Set_Meta & meta_n,
	Shared_Pixmaps_Set * cur_set_n )
{
	Shared_Pixmaps_Set * res ( 0 );

	// Try to find and pixmap set witch a matching config
	for ( int ii=0; ii < _list.size(); ++ii ) {
		Shared_Pixmaps_Set * sps ( _list[ii] );
		if ( meta_n.equal ( *sps->meta() ) ) {
			res = sps;
			break;
		}
	}

	if ( ( res != 0 ) && ( res != cur_set_n ) ) {
		res->increment_num_users();
	}

	return res;
}


void
Shared_Pixmaps_Set_Buffer::append_pset (
	Shared_Pixmaps_Set * pxmaps_n )
{
	_list.append ( pxmaps_n );
}


void
Shared_Pixmaps_Set_Buffer::return_pset (
	Shared_Pixmaps_Set * ret_set_n )
{
	if ( ret_set_n != 0 ) {
		unsigned int idx ( _list.size() );

		for ( int ii=0; ii < _list.size(); ++ii ) {
			Shared_Pixmaps_Set * sps ( _list[ii] );
			if ( sps == ret_set_n ) {
				idx = ii;
				break;
			}
		}

		if ( idx < (unsigned int)_list.size() ) {
			// Pixmap found in list
			ret_set_n->decrement_num_users();
			if ( ret_set_n->num_users() == 0 ) {
				if ( _list.size() <= 16 ) {
					// Delayed deletion
					ret_set_n->remove_time().start();
					ret_set_n->remove_time().addMSecs ( 1000 );
					if ( !_remove_poll_timer.isActive() ) {
						_remove_poll_timer.start();
					}
				} else {
					// Delete now
					delete ret_set_n;
					_list.removeAt ( idx );
				}
			}
		}
	}

	//std::cout << "Shared_Pixmaps_Set_Buffer::return_pset: List size " << _list.size() << "\n";
}


void
Shared_Pixmaps_Set_Buffer::remove_poll ( )
{
	unsigned int pending ( 0 );

	QTime tnow;
	tnow.start();

	for ( int ii=0; ii < _list.size(); ++ii ) {
		Shared_Pixmaps_Set * sps ( _list[ii] );
		if ( sps->num_users() == 0 ) {
			bool do_remove ( true );
			if ( sps->remove_time().isValid() ) {
				if ( sps->remove_time() > tnow ) {
					do_remove = false;
				} else {
					++pending;
				}
			}
			if ( do_remove ) {
				delete sps;
				_list.removeAt ( ii );
			}
		}
	}

	if ( pending == 0 ) {
		_remove_poll_timer.stop();
	}

	//std::cout << "Shared_Pixmaps_Set_Buffer::remove_poll: List size " << _list.size() << "\n";
}


} // End of namespace
