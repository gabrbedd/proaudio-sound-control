//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_shared_pixmaps_set_buffer_hpp__
#define __INC_shared_pixmaps_set_buffer_hpp__

#include <QObject>
#include <QTimer>
#include "shared_pixmaps_set.hpp"


namespace Wdg
{


///
/// @brief Shared_Pixmaps_Set_Buffer
///
class Shared_Pixmaps_Set_Buffer :
	public QObject
{
	Q_OBJECT

	// Public methods
	public:

	Shared_Pixmaps_Set_Buffer ( );

	~Shared_Pixmaps_Set_Buffer ( );


	void
	clear ( );


	// Buffer info

	unsigned int
	num_sets ( ) const;


	// Image set accquiring an returning

	Shared_Pixmaps_Set *
	acquire_pset (
		const Shared_Pixmaps_Set_Meta & cfg_n,
		Shared_Pixmaps_Set * cur_set_n = 0 );

	void
	append_pset (
		Shared_Pixmaps_Set * pxmaps_n );

	void
	return_pset (
		Shared_Pixmaps_Set * ret_pxmaps_n );


	// Protected methods
	protected:

	QList < Shared_Pixmaps_Set * > &
	list ( );


	// Protected slots
	protected slots:

	void
	remove_poll ( );


	// Private attributes
	private:

	QList < Shared_Pixmaps_Set * > _list;

	unsigned int _pixmap_type;

	QTimer _remove_poll_timer;
};


inline
unsigned int
Shared_Pixmaps_Set_Buffer::num_sets ( ) const
{
	return _list.size();
}


} // End of namespace


#endif
