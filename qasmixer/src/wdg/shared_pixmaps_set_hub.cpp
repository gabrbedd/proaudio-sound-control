//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "shared_pixmaps_set_hub.hpp"

#include "shared_pixmaps_set_buffer.hpp"
#include "shared_pixmaps_set_painter.hpp"


namespace Wdg
{


Shared_Pixmaps_Set_Hub::Shared_Pixmaps_Set_Hub (
	Shared_Pixmaps_Set_Painter * default_painter_n,
	unsigned int num_buffers_n ) :
_buffers ( num_buffers_n ),
_painter_def ( default_painter_n )
{
	for ( unsigned int ii=0; ii < num_buffers_n; ++ii ) {
		_buffers[ii] = new Shared_Pixmaps_Set_Buffer;
	}
}


Shared_Pixmaps_Set_Hub::~Shared_Pixmaps_Set_Hub ( )
{
	for ( unsigned int ii=0; ii < _buffers.size(); ++ii ) {
		delete _buffers[ii];
	}
	for ( int ii=0; ii< _painters.size(); ++ii ) {
		if ( _painters[ii] != 0 ) {
			delete _painters[ii];
		}
	}
	delete _painter_def;
}


void
Shared_Pixmaps_Set_Hub::set_painter (
	unsigned int style_n,
	Shared_Pixmaps_Set_Painter * painter_n )
{
	if ( painter_n == 0 ) {
		return;
	}

	while ( style_n >= (unsigned int)_painters.size()  ) {
		_painters.append ( 0 );
	}
	if ( _painters[style_n] != 0 ) {
		delete _painters[style_n];
	}
	_painters[style_n] = painter_n;
}


Shared_Pixmaps_Set_Painter *
Shared_Pixmaps_Set_Hub::sure_painter (
	unsigned int style_n ) const
{
	Shared_Pixmaps_Set_Painter * res ( 0 );
	if ( style_n < (unsigned int)_painters.size() ) {
		res = _painters[style_n];
	}
	if ( res == 0 ) {
		res = _painter_def;
	}
	return res;
}


void
Shared_Pixmaps_Set_Hub::return_pixmaps (
	Shared_Pixmaps_Set * & ret_set_n )
{
	if ( ret_set_n != 0 ) {
		const unsigned int type_id ( ret_set_n->meta()->type_id );
		Shared_Pixmaps_Set_Buffer * cur_buff ( buffer ( type_id ) );
		cur_buff->return_pset ( ret_set_n );
		ret_set_n = 0;
	}
}


void
Shared_Pixmaps_Set_Hub::update_pixmaps (
	Shared_Pixmaps_Set * & upd_set_n,
	const Shared_Pixmaps_Set_Meta & cfg_n )
{
	Shared_Pixmaps_Set_Buffer * cur_buffer ( buffer ( cfg_n.type_id ) );

	Shared_Pixmaps_Set * new_set ( cur_buffer->acquire_pset ( cfg_n, upd_set_n ) );
	if ( new_set == 0 ) {
		// Create new set
		new_set = new Shared_Pixmaps_Set ( cfg_n.new_clone() );

		sure_painter ( cfg_n.style_id )->paint_pixmaps ( new_set, cfg_n.type_id );
		cur_buffer->append_pset ( new_set );
	}

	if ( new_set != upd_set_n ) {
		cur_buffer->return_pset ( upd_set_n );
		upd_set_n = new_set;
	}
}


} // End of namespace
