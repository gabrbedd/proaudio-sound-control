//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_shared_pixmaps_set_hub_hpp__
#define __INC_shared_pixmaps_set_hub_hpp__

#include <vector>
#include <QList>


namespace Wdg
{


// Forward declarations
class Shared_Pixmaps_Set;
class Shared_Pixmaps_Set_Meta;
class Shared_Pixmaps_Set_Buffer;
class Shared_Pixmaps_Set_Painter;


///
/// @brief Dynamic size slider hub
///
class Shared_Pixmaps_Set_Hub
{
	// Public methods
	public:

	Shared_Pixmaps_Set_Hub (
		Shared_Pixmaps_Set_Painter * default_painter_n,
		unsigned int num_buffers_n = 0 );

	~Shared_Pixmaps_Set_Hub ( );


	// Painter

	/// @brief May return 0
	Shared_Pixmaps_Set_Painter *
	painter (
		unsigned int style_n ) const;

	void
	set_painter (
		unsigned int style_n,
		Shared_Pixmaps_Set_Painter * painter_n );

	/// @brief Never returns 0 but the default painter instead
	Shared_Pixmaps_Set_Painter *
	sure_painter (
		unsigned int style_n ) const;


	Shared_Pixmaps_Set_Buffer *
	buffer (
		unsigned int type_n );

	void
	return_pixmaps (
		Shared_Pixmaps_Set * & pxmaps_n );

	void
	update_pixmaps (
		Shared_Pixmaps_Set * & pxmaps_n,
		const Shared_Pixmaps_Set_Meta & cfg_n );


	// Private attributes
	private:

	std::vector < Shared_Pixmaps_Set_Buffer * > _buffers;

	QList < Shared_Pixmaps_Set_Painter * > _painters;

	Shared_Pixmaps_Set_Painter * _painter_def;
};


inline
Shared_Pixmaps_Set_Painter *
Shared_Pixmaps_Set_Hub::painter (
	unsigned int style_n ) const
{
	Shared_Pixmaps_Set_Painter * res ( 0 );
	if ( style_n < (unsigned int)_painters.size() ) {
		res = _painters[style_n];
	}
	return res;
}


inline
Shared_Pixmaps_Set_Buffer *
Shared_Pixmaps_Set_Hub::buffer (
	unsigned int type_n )
{
	return _buffers[type_n];
}


} // End of namespace


#endif
