//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "shared_pixmaps_set_meta.hpp"


namespace Wdg
{


//
// Shared_Pixmap_Config
//

Shared_Pixmaps_Set_Meta::Shared_Pixmaps_Set_Meta (
	const QSize & size_n,
	unsigned int type_id,
	unsigned int style_id ) :
type_id ( type_id ),
style_id ( style_id ),
size ( size_n )
{
}


Shared_Pixmaps_Set_Meta::~Shared_Pixmaps_Set_Meta ( )
{
}


bool
Shared_Pixmaps_Set_Meta::equal (
	const Shared_Pixmaps_Set_Meta & cfg_n ) const
{
	return (
		( cfg_n.type_id == type_id ) &&
		( cfg_n.style_id == style_id ) &&
		( cfg_n.size == size ) &&
		( cfg_n.palette == palette ) &&
		( cfg_n.palette.currentColorGroup() == palette.currentColorGroup() ) );
}


Shared_Pixmaps_Set_Meta *
Shared_Pixmaps_Set_Meta::new_clone ( ) const
{
	Shared_Pixmaps_Set_Meta * res (
		new Shared_Pixmaps_Set_Meta ( *this ) );
	return res;
}


} // End of namespace

