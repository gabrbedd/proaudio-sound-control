//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_wdg_shared_pixmaps_set_meta_hpp__
#define __INC_wdg_shared_pixmaps_set_meta_hpp__

#include <QSize>
#include <QPalette>


namespace Wdg
{


///
/// @brief Shared_Pixmaps_Set_Meta
///
class Shared_Pixmaps_Set_Meta
{
	// Public methods
	public:

	Shared_Pixmaps_Set_Meta (
		const QSize & size_n = QSize(),
		unsigned int type_id = 0,
		unsigned int style_id = 0 );

	virtual
	~Shared_Pixmaps_Set_Meta ( );

	virtual
	bool
	equal (
		const Shared_Pixmaps_Set_Meta & cfg_n ) const;

	virtual
	Shared_Pixmaps_Set_Meta *
	new_clone ( ) const;


	// Attributes

	unsigned int type_id;

	unsigned int style_id;

	QSize size;

	QPalette palette;
};


} // End of namespace


#endif
