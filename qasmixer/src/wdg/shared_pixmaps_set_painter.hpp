//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_wdg_shared_pixmaps_set_painter_hpp__
#define __INC_wdg_shared_pixmaps_set_painter_hpp__

#include "shared_pixmaps_set.hpp"


namespace Wdg
{


///
/// @brief Shared_Pixmaps_Set_Painter
///
class Shared_Pixmaps_Set_Painter
{
	public:

	virtual
	~Shared_Pixmaps_Set_Painter ( );

	virtual
	int
	paint_pixmaps (
		Shared_Pixmaps_Set * imgs_n,
		unsigned int image_type_n = 0 ) = 0;
};


} // End of namespace

#endif


