//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "sliders_pad.hpp"
#include "sliders_pad_header.hpp"
#include "sliders_pad_layout.hpp"
#include "equal_columns_layout_group.hpp"
#include "ds_slider.hpp"
#include "ds_slider_painter_basic.hpp"
#include "ds_check_button.hpp"
#include "ds_check_button_painter_basic.hpp"
#include "color_methods.hpp"
#include "pass_events.hpp"


#include <cmath>
#include <QPainter>
#include <QEvent>
#include <QKeyEvent>
#include <QFocusEvent>

#include <iostream>


namespace Wdg
{


//
// Sliders_Pad
//

Sliders_Pad::Sliders_Pad (
	QWidget * parent_n ) :
QWidget ( parent_n ),
_proxies_groups ( 0 ),
_header ( new Sliders_Pad_Header ( this ) ),
_sp_layout ( 0 ),
_slider_hub ( new Painter::DS_Slider_Painter_Basic, 3 ),
_check_button_hub ( new Painter::DS_Check_Button_Painter_Basic, 2 ),
_stem_corner_indent ( 1.0 )
{
	setSizePolicy ( QSizePolicy::Expanding, QSizePolicy::Expanding );

	connect ( _header, SIGNAL ( sig_label_selected ( unsigned int ) ),
		this, SLOT ( set_focus_proxy ( unsigned int ) ) );

	update_colors();
}


Sliders_Pad::~Sliders_Pad ( )
{
	set_proxies_groups ( 0 );
}


void
Sliders_Pad::set_wheel_degrees (
	unsigned int delta_n )
{
	if ( delta_n == 0 ) {
		return;
	}

	unsigned int num ( num_proxies_groups() );
	for ( unsigned int ii=0; ii < num; ++ii ) {
		Wdg::DS_Slider * slider (
			dynamic_cast < Wdg::DS_Slider * > (  widget ( ii ) ) );
		if ( slider != 0 ) {
			slider->set_wheel_degrees ( delta_n );
		}
	}
}


void
Sliders_Pad::clear_widgets ( )
{
	if ( layout() != 0 ) {
		delete layout();
		_sp_layout = 0;
	}

	if ( _proxies_groups != 0 ) {
		for ( int ii=0; ii < _proxies_groups->size(); ++ii ) {
			Sliders_Pad_Proxies_Group * grp ( (*_proxies_groups)[ii] );
			grp->set_sliders_pad ( 0 );
			for ( unsigned int jj=0; jj < grp->num_columns(); ++jj ) {
				Sliders_Pad_Proxies_Column * col ( grp->column ( jj ) );
				if ( col->slider_proxy() != 0 ) {
					col->slider_proxy()->set_widget ( 0 );
				}
				if ( col->switch_proxy() != 0 ) {
					col->switch_proxy()->set_widget ( 0 );
				}
			}
		}
	}

	// Clear widgets

	if ( _widgets.size() > 0 ) {
		for ( int ii=0; ii < _widgets.size(); ++ii ) {
			delete _widgets[ii];
		}
		_widgets.clear();
	}
	_header->vars().labels.clear();

}


void
Sliders_Pad::create_widgets ( )
{
	if ( _proxies_groups == 0 ) {
		return;
	}

	const unsigned int num_grps ( _proxies_groups->size() );
	if ( num_grps == 0 ) {
		return;
	}

	if ( _sp_layout == 0 ) {
		_sp_layout = new Sliders_Pad_Layout ( _header->vars() );
		_sp_layout->setContentsMargins ( 0, 0, 0, 0 );
		_sp_layout->set_header_widget ( _header );
	}

	_header->vars().labels.resize ( num_grps );
	_header->vars().update_elided_texts = true;


	for ( unsigned int ii=0; ii < num_grps; ++ii ) {

		Sliders_Pad_Proxies_Group * sppg ( (*_proxies_groups)[ii] );
		sppg->set_sliders_pad ( this );
		sppg->set_group_index ( ii );

		// Set header label variables
		{
			Sliders_Pad_Header_Label & lbl ( _header->vars().labels[ii] );
			lbl.text = sppg->group_name();
			lbl.tool_tip = sppg->tool_tip();
			lbl.col_fg = sppg->palette().color ( QPalette::WindowText );
		}

		// Add widgets to the widgets groups
		for ( unsigned int jj=0; jj < sppg->num_columns(); ++jj ) {
			Sliders_Pad_Proxies_Column * sppc ( sppg->column ( jj ) );

			// Slider
			if ( sppc->has_slider() ) {
				Sliders_Pad_Proxy_Slider * spps ( sppc->slider_proxy() );

				Wdg::DS_Slider * sl_wdg ( new Wdg::DS_Slider ( 0, &_slider_hub ) );
				sl_wdg->set_maximum_index ( spps->slider_index_max() );
				sl_wdg->set_current_index ( spps->slider_index() );
				sl_wdg->setToolTip ( spps->tool_tip() );

				const Sliders_Pad_Proxy_Style * pstyle ( spps->style_info() );
				if ( pstyle != 0 ) {

					Wdg::DS_Slider_Pixmaps_Meta_Bg pxm_cfg_bg ( sl_wdg->pixmaps_meta_bg() );
					if ( pstyle->slider_has_minimum ) {
						pxm_cfg_bg.bg_show_image = true;
						pxm_cfg_bg.bg_tick_min_idx = pstyle->slider_minimum_idx;
					}
					sl_wdg->set_pixmaps_meta_bg ( pxm_cfg_bg );
					sl_wdg->setPalette ( pstyle->palette );
				}

				connect ( sl_wdg, SIGNAL ( sig_current_index_changed ( unsigned long ) ),
					spps, SLOT ( set_slider_index ( unsigned long ) ) );

				connect ( spps, SIGNAL ( sig_slider_index_changed ( unsigned long ) ),
					sl_wdg, SLOT ( set_current_index ( unsigned long ) ) );

				connect ( spps, SIGNAL ( sig_slider_index_max_changed ( unsigned long ) ),
					sl_wdg, SLOT ( set_maximum_index ( unsigned long ) ) );

				sl_wdg->installEventFilter ( spps ); // First filter to be passed

				spps->set_widget ( sl_wdg );
				_widgets.append ( sl_wdg );
				_sp_layout->add_group_widget ( sl_wdg, ii, jj, 0 );
			}

			// Button
			if ( sppc->has_switch() ) {
				Sliders_Pad_Proxy_Switch * spps ( sppc->switch_proxy() );

				Wdg::DS_Check_Button * sw_wdg (
					new Wdg::DS_Check_Button ( 0, &_check_button_hub ) );

				sw_wdg->setChecked ( spps->switch_state() );
				sw_wdg->setToolTip ( spps->tool_tip() );

				const Sliders_Pad_Proxy_Style * pstyle ( spps->style_info() );
				if ( pstyle != 0 ) {
					sw_wdg->setPalette ( pstyle->palette );
				}

				connect ( sw_wdg, SIGNAL ( toggled  ( bool ) ),
					spps, SLOT ( set_switch_state ( bool ) ) );

				connect ( spps, SIGNAL ( sig_switch_state_changed ( bool ) ),
					sw_wdg, SLOT ( setChecked ( bool ) ) );

				sw_wdg->installEventFilter ( spps ); // First filter to be passed

				spps->set_widget ( sw_wdg );
				_widgets.append ( sw_wdg );
				_sp_layout->add_group_widget ( sw_wdg, ii, jj, 1 );
			}

			unsigned int llength ( _header->label_str_length_px_max ( sppg->group_name() ) );
			_sp_layout->set_group_label_length ( ii, llength );

		}

	}

	setLayout ( _sp_layout );
}


void
Sliders_Pad::set_proxies_groups (
	Sliders_Pad_Proxies_Group_List * list_n )
{
	if ( list_n != _proxies_groups ) {
		clear_widgets();

		_proxies_groups = list_n;

		create_widgets();
		_update_decoration = true;
	}
}


void
Sliders_Pad::update_colors ( )
{
	{
		QPalette pal ( palette() );
		const QColor col_bg ( pal.color ( QPalette::Button ) );
		const QColor col_fg ( pal.color ( QPalette::ButtonText ) );
		QColor col = Wdg::Painter::col_mix ( col_bg, col_fg, 1, 1 );

		_header->vars().stem_pen.setColor ( col );
	}
}


void
Sliders_Pad::update_decoration_graphics ( )
{
	if ( _sp_layout == 0 ) {
		QPainterPath ppath;
		_header->set_decoration_paths ( ppath );
		_deco_paths = ppath;
	} else {
		update_decoration_top();
		update_decoration_bottom();
	}
}


void
Sliders_Pad::update_decoration_top ( )
{
	QPainterPath ppath;

	const Sliders_Pad_Header_Vars & hvars ( _header->vars() );
	const qreal fnt_hhalf ( fontMetrics().height() / 2.0 );
	const qreal c_off ( _stem_corner_indent );
	const qreal x_left ( 0.0 );
	const qreal y_bottom ( _header->height() );
	const qreal y_mid ( ( _header->height() - _header->vars().spacing_inter ) + qreal ( 0.5 ) );

	unsigned int num_grps ( _sp_layout->num_active_groups() );
	for ( unsigned int ii=0; ii < num_grps; ++ii ) {

		const Equal_Columns_Layout_Group * splg ( _sp_layout->active_group ( ii ) );

		unsigned int num_sliders ( 0 );
		if ( splg->num_rows() > 0 ) {
			num_sliders = splg->row_stat ( 0 );
		}

		if ( num_sliders == 0 ) {
			continue;
		}

		const unsigned int sl_max_idx ( num_sliders - 1 );

		{
			qreal stem_height ( hvars.angle_cos * fnt_hhalf );
			qreal stem_bottom;
			if ( num_sliders == 1 ) {
				stem_height += hvars.spacing_bottom;
				stem_bottom = y_bottom;
			} else {
				stem_height += 0.5;
				stem_bottom = y_mid;
			}

			if ( stem_height > 0.5 ) {

				qreal stem_l1 ( stem_height / ( 1.0 + hvars.angle_sin ) );
				qreal stem_dx ( stem_l1 * hvars.angle_cos );
				stem_dx = std::floor ( stem_dx );
				if ( ( splg->group_width() % 2 ) == 0 ) {
					stem_dx += 0.5;
				}

				qreal stem_h2 ( 0.0 );
				if ( hvars.angle_cos > 0.002 ) {
					stem_h2 = stem_dx * hvars.angle_sin / hvars.angle_cos;
				}

				qreal grp_x_mid ( x_left + splg->group_pos() );
				grp_x_mid += qreal ( splg->group_width() ) / qreal ( 2.0 );
				qreal stem_x1 = grp_x_mid - stem_dx;
				qreal stem_h1 ( stem_height - stem_h2 );

				ppath.moveTo ( grp_x_mid, stem_bottom - stem_height );
				ppath.lineTo ( stem_x1, stem_bottom - stem_h1 );
				ppath.lineTo ( stem_x1, stem_bottom );
			}
		}

		if ( num_sliders == 1 ) {
			continue;
		}

		// Bridge lines

		// Draw bridge line from the leftmost slider to the rightmost
		{
			const Equal_Columns_Layout_Column * splc_b ( splg->column ( 0 ) );
			const Equal_Columns_Layout_Column * splc_e ( splg->column ( sl_max_idx ) );

			qreal cc_min ( x_left + splc_b->column_pos() );
			qreal cc_max ( x_left + splc_e->column_pos() );

			cc_min += calc_col_center (
				splc_b->column_width(), 0, num_sliders );

			cc_max += calc_col_center (
				splc_b->column_width(), sl_max_idx, num_sliders );

			ppath.moveTo ( cc_min, y_bottom );
			ppath.lineTo ( cc_min, y_mid + c_off );
			ppath.lineTo ( cc_min + c_off, y_mid );
			ppath.lineTo ( cc_max - c_off, y_mid );
			ppath.lineTo ( cc_max, y_mid + c_off );
			ppath.lineTo ( cc_max, y_bottom );
		}

		// Draw vertical lines from the bridge line down to the inner sliders
		for ( unsigned int jj=1; jj < sl_max_idx; ++jj ) {
			const Equal_Columns_Layout_Column * splc ( splg->column ( jj ) );

			qreal cc ( x_left + splc->column_pos() );
			cc += calc_col_center (
				splc->column_width(), jj, num_sliders );

			ppath.moveTo ( cc, y_mid );
			ppath.lineTo ( cc, y_bottom );
		}

	}

	_header->set_decoration_paths ( ppath );
}


void
Sliders_Pad::update_decoration_bottom ( )
{
	QPainterPath ppath;

	const unsigned int vspace ( _sp_layout->spacing_vertical() );

	qreal x_left ( _sp_layout->switches_area().left() );
	qreal y_top ( _sp_layout->switches_area().top() - vspace );
	qreal y_bottom ( _sp_layout->switches_area().top() );
	qreal y_mid ( vspace );
	if ( ( vspace % 2 ) == 0 ) {
		y_mid -= 1;
	}
	y_mid = y_mid / qreal ( 2.0 );
	y_mid += y_top;

	unsigned int num_grps ( _sp_layout->num_active_groups() );
	for ( unsigned int ii=0; ii < num_grps; ++ii ) {

		const Equal_Columns_Layout_Group * splg ( _sp_layout->active_group ( ii ) );

		unsigned int num_sliders ( 0 );
		unsigned int num_switches ( 0 );
		if ( splg->num_rows() > 0 ) {
			num_sliders = splg->row_stat ( 0 );
			if ( splg->num_rows() > 1 ) {
				num_switches = splg->row_stat ( 1 );
			}
		}

		// No lines required
		if ( ( num_sliders <= 1 ) &&
			( num_switches == 0 ) )
		{
			continue;
		}


		// Declaration of variables that may be needed later
		bool draw_sl_bridge ( false );
		bool draw_sw_bridge ( false );

		const unsigned int sl_max_idx ( num_sliders - 1 );
		const unsigned int sw_max_idx ( num_switches - 1 );
		qreal c_off ( _stem_corner_indent ); // Corner offset

		qreal grp_x_mid;
		if ( ( splg->group_width() % 2 ) != 0 ) {
			grp_x_mid = splg->group_width();
		} else {
			grp_x_mid = splg->group_width() - 1;
		}
		grp_x_mid = grp_x_mid / qreal ( 2.0 );
		grp_x_mid += x_left + splg->group_pos();

		// Slider Bridge lines required
		if ( num_sliders > 1 ) {
			draw_sl_bridge = true;
		}

		// Vertical lines from the sliders downwards to the switches required
		if ( num_sliders == num_switches ) {
			for ( unsigned int jj=0; jj < num_sliders; ++jj ) {
				const Equal_Columns_Layout_Column * splc ( splg->column ( jj ) );

				qreal cx_mid ( x_left + splc->column_pos() );
				cx_mid += calc_col_center (
					splc->column_width(), jj, num_sliders );

				ppath.moveTo ( cx_mid, y_top );
				ppath.lineTo ( cx_mid, y_bottom );
			}
		}

		// Single switch multiple sliders
		if ( ( num_sliders > 1 ) && ( num_switches == 1 ) )
		{
			// Draw downward from the bridge line to the single switch
			ppath.moveTo ( grp_x_mid, y_mid );
			ppath.lineTo ( grp_x_mid, y_bottom );
		}

		// Single slider multiple switches
		if ( ( num_sliders == 1 ) && ( num_switches > 1 ) )
		{
			// Draw downward from the single slider to the bridge line
			ppath.moveTo ( grp_x_mid, y_top );
			ppath.lineTo ( grp_x_mid, y_mid );

			draw_sw_bridge = true;
		}


		// Draw a slider connecting bridge line
		if ( draw_sl_bridge ) {

			// Draw bridge line from the leftmost slider to the rightmost
			{
				const Equal_Columns_Layout_Column * splc_b ( splg->column ( 0 ) );
				const Equal_Columns_Layout_Column * splc_e ( splg->column ( sl_max_idx ) );

				qreal cx_mid_min ( x_left + splc_b->column_pos() );
				qreal cx_mid_max ( x_left + splc_e->column_pos() );

				cx_mid_min += calc_col_center (
					splc_b->column_width(), 0, num_sliders );

				cx_mid_max += calc_col_center (
					splc_b->column_width(), sl_max_idx, num_sliders );

				ppath.moveTo ( cx_mid_min,         y_top );
				ppath.lineTo ( cx_mid_min,         y_mid - c_off );
				ppath.lineTo ( cx_mid_min + c_off, y_mid );
				ppath.lineTo ( cx_mid_max - c_off, y_mid );
				ppath.lineTo ( cx_mid_max,         y_mid - c_off );
				ppath.lineTo ( cx_mid_max,         y_top );
			}

			// Draw vertical lines from the inner sliders down to the bridge line
			for ( unsigned int jj=1; jj < sl_max_idx; ++jj ) {
				const Equal_Columns_Layout_Column * splc ( splg->column ( jj ) );
				qreal cc ( x_left + splc->column_pos() );
				cc += calc_col_center (
					splc->column_width(), jj, num_sliders );

				ppath.moveTo ( cc, y_top );
				ppath.lineTo ( cc, y_mid );
			}

		}


		// Draw a switches connecting bridge line
		if ( draw_sw_bridge ) {

			// Draw bridge line from the leftmost switch to the rightmost
			{
				const Equal_Columns_Layout_Column * splc_b ( splg->column ( 0 ) );
				const Equal_Columns_Layout_Column * splc_e ( splg->column ( sw_max_idx ) );

				qreal cx_mid_min ( x_left + splc_b->column_pos() );
				qreal cx_mid_max ( x_left + splc_e->column_pos() );

				cx_mid_min += calc_col_center (
					splc_b->column_width(), 0, num_switches );

				cx_mid_max += calc_col_center (
					splc_b->column_width(), sw_max_idx, num_switches );

				ppath.moveTo ( cx_mid_min, y_bottom );
				ppath.lineTo ( cx_mid_min, y_mid + c_off );
				ppath.lineTo ( cx_mid_min + c_off, y_mid );
				ppath.lineTo ( cx_mid_min - c_off, y_mid );
				ppath.lineTo ( cx_mid_max, y_mid + c_off );
				ppath.lineTo ( cx_mid_max, y_bottom );
			}


			// Draw vertical lines down from the bridge line to the inner switches
			for ( unsigned int jj=1; jj < sw_max_idx; ++jj ) {
				const Equal_Columns_Layout_Column * splc ( splg->column ( jj ) );

				qreal cx_mid ( x_left + splc->column_pos() );
				cx_mid += calc_col_center (
					splc->column_width(), jj, num_switches );

				ppath.moveTo ( cx_mid, y_mid );
				ppath.lineTo ( cx_mid, y_bottom );
			}
		}
	}

	_deco_paths = ppath;
}


void
Sliders_Pad::set_focus_proxy (
	unsigned int proxy_group_idx_n )
{
	set_focus_proxy ( proxy_group_idx_n, 0 );
}


void
Sliders_Pad::set_focus_proxy (
	unsigned int proxy_group_idx_n,
	unsigned int column_idx_n )
{
	if ( _proxies_groups != 0 ) {
		const unsigned int num_groups ( _proxies_groups->size() );
		if ( proxy_group_idx_n < num_groups ) {
			Sliders_Pad_Proxies_Group * sppg (
				proxies_group ( proxy_group_idx_n ) );
			sppg->take_focus ( column_idx_n );
		}
	}
}


bool
Sliders_Pad::event (
	QEvent * event_n )
{
	Focus_Pass_Event * ev_fp (
		dynamic_cast < Focus_Pass_Event * > ( event_n ) );
	if ( ev_fp != 0 ) {

		_focus_info.clear();
		if ( ev_fp->gotFocus() && ( ev_fp->child_idx() < num_proxies_groups() ) ) {
			Sliders_Pad_Proxies_Group * sppg (
				proxies_group ( ev_fp->child_idx() ) );
			_focus_info.has_focus = true;
			_focus_info.group_idx = ev_fp->child_idx();
			_focus_info.column_idx = sppg->focus_column();
		}

		{	// Update header focus
			int idx ( -1 );
			if ( _focus_info.has_focus ) {
				idx = _focus_info.group_idx;
			}
			_header->set_focus_idx ( idx );
		}

		emit sig_focus_changed();
		update();

		return true;
	}

	return QWidget::event ( event_n );
}


void
Sliders_Pad::resizeEvent (
	QResizeEvent * event )
{
	//std::cout << "Resize event " << width() << ":" << height() << "\n";
	QWidget::resizeEvent ( event );
	_update_decoration = true;
}


void
Sliders_Pad::paintEvent (
	QPaintEvent * event )
{
	QWidget::paintEvent ( event );

	if ( _update_decoration ) {
		_update_decoration = false;
		update_decoration_graphics();
	}

	{
		QPainter painter ( this );
		painter.setRenderHints (
			QPainter::Antialiasing |
			QPainter::TextAntialiasing |
			QPainter::SmoothPixmapTransform );

		painter.setBrush ( Qt::NoBrush );
		painter.setPen ( _header->vars().stem_pen );

		painter.drawPath ( _deco_paths );
	}
}


} // End of namespace
