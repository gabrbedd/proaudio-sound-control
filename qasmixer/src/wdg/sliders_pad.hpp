//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_sliders_pad_hpp__
#define __INC_sliders_pad_hpp__

#include "pad_focus_info.hpp"
#include "shared_pixmaps_set_hub.hpp"
#include "sliders_pad_proxies_group.hpp"

#include <QWidget>
#include <QFrame>
#include <QScrollArea>
#include <QScrollBar>
#include <QList>


namespace Wdg
{


// Forward declaration
class Sliders_Pad_Header;
class Sliders_Pad_Layout;


///
/// @brief Sliders_Pad
///
class Sliders_Pad :
	public QWidget
{
	Q_OBJECT

	// Public methods
	public:

	Sliders_Pad (
		QWidget * parent_n = 0 );

	~Sliders_Pad ( );


	// Proxies groups

	Sliders_Pad_Proxies_Group_List *
	proxies_groups ( ) const;

	void
	set_proxies_groups (
		Sliders_Pad_Proxies_Group_List * list_n );

	unsigned int
	num_proxies_groups ( ) const;

	Sliders_Pad_Proxies_Group *
	proxies_group (
		unsigned int idx_n );


	// Wheel degrees

	void
	set_wheel_degrees (
		unsigned int delta_n );


	// Widgets groups

	unsigned int
	num_widgets ( ) const;

	QWidget *
	widget (
		unsigned int idx_n );


	// Focus info

	const Pad_Focus_Info &
	focus_info ( ) const;


	/// @brief Event handler reimplementation
	bool
	event (
		QEvent * event_n );


	// Public slots
	public slots:

	void
	set_focus_proxy (
		unsigned int proxy_group_idx_n );

	void
	set_focus_proxy (
		unsigned int proxy_group_idx_n,
		unsigned int column_idx_n );


	// Public signals
	signals:

	void
	sig_focus_changed ( );


	// Protected methods
	protected:

	void
	clear_widgets ( );

	void
	create_widgets ( );


	// Event methods

	void
	resizeEvent (
		QResizeEvent * event );

	void
	paintEvent (
		QPaintEvent * event );


	// Private methods
	private:

	void
	update_colors ( );

	void
	update_decoration_graphics ( );

	void
	update_decoration_top ( );

	void
	update_decoration_bottom ( );

	qreal
	calc_col_center (
		unsigned int col_width_n,
		unsigned int col_idx_n ,
		unsigned int num_cols_n ) const;


	// Private attributes
	private:

	Sliders_Pad_Proxies_Group_List * _proxies_groups;

	QList < QWidget * > _widgets;


	Sliders_Pad_Header * _header;

	Sliders_Pad_Layout * _sp_layout;


	QPainterPath _deco_paths;


	Pad_Focus_Info _focus_info;

	bool _update_decoration;


	Shared_Pixmaps_Set_Hub _slider_hub;

	Shared_Pixmaps_Set_Hub _check_button_hub;


	qreal _stem_corner_indent;
};


inline
Sliders_Pad_Proxies_Group_List *
Sliders_Pad::proxies_groups ( ) const
{
	return _proxies_groups;
}


inline
unsigned int
Sliders_Pad::num_proxies_groups ( ) const
{
	unsigned int res ( 0 );
	if ( proxies_groups() != 0 ) {
		res = proxies_groups()->size();
	}
	return res;
}


inline
Sliders_Pad_Proxies_Group *
Sliders_Pad::proxies_group (
	unsigned int idx_n )
{
	return (*_proxies_groups)[idx_n];
}


inline
unsigned int
Sliders_Pad::num_widgets ( ) const
{
	return _widgets.size();
}


inline
QWidget *
Sliders_Pad::widget (
	unsigned int idx_n )
{
	return _widgets[idx_n];
}


inline
const Pad_Focus_Info &
Sliders_Pad::focus_info ( ) const
{
	return _focus_info;
}


inline
qreal
Sliders_Pad::calc_col_center (
	unsigned int col_width_n,
	unsigned int col_idx_n ,
	unsigned int num_cols_n ) const
{
	unsigned int div_val ( col_width_n );

	if ( col_width_n > 0 ) {
		if ( ( col_width_n % 2 ) == 0 ) {
			if ( col_idx_n >= ( num_cols_n / 2 ) ) {
				++div_val;
			} else {
				--div_val;
			}
		}
	}

	return qreal ( div_val ) / qreal ( 2.0 );
}


} // End of namespace


#endif
