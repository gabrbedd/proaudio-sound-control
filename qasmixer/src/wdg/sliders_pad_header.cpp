//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "sliders_pad_header.hpp"

#include <QMouseEvent>
#include <QPainter>

#define _USE_MATH_DEFINES
#include <cmath>
#include <climits>

#include <iostream>


namespace Wdg
{


Sliders_Pad_Header::Sliders_Pad_Header (
	QWidget * parent_n ) :
QWidget ( parent_n ),
_focus_idx ( -1 ),
_weak_focus_idx ( -1 )
{
	_vars.max_str_length_px = fontMetrics().averageCharWidth() * 18;

	setMouseTracking ( true );

	int max_h ( _vars.spacing_bottom );
	max_h += _vars.pad_left;
	max_h += _vars.pad_right;
	max_h += _vars.max_str_length_px;
	setMaximumHeight ( max_h  );
}


QSize
Sliders_Pad_Header::minimumSizeHint ( ) const
{
	QSize res ( 64, 0 );
	res.rheight() += vars().spacing_bottom;
	res.rheight() += fontMetrics().height() * 5 / 2;

	return res;
}


QSize
Sliders_Pad_Header::sizeHint ( ) const
{
	return minimumSizeHint();
}


unsigned int
Sliders_Pad_Header::label_str_length_px_max (
	const QString & str_n ) const
{
	unsigned int res;
	res = std::ceil ( fontMetrics().width ( str_n ) );
	res = qMin ( res, vars().max_str_length_px );
	return res;
}


void
Sliders_Pad_Header::set_decoration_paths (
	const QPainterPath & path_n )
{
	_deco_paths = path_n;
}


void
Sliders_Pad_Header::enterEvent (
	QEvent * )
{
	if ( _weak_focus_idx >= 0 ) {
		_weak_focus_idx = -1;
		update();
	}
}


void
Sliders_Pad_Header::leaveEvent (
	QEvent * )
{
	if ( _weak_focus_idx >= 0 ) {
		_weak_focus_idx = -1;
		update();
	}
}


void
Sliders_Pad_Header::mousePressEvent (
	QMouseEvent * event_n )
{
	const int idx ( find_label ( event_n->pos() ) );
	if ( idx >= 0 ) {
		sig_label_selected ( idx );

		QCursor cursor_new ( cursor() );
		cursor_new.setShape ( Qt::ArrowCursor );
		setCursor ( cursor_new );
	}
}


void
Sliders_Pad_Header::mouseMoveEvent (
	QMouseEvent * event_n )
{
	const int idx ( find_label ( event_n->pos() ) );
	if ( idx != _weak_focus_idx ) {
		_weak_focus_idx = idx;

		Qt::CursorShape cursor_shape ( Qt::ArrowCursor );

		if ( idx < 0 ) {
			setToolTip ( QString() );
		} else {
			setToolTip ( _vars.labels[idx].tool_tip );
			if ( ( event_n->buttons() & Qt::LeftButton ) != 0 ) {
				emit sig_label_selected ( idx );
			} else {
				cursor_shape = Qt::PointingHandCursor;
			}
		}

		// Adjust the cursor
		{
			QCursor cursor_new ( cursor() );
			if ( cursor_new.shape() != cursor_shape ) {
				cursor_new.setShape ( cursor_shape );
				setCursor ( cursor_new );
			}
		}
		update();
	}
}


void
Sliders_Pad_Header::paintEvent (
	QPaintEvent * event_n )
{
	//std::cout << "Sliders_Pad_Header::paintEvent" << "\n";

	QWidget::paintEvent ( event_n );

	// Update elided text strings on demand
	if ( _vars.update_elided_texts ) {
		_vars.update_elided_texts = false;

		QFontMetrics fmet ( fontMetrics() );

		for ( unsigned int ii=0; ii < _vars.labels.size(); ++ii ) {
			Sliders_Pad_Header_Label & lbl ( _vars.labels[ii] );

			// Adjust text rectangle
			QRectF lbl_rect ( lbl.label_rect );
			lbl_rect.moveLeft ( lbl_rect.left() + vars().pad_left );
			lbl_rect.setWidth (
				lbl_rect.width() - vars().pad_left - vars().pad_right );

			lbl.text_elided = fmet.elidedText (
				lbl.text, Qt::ElideRight, lbl_rect.width() );
		}
	}

	QPainter painter ( this );

	painter.setRenderHints (
		QPainter::Antialiasing |
		QPainter::TextAntialiasing |
		QPainter::SmoothPixmapTransform );


	//
	// Draw decoration lines
	//
	{
		painter.setBrush ( Qt::NoBrush );
		painter.setPen ( vars().stem_pen );
		painter.drawPath ( _deco_paths );
	}

	//
	// Draw non focus text labels
	//
	painter.setBrush ( Qt::NoBrush );
	painter.setFont ( font() );

	int focus_idx ( -1 );
	int weak_focus_idx ( -1 );

	for ( unsigned int ii=0; ii < _vars.labels.size(); ++ii ) {

		const Sliders_Pad_Header_Label & lbl ( _vars.labels[ii] );

		// Draw focus items later
		if ( (int)ii == _focus_idx ) {
			focus_idx = ii;
			continue;
		}

		// Draw weak focus items later
		if ( (int)ii == _weak_focus_idx ) {
			weak_focus_idx = ii;
			continue;
		}

		// Pen color
		{
			const QPen & ppen ( painter.pen() );
			if ( lbl.col_fg != ppen.color() ) {
				QPen npen ( ppen );
				npen.setColor ( lbl.col_fg );
				painter.setPen ( npen );
			}
		}

		// Adjust text rectangle
		QRectF lbl_rect ( lbl.label_rect );
		lbl_rect.moveLeft ( lbl_rect.left() + vars().pad_left );
		lbl_rect.setWidth (
			lbl_rect.width() - vars().pad_left - vars().pad_right );

		// Draw text
		painter.setTransform ( lbl.label_trans );
		painter.drawText ( lbl_rect, Qt::AlignLeft | Qt::AlignTop, lbl.text_elided );
	}


	//
	// Draw weak focus text labels
	//
	if ( weak_focus_idx >= 0 ) {
		{
			QFont fnt ( font() );
			fnt.setUnderline ( !fnt.underline() );
			painter.setFont ( fnt );
		}

		const Sliders_Pad_Header_Label & lbl ( _vars.labels[weak_focus_idx] );

		// Pen color
		{
			const QPen & ppen ( painter.pen() );
			if ( lbl.col_fg != ppen.color() ) {
				QPen npen ( ppen );
				npen.setColor ( lbl.col_fg );
				painter.setPen ( npen );
			}
		}

		// Adjust text rectangle
		QRectF lbl_rect ( lbl.label_rect );
		lbl_rect.moveLeft ( lbl_rect.left() + vars().pad_left );
		lbl_rect.setWidth (
			lbl_rect.width() - vars().pad_left - vars().pad_right );

		// Draw text
		painter.setTransform ( lbl.label_trans );
		painter.drawText ( lbl_rect, Qt::AlignLeft | Qt::AlignTop, lbl.text_elided );
	}


	//
	// Draw full focus text labels
	//
	if ( focus_idx >= 0 ) {
		const Sliders_Pad_Header_Label & lbl ( _vars.labels[focus_idx] );

		painter.setTransform ( lbl.label_trans );

		// Draw area
		painter.setPen ( Qt::NoPen );
		{
			QBrush brush_area;
			brush_area.setColor ( palette().color ( QPalette::Highlight ) );
			brush_area.setStyle ( Qt::SolidPattern );
			painter.setBrush ( brush_area );
		}
		painter.drawRect ( lbl.label_rect );

		// Draw text
		{
			QPen pen;
			pen.setColor ( palette().color ( QPalette::HighlightedText ) );
			painter.setPen ( pen );
		}
		painter.setBrush ( Qt::NoBrush );
		painter.setFont ( font() );

		// Adjust text rectangle
		QRectF lbl_rect ( lbl.label_rect );
		lbl_rect.moveLeft ( lbl_rect.left() + vars().pad_left );
		lbl_rect.setWidth (
			lbl_rect.width() - vars().pad_left - vars().pad_right );

		// Draw text
		painter.setTransform ( lbl.label_trans );
		painter.drawText ( lbl_rect, Qt::AlignLeft | Qt::AlignTop, lbl.text_elided );
	}
}


int
Sliders_Pad_Header::find_label (
	QPoint pos_n )
{
	int res ( -1 );

	const QPointF pos_f ( pos_n );
	const unsigned int num_lbl ( _vars.labels.size() );
	unsigned int lbl_idx ( num_lbl );
	for ( unsigned int ii = 0; ii < num_lbl; ++ii ) {
		--lbl_idx;
		const Sliders_Pad_Header_Label & lbl (
			_vars.labels[lbl_idx] );

		if ( pos_n.x() >= (int)lbl.group_pos ) {
			QPointF pos_map = lbl.label_trans_inv.map ( pos_f );
			QRectF rect_f ( lbl.label_rect );
			rect_f.adjust ( 0, -1.5, 0, 3.0 );
			if ( rect_f.contains ( pos_map ) ) {
				res = lbl_idx;
				break;
			}
		}
	}

	return res;
}


} // End of namespace
