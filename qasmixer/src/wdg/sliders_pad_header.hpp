//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_sliders_pad_header_hpp__
#define __INC_sliders_pad_header_hpp__

#include "sliders_pad_header_vars.hpp"

#include <QWidget>


namespace Wdg
{


///
/// @brief Sliders_Pad_Header
///
class Sliders_Pad_Header :
	public QWidget
{
	Q_OBJECT

	// Public methods
	public:

	Sliders_Pad_Header (
		QWidget * parent_n = 0 );


	QSize
	minimumSizeHint ( ) const;

	QSize
	sizeHint ( ) const;


	// Header variables

	const Sliders_Pad_Header_Vars &
	vars ( ) const;

	Sliders_Pad_Header_Vars &
	vars ( );


	// Decoration graphics

	void
	set_decoration_paths (
		const QPainterPath & path_n );

	/// @brief Sets the index of the label/group with the focus
	void
	set_focus_idx (
		int idx_n );


	unsigned int
	label_str_length_px_max (
		const QString & str_n ) const;


	// Signals
	signals:

	void
	sig_label_selected (
		unsigned int idx_n );


	// Protected methods
	protected:

	void
	enterEvent (
		QEvent * event_n );

	void
	leaveEvent (
		QEvent * event_n );

	void
	mousePressEvent (
		QMouseEvent * event_n );

	void
	mouseMoveEvent (
		QMouseEvent * event_n );

	void
	paintEvent (
		QPaintEvent * event_n );


	int
	find_label (
		QPoint pos_n );


	// Private attributes
	private:

	QPainterPath _deco_paths;

	Sliders_Pad_Header_Vars _vars;

	int _focus_idx;

	int _weak_focus_idx;
};


inline
const Sliders_Pad_Header_Vars &
Sliders_Pad_Header::vars ( ) const
{
	return _vars;
}


inline
Sliders_Pad_Header_Vars &
Sliders_Pad_Header::vars ( )
{
	return _vars;
}


inline
void
Sliders_Pad_Header::set_focus_idx (
	int idx_n )
{
	_focus_idx = idx_n;
}


} // End of namespace


#endif
