//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "sliders_pad_header_vars.hpp"


namespace Wdg
{


//
// Sliders_Pad_Header_Label
//
Sliders_Pad_Header_Label::Sliders_Pad_Header_Label ( ) :
group_pos ( 0 ),
group_width ( 0 ),
label_length ( 0 )
{
}


//
// Sliders_Pad_Header_Vars
//

Sliders_Pad_Header_Vars::Sliders_Pad_Header_Vars ( ) :
update_elided_texts ( false ),
angle ( 0.0 ),
angle_sin ( 0.0 ),
angle_cos ( 1.0 ),
max_str_length_px ( 100 ),
pad_left ( 2 ),
pad_right ( 1 ),
spacing_inter ( 3 ),
spacing_bottom ( 3 )
{
	stem_pen.setWidthF ( 1 );
	stem_pen.setCapStyle ( Qt::FlatCap );
	stem_pen.setJoinStyle ( Qt::BevelJoin );
}


} // End of namespace
