//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_sliders_pad_header_vars_hpp__
#define __INC_sliders_pad_header_vars_hpp__

#include <QPen>
#include <vector>


namespace Wdg
{


class Sliders_Pad_Header_Label
{
	// Public methods
	public:

	Sliders_Pad_Header_Label ( );


	// Attributes

	unsigned int group_pos;

	unsigned int group_width;

	unsigned int label_length;

	QTransform label_trans;

	QTransform label_trans_inv;

	QRectF label_rect;

	QString text;

	QString text_elided;

	QString tool_tip;

	QColor col_fg;
};


///
/// @brief Sliders_Pad_Header_Vars
///
class Sliders_Pad_Header_Vars
{
	// Public methods
	public:

	Sliders_Pad_Header_Vars ( );

	bool update_elided_texts;

	std::vector < Sliders_Pad_Header_Label > labels;

	QPen stem_pen;

	double angle;
	double angle_sin;
	double angle_cos;

	qreal center_x;
	qreal center_y;

	unsigned int max_str_length_px;

	const int pad_left;
	const int pad_right;
	const int spacing_inter;
	const int spacing_bottom;
};


} // End of namespace


#endif
