//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "sliders_pad_layout.hpp"
#include "equal_columns_layout_group.hpp"

#define _USE_MATH_DEFINES
#include <cmath>
#include <algorithm>
#include <iostream>

#include <QApplication>
#include <QFontMetrics>


namespace Wdg
{


//
// Sliders_Pad_Layout
//
Sliders_Pad_Layout::Sliders_Pad_Layout (
	Sliders_Pad_Header_Vars & header_vars_n,
	QWidget * parent_n ) :
Equal_Columns_Layout ( parent_n ),
_header_vars ( header_vars_n ),
_header_item ( 0 )
{
}


Sliders_Pad_Layout::~Sliders_Pad_Layout ( )
{
	if ( _header_item != 0 ) {
		delete _header_item;
	}
}


//
// QLayout methods
//

void
Sliders_Pad_Layout::set_header_item (
	QLayoutItem * item_n )
{
	if ( _header_item != 0 ) {
		delete _header_item;
	}
	_header_item = item_n;
	invalidate();
}


void
Sliders_Pad_Layout::set_header_widget (
	QWidget * wdg_n )
{
	if ( wdg_n != 0 ) {
		set_header_item ( new QWidgetItem ( wdg_n ) );
	}
}


void
Sliders_Pad_Layout::set_group_label_length (
	unsigned int grp_idx_n,
	unsigned int length_n )
{
	if ( grp_idx_n < num_header_labels() ) {
		Sliders_Pad_Header_Label & lbl ( header_label ( grp_idx_n ) );
		if ( lbl.label_length != length_n ) {
			lbl.label_length = length_n;
			invalidate();
		}
	}
}


QLayoutItem *
Sliders_Pad_Layout::itemAt (
	int idx_n ) const
{
	QLayoutItem * res ( 0 );
	if ( idx_n >= 0 ) {
		int cnt ( Equal_Columns_Layout::count() );
		if ( idx_n < cnt ) {
			res = Equal_Columns_Layout::itemAt ( idx_n );
		} else if ( ( idx_n == cnt ) && ( _header_item != 0 ) ) {
			res = _header_item;
		}
	}
	return res;
}


QLayoutItem *
Sliders_Pad_Layout::takeAt (
	int idx_n )
{
	QLayoutItem * res ( 0 );
	if ( idx_n >= 0 ) {
		int cnt ( Equal_Columns_Layout::count() );
		if ( idx_n < cnt ) {
			res = Equal_Columns_Layout::takeAt ( idx_n );
		} else if ( ( idx_n == cnt ) && ( _header_item != 0 ) ) {
			res = _header_item;
			_header_item = 0;
			invalidate();
		}
	}
	return res;
}


int
Sliders_Pad_Layout::count ( ) const
{
	int res ( Equal_Columns_Layout::count() );
	if ( _header_item != 0 ) {
		++res;
	}
	return res;
}


QSize
Sliders_Pad_Layout::minimumSize ( ) const
{
	QSize res ( Equal_Columns_Layout::minimumSize() );
	if ( _header_item != 0 ) {
		QSize hms ( _header_item->minimumSize() );
		res.rwidth() = qMax ( hms.width(), res.rwidth() );
		res.rheight() += hms.height();
	}

	//std::cout << "Sliders_Pad_Layout::minimumSize " << res.width() << ":" << res.height() << "\n";
	return res;
}


QSize
Sliders_Pad_Layout::sizeHint ( ) const
{
	QSize res ( minimumSize() );
	int & ww ( res.rwidth() );
	int & hh ( res.rheight() );

	int mg_hor ( 0 );
	int mg_vert ( 0 );
	{
		const QMargins mgs ( contentsMargins() );
		mg_hor = mgs.left() + mgs.right();
		mg_vert = mgs.top() + mgs.bottom();
	}

	ww -= mg_hor;
	hh -= mg_vert;

	ww *= 3;
	hh *= 3;
	ww /= 2;
	hh /= 2;

	ww += mg_hor;
	hh += mg_vert;

	return res;
}


unsigned int
Sliders_Pad_Layout::header_height_hint ( ) const
{
	unsigned int max_label_len ( 0 );
	{
		unsigned int num_lbl ( num_header_labels() );
		if ( num_lbl > 0 ) {
			for ( unsigned int ii=0; ii < num_lbl; ++ii ) {
				unsigned int len ( header_label ( ii ).label_length );
				max_label_len = qMax ( len, max_label_len );
			}
		} else {
			max_label_len = header_vars().max_str_length_px;
		}
	}

	max_label_len += header_vars().pad_left;
	max_label_len += header_vars().pad_right;

	qreal hh ( header_vars().spacing_bottom );
	hh += max_label_len * header_vars().angle_sin;
	hh += fontMetrics().height() * header_vars().angle_cos;

	return std::ceil ( hh );
}


void
Sliders_Pad_Layout::calc_column_widths_sync (
	unsigned int width_n )
{
	calc_column_widths ( width_n );

	// Copy groups position data to label data
	unsigned int num_grps ( num_groups() );
	for ( unsigned int ii=0; ii < num_grps; ++ii )	{
		const Equal_Columns_Layout_Group * grp ( group ( ii ) );
		Sliders_Pad_Header_Label & lbl ( header_label ( ii ) );
		lbl.group_pos   = grp->group_pos();
		lbl.group_width = grp->group_width();
	}
}


void
Sliders_Pad_Layout::calc_label_angle (
	unsigned int width_n,
	unsigned int min_width_n )
{
	QFontMetrics fmet ( fontMetrics() );

	double angle_sin;
	const unsigned int lbl_v_dist ( fmet.height() + header_vars().spacing_inter );
	const unsigned int col_h_dist ( qMax ( (unsigned int)1, smallest_group_width() ) );

	//std::cout << "lbl_v_dist " << lbl_v_dist << "\n";
	//std::cout << "col_h_dist " << col_h_dist << "\n";

	if ( ( col_h_dist >= lbl_v_dist ) && ( width_n > min_width_n ) )
	{
		angle_sin = double ( lbl_v_dist ) / double ( col_h_dist );
	} else {
		// 90 degree
		angle_sin = 1.0;
	}
	angle_sin = std::asin ( angle_sin );
	_header_vars.angle = angle_sin;
	_header_vars.angle_sin = std::sin ( angle_sin );
	_header_vars.angle_cos = std::cos ( angle_sin );

	//std::cout << "angle_sin " << _header_vars.angle_sin << "\n";
	//std::cout << "angle_cos " << _header_vars.angle_cos << "\n";

	const double fh2 ( fmet.height() / 2.0 );
	_header_vars.center_x = _header_vars.angle_sin * fh2;
	_header_vars.center_y = _header_vars.angle_cos * fh2;

	if ( _header_vars.center_x < 0.001 ) {
		_header_vars.center_x = 0;
	}
	if ( _header_vars.center_y < 0.001 ) {
		_header_vars.center_y = 0;
	}

	_header_vars.update_elided_texts = true;
}


int
Sliders_Pad_Layout::calc_labels_max_x ( )
{
	int res ( 0 );

	const Sliders_Pad_Header_Vars & hvars ( header_vars() );

	unsigned int lbl_idx ( hvars.labels.size() );
	unsigned int lim_num ( 4 ); // Check these number of labels
	if ( lbl_idx < lim_num ) {
		lim_num = lbl_idx;
	}
	for ( unsigned int ii=0; ii < lim_num; ++ii ) {
		--lbl_idx;
		const Sliders_Pad_Header_Label & lbl ( hvars.labels[lbl_idx] );

		unsigned int label_len ( lbl.label_length );
		label_len += hvars.pad_left;
		label_len += hvars.pad_right;

		qreal x_max ( lbl.group_pos );
		x_max += qreal ( lbl.group_width ) / 2.0;
		x_max += hvars.center_x;
		x_max += qreal ( label_len ) * hvars.angle_cos;

		const int x_max_int ( std::ceil ( x_max ) );
		if ( x_max_int > res ) {
			res = x_max_int;
		}
	}

	return res;
}


void
Sliders_Pad_Layout::calc_columns_sizes (
	unsigned int area_width_n,
	unsigned int area_height_n )
{
	//std::cout << "Sliders_Pad_Layout::calc_columns_sizes " << area_width_n << ":" << area_height_n << "\n";

	// Require synchronized label storage sizes
	if ( _header_vars.labels.size() != num_groups() ) {
		return;
	}

	// Minimum size of the sliders block
	QSize inputs_min_size ( Equal_Columns_Layout::minimumSize() );

	//
	// Calculate first estimate for the row heights
	//

	unsigned h_header = 0;
	unsigned h_inputs = 0;
	if ( _header_item != 0 ) {
		h_header = _header_item->minimumSize().height();
	}
	h_inputs = area_height_n - h_header;

	calc_row_heights ( h_inputs );

	// Iterate columns width so that the last labels fit into the give frame
	// The column widths are fixed after this step

	{
		unsigned int max_width ( area_width_n );
		unsigned int min_width ( ( area_width_n * 4 ) / 5 );
		if ( (int)min_width < inputs_min_size.width() ) {
			min_width = inputs_min_size.width();
		}

		unsigned int w_test ( max_width );
		while ( ( w_test >= min_width ) && ( w_test > 0 ) ) {
			calc_column_widths_sync ( w_test );
			calc_label_angle ( w_test, inputs_min_size.width() );
			const unsigned int lbl_max_x ( calc_labels_max_x() );
			if ( lbl_max_x < max_width ) {
				break;
			}
			--w_test;
		}
		//std::cout << "Iterations min_width " << min_width  << "\n";
		//std::cout << "Iterations max_width " << max_width  << "\n";
		//std::cout << "Iterations w_test " << w_test  << "\n";
		//std::cout << "Iterations " << max_width - w_test << "\n";
	}


	//
	// Adjust header and sliders block heights
	//

	const unsigned int h_header_hint ( header_height_hint() );
	h_header = h_header_hint;
	h_inputs = all_rows_height();

	// The header should always be smaller than the sliders block
	if ( ( h_header * 3 ) > area_height_n ) {
		h_header = area_height_n / 3;
		if ( h_header > h_header_hint )	{
			h_header = h_header_hint;
		}
	}

	h_inputs = area_height_n - h_header;

	// Check inputs area minimum size and shrink the header on demand
	if ( (int)h_inputs < inputs_min_size.height() ) {
		h_inputs = inputs_min_size.height();
		if ( h_inputs < area_height_n ) {
			h_header = area_height_n - h_inputs;
		} else {
			h_header = 0;
		}
	}

	calc_row_heights ( h_inputs );


	post_adjust_row_heights();


	// Remember area sizes
	_header_area.setHeight ( h_header );
	_inputs_area.setHeight ( h_inputs );
	{
		unsigned int aheight ( 0 );
		if ( num_rows() > 0 ) {
			aheight = row_height ( 0 );
		}
		_sliders_area.setHeight ( aheight );
	}
	{
		unsigned int aheight ( 0 );
		if ( num_rows() > 1 ) {
			aheight = row_height ( 1 );
		}
		_switches_area.setHeight ( aheight );
	}
}


void
Sliders_Pad_Layout::post_adjust_row_heights ( )
{
	if ( num_rows() < 2 ) {
		return;
	}
	if ( row_height ( 0 ) <= row_min_height ( 0 ) ) {
		return;
	}

	unsigned int sw_height ( row_height ( 0 ) / 16 );
	// Switch row should not be higher than broad
	for ( unsigned int ii=2; ii <=3; ++ii ) {
		const unsigned int twidth ( col_type_width ( ii ) );
		if ( ( twidth > 0 ) && ( sw_height > twidth ) ) {
			sw_height = twidth;
		}
	}

	if ( sw_height <= row_height ( 1 ) ) {
		return;
	}
	unsigned int delta ( sw_height - row_height ( 1 ) );
	unsigned int max_delta ( row_height ( 0 ) - row_min_height ( 0 ) );
	if ( delta > max_delta ) {
		delta = max_delta;
	}

	set_row_height ( 0, row_height ( 0 ) - delta );
	set_row_height ( 1, row_height ( 1 ) + delta );
}


void
Sliders_Pad_Layout::set_geometries (
	const QRect & crect_n )
{

	// Header area
	_header_area.moveLeft ( crect_n.left() );
	_header_area.moveTop ( crect_n.top() );
	_header_area.setWidth ( crect_n.width() );

	// Inputs area
	_inputs_area.moveLeft ( crect_n.left() );
	_inputs_area.moveTop ( _header_area.top() + _header_area.height() );
	_inputs_area.setWidth ( crect_n.width() );

	// Sliders area
	_sliders_area.moveLeft ( _inputs_area.left() );
	_sliders_area.moveTop ( _inputs_area.top() );
	_sliders_area.setWidth ( _inputs_area.width() );

	// Switches area
	_switches_area.moveLeft ( crect_n.left() );
	_switches_area.moveTop (
		_sliders_area.top() + _sliders_area.height() + spacing_vertical() );
	_switches_area.setWidth ( crect_n.width() );

	// Set header geometry
	if ( _header_item != 0 ) {
		_header_item->setGeometry ( _header_area );
		update_labels_transforms ( _header_area );
	}

	QRect ipt_area (
		crect_n.left(), _sliders_area.top(),
		crect_n.width(), all_rows_height() );

	Equal_Columns_Layout::set_geometries ( ipt_area );
}


void
Sliders_Pad_Layout::set_geometry_row (
	const QRect & row_rect_n,
	const Equal_Columns_Layout_Group * grp_n,
	const Equal_Columns_Layout_Column * col_n,
	const Equal_Columns_Layout_Row * row_n )
{
	QRect re_wdg (
		row_rect_n.left() + col_n->column_pos(), row_rect_n.top(),
		col_n->column_width(), row_rect_n.height() );

	// Count type numbers

	unsigned int num_sliders ( 0 );
	unsigned int num_switches ( 0 );
	if ( grp_n->num_rows() > 0 ) {
		num_sliders = grp_n->row_stat ( 0 );
		if ( grp_n->num_rows() > 1 ) {
			num_switches = grp_n->row_stat ( 1 );
		}
	}

	bool center_widget ( false );
	if ( row_n->row_index() == 0 ) {
		// Center slider on demand
		if ( ( num_sliders == 1 ) && ( num_switches > 1 ) ) {
			center_widget = true;
		}
	} else if ( row_n->row_index() == 1 ) {
		// Center switch on demand
		if ( ( num_sliders > 1 ) && ( num_switches == 1 ) ) {
			center_widget = true;
		}
	}

	if ( center_widget ) {
		// Center widget horizontally in group
		int xx ( grp_n->group_pos() );
		xx += ( grp_n->group_width() - re_wdg.width() ) / 2;
		re_wdg.moveLeft ( xx );
	}

	//std::cout << "re_wdg " << re_wdg.left() << " " << re_wdg.top() << " " << re_wdg.width() << " " << re_wdg.height() << "\n";

	row_n->item()->setGeometry ( re_wdg );
}



void
Sliders_Pad_Layout::update_labels_transforms (
	const QRect & hrect_n )
{
	//std::cout << "Sliders_Pad_Layout::update_labels_transforms\n";

	if ( !hrect_n.isValid() ) {
		return;
	}

	Sliders_Pad_Header_Vars & hvars ( header_vars() );

	const qreal fnt_h ( fontMetrics().height() );
	qreal x_right ( hrect_n.width() );
	qreal y_base ( hrect_n.height() );
	y_base -= hvars.spacing_bottom;
	y_base = qMax ( qreal ( 0.0 ), y_base );

	unsigned int num_lbl ( hvars.labels.size() );
	for ( unsigned int ii=0; ii < num_lbl; ++ii ) {
		Sliders_Pad_Header_Label & lbl ( hvars.labels[ii] );

		qreal x_center ( lbl.group_pos );
		x_center += lbl.group_width / 2.0;
		x_center += hvars.center_x;

		// Label transformation and inverse

		{
			QTransform trans (
				hvars.angle_cos, -hvars.angle_sin, 0.0,
				hvars.angle_sin, hvars.angle_cos, 0.0,
				x_center, y_base, 1.0 );
			lbl.label_trans = trans;
			lbl.label_trans_inv = trans.inverted();
		}

		// Label rectangle

		qreal lbl_width;
		if ( hvars.angle_sin > 0.002 ) {
			qreal hh ( y_base );
			hh -= fnt_h * hvars.angle_cos;
			lbl_width = hh / hvars.angle_sin;
		} else {
			lbl_width = hvars.max_str_length_px * 2;
		}

		if ( hvars.angle_cos > 0.002 ) {
			// Shorten the length if the label is too near to the right edge
			qreal xdist ( ( x_right - x_center ) / hvars.angle_cos );
			if ( xdist < lbl_width ) {
				lbl_width = xdist;
			}
		}

		lbl.label_rect = QRectF ( 0, -fnt_h, lbl_width, fnt_h );
	}
}


} // End of namespace
