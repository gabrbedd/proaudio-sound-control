//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_sliders_pad_layout_hpp__
#define __INC_sliders_pad_layout_hpp__

#include <QPen>
#include <QObject>
#include <QWidget>
#include <QList>

#include "equal_columns_layout.hpp"
#include "sliders_pad_header_vars.hpp"


namespace Wdg
{


///
/// @brief Sliders_Pad_Layout
///
class Sliders_Pad_Layout :
	public Equal_Columns_Layout
{
	// Public methods
	public:

	Sliders_Pad_Layout (
		Sliders_Pad_Header_Vars & header_vars_n,
		QWidget * parent_n = 0 );

	~Sliders_Pad_Layout ( );


	//
	// Size hints
	//

	QSize
	minimumSize ( ) const;

	QSize
	sizeHint ( ) const;


	// Header specific

	const Sliders_Pad_Header_Vars &
	header_vars ( ) const;

	Sliders_Pad_Header_Vars &
	header_vars ( );


	// Header labels

	unsigned int
	num_header_labels ( ) const;

	const Sliders_Pad_Header_Label &
	header_label (
		unsigned int idx_n ) const;

	Sliders_Pad_Header_Label &
	header_label (
		unsigned int idx_n );


	// Areas

	const QRect &
	sliders_area ( ) const;

	const QRect &
	switches_area ( ) const;


	//
	// QLayout methods
	//

	void
	set_header_item (
		QLayoutItem * item_n );

	void
	set_header_widget (
		QWidget * wdg_n );

	void
	set_group_label_length (
		unsigned int grp_idx_n,
		unsigned int length_n );


	QLayoutItem *
	itemAt (
		int index_n ) const;

	QLayoutItem *
	takeAt (
		int index_n );

	int
	count ( ) const;


	// Protected methods
	protected:

	unsigned int
	header_height_hint ( ) const;


	void
	calc_column_widths_sync (
		unsigned int width_n );

	void
	calc_label_angle (
		unsigned int width_n,
		unsigned int min_width_n );

	int
	calc_labels_max_x ( );

	void
	calc_columns_sizes (
		unsigned int area_width_n,
		unsigned int area_height_n );

	void
	post_adjust_row_heights ( );



	void
	set_geometries (
		const QRect & crect_n );

	void
	set_geometry_row (
		const QRect & row_rect_n,
		const Equal_Columns_Layout_Group * grp_n,
		const Equal_Columns_Layout_Column * col_n,
		const Equal_Columns_Layout_Row * row_n );

	void
	update_labels_transforms (
		const QRect & hrect_n );


	// Private attributes;
	private:

	Sliders_Pad_Header_Vars & _header_vars;

	QLayoutItem * _header_item;

	QRect _header_area;

	QRect _inputs_area;

	QRect _sliders_area;

	QRect _switches_area;
};


inline
const Sliders_Pad_Header_Vars &
Sliders_Pad_Layout::header_vars ( ) const
{
	return _header_vars;
}


inline
Sliders_Pad_Header_Vars &
Sliders_Pad_Layout::header_vars ( )
{
	return _header_vars;
}


inline
unsigned int
Sliders_Pad_Layout::num_header_labels ( ) const
{
	return header_vars().labels.size();
}


inline
const Sliders_Pad_Header_Label &
Sliders_Pad_Layout::header_label (
	unsigned int idx_n ) const
{
	return header_vars().labels[idx_n];
}


inline
Sliders_Pad_Header_Label &
Sliders_Pad_Layout::header_label (
	unsigned int idx_n )
{
	return header_vars().labels[idx_n];
}


inline
const QRect &
Sliders_Pad_Layout::sliders_area ( ) const
{
	return _sliders_area;
}


inline
const QRect &
Sliders_Pad_Layout::switches_area ( ) const
{
	return _switches_area;
}


} // End of namespace


#endif
