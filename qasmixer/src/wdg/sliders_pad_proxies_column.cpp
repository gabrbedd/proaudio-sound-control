//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "sliders_pad_proxies_column.hpp"
#include "pass_events.hpp"
#include <QCoreApplication>
#include <QFocusEvent>
#include <iostream>


namespace Wdg
{


//
// Sliders_Pad_Proxies_Column
//

Sliders_Pad_Proxies_Column::Sliders_Pad_Proxies_Column (
	QObject * parent_n,
	unsigned int col_idx_n ) :
QObject ( parent_n ),
_column_index ( col_idx_n ),
_proxy_slider ( 0 ),
_proxy_switch ( 0 ),
_has_focus ( false )
{
}


Sliders_Pad_Proxies_Column::~Sliders_Pad_Proxies_Column ( )
{
	clear_proxies();
}


void
Sliders_Pad_Proxies_Column::set_column_index (
	unsigned int idx_n )
{
	_column_index = idx_n;
}


void
Sliders_Pad_Proxies_Column::clear_proxies ( )
{
	if ( _proxy_slider != 0 ) {
		delete _proxy_slider;
		_proxy_slider = 0;
	}
	if ( _proxy_switch != 0 ) {
		delete _proxy_switch;
		_proxy_switch = 0;
	}
}


void
Sliders_Pad_Proxies_Column::set_slider_proxy (
	Sliders_Pad_Proxy_Slider * proxy_n )
{
	if ( proxy_n != 0 ) {
		proxy_n->setParent ( this );
	}
	_proxy_slider = proxy_n;
}


void
Sliders_Pad_Proxies_Column::set_switch_proxy (
	Sliders_Pad_Proxy_Switch * proxy_n )
{
	if ( proxy_n != 0 ) {
		proxy_n->setParent ( this );
	}
	_proxy_switch = proxy_n;
}


bool
Sliders_Pad_Proxies_Column::event (
	QEvent * event_n )
{
	Focus_Pass_Event * ev_fp (
		dynamic_cast < Focus_Pass_Event * > ( event_n ) );
	if ( ev_fp != 0 ) {
		_has_focus = ev_fp->gotFocus();
		if ( parent() != 0 ) {
			Focus_Pass_Event ev_pass ( *ev_fp, column_index() );
			QCoreApplication::sendEvent ( parent(), &ev_pass );
		}
		return true;
	}

	return QObject::event ( event_n );
}


} // End of namespace
