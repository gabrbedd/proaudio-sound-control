//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_sliders_pad_proxies_column_hpp__
#define __INC_sliders_pad_proxies_column_hpp__

#include <QObject>
#include <QList>

#include "sliders_pad_proxy.hpp"
#include "sliders_pad_proxy_slider.hpp"
#include "sliders_pad_proxy_switch.hpp"


namespace Wdg
{


class Sliders_Pad_Proxies_Column :
	public QObject
{
	Q_OBJECT

	// Public methods
	public:

	Sliders_Pad_Proxies_Column (
		QObject * parent_n = 0,
		unsigned int col_idx_n = 0 );

	virtual
	~Sliders_Pad_Proxies_Column ( );


	// Column index

	unsigned int
	column_index ( ) const;

	void
	set_column_index (
		unsigned int idx_n );


	void
	clear_proxies ( );


	// Slider proxy

	Sliders_Pad_Proxy_Slider *
	slider_proxy ( );

	void
	set_slider_proxy (
		Sliders_Pad_Proxy_Slider * proxy_n );


	// Switch proxy

	Sliders_Pad_Proxy_Switch *
	switch_proxy ( );

	void
	set_switch_proxy (
		Sliders_Pad_Proxy_Switch * proxy_n );


	// State info

	bool
	has_slider ( ) const;

	bool
	has_switch ( ) const;

	bool
	has_focus ( ) const;


	// Event handling

	bool
	event (
		QEvent * event_n );


	// Private attributes
	private:

	unsigned int _column_index;

	Sliders_Pad_Proxy_Slider * _proxy_slider;

	Sliders_Pad_Proxy_Switch * _proxy_switch;

	bool _has_focus;
};


inline
unsigned int
Sliders_Pad_Proxies_Column::column_index ( ) const
{
	return _column_index;
}


inline
Sliders_Pad_Proxy_Slider *
Sliders_Pad_Proxies_Column::slider_proxy ( )
{
	return _proxy_slider;
}


inline
Sliders_Pad_Proxy_Switch *
Sliders_Pad_Proxies_Column::switch_proxy ( )
{
	return _proxy_switch;
}


inline
bool
Sliders_Pad_Proxies_Column::has_slider ( ) const
{
	return ( _proxy_slider != 0 );
}


inline
bool
Sliders_Pad_Proxies_Column::has_switch ( ) const
{
	return ( _proxy_switch != 0 );
}


typedef QList < Sliders_Pad_Proxies_Column * > Sliders_Pad_Proxies_Column_List;


} // End of namespace


#endif

