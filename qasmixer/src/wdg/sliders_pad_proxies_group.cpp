//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "sliders_pad_proxies_group.hpp"
#include "pass_events.hpp"
#include <QCoreApplication>
#include <iostream>


namespace Wdg
{


Sliders_Pad_Proxies_Group::Sliders_Pad_Proxies_Group (
	QObject * parent_n ) :
QObject ( parent_n ),
_sliders_pad ( 0 ),
_group_index ( 0 ),
_num_sliders ( 0 ),
_num_switches ( 0 ),
_focus_column ( 0 ),
_has_focus ( false )
{
}


Sliders_Pad_Proxies_Group::~Sliders_Pad_Proxies_Group ( )
{
	clear_columns();
}


void
Sliders_Pad_Proxies_Group::set_sliders_pad (
	QObject * pad_n )
{
	_sliders_pad = pad_n;
}


void
Sliders_Pad_Proxies_Group::set_group_index (
	unsigned int idx_n )
{
	_group_index = idx_n;
}


void
Sliders_Pad_Proxies_Group::clear_columns ( )
{
	if ( _columns.size() > 0 ) {
		for ( int ii=0; ii < _columns.size(); ++ii ) {
			delete _columns[ii];
		}
		_columns.clear();

		_num_sliders = 0;
		_num_switches = 0;
	}
}


void
Sliders_Pad_Proxies_Group::append_column (
	Sliders_Pad_Proxies_Column * column_n )
{
	if ( column_n != 0 ) {

		column_n->setParent ( this );
		column_n->set_column_index ( _columns.size() );

		_columns.append ( column_n );
		if ( column_n->has_slider() != 0 ) {
			++_num_sliders;
		}
		if ( column_n->has_switch() != 0 ) {
			++_num_switches;
		}
	}
}


void
Sliders_Pad_Proxies_Group::set_group_name (
	const QString & name_n )
{
	_group_name = name_n;
}


void
Sliders_Pad_Proxies_Group::set_tool_tip (
	const QString & tip_n )
{
	_tool_tip = tip_n;
}


void
Sliders_Pad_Proxies_Group::set_palette (
	const QPalette & pal_n )
{
	_palette = pal_n;
}


void
Sliders_Pad_Proxies_Group::take_focus (
	unsigned int column_n )
{
	if ( column_n < num_columns() ) {
		Sliders_Pad_Proxies_Column * col ( column ( column_n ) );
		if ( col->slider_proxy() != 0 ) {
			if ( col->slider_proxy()->widget() != 0 ) {
				col->slider_proxy()->widget()->setFocus();
			}
		}
	}
}


bool
Sliders_Pad_Proxies_Group::event (
	QEvent * event_n )
{
	Focus_Pass_Event * ev_fp (
		dynamic_cast < Focus_Pass_Event * > ( event_n ) );
	if ( ev_fp != 0 ) {
		_has_focus = ev_fp->gotFocus();
		_focus_column = ev_fp->child_idx();

		if ( sliders_pad() != 0 ) {
			Focus_Pass_Event ev_pass ( *ev_fp, group_index() );
			QCoreApplication::sendEvent ( sliders_pad(), &ev_pass );
		}
		return true;
	}

	return QObject::event ( event_n );
}


} // End of namespace
