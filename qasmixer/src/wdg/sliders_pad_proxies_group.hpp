//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_sliders_pad_proxies_group_hpp__
#define __INC_sliders_pad_proxies_group_hpp__

#include <QObject>
#include <QList>

#include "sliders_pad_proxies_column.hpp"


namespace Wdg
{


class Sliders_Pad_Proxies_Group :
	public QObject
{
	Q_OBJECT

	// Public methods
	public:

	Sliders_Pad_Proxies_Group (
		QObject * parent_n = 0 );

	virtual
	~Sliders_Pad_Proxies_Group ( );


	/// @brief The sliders pad object
	/// Gets set by the sliders pad and shouldn't be set manually
	QObject *
	sliders_pad ( ) const;

	void
	set_sliders_pad (
		QObject * pad_n );


	// Group index

	/// @brief The group index
	/// Gets set by the sliders pad and shouldn't be set manually
	unsigned int
	group_index ( ) const;

	void
	set_group_index (
		unsigned int idx_n );


	// Group name

	const QString &
	group_name ( ) const;

	void
	set_group_name (
		const QString & name_n );


	// Tool tip

	const QString &
	tool_tip ( ) const;

	void
	set_tool_tip (
		const QString & tip_n );


	// Palette

	const QPalette &
	palette ( ) const;

	void
	set_palette (
		const QPalette & pal_n );


	// Proxies columns

	unsigned int
	num_columns ( ) const;

	void
	append_column (
		Sliders_Pad_Proxies_Column * column_n );

	void
	clear_columns ( );

	Sliders_Pad_Proxies_Column *
	column (
		unsigned int idx_n );


	// Statistics

	unsigned int
	num_sliders ( ) const;

	unsigned int
	num_switches ( ) const;


	// Focus

	unsigned int
	focus_column ( ) const;

	bool
	has_focus ( ) const;



	// Public slots
	public slots:

	void
	take_focus (
		unsigned int column_n );


	// Protected methods
	protected:

	bool
	event (
		QEvent * event_n );


	// Private attributes
	private:

	QObject * _sliders_pad;

	unsigned int _group_index;

	Sliders_Pad_Proxies_Column_List _columns;

	QString _group_name;

	QString _tool_tip;

	unsigned int _num_sliders;

	unsigned int _num_switches;

	unsigned int _focus_column;

	bool _has_focus;

	QPalette _palette;
};


inline
QObject *
Sliders_Pad_Proxies_Group::sliders_pad ( ) const
{
	return _sliders_pad;
}


inline
unsigned int
Sliders_Pad_Proxies_Group::group_index ( ) const
{
	return _group_index;
}


inline
unsigned int
Sliders_Pad_Proxies_Group::num_columns ( ) const
{
	return _columns.size();
}


inline
Sliders_Pad_Proxies_Column *
Sliders_Pad_Proxies_Group::column (
	unsigned int idx_n )
{
	return _columns[idx_n];
}


inline
const QString &
Sliders_Pad_Proxies_Group::group_name ( ) const
{
	return _group_name;
}


inline
const QString &
Sliders_Pad_Proxies_Group::tool_tip ( ) const
{
	return _tool_tip;
}


inline
const QPalette &
Sliders_Pad_Proxies_Group::palette ( ) const
{
	return _palette;
}


inline
unsigned int
Sliders_Pad_Proxies_Group::num_sliders ( ) const
{
	return _num_sliders;
}


inline
unsigned int
Sliders_Pad_Proxies_Group::num_switches ( ) const
{
	return _num_switches;
}


inline
unsigned int
Sliders_Pad_Proxies_Group::focus_column ( ) const
{
	return _focus_column;
}


inline
bool
Sliders_Pad_Proxies_Group::has_focus ( ) const
{
	return _has_focus;
}


typedef QList < Sliders_Pad_Proxies_Group * > Sliders_Pad_Proxies_Group_List;


} // End of namespace


#endif
