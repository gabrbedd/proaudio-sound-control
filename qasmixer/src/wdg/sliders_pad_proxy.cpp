//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "sliders_pad_proxy.hpp"
#include <QCoreApplication>
#include <QFocusEvent>
#include "pass_events.hpp"
#include <iostream>


namespace Wdg
{


//
// Sliders_Pad_Proxy_Style
//

Sliders_Pad_Proxy_Style::Sliders_Pad_Proxy_Style (
	unsigned int style_id_n ) :
style_id ( style_id_n ),
slider_minimum_idx ( 0 ),
slider_has_minimum ( false )
{
}


//
// Sliders_Pad_Proxy
//

Sliders_Pad_Proxy::Sliders_Pad_Proxy (
	QObject * parent_n ) :
QObject ( parent_n ),
_widget ( 0 ),
_style_info ( 0 ),
_has_focus ( false )
{
}


Sliders_Pad_Proxy::~Sliders_Pad_Proxy ( )
{
	if ( _style_info != 0 ) {
		delete _style_info;
	}
}


void
Sliders_Pad_Proxy::set_has_focus (
	bool flag_n )
{
	_has_focus = flag_n;
}


void
Sliders_Pad_Proxy::set_widget (
	QWidget * wdg_n )
{
	_widget = wdg_n;
}


void
Sliders_Pad_Proxy::set_item_name (
	const QString & name_n )
{
	_item_name = name_n;
}


void
Sliders_Pad_Proxy::set_group_name (
	const QString & name_n )
{
	_group_name = name_n;
}


void
Sliders_Pad_Proxy::set_tool_tip (
	const QString & tip_n )
{
	_tool_tip = tip_n;
}


void
Sliders_Pad_Proxy::set_style_info (
	Sliders_Pad_Proxy_Style * style_n )
{
	if ( _style_info != 0 ) {
		delete _style_info;
	}
	_style_info = style_n;
}


bool
Sliders_Pad_Proxy::eventFilter (
	QObject * obj_n,
	QEvent * event_n )
{
	bool res ( QObject::eventFilter ( obj_n, event_n ) );

	if ( !res ) {
		QFocusEvent * ev_foc (
			dynamic_cast < QFocusEvent * > ( event_n ) );
		if ( ev_foc != 0 ) {
			_has_focus = ev_foc->gotFocus();
			//std::cout << "Sliders_Pad_Proxy::eventFilter: QFocusEvent " << _has_focus  << "\n";
			if ( parent() != 0 ) {
				Focus_Pass_Event ev_pass ( *ev_foc, 0 );
				QCoreApplication::sendEvent ( parent(), &ev_pass );
			}
		}
	}

	return res;
}


} // End of namespace
