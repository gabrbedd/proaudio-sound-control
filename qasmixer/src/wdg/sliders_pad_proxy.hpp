//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_sliders_pad_proxy_hpp__
#define __INC_sliders_pad_proxy_hpp__

#include <QObject>
#include <QPalette>
#include <QWidget>


namespace Wdg
{


///
/// @brief Sliders_Pad_Proxy_Style
///
class Sliders_Pad_Proxy_Style
{
	// Public methods
	public:

	Sliders_Pad_Proxy_Style (
		unsigned int style_id_n = 0 );

	// Public attributes
	QPalette palette;

	unsigned int style_id;

	unsigned int slider_minimum_idx;

	bool slider_has_minimum;
};


///
/// @brief Sliders_Pad_Proxy
///
class Sliders_Pad_Proxy :
	public QObject
{
	Q_OBJECT;

	// Public methods
	public:

	Sliders_Pad_Proxy (
		QObject * parent_n = 0 );

	virtual
	~Sliders_Pad_Proxy ( );


	// Focus

	bool
	has_focus ( ) const;

	void
	set_has_focus (
		bool flag_n );


	// Widget

	QWidget *
	widget ( ) const;

	void
	set_widget (
		QWidget * wdg_n );


	// Item name

	const QString &
	item_name ( ) const;

	void
	set_item_name (
		const QString & name_n );


	// Group name

	const QString &
	group_name ( ) const;

	void
	set_group_name (
		const QString & name_n );


	// Tool tip

	const QString &
	tool_tip ( ) const;

	void
	set_tool_tip (
		const QString & tip_n );


	// Style informations

	void
	set_style_info (
		Sliders_Pad_Proxy_Style * style_n );

	const Sliders_Pad_Proxy_Style *
	style_info ( ) const;


	// Protected methods
	protected:

	bool
	eventFilter (
		QObject * obj_n,
		QEvent * event_n );


	// Private attributes
	private:

	QWidget * _widget;

	QString _item_name;

	QString _group_name;

	QString _tool_tip;

	Sliders_Pad_Proxy_Style * _style_info;

	bool _has_focus;
};


inline
bool
Sliders_Pad_Proxy::has_focus ( ) const
{
	return _has_focus;
}


inline
QWidget *
Sliders_Pad_Proxy::widget ( ) const
{
	return _widget;
}


inline
const QString &
Sliders_Pad_Proxy::item_name ( ) const
{
	return _item_name;
}


inline
const QString &
Sliders_Pad_Proxy::group_name ( ) const
{
	return _group_name;
}


inline
const QString &
Sliders_Pad_Proxy::tool_tip ( ) const
{
	return _tool_tip;
}


inline
const Sliders_Pad_Proxy_Style *
Sliders_Pad_Proxy::style_info ( ) const
{
	return _style_info;
}


} // End of namespace


#endif


