//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "sliders_pad_proxy_slider.hpp"
#include "sliders_pad_proxies_column.hpp"


namespace Wdg
{


Sliders_Pad_Proxy_Slider::Sliders_Pad_Proxy_Slider (
	QObject * parent_n ) :
Sliders_Pad_Proxy ( parent_n ),
_slider_index ( 0 ),
_slider_index_max ( 0 )
{
	if ( parent_n != 0 ) {
		typedef Sliders_Pad_Proxies_Column PCol;
		PCol * pcol ( dynamic_cast < PCol * > ( parent_n ) );
		if ( pcol != 0 ) {
			pcol->set_slider_proxy ( this );
		}
	}
}


Sliders_Pad_Proxy_Slider::~Sliders_Pad_Proxy_Slider ( )
{
}


void
Sliders_Pad_Proxy_Slider::set_slider_index (
	unsigned long index_n )
{
	if ( index_n > slider_index_max() ) {
		index_n = slider_index_max();
	}
	if ( slider_index() != index_n ) {
		_slider_index = index_n;
		this->slider_index_changed();
		emit sig_slider_index_changed ( slider_index() );
	}
}


void
Sliders_Pad_Proxy_Slider::set_slider_index_max (
	unsigned long index_n )
{
	if ( slider_index_max() != index_n ) {
		_slider_index_max = index_n;
		if ( slider_index() > slider_index_max() ) {
			set_slider_index ( slider_index_max() );
		}
		this->slider_index_max_changed();
		emit sig_slider_index_max_changed ( slider_index_max() );
	}
}


void
Sliders_Pad_Proxy_Slider::slider_index_changed ( )
{
	// Dummy implementation
}


void
Sliders_Pad_Proxy_Slider::slider_index_max_changed ( )
{
	// Dummy implementation
}


} // End of namespace
