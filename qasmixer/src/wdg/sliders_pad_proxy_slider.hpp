//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_sliders_pad_proxy_slider_hpp__
#define __INC_sliders_pad_proxy_slider_hpp__

#include "sliders_pad_proxy.hpp"


namespace Wdg
{


///
/// @brief Sliders_Pad_Proxy_Slider
///
class Sliders_Pad_Proxy_Slider :
	public Sliders_Pad_Proxy
{
	Q_OBJECT;

	// Public methods
	public:

	Sliders_Pad_Proxy_Slider (
		QObject * parent_n = 0 );

	~Sliders_Pad_Proxy_Slider ( );


	unsigned long
	slider_index ( ) const;

	unsigned long
	slider_index_max ( ) const;


	// Signals
	signals:

	void
	sig_slider_index_changed (
		unsigned long idx_n );

	void
	sig_slider_index_max_changed (
		unsigned long idx_n );


	// Public slots
	public slots:

	void
	set_slider_index (
		unsigned long idx_n );

	void
	set_slider_index_max (
		unsigned long idx_n );


	// Protected methods
	protected:

	virtual
	void
	slider_index_changed ( );

	virtual
	void
	slider_index_max_changed ( );


	// Private attributes
	private:

	unsigned long _slider_index;

	unsigned long _slider_index_max;
};


inline
unsigned long
Sliders_Pad_Proxy_Slider::slider_index ( ) const
{
	return _slider_index;
}


inline
unsigned long
Sliders_Pad_Proxy_Slider::slider_index_max ( ) const
{
	return _slider_index_max;
}


} // End of namespace


#endif
