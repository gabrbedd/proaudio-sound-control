//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "sliders_pad_proxy_switch.hpp"
#include "sliders_pad_proxies_column.hpp"


namespace Wdg
{


//
// Sliders_Pad_Proxy_Switch
//

Sliders_Pad_Proxy_Switch::Sliders_Pad_Proxy_Switch (
	QObject * parent_n ) :
Sliders_Pad_Proxy ( parent_n ),
_switch_state ( false )
{
	if ( parent_n != 0 ) {
		typedef Sliders_Pad_Proxies_Column PCol;
		PCol * pcol ( dynamic_cast < PCol * > ( parent_n ) );
		if ( pcol != 0 ) {
			pcol->set_switch_proxy ( this );
		}
	}
}


Sliders_Pad_Proxy_Switch::~Sliders_Pad_Proxy_Switch ( )
{
}


void
Sliders_Pad_Proxy_Switch::set_switch_state (
	bool state_n )
{
	if ( state_n != _switch_state ) {
		_switch_state = state_n;
		this->switch_state_changed();
		emit sig_switch_state_changed ( switch_state() );
	}
}


void
Sliders_Pad_Proxy_Switch::switch_state_changed ( )
{
	// Dummy implementation
}


} // End of namespace

