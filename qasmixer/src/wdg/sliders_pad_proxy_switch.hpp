//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_sliders_pad_proxy_switch_hpp__
#define __INC_sliders_pad_proxy_switch_hpp__

#include "sliders_pad_proxy.hpp"


namespace Wdg
{


///
/// @brief Sliders_Pad_Proxy_Switch
///
class Sliders_Pad_Proxy_Switch :
	public Sliders_Pad_Proxy
{
	Q_OBJECT;

	// Public methods
	public:

	Sliders_Pad_Proxy_Switch (
		QObject * parent_n = 0 );

	~Sliders_Pad_Proxy_Switch ( );


	bool
	switch_state ( ) const;



	// Signals
	signals:

	void
	sig_switch_state_changed (
		bool state_n );


	// Public slots
	public slots:

	void
	set_switch_state (
		bool state_n );


	// Protected methods
	protected:

	virtual
	void
	switch_state_changed ( );


	// Private attributes
	private:

	bool _switch_state;
};


inline
bool
Sliders_Pad_Proxy_Switch::switch_state ( ) const
{
	return _switch_state;
}


} // End of namespace


#endif


