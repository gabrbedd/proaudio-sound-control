//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "switches_area.hpp"

#include "switches_pad.hpp"

#include <QScrollBar>
#include <QEvent>

#include <iostream>


namespace Wdg
{


//
// Switches_Area
//
Switches_Area::Switches_Area (
	QWidget * parent_n ) :
QScrollArea ( parent_n )
{
	setHorizontalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );
	setWidgetResizable ( true );
}


QSize
Switches_Area::minimumSizeHint ( ) const
{
	QSize res ( QScrollArea::minimumSizeHint() );

	if ( widget() != 0 ) {
		res.setWidth ( 0 );
		{
			const QSize wmsh ( widget()->minimumSizeHint() );
			if ( wmsh.width() > 0 ) {
				res.rwidth() += wmsh.width();
			}
		}

		if ( verticalScrollBar() != 0 ) {
			QSize sb_msh ( verticalScrollBar()->minimumSizeHint() );
			if ( sb_msh.width() <= 0 ) {
				sb_msh = verticalScrollBar()->sizeHint();
			}
			if ( sb_msh.width() > 0 ) {
				res.rwidth() += sb_msh.width();
			}
		}

		{
			const QMargins mgs ( contentsMargins() );
			res.rwidth() += ( mgs.left() + mgs.right() );
		}
	}

	return res;
}


void
Switches_Area::set_widget (
	QWidget * wdg_n )
{
	if ( widget() != 0 ) {
		widget()->removeEventFilter ( this );
	}

	setWidget ( wdg_n );

	if ( widget() != 0 ) {
		widget()->installEventFilter ( this );
	}

	updateGeometry();
}


QWidget *
Switches_Area::take_widget ( )
{
	if ( widget() != 0 ) {
		widget()->removeEventFilter ( this );
	}
	return takeWidget();
}


void
Switches_Area::resizeEvent (
	QResizeEvent * event_n )
{
	if ( widget() != 0 ) {
		Switches_Pad * spad ( dynamic_cast < Switches_Pad * > ( widget() ) );
		if ( spad != 0 ) {
			spad->set_viewport_geometry ( viewport()->contentsRect() );
		}
	}
	QScrollArea::resizeEvent ( event_n );
}


bool
Switches_Area::eventFilter (
	QObject * watched_n,
	QEvent * event_n )
{
	if ( watched_n == widget() ) {
		if ( event_n->type() == QEvent::LayoutRequest ) {
			//std::cout << "Switches_Area::eventFilter: " << event_n->type() << "\n";
			updateGeometry();
		}
	}

	return false;
}


} // End of namespace

