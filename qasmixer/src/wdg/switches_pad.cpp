//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "switches_pad.hpp"
#include "switches_pad_proxy_enum.hpp"
#include "switches_pad_proxy_switch.hpp"
#include "switches_pad_widgets_group.hpp"
#include "fill_columns_layout.hpp"
#include "color_methods.hpp"
#include "pass_events.hpp"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QScrollBar>
#include <QCheckBox>

#include <QEvent>
#include <QFocusEvent>

#include <iostream>


namespace Wdg
{


Switches_Pad::Switches_Pad (
	QWidget * parent_n ) :
QWidget ( parent_n ),
_proxies_groups ( 0 )
{
	_stem_pen.setWidth ( 1 );
	update_colors();
}


Switches_Pad::~Switches_Pad ( )
{
	set_proxies_groups ( 0 );
}


void
Switches_Pad::set_viewport_geometry (
	const QRect & rect_n )
{
	//std::cout << "Switches_Pad::set_viewport_geometry\n";
	if ( _viewport != rect_n ) {
		_viewport = rect_n;
		Fill_Columns_Layout * lay_cols (
			dynamic_cast < Fill_Columns_Layout * > ( layout() ) );
		if ( lay_cols != 0 ) {
			lay_cols->set_viewport_geometry ( _viewport );
		}
	}
}


void
Switches_Pad::set_proxies_groups (
	Switches_Pad_Proxies_Group_List * groups_n )
{
	if ( groups_n != _proxies_groups ) {
		clear_widgets_groups();

		_proxies_groups = groups_n;

		create_widgets_groups();
	}
}


void
Switches_Pad::clear_widgets_groups ( )
{
	if ( layout() != 0 ) {
		delete layout();
	}

	if ( _widgets_groups.size() > 0 ) {
		for ( int ii=0; ii < _widgets_groups.size(); ++ii ) {
			delete _widgets_groups[ii];
		}
		_widgets_groups.clear();
	}
}


void
Switches_Pad::create_widgets_groups ( )
{
	//std::cout << "Mixer_Switches::create_widgets_groups\n";

	if ( _proxies_groups == 0 ) {
		return;
	}

	Fill_Columns_Layout * lay_cols ( new Fill_Columns_Layout );
	lay_cols->set_viewport_geometry ( _viewport );

	for ( int ii=0; ii < (*_proxies_groups).size(); ++ii ) {
		Switches_Pad_Proxies_Group * sppg ( (*_proxies_groups)[ii] );
		sppg->set_switches_pad ( this );
		sppg->set_group_index ( ii );

		Switches_Pad_Widgets_Group * spwg (
			new Switches_Pad_Widgets_Group );

		if ( sppg->is_multichannel() ) {
			QLabel * label ( new QLabel ( spwg ) );
			label->setText ( sppg->group_name() );
			label->setToolTip ( sppg->tool_tip() );
			label->setFocusPolicy ( Qt::ClickFocus );
			label->setContextMenuPolicy ( Qt::NoContextMenu );

			{
				QFont fnt ( font() );
				fnt.setBold ( true );
				label->setFont ( fnt );
			}

			label->installEventFilter ( spwg );

			spwg->set_label ( label );
			spwg->set_stem_pen ( _stem_pen );
		}

		for ( unsigned int jj=0; jj < sppg->num_proxies(); ++jj ) {

			Switches_Pad_Proxy * spp ( sppg->proxy ( jj ) );

			unsigned int style_id ( 0 );
			if ( spp->style_info() != 0 ) {
				style_id = spp->style_info()->style_id;
			}

			Switches_Pad_Proxy_Enum * spp_enum (
				dynamic_cast < Switches_Pad_Proxy_Enum * > ( spp ) );
			if ( spp_enum != 0 ) {

				// Is enumerated

				Switches_Pad_Widgets * spw ( new Switches_Pad_Widgets );

				{  // Label
					QLabel * label ( new QLabel ( spwg ) );
					label->setText ( spp_enum->item_name() );
					label->setToolTip ( spp_enum->tool_tip() );
					spw->set_label_wdg ( label );
				}

				{ // Combo box
					// Setup selectable items
					QComboBox * cbox ( new QComboBox ( spwg ) );
					for ( int ee=0; ee < spp_enum->enum_num_items(); ++ee ) {
						cbox->addItem ( spp_enum->enum_item_name ( ee ) );
					}
					cbox->setCurrentIndex ( spp_enum->enum_index() );
					cbox->setToolTip ( spp_enum->tool_tip() );

					connect ( cbox,
						SIGNAL ( currentIndexChanged ( int ) ),
						spp_enum,
						SLOT ( set_enum_index ( int ) ) );
					connect ( spp_enum,
						SIGNAL ( sig_enum_index_changed ( int ) ),
						cbox,
						SLOT ( setCurrentIndex ( int ) ) );

					// Install event filter
					cbox->installEventFilter ( spp );

					spw->set_input_wdg ( cbox );
				}

				spwg->append_widgets ( spw );
				continue;
			}

			Switches_Pad_Proxy_Switch * spp_switch (
				dynamic_cast < Switches_Pad_Proxy_Switch * > ( spp ) );
			if ( spp_switch != 0 ) {

				// Is switch

				Switches_Pad_Widgets * spw ( new Switches_Pad_Widgets );

				QCheckBox * switch_wdg ( new QCheckBox ( spwg ) );

				if ( sppg->is_multichannel() ) {
					switch_wdg->setText ( spp_switch->item_name() );
				} else {
					switch_wdg->setText ( sppg->group_name() );
				}
				switch_wdg->setChecked ( spp_switch->switch_state() );
				switch_wdg->setToolTip ( spp_switch->tool_tip() );
				switch_wdg->setPalette ( palette() );

				connect ( switch_wdg, SIGNAL ( toggled ( bool ) ),
					spp_switch, SLOT ( set_switch_state ( bool ) ) );

				connect ( spp_switch, SIGNAL ( sig_switch_state_changed ( bool ) ),
					switch_wdg, SLOT ( setChecked ( bool ) ) );

				// Install event filter
				switch_wdg->installEventFilter ( spp );

				spw->set_input_wdg ( switch_wdg );
				spwg->append_widgets ( spw );
				continue;
			}

		}

		// Append widgets group to the list
		if ( spwg->num_widgets() > 0 ) {
			_widgets_groups.append ( spwg );
		} else {
			delete spwg;
		}
	}

	// Append to layout
	for ( int ii=0; ii < _widgets_groups.size(); ++ii ) {
		lay_cols->addWidget ( _widgets_groups[ii] );
	}
	setLayout ( lay_cols );
}


void
Switches_Pad::set_focus_proxy (
	unsigned int proxies_group_idx_n )
{
	set_focus_proxy ( proxies_group_idx_n, 0 );
}


void
Switches_Pad::set_focus_proxy (
	unsigned int proxies_group_idx_n,
	unsigned int proxy_idx_n )
{
	const unsigned int num_groups ( _widgets_groups.size() );
	if ( proxies_group_idx_n < num_groups ) {
		Switches_Pad_Widgets_Group * spwg (
			_widgets_groups[ proxies_group_idx_n ] );

		QWidget * wdg ( 0 );

		// Select the slider of given widget set
		if ( proxy_idx_n < spwg->num_widgets() ) {
			wdg = spwg->widgets ( proxy_idx_n )->input_wdg();
		}

		// No widget found so far - Take first slider then
		if ( wdg == 0 ) {
			for ( unsigned int ii=0; ii < spwg->num_widgets(); ++ii ) {
				Switches_Pad_Widgets * spw ( spwg->widgets ( ii ) );
				if ( spw->input_wdg() != 0 ) {
					wdg = spw->input_wdg();
					break;
				}
			}
		}

		if ( wdg != 0 ) {
			wdg->setFocus();
		}
	}
}


void
Switches_Pad::update_colors ( )
{
	{
		QPalette pal ( palette() );
		const QColor col_bg ( pal.color ( QPalette::Button ) );
		const QColor col_fg ( pal.color ( QPalette::ButtonText ) );
		QColor col = Wdg::Painter::col_mix ( col_bg, col_fg, 1, 1 );

		_stem_pen.setColor ( col );
	}
}


bool
Switches_Pad::event (
	QEvent * event_n )
{
	Focus_Pass_Event * ev_fp (
		dynamic_cast < Focus_Pass_Event * > ( event_n ) );
	if ( ev_fp != 0 ) {

		_focus_info.clear();
		if ( ev_fp->gotFocus() && ( ev_fp->child_idx() < num_groups() ) ) {
			Switches_Pad_Proxies_Group * sppg (
				proxies_group ( ev_fp->child_idx() ) );
			_focus_info.has_focus = true;
			_focus_info.group_idx = ev_fp->child_idx();
			_focus_info.column_idx = sppg->focus_proxy();
		}

		emit sig_focus_changed();

		return true;
	}

	return QWidget::event ( event_n );
}


} // End of namespace
