//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_switches_pad_hpp__
#define __INC_switches_pad_hpp__

#include <QWidget>
#include <QList>
#include <QPen>

#include "pad_focus_info.hpp"
#include "switches_pad_proxies_group.hpp"


namespace Wdg
{


// Forward declaration
class Switches_Pad_Widgets_Group;


///
/// @brief Switches_Pad
///
class Switches_Pad :
	public QWidget
{
	Q_OBJECT

	// Public methods
	public:

	Switches_Pad (
		QWidget * parent_n = 0 );

	~Switches_Pad ( );


	void
	set_viewport_geometry (
		const QRect & rect_n );


	// Proxies groups

	Switches_Pad_Proxies_Group_List *
	proxies_groups ( ) const;

	void
	set_proxies_groups (
		Switches_Pad_Proxies_Group_List * groups_n );

	Switches_Pad_Proxies_Group *
	proxies_group (
		unsigned int idx_n );

	unsigned int
	num_groups ( ) const;


	// Focus item getting / setting

	const Pad_Focus_Info &
	focus_info ( );

	// Event handling

	bool
	event (
		QEvent * event_n );



	// Public slots
	public slots:

	void
	set_focus_proxy (
		unsigned int proxies_group_idx_n );

	void
	set_focus_proxy (
		unsigned int proxies_group_idx_n,
		unsigned int proxy_idx_n );


	// Public signals
	signals:

	void
	sig_focus_changed ( );


	// Protected methods
	protected:

	void
	clear_widgets_groups ( );

	void
	create_widgets_groups ( );

	void
	update_colors();


	// Private attributes
	private:

	Switches_Pad_Proxies_Group_List * _proxies_groups;

	QList < Switches_Pad_Widgets_Group * > _widgets_groups;


	QRect _viewport;

	Pad_Focus_Info _focus_info;


	QPen _stem_pen;
};


inline
Switches_Pad_Proxies_Group_List *
Switches_Pad::proxies_groups ( ) const
{
	return _proxies_groups;
}


inline
Switches_Pad_Proxies_Group *
Switches_Pad::proxies_group (
	unsigned int idx_n )
{
	return (*_proxies_groups)[idx_n];
}


inline
unsigned int
Switches_Pad::num_groups ( ) const
{
	unsigned int res ( 0 );
	if ( _proxies_groups != 0 ) {
		res = _proxies_groups->size();
	}
	return res;
}


inline
const Pad_Focus_Info &
Switches_Pad::focus_info ( )
{
	return _focus_info;
}


} // End of namespace


#endif
