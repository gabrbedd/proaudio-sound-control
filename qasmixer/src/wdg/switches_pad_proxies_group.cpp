//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "switches_pad_proxies_group.hpp"
#include "pass_events.hpp"
#include <QCoreApplication>


namespace Wdg
{


//
// Switches_Pad_Proxies_Group
//

Switches_Pad_Proxies_Group::Switches_Pad_Proxies_Group (
	QObject * parent_n ) :
QObject ( parent_n ),
_switches_pad ( 0 ),
_group_index ( 0 ),
_is_switch ( false ),
_is_enum ( false ),
_has_focus ( false ),
_focus_proxy ( 0 )
{
}


Switches_Pad_Proxies_Group::~Switches_Pad_Proxies_Group ( )
{
	clear_proxies();
}


void
Switches_Pad_Proxies_Group::set_switches_pad (
	QObject * pad_n )
{
	_switches_pad = pad_n;
}


void
Switches_Pad_Proxies_Group::set_group_index (
	unsigned int idx_n )
{
	_group_index = idx_n;
}


void
Switches_Pad_Proxies_Group::clear_proxies ( )
{
	if ( _proxies.size() > 0 ) {
		for ( int ii=0; ii < _proxies.size(); ++ii ) {
			delete _proxies[ii];
		}
		_proxies.clear();
	}
}


void
Switches_Pad_Proxies_Group::append_proxy (
	Switches_Pad_Proxy * proxy_n )
{
	if ( proxy_n != 0 ) {
		proxy_n->setParent ( this );
		proxy_n->set_proxy_index ( _proxies.size() );
		_proxies.append ( proxy_n );
	}
}


void
Switches_Pad_Proxies_Group::set_is_switch (
	bool flag_n )
{
	_is_switch = flag_n;
}


void
Switches_Pad_Proxies_Group::set_is_enum (
	bool flag_n )
{
	_is_enum = flag_n;
}


bool
Switches_Pad_Proxies_Group::event (
	QEvent * event_n )
{
	Focus_Pass_Event * ev_fp (
		dynamic_cast < Focus_Pass_Event * > ( event_n ) );
	if ( ev_fp != 0 ) {
		_has_focus = ev_fp->gotFocus();
		_focus_proxy = ev_fp->child_idx();
		if ( switches_pad() != 0 ) {
			Focus_Pass_Event ev_pass ( *ev_fp, group_index() );
			QCoreApplication::sendEvent ( switches_pad(), &ev_pass );
		}
		return true;
	}

	return QObject::event ( event_n );
}


} // End of namespace

