//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_switches_pad_proxy_group_hpp__
#define __INC_switches_pad_proxy_group_hpp__

#include <QObject>
#include <QString>
#include <QList>

#include "switches_pad_proxy.hpp"


namespace Wdg
{


///
/// @brief Switches_Pad_Proxies_Group
///
class Switches_Pad_Proxies_Group :
	public QObject
{
	Q_OBJECT


	// Public typedefs
	public:

	typedef QList < Switches_Pad_Proxy * > Proxy_List;


	// Public methods
	public:

	Switches_Pad_Proxies_Group (
		QObject * parent_n = 0 );

	virtual
	~Switches_Pad_Proxies_Group ( );


	// Switches pad

	QObject *
	switches_pad ( ) const;

	void
	set_switches_pad (
		QObject * pad_n );


	// Group index

	unsigned int
	group_index ( ) const;

	void
	set_group_index (
		unsigned int idx_n );


	// Proxies

	unsigned int
	num_proxies ( ) const;

	void
	clear_proxies ( );

	void
	append_proxy (
		Switches_Pad_Proxy * proxy_n );

	Switches_Pad_Proxy *
	proxy (
		int idx_n );


	// Type flags

	bool
	is_switch ( ) const;

	void
	set_is_switch (
		bool flag_n );

	bool
	is_enum ( ) const;

	void
	set_is_enum (
		bool flag_n );


	bool
	is_multichannel ( );

	void
	set_is_multichannel (
		bool flag_n );


	// Group name

	const QString &
	group_name ( ) const;

	void
	set_group_name (
		const QString & name_n );


	// Tool tip

	const QString &
	tool_tip ( ) const;

	void
	set_tool_tip (
		const QString & tip_n );


	// Focus

	bool
	has_focus ( ) const;

	unsigned int
	focus_proxy ( ) const;


	// Event handling

	bool
	event (
		QEvent * event_n );


	// Private attributes
	private:

	QObject * _switches_pad;

	unsigned int _group_index;

	Proxy_List _proxies;

	QString _group_name;

	QString _tool_tip;

	bool _is_switch;

	bool _is_enum;

	bool _has_focus;

	unsigned int _focus_proxy;
};


inline
QObject *
Switches_Pad_Proxies_Group::switches_pad ( ) const
{
	return _switches_pad;
}


inline
unsigned int
Switches_Pad_Proxies_Group::group_index ( ) const
{
	return _group_index;
}


inline
unsigned int
Switches_Pad_Proxies_Group::num_proxies ( ) const
{
	return _proxies.size();
}


inline
Switches_Pad_Proxy *
Switches_Pad_Proxies_Group::proxy (
	int idx_n )
{
	return _proxies[idx_n];
}


inline
const QString &
Switches_Pad_Proxies_Group::group_name ( ) const
{
	return _group_name;
}


inline
void
Switches_Pad_Proxies_Group::set_group_name (
	const QString & name_n )
{
	_group_name = name_n;
}


inline
const QString &
Switches_Pad_Proxies_Group::tool_tip ( ) const
{
	return _tool_tip;
}


inline
void
Switches_Pad_Proxies_Group::set_tool_tip (
	const QString & tip_n )
{
	_tool_tip = tip_n;
}


inline
bool
Switches_Pad_Proxies_Group::is_switch ( ) const
{
	return _is_switch;
}


inline
bool
Switches_Pad_Proxies_Group::is_enum ( ) const
{
	return _is_enum;
}


inline
bool
Switches_Pad_Proxies_Group::is_multichannel ( )
{
	return ( _proxies.size() > 1 );
}


inline
bool
Switches_Pad_Proxies_Group::has_focus ( ) const
{
	return _has_focus;
}


inline
unsigned int
Switches_Pad_Proxies_Group::focus_proxy ( ) const
{
	return _focus_proxy;
}


typedef QList < Switches_Pad_Proxies_Group * > Switches_Pad_Proxies_Group_List;


} // End of namespace


#endif


