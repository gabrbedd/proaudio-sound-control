//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "switches_pad_proxy.hpp"
#include <QFocusEvent>
#include <QCoreApplication>
#include "pass_events.hpp"


namespace Wdg
{


//
// Switches_Pad_Proxy_Style
//

Switches_Pad_Proxy_Style::Switches_Pad_Proxy_Style (
	unsigned int style_id_n ) :
style_id ( style_id_n )
{
}


//
// Switches_Pad_Proxy
//

Switches_Pad_Proxy::Switches_Pad_Proxy (
	QObject * parent_n ) :
QObject ( parent_n ),
_proxy_index ( 0 ),
_style_info ( 0 ),
_has_focus ( false )
{
}


Switches_Pad_Proxy::~Switches_Pad_Proxy ( )
{
	if ( _style_info != 0 ) {
		delete _style_info;
	}
}


void
Switches_Pad_Proxy::set_proxy_index (
	unsigned int idx_n )
{
	_proxy_index = idx_n;
}


void
Switches_Pad_Proxy::set_item_name (
	const QString & name_n )
{
	_item_name = name_n;
}


void
Switches_Pad_Proxy::set_group_name (
	const QString & name_n )
{
	_group_name = name_n;
}


void
Switches_Pad_Proxy::set_tool_tip (
	const QString & tip_n )
{
	_tool_tip = tip_n;
}


void
Switches_Pad_Proxy::set_style_info (
	Switches_Pad_Proxy_Style * style_n )
{
	if ( _style_info != 0 ) {
		delete _style_info;
	}
	_style_info = style_n;
}


bool
Switches_Pad_Proxy::eventFilter (
	QObject * obj_n,
	QEvent * event_n )
{
	bool res ( QObject::eventFilter ( obj_n, event_n ) );

	if ( !res ) {
		QFocusEvent * fev (
			dynamic_cast < QFocusEvent * > ( event_n ) );
		if ( fev != 0 ) {
			_has_focus = fev->gotFocus();
			//std::cout << "Switches_Pad_Proxy::eventFilter: QFocusEvent " << _has_focus  << "\n";
			if ( parent() != 0 ) {
				Focus_Pass_Event ev_pass ( *fev, proxy_index() );
				QCoreApplication::sendEvent ( parent(), &ev_pass );
			}
		}
	}

	return res;
}


} // End of namespace

