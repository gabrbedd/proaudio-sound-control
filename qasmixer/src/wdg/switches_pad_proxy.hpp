//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef __INC_switches_pad_proxy_hpp__
#define __INC_switches_pad_proxy_hpp__

#include <QObject>
#include <QString>


namespace Wdg
{


///
/// @brief Switches_Pad_Proxy_Style
///
class Switches_Pad_Proxy_Style
{
	// Public methods
	public:

	Switches_Pad_Proxy_Style (
		unsigned int style_id_n = 0 );

	// Public attributes
	unsigned int style_id;
};


///
/// @brief Switches_Pad_Proxy
///
class Switches_Pad_Proxy :
	public QObject
{
	Q_OBJECT

	// Public methods
	public:

	Switches_Pad_Proxy (
		QObject * parent_n = 0 );

	virtual
	~Switches_Pad_Proxy ( );


	// Proxy index

	unsigned int
	proxy_index ( ) const;

	void
	set_proxy_index (
		unsigned int idx_n );


	// Item name

	const QString &
	item_name ( ) const;

	void
	set_item_name (
		const QString & name_n );


	// Group name

	const QString &
	group_name ( ) const;

	void
	set_group_name (
		const QString & name_n );


	// Tool tip

	const QString &
	tool_tip ( ) const;

	void
	set_tool_tip (
		const QString & tip_n );


	// Style informations

	void
	set_style_info (
		Switches_Pad_Proxy_Style * style_n );

	const Switches_Pad_Proxy_Style *
	style_info ( ) const;


	// Focus

	bool
	has_focus ( ) const;


	// Protected methods
	protected:

	bool
	eventFilter (
		QObject * obj_n,
		QEvent * event_n );


	// Private attributes
	private:

	unsigned int _proxy_index;

	QString _item_name;

	QString _group_name;

	QString _tool_tip;

	Switches_Pad_Proxy_Style * _style_info;

	bool _has_focus;
};


inline
unsigned int
Switches_Pad_Proxy::proxy_index ( ) const
{
	return _proxy_index;
}


inline
const QString &
Switches_Pad_Proxy::item_name ( ) const
{
	return _item_name;
}


inline
const QString &
Switches_Pad_Proxy::group_name ( ) const
{
	return _group_name;
}


inline
const QString &
Switches_Pad_Proxy::tool_tip ( ) const
{
	return _tool_tip;
}


inline
const Switches_Pad_Proxy_Style *
Switches_Pad_Proxy::style_info ( ) const
{
	return _style_info;
}


inline
bool
Switches_Pad_Proxy::has_focus ( ) const
{
	return _has_focus;
}


} // End of namespace


#endif
