//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "switches_pad_proxy_enum.hpp"


namespace Wdg
{


//
// Switches_Pad_Proxy_Enum
//

Switches_Pad_Proxy_Enum::Switches_Pad_Proxy_Enum (
	QObject * parent_n ) :
Switches_Pad_Proxy ( parent_n ),
_enum_num_items ( 0 ),
_enum_index ( 0 )
{
}


void
Switches_Pad_Proxy_Enum::set_enum_num_items (
	int num_n )
{
	_enum_num_items = num_n;
}


QString
Switches_Pad_Proxy_Enum::enum_item_name (
	int )
{
	// Dummy implementation
	return QString();
}


void
Switches_Pad_Proxy_Enum::set_enum_index (
	int idx_n )
{
	if ( idx_n != enum_index() ) {
		_enum_index = idx_n;
		this->enum_index_changed();
		emit sig_enum_index_changed ( enum_index() );
	}
}


void
Switches_Pad_Proxy_Enum::enum_index_changed ( )
{
	// Dummy implementation
}


} // End of namespace

