//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_switches_pad_proxy_enum_hpp__
#define __INC_switches_pad_proxy_enum_hpp__

#include "switches_pad_proxy.hpp"


namespace Wdg
{


///
/// @brief Switches_Pad_Proxy_Enum
///
class Switches_Pad_Proxy_Enum :
	public Switches_Pad_Proxy
{
	Q_OBJECT

	// Public methods
	public:

	Switches_Pad_Proxy_Enum (
		QObject * parent_n = 0 );


	int
	enum_num_items ( ) const;

	void
	set_enum_num_items (
		int num_n );

	virtual
	QString
	enum_item_name (
		int idx_n );

	int
	enum_index ( ) const;


	// Public slots
	public slots:

	void
	set_enum_index (
		int idx_n );


	// Signals
	signals:

	void
	sig_enum_index_changed (
		int value_n );


	// Protected methods
	protected:

	virtual
	void
	enum_index_changed ( );


	// Private attributes
	private:

	int _enum_num_items;

	int _enum_index;
};


inline
int
Switches_Pad_Proxy_Enum::enum_num_items ( ) const
{
	return _enum_num_items;
}


inline
int
Switches_Pad_Proxy_Enum::enum_index ( ) const
{
	return _enum_index;
}


} // End of namespace


#endif
