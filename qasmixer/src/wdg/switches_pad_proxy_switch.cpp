//
// C++ Implementation:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "switches_pad_proxy_switch.hpp"


namespace Wdg
{


//
// Switches_Pad_Proxy_Switch
//

Switches_Pad_Proxy_Switch::Switches_Pad_Proxy_Switch (
	QObject * parent_n ) :
Switches_Pad_Proxy ( parent_n ),
_switch_state ( false )
{
}


void
Switches_Pad_Proxy_Switch::set_switch_state (
	bool flag_n )
{
	if ( flag_n != switch_state() ) {
		_switch_state = flag_n;
		this->switch_state_changed();
		emit sig_switch_state_changed ( switch_state() );
	}
}


void
Switches_Pad_Proxy_Switch::switch_state_changed ( )
{
	// Dummy implementation
}


} // End of namespace
