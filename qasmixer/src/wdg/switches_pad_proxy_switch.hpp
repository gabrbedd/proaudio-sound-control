//
// C++ Interface:
//
// Description:
//
//
// Author: Sebastian Holtermann <sebholt@xwmw.org>, (C) 2010-2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef __INC_switches_pad_proxy_switch_hpp__
#define __INC_switches_pad_proxy_switch_hpp__

#include "switches_pad_proxy.hpp"


namespace Wdg
{


///
/// @brief Switches_Pad_Proxy_Switch
///
class Switches_Pad_Proxy_Switch :
	public Switches_Pad_Proxy
{
	Q_OBJECT

	// Public methods
	public:

	Switches_Pad_Proxy_Switch (
		QObject * parent_n = 0 );


	bool
	switch_state ( ) const;


	// Public slots
	public slots:

	void
	set_switch_state (
		bool flag_n );


	// Signals
	signals:

	void
	sig_switch_state_changed (
		bool state_n );


	// Protected methods
	protected:

	virtual
	void
	switch_state_changed ( );


	// Private attributes
	private:

	bool _switch_state;
};


inline
bool
Switches_Pad_Proxy_Switch::switch_state ( ) const
{
	return _switch_state;
}


} // End of namespace


#endif
