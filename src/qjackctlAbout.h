// qjackctlAbout.h
//
/****************************************************************************
   Copyright (C) 2003-2010, rncbc aka Rui Nuno Capela. All rights reserved.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*****************************************************************************/

#ifndef __qjackctlAbout_h
#define __qjackctlAbout_h

#include "config.h"

#define QJACKCTL_TITLE      PACKAGE_NAME
#define QJACKCTL_VERSION    PACKAGE_VERSION

#define QJACKCTL_SUBTITLE0  "Pro-Audio"
#define QJACKCTL_SUBTITLE1  QJACKCTL_SUBTITLE0 " Sound Control"
#define QJACKCTL_SUBTITLE2  "JACK and ALSA interface"

#define QJACKCTL_SUBTITLE   QJACKCTL_SUBTITLE1 " - " QJACKCTL_SUBTITLE2
#define QJACKCTL_WEBSITE    "http://gitorious.org/gabrbedd/proaudio-sound-control"
#define QJACKCTL_COPYRIGHT  "Copyright (C) 2003-2010, rncbc aka Rui Nuno Capela. All rights reserved. Copyright 2010-2011 Sebastian Holtermann <sebholt@xwmw.org>. Copyright 2011 Gabriel M. Beddingfield. Copyright 2011 Trinity Audio Group."

#define QJACKCTL_DOMAIN     "gabrbedd.gitorious.org"

#endif  // __qjackctlAbout_h

// end of qjackctlAbout.h
