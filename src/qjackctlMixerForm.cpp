// qjackctlMixerForm.cpp
//
/****************************************************************************
   Copyright 2011 Gabriel M. Beddingfield <gabriel@teuton.org>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*****************************************************************************/

#include "qjackctlMixerForm.h"

#include <QDebug>
#include <QHBoxLayout>

#include "qjackctlSetup.h"

///////// QASMIXER CLASSES ///////////
#include "main_mixer_window.hpp"
//////////////////////////////////////

#include "qjackctlMixerFormPrivate.h"

//----------------------------------------------------------------------------
// qjackctlMixerForm -- UI wrapper form.

qjackctlMixerForm::qjackctlMixerForm(QWidget *pParent)
    : QWidget(pParent),
      d(new qjackctlMixerFormPrivate(this))
{
    QHBoxLayout *lay =  new QHBoxLayout;
    lay->addWidget(d);
    setLayout(lay);
}


// Destructor.
qjackctlMixerForm::~qjackctlMixerForm (void)
{
}


// Set reference to global options, mostly needed for the
// initial sizes of the main splitter views and those
// client/port aliasing feature.
void qjackctlMixerForm::setup ( qjackctlSetup *pSetup )
{
    d->setup(pSetup);
}

qjackctlMixerFormPrivate::qjackctlMixerFormPrivate(QWidget *parent)
    : QWidget(parent),
      m_pSetup(0),
      m_pMixer(0)
{
    QHBoxLayout *lay = new QHBoxLayout;

    m_pMixer = new Main_Mixer_Window(this, 0);
    lay->addWidget(m_pMixer);
    setLayout(lay);

    m_pMixer->enter_setup_stage();
    m_pMixer->select_default_ctl();
    m_pMixer->select_view_type(0);
    m_pMixer->finish_setup_stage();

}

qjackctlMixerFormPrivate::~qjackctlMixerFormPrivate()
{
}

void qjackctlMixerFormPrivate::setup(qjackctlSetup* pSetup)
{
    m_pSetup = pSetup;
}

// end of qjackctlMixerForm.cpp
