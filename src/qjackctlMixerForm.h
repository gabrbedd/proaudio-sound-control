// qjackctlMixerForm.h
//
/****************************************************************************
   Copyright 2011, Gabriel M. Beddingfield <gabriel@teuton.org>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*****************************************************************************/

#ifndef __qjackctlMixerForm_h
#define __qjackctlMixerForm_h

#include <QWidget>

// Forward declarations.
class qjackctlSetup;
class qjackctlMixerFormPrivate;

//----------------------------------------------------------------------------
// qjackctlMixerForm -- UI wrapper form.

class qjackctlMixerForm : public QWidget
{
public:
	qjackctlMixerForm(QWidget *pParent = 0);
	~qjackctlMixerForm();

	void setup(qjackctlSetup *pSetup);

private:
	qjackctlMixerFormPrivate * const d;
};


#endif	// __qjackctlMixerForm_h


// end of qjackctlMixerForm.h
