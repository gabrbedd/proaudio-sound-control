// qjackctlMixerFormPrivate.h
//
/****************************************************************************
   Copyright 2011, Gabriel M. Beddingfield <gabriel@teuton.org>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*****************************************************************************/

#ifndef __qjackctlMixerFormPrivate_h
#define __qjackctlMixerFormPrivate_h

#include <QObject>
#include <QWidget>
#include <memory>

// Forward declarations from QJackCtl
class qjackctlSetup;

// Forward declarations from QasMixer
class Main_Mixer_Window;

namespace QSnd
{
	class Mixer_Style;
}

//----------------------------------------------------------------------------
// qjackctlMixerFormPrivate -- UI wrapper form.

class qjackctlMixerFormPrivate : public QWidget
{
public:
	qjackctlMixerFormPrivate(QWidget *pParent = 0);
	~qjackctlMixerFormPrivate();

	void setup(qjackctlSetup *pSetup);

private:
	qjackctlSetup *m_pSetup;

	Main_Mixer_Window *m_pMixer;
};


#endif	// __qjackctlMixerFormPrivate_h


// end of qjackctlMixerFormPrivate.h
