<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs_CZ">
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="38"/>
        <source>(default)</source>
        <translation>(výchozí)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="435"/>
        <source>Usage: %1 [options] [command-and-args]</source>
        <translation>Použití: %1 [volby] [příkazy a argumenty]</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="438"/>
        <source>Options:</source>
        <translation>Volby:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="440"/>
        <source>Start JACK audio server immediately</source>
        <translation>JACK audioserver spustit okamžitě</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="448"/>
        <source>Show help about command line options</source>
        <translation>Ukázat nápovědu k volbám (argumentům) příkazového řádku</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="450"/>
        <source>Show version information</source>
        <translation>Ukázat informace o verzi</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="486"/>
        <source>Option -p requires an argument (preset).</source>
        <translation>Volba -p vyžaduje argument (preset, přednastavení).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="517"/>
        <source>Qt: %1
</source>
        <translation>Qt: %1</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="442"/>
        <source>Set default settings preset name</source>
        <translation>Určit název přednastavení výchozího nastavení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="444"/>
        <source>Set active patchbay definition file</source>
        <translation>Nastavit soubor s vymezením činné zapojovací desky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="495"/>
        <source>Option -a requires an argument (path).</source>
        <translation>Volba -a vyžaduje argument (cesta).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="446"/>
        <source>Set default JACK audio server name</source>
        <translation>Nastavit výchozí název pro zvukový server JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="505"/>
        <source>Option -n requires an argument (name).</source>
        <translation>Volba -n vyžaduje argument (název).</translation>
    </message>
</context>
<context>
    <name>qjackctlAboutForm</name>
    <message>
        <location filename="../src/qjackctlAboutForm.ui" line="36"/>
        <source>About QjackCtl</source>
        <translation>O QjackCtl</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.ui" line="67"/>
        <source>&amp;Close</source>
        <translation>&amp;Zavřít</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.ui" line="70"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.ui" line="80"/>
        <source>About Qt</source>
        <translation>O Qt</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="43"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="44"/>
        <source>Build</source>
        <translation>Sestavení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="47"/>
        <source>Debugging option enabled.</source>
        <translation>Povolena volba pro ladění.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="53"/>
        <source>System tray disabled.</source>
        <translation>Zakázána oznamovací oblast.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="59"/>
        <source>Transport status control disabled.</source>
        <translation>Zakázáno ovládání stavu předání.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="65"/>
        <source>Realtime status disabled.</source>
        <translation>Zakázán stav provádění ve skutečném čase.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="71"/>
        <source>XRUN delay status disabled.</source>
        <translation>Zakázán stav zpoždění XRUN.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="77"/>
        <source>Maximum delay status disabled.</source>
        <translation>Zakázán stav největšího zpoždění.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="83"/>
        <source>JACK MIDI support disabled.</source>
        <translation>JACK MIDI není podporován.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="96"/>
        <source>ALSA/MIDI sequencer support disabled.</source>
        <translation>ALSA/MIDI řadič (sequencer) není podporován.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="110"/>
        <source>Website</source>
        <translation>Stránky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="115"/>
        <source>This program is free software; you can redistribute it and/or modify it</source>
        <translation>Tento program je svobodným programem. Můžete jej šířit a/nebo upravit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="116"/>
        <source>under the terms of the GNU General Public License version 2 or later.</source>
        <translation>za podmínek GNU General Public License ve verzi 2 nebo pozdější.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="89"/>
        <source>JACK Port aliases support disabled.</source>
        <translation>Podpora pro přezdívky přípojek JACK zakázána.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="104"/>
        <source>D-Bus interface support disabled.</source>
        <translation>Podpora pro rozhraní D-Bus zakázána.</translation>
    </message>
</context>
<context>
    <name>qjackctlClientListView</name>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="664"/>
        <source>Readable Clients / Output Ports</source>
        <translation>Čitelné přípojky pro klienty/výstupy</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="666"/>
        <source>Writable Clients / Input Ports</source>
        <translation>Zapisovatelné přípojky pro klienty/výstupy</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="986"/>
        <source>&amp;Connect</source>
        <translation>&amp;Spojit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="987"/>
        <source>Alt+C</source>
        <comment>Connect</comment>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="990"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Rozpojit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="991"/>
        <source>Alt+D</source>
        <comment>Disconnect</comment>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="994"/>
        <source>Disconnect &amp;All</source>
        <translation>&amp;Rozpojit vše</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="995"/>
        <source>Alt+A</source>
        <comment>Disconect All</comment>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1000"/>
        <source>Re&amp;name</source>
        <translation>&amp;Přejmenovat</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1001"/>
        <source>Alt+N</source>
        <comment>Rename</comment>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1007"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Obnovit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1008"/>
        <source>Alt+R</source>
        <comment>Refresh</comment>
        <translation>Alt+R</translation>
    </message>
</context>
<context>
    <name>qjackctlConnect</name>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1756"/>
        <source>Warning</source>
        <translation>Varování</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1757"/>
        <source>This will suspend sound processing
from all client applications.

Are you sure?</source>
        <translation>Tímto se pozastaví zpracování
zvuku u všech klientských aplikací.

Jste si jistý?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Ano</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Ne</translation>
    </message>
</context>
<context>
    <name>qjackctlConnectionsForm</name>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="44"/>
        <source>Connections - JACK Audio Connection Kit</source>
        <translation>Spojení - JACK Audio Connection Kit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="63"/>
        <source>Audio</source>
        <translation>Zvuk</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="98"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="216"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="334"/>
        <source>Connect currently selected ports</source>
        <translation>Spojit nyní vybrané přípojky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="101"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="219"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="337"/>
        <source>&amp;Connect</source>
        <translation>&amp;Spojit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="107"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="225"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="343"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="114"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="232"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="350"/>
        <source>Disconnect currently selected ports</source>
        <translation>Rozpojit nyní vybrané přípojky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="117"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="235"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="353"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Rozpojit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="123"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="241"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="359"/>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="130"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="248"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="366"/>
        <source>Disconnect all currently connected ports</source>
        <translation>Rozpojit všechny nyní spojené přípojky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="133"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="251"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="369"/>
        <source>Disconnect &amp;All</source>
        <translation>&amp;Rozpojit vše</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="139"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="257"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="375"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="162"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="280"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="398"/>
        <source>Refresh current connections view</source>
        <translation>Obnovit pohled na nynější spojení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="165"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="283"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="401"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Obnovit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="171"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="289"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="407"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="181"/>
        <source>MIDI</source>
        <translation>JACK-MIDI</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="299"/>
        <source>ALSA</source>
        <translation>ALSA-MIDI</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.cpp" line="239"/>
        <source>Warning</source>
        <translation>Varování</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.cpp" line="240"/>
        <source>The preset aliases have been changed:

&quot;%1&quot;

Do you want to save the changes?</source>
        <translation>Přezdívky pro přednastavení byly změněny:
&quot;%1&quot;

Chcete uložit změny?</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Uložit</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="obsolete">Odmítnout</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Zrušit</translation>
    </message>
</context>
<context>
    <name>qjackctlConnectorView</name>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1173"/>
        <source>&amp;Connect</source>
        <translation>&amp;Spojit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1174"/>
        <source>Alt+C</source>
        <comment>Connect</comment>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1177"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Rozpojit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1178"/>
        <source>Alt+D</source>
        <comment>Disconnect</comment>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1181"/>
        <source>Disconnect &amp;All</source>
        <translation>Rozpojit &amp;vše</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1182"/>
        <source>Alt+A</source>
        <comment>Disconect All</comment>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1187"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Obnovit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1188"/>
        <source>Alt+R</source>
        <comment>Refresh</comment>
        <translation>Alt+R</translation>
    </message>
</context>
<context>
    <name>qjackctlMainForm</name>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="49"/>
        <source>QjackCtl</source>
        <translation>QjackCtl</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="385"/>
        <source>Quit processing and exit</source>
        <translation>Ukončit zpracování signálu a ukončit program</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="388"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3169"/>
        <source>&amp;Quit</source>
        <translation>&amp;Ukončit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="395"/>
        <source>Alt+Q</source>
        <translation>ALT+Q</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="76"/>
        <source>Start the JACK server</source>
        <translation>Spustit server JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="79"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3087"/>
        <source>&amp;Start</source>
        <translation>&amp;Spustit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="86"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="114"/>
        <source>Stop the JACK server</source>
        <translation>Zastavit server JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="117"/>
        <source>S&amp;top</source>
        <translation>&amp;Zastavit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="124"/>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="423"/>
        <source>Levels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="445"/>
        <source>Show/hide the extended status window</source>
        <translation>Ukázat/skrýt rozšířené stavové okno</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="448"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3128"/>
        <source>St&amp;atus</source>
        <translation>&amp;Stav</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="350"/>
        <source>Show information about this application</source>
        <translation>Ukázat informace o této aplikaci</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="353"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3164"/>
        <source>Ab&amp;out...</source>
        <translation>&amp;O...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="204"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="610"/>
        <source>Show settings and options dialog</source>
        <translation>Ukázat dialogové okno pro nastavení a volby</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="613"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3160"/>
        <source>S&amp;etup...</source>
        <translation>&amp;Nastavení...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="620"/>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="559"/>
        <source>Show/hide the messages log window</source>
        <translation>Ukázat/skrýt okno se zápisy pro hlášení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="562"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3124"/>
        <source>&amp;Messages</source>
        <translation>&amp;Hlášení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="569"/>
        <source>Alt+M</source>
        <translation>Alt+M</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="521"/>
        <source>Show/hide the patchbay editor window</source>
        <translation>Ukázat/skrýt okno editoru se zapojovací deskou</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="524"/>
        <source>&amp;Patchbay</source>
        <translation>&amp;Zapojovací deska</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="483"/>
        <source>Show/hide the actual connections patchbay window</source>
        <translation>Ukázat/skrýt okno se zapojovací deskou se stávajícími spojeními</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="486"/>
        <source>&amp;Connect</source>
        <translation>&amp;Spojit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="493"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>JACK server state</source>
        <translation type="obsolete">Stav serveru JACK</translation>
    </message>
    <message>
        <source>JACK server mode</source>
        <translation type="obsolete">Režim serveru JACK</translation>
    </message>
    <message>
        <source>DSP Load</source>
        <translation type="obsolete">Zatížení DSP</translation>
    </message>
    <message>
        <source>Sample rate</source>
        <translation type="obsolete">Vzorkovací kmitočet</translation>
    </message>
    <message>
        <source>XRUN Count (notifications)</source>
        <translation type="obsolete">Počet XRUN (oznámení)</translation>
    </message>
    <message>
        <source>Time display</source>
        <translation type="obsolete">Údaj o čase</translation>
    </message>
    <message>
        <source>Transport state</source>
        <translation type="obsolete">Stav předání</translation>
    </message>
    <message>
        <source>Transport BPM</source>
        <translation type="obsolete">Předání BPM</translation>
    </message>
    <message>
        <source>Transport time</source>
        <translation type="obsolete">Čas předání</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="194"/>
        <source>Backward transport</source>
        <translation>Předání zpět</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="197"/>
        <source>&amp;Backward</source>
        <translation>&amp;Zpět</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="302"/>
        <source>Forward transport</source>
        <translation>Předání dopředu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="305"/>
        <source>&amp;Forward</source>
        <translation>&amp;Dopředu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="312"/>
        <source>Alt+F</source>
        <translation>Alt+F</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="162"/>
        <source>Rewind transport</source>
        <translation>Předání přetočit zpět</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="165"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3143"/>
        <source>&amp;Rewind</source>
        <translation>&amp;Přetočit zpět</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="172"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="270"/>
        <source>Stop transport rolling</source>
        <translation>Zastavit chod předání</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="273"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3152"/>
        <source>Pa&amp;use</source>
        <translation>&amp;Pozastavit</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+U</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="229"/>
        <source>Start transport rolling</source>
        <translation>Spustit chod předání</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="232"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3149"/>
        <source>&amp;Play</source>
        <translation>&amp;Přehrát</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="531"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="597"/>
        <source>Could not open ALSA sequencer as a client.

ALSA MIDI patchbay will be not available.</source>
        <translation>Nepodařilo se otevřít řadič (sekvencer) ALSA jako klienta.

Zapojovací deska ALSA MIDI nebude dostupná.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="649"/>
        <source>D-BUS: Service is available (%1 aka jackdbus).</source>
        <translation>D-BUS: Služba je dostupná (%1 aka jackdbus).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="680"/>
        <source>D-BUS: Service not available (%1 aka jackdbus).</source>
        <translation>D-BUS: Služba není dostupná (%1 aka jackdbus).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="728"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3207"/>
        <source>Information</source>
        <translation>Informace</translation>
    </message>
    <message>
        <source>The program will keep running in the system tray.

To terminate the program, please choose &quot;Quit&quot; in the context menu of the system tray entry.</source>
        <translation type="obsolete">Program poběží dále viditelný jako symbol v oznamovací části panelu.

Pro ukončení programu zvolte, prosím, &quot;Ukončit&quot; v nabídce souvisejících
kroků u symbolu v oznamovací části panelu.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="752"/>
        <location filename="../src/qjackctlMainForm.cpp" line="903"/>
        <location filename="../src/qjackctlMainForm.cpp" line="1216"/>
        <location filename="../src/qjackctlMainForm.cpp" line="2261"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3182"/>
        <source>Warning</source>
        <translation>Varování</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="753"/>
        <source>JACK is currently running.

Do you want to terminate the JACK audio server?</source>
        <translation>Server JACK nyní běží.

Chcete ukončit zvukový server JACK?</translation>
    </message>
    <message>
        <source>Terminate</source>
        <translation type="obsolete">Ukončit</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Zrušit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="856"/>
        <source>successfully</source>
        <translation>úspěšně</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="858"/>
        <source>with exit status=%1</source>
        <translation>s vrácenou hodnotou = %1</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="904"/>
        <source>Could not start JACK.

Maybe JACK audio server is already started.</source>
        <translation>JACK se nepodařilo spustit.

Možná je server JACK už spuštěn.</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="obsolete">Zastavit</translation>
    </message>
    <message>
        <source>Kill</source>
        <translation type="obsolete">Ukončit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="938"/>
        <source>Could not load preset &quot;%1&quot;.

Retrying with default.</source>
        <translation>Nastavení &quot;%1&quot; se nepodařilo nahrát.

Zkouší se znovu s výchozím přednastavením.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="941"/>
        <source>Could not load default preset.

Sorry.</source>
        <translation>Nepodařilo se nahrát výchozí přednastavení.

Promiňte.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="951"/>
        <source>Startup script...</source>
        <translation>Skript pro spuštění...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="952"/>
        <source>Startup script terminated</source>
        <translation>Skript pro spuštění ukončen</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1150"/>
        <source>D-BUS: JACK server is starting...</source>
        <translation>D-BUS: Spouští se server JACK...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1153"/>
        <source>D-BUS: JACK server could not be started.

Sorry</source>
        <translation>D-BUS: Server JACK se nepodařilo spustit.

Promiňte</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1190"/>
        <source>JACK is starting...</source>
        <translation>JACK se spouští...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1217"/>
        <source>Some client audio applications
are still active and connected.

Do you want to stop the JACK audio server?</source>
        <translation>Některé z klientských zvukových aplikací
jsou stále činné a spojené.

Chcete zastavit zvukový server JACK?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1259"/>
        <source>JACK is stopping...</source>
        <translation>JACK se zastavuje...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1252"/>
        <source>Shutdown script...</source>
        <translation>Skript pro zastavení chodu...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1253"/>
        <source>Shutdown script terminated</source>
        <translation>Skript pro zastavení chodu ukončen</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1274"/>
        <source>D-BUS: JACK server is stopping...</source>
        <translation>D-BUS: Zastavuje se server JACK...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1277"/>
        <source>D-BUS: JACK server could not be stopped.

Sorry</source>
        <translation>D-BUS: Server JACK se nepodařilo zastavit.

Promiňte</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1293"/>
        <location filename="../src/qjackctlMainForm.cpp" line="1422"/>
        <source>Post-shutdown script...</source>
        <translation>Skript pro po-zastavení chodu...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1294"/>
        <location filename="../src/qjackctlMainForm.cpp" line="1423"/>
        <source>Post-shutdown script terminated</source>
        <translation>Skript pro po-zastavení chodu ukončen</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1343"/>
        <source>JACK was started with PID=%1.</source>
        <translation>JACK byl puštěn s PID = %1.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1351"/>
        <source>D-BUS: JACK server was started (%1 aka jackdbus).</source>
        <translation>D-BUS: Server JACK byl spuštěn (%1 aka jackdbus).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1390"/>
        <source>JACK is being forced...</source>
        <translation>JACK je nucen...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1397"/>
        <source>JACK was stopped</source>
        <translation>JACK byl zastaven</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1410"/>
        <source>D-BUS: JACK server was stopped (%1 aka jackdbus).</source>
        <translation>D-BUS: Server JACK byl zastaven (%1 aka jackdbus).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1520"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1634"/>
        <source>Transport BBT (bar:beat.ticks)</source>
        <translation>Předání BBT (takt:doba.tiknutí - bar:beat.ticks)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1635"/>
        <source>Transport time code</source>
        <translation>Předání časového kódu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1646"/>
        <source>Elapsed time since last reset</source>
        <translation>Čas uplynulý od posledního znovunastavení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1649"/>
        <source>Elapsed time since last XRUN</source>
        <translation>Čas uplynulý od posledního  XRUN</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1770"/>
        <source>Could not load active patchbay definition.

Disabled.</source>
        <translation>Nepodařilo se nahrát vymezení aktivní zapojovací desky.

Zakázáno.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1773"/>
        <source>Patchbay activated.</source>
        <translation>Zapojovací deska uvedena do chodu.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1781"/>
        <source>Patchbay deactivated.</source>
        <translation>Zapojovací deska vyřazena z provozu.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1859"/>
        <source>Statistics reset.</source>
        <translation>Nastavit znovu statistiku.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1998"/>
        <source>msec</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2015"/>
        <source>JACK connection graph change.</source>
        <translation>Nákres spojení JACK změněn.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2042"/>
        <source>XRUN callback (%1).</source>
        <translation>Zavolání nazpátek XRUN (%1).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2052"/>
        <source>Buffer size change (%1).</source>
        <translation>Velikost vyrovnávací paměti změněna (%1).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2061"/>
        <source>Shutdown notification.</source>
        <translation>Oznámení o zastavení.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2081"/>
        <source>Could not start JACK.

Sorry.</source>
        <translation>Nepodařilo se spustit JACK.

Promiňte.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2085"/>
        <source>JACK has crashed.</source>
        <translation>JACK spadl.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2088"/>
        <source>JACK timed out.</source>
        <translation>Překročení času u JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2091"/>
        <source>JACK write error.</source>
        <translation>Chyba při psaní u JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2094"/>
        <source>JACK read error.</source>
        <translation>Chyba při čtení u JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2098"/>
        <source>Unknown JACK error (%d).</source>
        <translation>Neznámá chyba u JACK (%d).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2115"/>
        <source>ALSA connection graph change.</source>
        <translation>Nákres spojení ALSA změněn.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2146"/>
        <source>JACK active patchbay scan</source>
        <translation>Prohlédnutí činné zapojovací desky JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2156"/>
        <source>ALSA active patchbay scan</source>
        <translation>Prohlédnutí činné zapojovací desky ALSA</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2202"/>
        <source>JACK connection change.</source>
        <translation>Spojení JACK změněno.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2211"/>
        <source>ALSA connection change.</source>
        <translation>Spojení ALSA změněno.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2230"/>
        <source>checked</source>
        <translation>přezkoušeno</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2234"/>
        <source>connected</source>
        <translation>spojeno</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2238"/>
        <source>disconnected</source>
        <translation>rozpojeno</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2243"/>
        <source>failed</source>
        <translation>nepodařilo se</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2262"/>
        <source>A patchbay definition is currently active,
which is probable to redo this connection:

%1 -&gt; %2

Do you want to remove the patchbay connection?</source>
        <translation>Vymezení zapojovací desky je nyní aktivní,
což pravděpodobně znamená předělat toto spojení:

%1 -&gt; %2

Chcete odstranit spojení zapojovací desky?</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="obsolete">Odstranit</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation type="obsolete">Přehlížet</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="730"/>
        <source>The program will keep running in the system tray.

To terminate the program, please choose &quot;Quit&quot;
in the context menu of the system tray icon.</source>
        <translation>Program poběží dále v oznamovací části panelu.

Pro ukončení programu vyberte, prosím, &quot;Ukončit&quot;
v související nabídce vyskakující z ikony v oznamovací
části panelu.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2339"/>
        <source>Overall operation failed.</source>
        <translation>Celková operace se nezdařila.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2341"/>
        <source>Invalid or unsupported option.</source>
        <translation>Neplatná nebo nepodporovaná volba.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2343"/>
        <source>Client name not unique.</source>
        <translation>Název klienta není jedinečný.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2345"/>
        <source>Server is started.</source>
        <translation>Server je spuštěn.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2347"/>
        <source>Unable to connect to server.</source>
        <translation>Nelze se připojit k serveru.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2349"/>
        <source>Server communication error.</source>
        <translation>Chyba spojení se serverem.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2351"/>
        <source>Client does not exist.</source>
        <translation>Klient neexistuje.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2353"/>
        <source>Unable to load internal client.</source>
        <translation>Nepodařilo se nahrát vnitřního klienta.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2355"/>
        <source>Unable to initialize client.</source>
        <translation>Nepodařilo se inicializovat vnitřního klienta.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2357"/>
        <source>Unable to access shared memory.</source>
        <translation>Nepodařilo se přistoupit ke sdílené paměti.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2359"/>
        <source>Client protocol version mismatch.</source>
        <translation>Nevhodná verze klientského protokolu.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2361"/>
        <source>Could not connect to JACK server as client.
- %1
Please check the messages window for more info.</source>
        <translation>Nepodařilo se připojit k serveru JACK jako klient. - %1
Další informace hledejte, prosím, v okně s hlášením.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2404"/>
        <source>Server configuration saved to &quot;%1&quot;.</source>
        <translation>Nastavení serveru uloženo do &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2426"/>
        <source>Client activated.</source>
        <translation>Klient uveden do chodu.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2434"/>
        <source>Post-startup script...</source>
        <translation>Skript pro po-spuštění...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2435"/>
        <source>Post-startup script terminated</source>
        <translation>Skript pro po-spuštění ukončen</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2443"/>
        <source>Command line argument...</source>
        <translation>Argument pro příkazový řádek...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2444"/>
        <source>Command line argument started</source>
        <translation>Argument pro příkazový řádek spuštěn</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2469"/>
        <source>Client deactivated.</source>
        <translation>Klient vyřazen z provozu.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2700"/>
        <source>Transport rewind.</source>
        <translation>Předání přetočit zpět.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2720"/>
        <source>Transport backward.</source>
        <translation>Předání zpět.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2745"/>
        <location filename="../src/qjackctlMainForm.cpp" line="2852"/>
        <location filename="../src/qjackctlMainForm.cpp" line="2957"/>
        <source>Starting</source>
        <translation>Spouští se</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2747"/>
        <source>Transport start.</source>
        <translation>Spustit předání.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2760"/>
        <location filename="../src/qjackctlMainForm.cpp" line="2964"/>
        <source>Stopping</source>
        <translation>Zastavuje se</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2762"/>
        <source>Transport stop.</source>
        <translation>Zastavit předání.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2782"/>
        <source>Transport forward.</source>
        <translation>Předání dopředu.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2798"/>
        <location filename="../src/qjackctlMainForm.cpp" line="2968"/>
        <source>Stopped</source>
        <translation>Zastaveno</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2833"/>
        <source>%1 (%2%)</source>
        <translation>%1 (%2 %)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2836"/>
        <source>%1 %</source>
        <translation>%1 %</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2838"/>
        <source>%1 Hz</source>
        <translation>%1 Hz</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2840"/>
        <source>%1 frames</source>
        <translation>%1 snímků</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2845"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2845"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <source>RT</source>
        <translation type="obsolete">RT</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2855"/>
        <source>Rolling</source>
        <translation>Jede</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2858"/>
        <source>Looping</source>
        <translation>Zapíná se do jednoduchého obvodu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2895"/>
        <source>%1 msec</source>
        <translation>%1 ms</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2902"/>
        <source>XRUN callback (%1 skipped).</source>
        <translation>Zavolání nazpátek XRUN (%1 přeskočeno).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2961"/>
        <source>Started</source>
        <translation>Běží</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2971"/>
        <source>Active</source>
        <translation>Činný</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2974"/>
        <source>Activating</source>
        <translation>Uvádí se do chodu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2979"/>
        <source>Inactive</source>
        <translation>Nečinný</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3077"/>
        <source>&amp;Hide</source>
        <translation>&amp;Skrýt</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3077"/>
        <source>Mi&amp;nimize</source>
        <translation>&amp;Zmenšit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3079"/>
        <source>S&amp;how</source>
        <translation>&amp;Ukázat</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3079"/>
        <source>Rest&amp;ore</source>
        <translation>&amp;Nahrát znovu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3090"/>
        <source>&amp;Stop</source>
        <translation>&amp;Zastavit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3093"/>
        <source>&amp;Reset</source>
        <translation>&amp;Nastavit znovu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3099"/>
        <source>&amp;Presets</source>
        <translation>&amp;Přednastavení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3132"/>
        <source>&amp;Connections</source>
        <translation>&amp;Spojení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3136"/>
        <source>Patch&amp;bay</source>
        <translation>&amp;Zapojovací deska</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3141"/>
        <source>&amp;Transport</source>
        <translation>&amp;Předání</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3184"/>
        <source>Server settings will be only effective after
restarting the JACK audio server.</source>
        <translation>Nastavení serveru se projeví až po
novém spuštění serveru JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3443"/>
        <source>D-BUS: SetParameterValue(&apos;%1&apos;, &apos;%2&apos;):

%3.
(%4)</source>
        <translation>D-BUS: Nastavit hodnotu parametru
SetParameterValue(&apos;%1&apos;, &apos;%2&apos;):

%3.
(%4)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3474"/>
        <source>D-BUS: ResetParameterValue(&apos;%1&apos;):

%2.
(%3)</source>
        <translation>D-BUS: Nastavit znovu hodnotu parametru
ResetParameterValue(&apos;%1&apos;):

%2.
(%3)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3505"/>
        <source>D-BUS: GetParameterValue(&apos;%1&apos;):

%2.
(%3)</source>
        <translation>D-BUS: Získat hodnotu parametru
GetParameterValue(&apos;%1&apos;):

%2.
(%3)</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3209"/>
        <source>Some settings will be only effective
the next time you start this program.</source>
        <translation>Některá nastavení se projeví až tehdy,
když příště spustíte tento program.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="455"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="360"/>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="280"/>
        <source>Shift+Space</source>
        <translation>Shift+Mezerník</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="239"/>
        <source>Space</source>
        <translation>Mezerník</translation>
    </message>
</context>
<context>
    <name>qjackctlMessagesForm</name>
    <message>
        <location filename="../src/qjackctlMessagesForm.ui" line="42"/>
        <source>Messages - JACK Audio Connection Kit</source>
        <translation>Hlášení - JACK Audio Connection Kit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMessagesForm.ui" line="57"/>
        <source>Messages output log</source>
        <translation>Zápis hlášení o výstupu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMessagesForm.cpp" line="136"/>
        <source>Logging stopped --- %1 ---</source>
        <translation>Zápis zastaven --- %1 ---</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMessagesForm.cpp" line="146"/>
        <source>Logging started --- %1 ---</source>
        <translation>Zápis spuštěn --- %1 ---</translation>
    </message>
</context>
<context>
    <name>qjackctlPatchbay</name>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1738"/>
        <source>Warning</source>
        <translation>Varování</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1739"/>
        <source>This will disconnect all sockets.

Are you sure?</source>
        <translation>Tento krok odpojí všechny zásuvky.

Jste si jistýr?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Ano</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Ne</translation>
    </message>
</context>
<context>
    <name>qjackctlPatchbayForm</name>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="44"/>
        <source>Patchbay - JACK Audio Connection Kit</source>
        <translation>Zapojovací deska - JACK Audio Connection Kit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="105"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="233"/>
        <source>Move currently selected output socket down one position</source>
        <translation>Posunout nyní vybrané výstupní zásuvky dolů o jedno místo</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="108"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="236"/>
        <source>Down</source>
        <translation>Dolů</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="121"/>
        <source>Create a new output socket</source>
        <translation>Vytvořit novou výstupní zásuvku</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="124"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="284"/>
        <source>Add...</source>
        <translation>Přidat...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="153"/>
        <source>Edit currently selected input socket properties</source>
        <translation>Upravit vlastnosti nyní vybrané vstupní zásuvky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="156"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="300"/>
        <source>Edit...</source>
        <translation>Upravit...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="185"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="313"/>
        <source>Move currently selected output socket up one position</source>
        <translation>Posunout nyní vybrané výstupní zásuvky nahoru o jedno místo</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="188"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="316"/>
        <source>Up</source>
        <translation>Nahoru</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="201"/>
        <source>Remove currently selected output socket</source>
        <translation>Odstranit nyní vybranou výstupní zásuvku</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="204"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="252"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="217"/>
        <source>Duplicate (copy) currently selected output socket</source>
        <translation>Zdvojit (kopírovat) nyní vybranou výstupní zásuvku</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="220"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="268"/>
        <source>Copy...</source>
        <translation>Kopírovat...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="249"/>
        <source>Remove currently selected input socket</source>
        <translation>Odstranit nyní vybranou vstupní zásuvku</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="265"/>
        <source>Duplicate (copy) currently selected input socket</source>
        <translation>Zdvojit (kopírovat) nyní vybranou vstupní zásuvku</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="281"/>
        <source>Create a new input socket</source>
        <translation>Vytvořit novou vstupní zásuvku</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="297"/>
        <source>Edit currently selected output socket properties</source>
        <translation>Upravit vlastnosti nyní vybrané výstupní zásuvky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="355"/>
        <source>Connect currently selected sockets</source>
        <translation>Spojit nyní vybrané zásuvky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="358"/>
        <source>&amp;Connect</source>
        <translation>&amp;Spojit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="364"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="371"/>
        <source>Disconnect currently selected sockets</source>
        <translation>Odpojit nyní vybrané zásuvky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="374"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Odpojit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="380"/>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="387"/>
        <source>Disconnect all currently connected sockets</source>
        <translation>Odpojit všechny nyní spojené zásuvky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="390"/>
        <source>Disconnect &amp;All</source>
        <translation>&amp;Odpojit vše</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="396"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="419"/>
        <source>Refresh current patchbay view</source>
        <translation>Obnovit nynější pohled na zapojovací desku</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="422"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Obnovit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="428"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="448"/>
        <source>Create a new patchbay profile</source>
        <translation>Vytvořit nový profil zapojovací desky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="451"/>
        <source>&amp;New</source>
        <translation>&amp;Nový</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="457"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="467"/>
        <source>Load patchbay profile</source>
        <translation>Nahrát profil zapojovací desky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="470"/>
        <source>&amp;Load...</source>
        <translation>&amp;Nahrát...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="476"/>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="486"/>
        <source>Save current patchbay profile</source>
        <translation>Uložit nynější profil zapojovací desky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="489"/>
        <source>&amp;Save...</source>
        <translation>&amp;Uložit...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="495"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="519"/>
        <source>Current (recent) patchbay profile(s)</source>
        <translation>Nynější (naposledy použité) profil(y) zapojovací desky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="526"/>
        <source>Toggle activation of current patchbay profile</source>
        <translation>Přepnout spuštění nynějšího profilu zapojovací desky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="529"/>
        <source>Acti&amp;vate</source>
        <translation>&amp;Spustit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="535"/>
        <source>Alt+V</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="221"/>
        <source>Warning</source>
        <translation>Varování</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="222"/>
        <source>The patchbay definition has been changed:

&quot;%1&quot;

Do you want to save the changes?</source>
        <translation>Vymezení zapojovací desky bylo změněno:

&quot;%1&quot;

Chcete uložit změny?</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Uložit</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="obsolete">Odmítnout</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Zrušit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="278"/>
        <source>%1 [modified]</source>
        <translation>%1 [změněn]</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="345"/>
        <source>Untitled%1</source>
        <translation>Bez názvu%1</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="364"/>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="394"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="365"/>
        <source>Could not load patchbay definition file: 

&quot;%1&quot;</source>
        <translation>Nepodařilo se nahrát soubor s vymezením zapojovací desky:

&quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="395"/>
        <source>Could not save patchbay definition file: 

&quot;%1&quot;</source>
        <translation>Nepodařilo se uložit soubor s vymezením zapojovací desky:

&quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="439"/>
        <source>New Patchbay definition</source>
        <translation>Nové vymezení zapojovací desky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="440"/>
        <source>Create patchbay definition as a snapshot
of all actual client connections?</source>
        <translation>Vytvořit vymezení zapojovací desky jako snímek
všech skutečných klientských spojení?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Ano</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Ne</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="471"/>
        <source>Load Patchbay Definition</source>
        <translation>Nahrát vymezení zapojovací desky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="473"/>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="493"/>
        <source>Patchbay Definition files</source>
        <translation>Soubory s vymezením zapojovací desky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="491"/>
        <source>Save Patchbay Definition</source>
        <translation>Uložit vymezení zapojovací desky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="598"/>
        <source>active</source>
        <translation>činný</translation>
    </message>
</context>
<context>
    <name>qjackctlPatchbayView</name>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1280"/>
        <source>Add...</source>
        <translation>Přidat...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1282"/>
        <source>Edit...</source>
        <translation>Upravit...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1285"/>
        <source>Copy...</source>
        <translation>Kopírovat...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1288"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1292"/>
        <source>Exclusive</source>
        <translation>Výhradní</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1298"/>
        <source>Forward</source>
        <translation>Dopředu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1337"/>
        <source>(None)</source>
        <translation>(Žádný)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1351"/>
        <source>Move Up</source>
        <translation>Přesunout nahoru</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1354"/>
        <source>Move Down</source>
        <translation>Přesunout dolů</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1360"/>
        <source>&amp;Connect</source>
        <translation>&amp;Spojit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1361"/>
        <source>Alt+C</source>
        <comment>Connect</comment>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1364"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Rozpojit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1365"/>
        <source>Alt+D</source>
        <comment>Disconnect</comment>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1368"/>
        <source>Disconnect &amp;All</source>
        <translation>&amp;Rozpojit vše</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1369"/>
        <source>Alt+A</source>
        <comment>Disconect All</comment>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1374"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Obnovit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1375"/>
        <source>Alt+R</source>
        <comment>Refresh</comment>
        <translation>Alt+R</translation>
    </message>
</context>
<context>
    <name>qjackctlSetupForm</name>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="41"/>
        <source>Setup - JACK Audio Connection Kit</source>
        <translation>Nastavení - JACK Audio Connection Kit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="66"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="86"/>
        <source>Preset &amp;Name:</source>
        <translation>&amp;Název přednastavení:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="107"/>
        <source>Settings preset name</source>
        <translation>Název pro přednastavení nastavení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="114"/>
        <location filename="../src/qjackctlSetupForm.ui" line="885"/>
        <location filename="../src/qjackctlSetupForm.ui" line="976"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1046"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1134"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1196"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1216"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1340"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1663"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1863"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4249"/>
        <source>(default)</source>
        <translation>(výchozí)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="122"/>
        <source>Save settings as current preset name</source>
        <translation>Uložit nastavení s nynějším názvem přednastavení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="125"/>
        <source>&amp;Save</source>
        <translation>&amp;Uložit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="131"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2018"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3769"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="141"/>
        <source>Delete current settings preset</source>
        <translation>Smazat přednastavení nynějšího nastavení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="144"/>
        <source>&amp;Delete</source>
        <translation>&amp;Smazat</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="150"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2037"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3211"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3865"/>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="168"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="189"/>
        <source>Server &amp;Path:</source>
        <translation>&amp;Cesta k serveru:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="219"/>
        <source>The JACK Audio Connection Kit sound server path</source>
        <translation>Cesta ke zvukovému serveru JACK Audio Connection Kit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="226"/>
        <source>jackd</source>
        <translation>jackd</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="231"/>
        <source>jackdmp</source>
        <translation>jackdmp</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="236"/>
        <source>jackstart</source>
        <translation>jackstart</translation>
    </message>
    <message>
        <source>jackd-realtime</source>
        <translation type="obsolete">jackd-realtime</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="250"/>
        <source>Driv&amp;er:</source>
        <translation>&amp;Ovladač:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="272"/>
        <source>The audio backend driver interface to use</source>
        <translation>Rozhraní ovladače k zadní části zvuku, které se bude používat</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="279"/>
        <source>dummy</source>
        <translation>dummy</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="284"/>
        <source>sun</source>
        <translation>sun</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="289"/>
        <source>oss</source>
        <translation>oss</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="294"/>
        <source>alsa</source>
        <translation>alsa</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="299"/>
        <source>portaudio</source>
        <translation>portaudio</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="304"/>
        <source>coreaudio</source>
        <translation>coreaudio</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="309"/>
        <source>freebob</source>
        <translation>freebob</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="314"/>
        <source>firewire</source>
        <translation>firewire</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="341"/>
        <source>Parameters</source>
        <translation>Parametry</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="370"/>
        <source>MIDI Driv&amp;er:</source>
        <translation>Ovla&amp;dač MIDI:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="392"/>
        <source>The ALSA MIDI backend driver to use</source>
        <translation>Ovladač k zadní části ALSA MIDI, který se bude používat</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="399"/>
        <source>none</source>
        <translation>žádný</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="404"/>
        <source>raw</source>
        <translation>raw</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="409"/>
        <source>seq</source>
        <translation>seq</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="439"/>
        <source>Start De&amp;lay (secs):</source>
        <translation>&amp;Zpoždění spuštění (s):</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="461"/>
        <source>Time in seconds that client is delayed after server startup</source>
        <translation>Čas v sekundách, o který je klient opožděn po spuštění serveru</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="510"/>
        <source>Latency:</source>
        <translation>Skrytost neboli latence:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="543"/>
        <source>Output latency in milliseconds, calculated based on the period, rate and buffer settings</source>
        <translation>Výstupní skrytost (latence) v milisekundách, jejíž výpočet je založen na nastavení údobí (period), rychlosti (snímání) a vyrovnávací paměti</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="552"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="581"/>
        <source>Use realtime scheduling</source>
        <translation>Použít zpracování ve skutečném čase</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="584"/>
        <source>&amp;Realtime</source>
        <translation>&amp;Skutečný čas</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="587"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2978"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4114"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="600"/>
        <source>Do not attempt to lock memory, even if in realtime mode</source>
        <translation>Nepokoušejte se uzamknout paměť, dokonce ani v režimu skutečného času</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="603"/>
        <source>No Memory Loc&amp;k</source>
        <translation>&amp;Neuzamknout paměť</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="606"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3230"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3808"/>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="619"/>
        <source>Unlock memory of common toolkit libraries (GTK+, QT, FLTK, Wine)</source>
        <translation>Odemknout paměť běžných knihoven s nástroji (GTK+, QT, FLTK, Wine)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="622"/>
        <source>&amp;Unlock Memory</source>
        <translation>&amp;Odemknout paměť</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="625"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1999"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3972"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="638"/>
        <source>Ignore xruns reported by the backend driver</source>
        <translation>Přehlížet xruns hlášené ovladačem zadní části</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="641"/>
        <source>So&amp;ft Mode</source>
        <translation>&amp;Snadný režim</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="644"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3151"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3173"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3315"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3485"/>
        <source>Alt+F</source>
        <translation>Alt+F</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="657"/>
        <source>Provide output monitor ports</source>
        <translation>Poskytnout přípojky pro sledování výstupu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="660"/>
        <source>&amp;Monitor</source>
        <translation>&amp;Sledování</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="663"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2801"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3353"/>
        <source>Alt+M</source>
        <translation>Alt+M</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="676"/>
        <source>Force 16bit mode instead of failing over 32bit (default)</source>
        <translation>Vynutit 16bitový režim namísto selhání ve 32bitovém (výchozí)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="679"/>
        <source>Force &amp;16bit</source>
        <translation>Vynutit &amp;16bitový režim</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="682"/>
        <source>Alt+1</source>
        <translation>Alt+1</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="695"/>
        <source>Enable hardware monitoring of capture ports</source>
        <translation>Povolit přístrojové sledování zachytávacích přípojek</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="698"/>
        <source>H/W M&amp;onitor</source>
        <translation>H/W &amp;sledování</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="701"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3953"/>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="714"/>
        <source>Enable hardware metering on cards that support it</source>
        <translation>Povolit přístrojové měření u karet, které to podporují</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="717"/>
        <source>H/&amp;W Meter</source>
        <translation>H/&amp;W měřidlo</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="720"/>
        <source>Alt+W</source>
        <translation>Alt+W</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="733"/>
        <source>Ignore hardware period/buffer size</source>
        <translation>Přehlížet údobí (periodu)/velikost vyrovnávací paměti přístroje</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="736"/>
        <source>&amp;Ignore H/W</source>
        <translation>&amp;Přehlížet H/W</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="739"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2518"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3884"/>
        <source>Alt+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="752"/>
        <source>Whether to give verbose output on messages</source>
        <translation>Dát hlášením mnohomluvný výstup</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="755"/>
        <source>&amp;Verbose messages</source>
        <translation>&amp;Mnohomluvná hlášení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="758"/>
        <source>Alt+V</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="813"/>
        <source>&amp;Output Device:</source>
        <translation>&amp;Výstupní zařízení:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="835"/>
        <source>&amp;Output Channels:</source>
        <translation>&amp;Výstupní kanály:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="857"/>
        <source>&amp;Interface:</source>
        <translation>&amp;Rozhraní:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="879"/>
        <source>Maximum input audio hardware channels to allocate</source>
        <translation>Největší množství přidělitelných vstupních zvukových přístrojových kanálů</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="898"/>
        <source>&amp;Audio:</source>
        <translation>&amp;Zvuk:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="920"/>
        <source>&amp;Input Latency:</source>
        <translation>&amp;Vstupní skrytost (latence):</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="948"/>
        <source>Dit&amp;her:</source>
        <translation>&amp;Chvění:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="970"/>
        <source>External output latency (frames)</source>
        <translation>Vnější výstupní skrytost neboli latence (snímky)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="989"/>
        <source>&amp;Input Device:</source>
        <translation>Vstup&amp;ní zařízení:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1011"/>
        <source>Provide either audio capture, playback or both</source>
        <translation>Poskytnout buď zachytávání zvuku, přehrávání nebo obojí</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1015"/>
        <source>Duplex</source>
        <translation>Zdvojený</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1020"/>
        <source>Capture Only</source>
        <translation>Pouze zachytávání</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1025"/>
        <source>Playback Only</source>
        <translation>Pouze přehrávání</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1039"/>
        <source>The PCM device name to use</source>
        <translation>Název používaného zařízení PCM</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1051"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1139"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1221"/>
        <source>hw:0</source>
        <translation>hw:0</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1056"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1144"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1226"/>
        <source>plughw:0</source>
        <translation>plughw:0</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1061"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1149"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1231"/>
        <source>/dev/audio</source>
        <translation>/dev/audio</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1066"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1154"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1236"/>
        <source>/dev/dsp</source>
        <translation>/dev/dsp</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1095"/>
        <source>Select output device for playback</source>
        <translation>Vybrat výstupní zařízení pro přehrávání</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1098"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1290"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1321"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2092"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2178"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2240"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2345"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1127"/>
        <source>Alternate input device for capture</source>
        <translation>Střídat vstupní zařízení pro zachytávání</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1168"/>
        <source>&amp;Output Latency:</source>
        <translation>&amp;Výstupní skrytost neboli latence:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1190"/>
        <source>Maximum output audio hardware channels to allocate</source>
        <translation>Největší množství přidělitelných výstupních zvukových přístrojových kanálů</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1209"/>
        <source>Alternate output device for playback</source>
        <translation>Střídat výstupní zařízení pro přehrávání</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1250"/>
        <source>&amp;Input Channels:</source>
        <translation>&amp;Vstupní kanály:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1287"/>
        <source>Select input device for capture</source>
        <translation>Vybrat vstupní zařízení pro zachytávání</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1318"/>
        <source>Select PCM device name</source>
        <translation>Vybrat název zařízení PCM</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1334"/>
        <source>External input latency (frames)</source>
        <translation>Vnější vstupní skrytost neboli latence (snímky)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1353"/>
        <source>Set dither mode</source>
        <translation>Nastavit režim chvění</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1357"/>
        <source>None</source>
        <translation>Žádný</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1362"/>
        <source>Rectangular</source>
        <translation>Obdélníkový</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1367"/>
        <source>Shaped</source>
        <translation>Obalová křivka</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1372"/>
        <source>Triangular</source>
        <translation>Trojúhelníkový</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1396"/>
        <source>Number of periods in the hardware buffer</source>
        <translation>Počet údobí (period) ve vyrovnávací paměti přístroje</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1421"/>
        <source>Priorit&amp;y:</source>
        <translation>&amp;Přednost:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1443"/>
        <source>&amp;Frames/Period:</source>
        <translation>&amp;Snímky/Údobí (perioda):</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1465"/>
        <source>Frames per period between process() calls</source>
        <translation>Snímků za údobí (periodu) mezi voláním process() Aufrufen</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1472"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1730"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1477"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1735"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1482"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1740"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1487"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1778"/>
        <source>128</source>
        <translation>128</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1492"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1783"/>
        <source>256</source>
        <translation>256</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1497"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1788"/>
        <source>512</source>
        <translation>512</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1502"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1793"/>
        <source>1024</source>
        <translation>1024</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1507"/>
        <source>2048</source>
        <translation>2048</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1512"/>
        <source>4096</source>
        <translation>4096</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1526"/>
        <source>Port Ma&amp;ximum:</source>
        <translation>&amp;Největší počet přípojek:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1556"/>
        <source>&amp;Channels:</source>
        <translation>&amp;Kanály:</translation>
    </message>
    <message>
        <source>Number o microseconds to wait between engine processes (dummy)</source>
        <translation type="obsolete">Počet mikrosekund, po které se čeká mezi strojovými procesy (dummy)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1592"/>
        <source>21333</source>
        <translation>21333</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1606"/>
        <source>Sample rate in frames per second</source>
        <translation>Vzorkovací kmitočet (rychlost snímkování) ve snímcích za sekundu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1613"/>
        <source>22050</source>
        <translation>22050</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1618"/>
        <source>32000</source>
        <translation>32000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1623"/>
        <source>44100</source>
        <translation>44100</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1628"/>
        <source>48000</source>
        <translation>48000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1633"/>
        <source>88200</source>
        <translation>88200</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1638"/>
        <source>96000</source>
        <translation>96000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1643"/>
        <source>192000</source>
        <translation>192000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1657"/>
        <source>Scheduler priority when running realtime</source>
        <translation>Přednost rozvrhu při běhu ve skutečném čase</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1676"/>
        <source>&amp;Word Length:</source>
        <translation>&amp;Délka slova:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1698"/>
        <source>Periods/&amp;Buffer:</source>
        <translation>Ú&amp;dobí/Vyrovnávací paměť:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1723"/>
        <source>Word length</source>
        <translation>Délka slova</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1768"/>
        <source>Maximum number of ports the JACK server can manage</source>
        <translation>Největší počet přípojek, které server JACK může spravovat</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1807"/>
        <source>&amp;Wait (usec):</source>
        <translation>&amp;Čekat (µs):</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1835"/>
        <source>Sample &amp;Rate:</source>
        <translation>Vzorkovací &amp;kmitočet:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1857"/>
        <source>Maximum number of audio channels to allocate</source>
        <translation>Největší množství přidělitelných zvukových kanálů</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1883"/>
        <source>&amp;Timeout (msec):</source>
        <translation>&amp;Překročení času (ms):</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1905"/>
        <source>Set client timeout limit in miliseconds</source>
        <translation>Nastavit mez pro překročení času u klienta; údaj v milisekundách</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1915"/>
        <source>200</source>
        <translation>200</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1920"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3386"/>
        <source>500</source>
        <translation>500</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1925"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3391"/>
        <source>1000</source>
        <translation>1000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1930"/>
        <source>2000</source>
        <translation>2000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1935"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3401"/>
        <source>5000</source>
        <translation>5000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1940"/>
        <source>10000</source>
        <translation>10000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1954"/>
        <source>Options</source>
        <translation>Volby</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1972"/>
        <source>Scripting</source>
        <translation>Skriptování</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1993"/>
        <source>Whether to execute a custom shell script before starting up the JACK audio server.</source>
        <translation>Provést vlastní shellový skript před spuštěním zvukového serveru JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1996"/>
        <source>Execute script on Start&amp;up:</source>
        <translation>Provést skript při &amp;spuštění:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2012"/>
        <source>Whether to execute a custom shell script after starting up the JACK audio server.</source>
        <translation>Provést vlastní shellový skript po spuštění zvukového serveru JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2015"/>
        <source>Execute script after &amp;Startup:</source>
        <translation>Provést skript &amp;po spuštění:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2031"/>
        <source>Whether to execute a custom shell script before shuting down the JACK audio server.</source>
        <translation>Provést vlastní shellový skript před zastavením zvukového serveru JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2034"/>
        <source>Execute script on Shut&amp;down:</source>
        <translation>Provést skript při zasta&amp;vení:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2058"/>
        <source>Command line to be executed before starting up the JACK audio server</source>
        <translation>Příkazový řádek k provedení před spuštěním zvukového serveru JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2089"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2175"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2237"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2342"/>
        <source>Scripting argument meta-symbols</source>
        <translation>Meta symboly pro argument při skriptování</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2120"/>
        <source>Browse for script to be executed before starting up the JACK audio server</source>
        <translation>Vybrat skript, který se provede před spuštěním zvukového serveru JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2123"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2209"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2271"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2376"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2665"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2782"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2144"/>
        <source>Command line to be executed after starting up the JACK audio server</source>
        <translation>Příkazový řádek k provedení po spuštění zvukového serveru JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2206"/>
        <source>Browse for script to be executed after starting up the JACK audio server</source>
        <translation>Vybrat skript, který se provede po spuštění zvukového serveru JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2268"/>
        <source>Browse for script to be executed before shutting down the JACK audio server</source>
        <translation>Vybrat skript, který se provede před zastavením zvukového serveru JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2292"/>
        <source>Command line to be executed before shutting down the JACK audio server</source>
        <translation>Příkazový řádek k provedení před zastavením zvukového serveru JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2308"/>
        <source>Whether to execute a custom shell script after shuting down the JACK audio server.</source>
        <translation>Provést vlastní shellový skript po zastavení zvukového serveru JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2311"/>
        <source>Execute script after Shu&amp;tdown:</source>
        <translation>Provést skript po zas&amp;tavení:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2314"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2946"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4133"/>
        <source>Alt+T</source>
        <translation>ALT+T</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2373"/>
        <source>Browse for script to be executed after shutting down the JACK audio server</source>
        <translation>Vybrat skript, který se provede po zastavení zvukového serveru JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2397"/>
        <source>Command line to be executed after shutting down the JACK audio server</source>
        <translation>Příkazový řádek k provedení po zastavení zvukového serveru JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2416"/>
        <source>Statistics</source>
        <translation>Statistika</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2437"/>
        <source>Whether to capture standard output (stdout/stderr) into messages window</source>
        <translation>Vést obvyklý výstup (stdout/stderr) do okna s hlášeními</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2440"/>
        <source>&amp;Capture standard output</source>
        <translation>&amp;Vést obvyklý výstup</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2443"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3788"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2464"/>
        <source>&amp;XRUN detection regex:</source>
        <translation>Pravidelný výraz pro zjištění &amp;XRUN:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2491"/>
        <source>Regular expression used to detect XRUNs on server output messages</source>
        <translation>Pravidelný výraz užitý pro poznání XRUN v hlášeních posílaných serverem</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2498"/>
        <source>xrun of at least ([0-9|\.]+) msecs</source>
        <translation>xrun alespoň ([0-9|\.]+) ms</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2512"/>
        <source>Whether to ignore the first XRUN on server startup (most likely to occur on pre-0.80.0 servers)</source>
        <translation>Přehlížet první XRUN při spuštění serveru (většinou se vyskytuje na pre-0.80.0 serverech)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2515"/>
        <source>&amp;Ignore first XRUN occurrence on statistics</source>
        <translation>&amp;Přehlížet výskyt prvního XRUN ve statistice</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2534"/>
        <source>Connections</source>
        <translation>Spojení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2561"/>
        <source>Time in seconds between each auto-refresh cycle</source>
        <translation>Čas v sekundách mezi každým kolem automatického obnovení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2568"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2573"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4274"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2578"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2583"/>
        <source>30</source>
        <translation>30</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2588"/>
        <source>60</source>
        <translation>60</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2593"/>
        <source>120</source>
        <translation>120</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2631"/>
        <source>Patchbay definition file to be activated as connection persistence profile</source>
        <translation>Spustit soubor s vymezením zapojovací desky jako stálý profil spojení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2662"/>
        <source>Browse for a patchbay definition file to be activated</source>
        <translation>Vybrat soubor s vymezením zapojovací desky pro spuštění</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2678"/>
        <source>Whether to refresh the connections patchbay automatically</source>
        <translation>Obnovit automaticky pzapojovací desku pro spojení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2681"/>
        <source>&amp;Auto refresh connections Patchbay, every (secs):</source>
        <translation>Obnovit &amp;automaticky zapojovací desku pro spojení, každých (s):</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2684"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3913"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2697"/>
        <source>Whether to activate a patchbay definition for connection persistence profile.</source>
        <translation>Spustit stálý profil s vymezením zapojovací desky spojení.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2700"/>
        <source>Activate &amp;Patchbay persistence:</source>
        <translation>Spustit stálý profil se &amp;zapojovací deskou:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2703"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2719"/>
        <source>Logging</source>
        <translation>Provádění zápisu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2748"/>
        <source>Messages log file</source>
        <translation>Soubor se zápisem hlášení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2779"/>
        <source>Browse for the messages log file location</source>
        <translation>Vybrat místo pro umístění souboru se zápisem hlášení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2795"/>
        <source>Whether to activate a messages logging to file.</source>
        <translation>Spustit provádění zápisu hlášení do souboru.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2798"/>
        <source>&amp;Messages log file:</source>
        <translation>&amp;Soubor se zápisem hlášení:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2828"/>
        <source>Display</source>
        <translation>Zobrazit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2846"/>
        <source>Time Display</source>
        <translation>Údaj o čase</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2875"/>
        <source>Time F&amp;ormat:</source>
        <translation>&amp;Formát času:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2897"/>
        <source>The general time format on display</source>
        <translation>Obecný formát času pro zobrazení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2904"/>
        <source>hh:mm:ss</source>
        <translation>hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2909"/>
        <source>hh:mm:ss.d</source>
        <translation>hh:mm:ss.d</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2914"/>
        <source>hh:mm:ss.dd</source>
        <translation>hh:mm:ss.dd</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2919"/>
        <source>hh:mm:ss.ddd</source>
        <translation>hh:mm:ss.ddd</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2943"/>
        <source>Transport &amp;Time Code</source>
        <translation>Předání &amp;časového kódu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2959"/>
        <source>Transport &amp;BBT (bar:beat.ticks)</source>
        <translation>Předání &amp;BBT (takt:doba.tiknutí - bar:beat.ticks)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2962"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3651"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4010"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4152"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2975"/>
        <source>Elapsed time since last &amp;Reset</source>
        <translation>Čas uplynulý od posledního &amp;znovunastavení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2991"/>
        <source>Elapsed time since last &amp;XRUN</source>
        <translation>Čas uplynulý od posledního &amp;XRUN</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2994"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3061"/>
        <source>Sample front panel normal display font</source>
        <translation>Předvést písmo pro obvyklé zobrazení na přední straně panelu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3101"/>
        <source>Sample big time display font</source>
        <translation>Předvést písmo pro velké zobrazení údaje o čase</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3129"/>
        <source>Big Time display:</source>
        <translation>Velké zobrazení údaje o čase:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3145"/>
        <source>Select font for front panel normal display</source>
        <translation>Vybrat písmo pro zobrazení písma na přední straně panelu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3148"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3170"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3312"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3482"/>
        <source>&amp;Font...</source>
        <translation>&amp;Písmo...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3167"/>
        <source>Select font for big time display</source>
        <translation>Vybrat písmo pro velké zobrazení údaje o čase</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3189"/>
        <source>Normal display:</source>
        <translation>Obvyklé zobrazení:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3205"/>
        <source>Whether to enable a shiny glass light effect on the main display</source>
        <translation>Povolit zobrazení údajů s lesklým sklenným světelným efektem</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3208"/>
        <source>&amp;Display shiny glass light effect</source>
        <translation>&amp;Zobrazit údaje s lesklým sklenným světelným efektem</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3224"/>
        <source>Whether to enable blinking (flashing) of the server mode (RT) indicator</source>
        <translation>Povolit mrkání indikátoru serverového režimu (realtime -RT)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3227"/>
        <source>Blin&amp;k server mode indicator</source>
        <translation>Zobrazovat mr&amp;kání indikátoru režimu serveru</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3248"/>
        <source>Messages Window</source>
        <translation>Okno s hlášením</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3281"/>
        <source>Sample messages text font display</source>
        <translation>Předvést zobrazení textu v okně s hlášením</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3309"/>
        <source>Select font for the messages text display</source>
        <translation>Vybrat písmo pro zobrazení textu hlášení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3347"/>
        <source>Whether to keep a maximum number of lines in the messages window</source>
        <translation>Určit největší počet řádků zobrazovaných v okně s hlášením</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3350"/>
        <source>&amp;Messages limit:</source>
        <translation>&amp;Největší počet hlášení:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3366"/>
        <source>The maximum number of message lines to keep in view</source>
        <translation>Největší počet řádků zobrazovaných v okně s hlášením</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3376"/>
        <source>100</source>
        <translation>100</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3381"/>
        <source>250</source>
        <translation>250</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3396"/>
        <source>2500</source>
        <translation>2500</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3418"/>
        <source>Connections Window</source>
        <translation>Přehled spojení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3451"/>
        <source>Sample connections view font</source>
        <translation>Předvést zobrazení písma v přehledu spojení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3479"/>
        <source>Select font for the connections view</source>
        <translation>Vybrat písmo pro přehled spojení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3501"/>
        <source>&amp;Icon size:</source>
        <translation>&amp;Velikost ikon:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3523"/>
        <source>The icon size for each item of the connections view</source>
        <translation>Velikost jednotlivých symbolů v přehledu spojení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3533"/>
        <source>16 x 16</source>
        <translation>16 x 16</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3538"/>
        <source>32 x 32</source>
        <translation>32 x 32</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3543"/>
        <source>64 x 64</source>
        <translation>64 x 64</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3645"/>
        <source>Whether to enable in-place client/port name editing (rename)</source>
        <translation>Povolit úpravu vedlejšího názvu (přezdívka) klienta/přípojky (přejmenování)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3648"/>
        <source>Ena&amp;ble client/port aliases editing (rename)</source>
        <translation>Po&amp;volit úpravu vedlejšího názvu (přezdívka) klienta/přípojky (přejmenování)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3664"/>
        <source>Whether to enable client/port name aliases on the connections window</source>
        <translation>Povolit vedlejší názvy (přezdívky) klienta/přípojky v přehledu spojení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3667"/>
        <source>E&amp;nable client/port aliases</source>
        <translation>&amp;Povolit vedlejší názvy (přezdívky) klienta/přípojky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3670"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3991"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3683"/>
        <source>Whether to draw connection lines as cubic Bezier curves</source>
        <translation>Určit, zda se mají čáry spojení kreslit jako Bézierovy křivky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3686"/>
        <source>Draw connection and patchbay lines as Be&amp;zier curves</source>
        <translation>Čáry pro spojení a čáry zapojovací desky kreslit jako &amp;Bézierovy křivky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3689"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3846"/>
        <source>Alt+Z</source>
        <translation>Alt+Z</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3716"/>
        <source>Misc</source>
        <translation>Různé</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3734"/>
        <source>Other</source>
        <translation>Další</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3763"/>
        <source>Whether to start JACK audio server immediately on application startup</source>
        <translation>Spustit zvukový server JACK okamžitě při spuštění aplikace</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3766"/>
        <source>&amp;Start JACK audio server on application startup</source>
        <translation>&amp;Spustit zvukový server JACK okamžitě při spuštění aplikace</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3782"/>
        <source>Whether to ask for confirmation on application exit</source>
        <translation>Žádat o potvrzení při ukončení aplikace</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3785"/>
        <source>&amp;Confirm application close</source>
        <translation>&amp;Potvrdit ukončení aplikace</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3802"/>
        <source>Whether to keep all child windows on top of the main window</source>
        <translation>Všechna další okna udržovat nad hlavním oknem</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3805"/>
        <source>&amp;Keep child windows always on top</source>
        <translation>&amp;Všechna další okna udržovat vždy nahoře</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3821"/>
        <source>Whether to enable the system tray icon</source>
        <translation>Ukázat ikonu v systémové části panelu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3824"/>
        <source>&amp;Enable system tray icon</source>
        <translation>Po&amp;volit ikonu v systémové části panelu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3827"/>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3840"/>
        <source>Whether to start minimized to system tray</source>
        <translation>Spustit program zmenšený do systémové části panelu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3843"/>
        <source>Start minimi&amp;zed to system tray</source>
        <translation>Spustit program &amp;zmenšený do systémové části panelu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3859"/>
        <source>Whether to delay window positioning at application startup</source>
        <translation>Zpozdit umístění okna při spuštění programu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3862"/>
        <source>&amp;Delay window positioning at startup</source>
        <translation>Zpozd&amp;it umístění okna při spuštění programu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3907"/>
        <source>Whether to save the JACK server command-line configuration into a local file (auto-start)</source>
        <translation>Nastavení příkazového řádku ke spuštění serveru JACK-uložit do místního souboru (auto-start)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3910"/>
        <source>S&amp;ave JACK audio server configuration to:</source>
        <translation>Nastavení ke spuštění serveru JACK &amp;uložit jako:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3926"/>
        <source>The server configuration local file name (auto-start)</source>
        <translation>Název místního souboru s nastavením serveru (auto-start)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3933"/>
        <source>.jackdrc</source>
        <translation>.jackdrc</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3947"/>
        <source>Whether to exit once all clients have closed (auto-start)</source>
        <translation>Ukončit program, když jsou rozpojena všechna klientská spojení (auto-start)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3950"/>
        <source>C&amp;onfigure as temporary server</source>
        <translation>Nastavit jako &amp;dočasný server</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3966"/>
        <source>Whether to ask for confirmation on JACK audio server shutdown</source>
        <translation>Žádat o potvrzení při zastavení serveru JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3969"/>
        <source>Confirm server sh&amp;utdown</source>
        <translation>Potvrdit &amp;zastavení serveru</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3985"/>
        <source>Whether to enable ALSA Sequencer (MIDI) support on startup</source>
        <translation>Povolit podporu (MIDI) pro řadič (sekvencer) ALSA při spuštění</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3988"/>
        <source>E&amp;nable ALSA Sequencer support</source>
        <translation>P&amp;ovolit podporu (MIDI) pro řadič (sekvencer) ALSA</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4060"/>
        <source>Buttons</source>
        <translation>Tlačítka</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4089"/>
        <source>Whether to hide the left button group on the main window</source>
        <translation>Skrýt skupinu s tlačítky nalevo v hlavním okně</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4092"/>
        <source>Hide main window &amp;Left buttons</source>
        <translation>Skrýt &amp;levá tlačítka v hlavním okně</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4095"/>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4108"/>
        <source>Whether to hide the right button group on the main window</source>
        <translation>Skrýt skupinu s tlačítky napravo v hlavním okně</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4111"/>
        <source>Hide main window &amp;Right buttons</source>
        <translation>Skrýt &amp;pravá tlačítka v hlavním okně</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4127"/>
        <source>Whether to hide the transport button group on the main window</source>
        <translation>Skrýt skupinu s tlačítky pro předání v hlavním okně</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4130"/>
        <source>Hide main window &amp;Transport buttons</source>
        <translation>Skrýt &amp;předávací tlačítka v hlavním okně</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4146"/>
        <source>Whether to hide the text labels on the main window buttons</source>
        <translation>Skrýt textové popisky tlačítek v hlavním okně</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4149"/>
        <source>Hide main window &amp;button text labels</source>
        <translation>Skrýt textové popisky &amp;tlačítek v hlavním okně</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4323"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4333"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="796"/>
        <location filename="../src/qjackctlSetupForm.cpp" line="848"/>
        <location filename="../src/qjackctlSetupForm.cpp" line="1844"/>
        <source>Warning</source>
        <translation>Varování</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="797"/>
        <source>Some settings have been changed:

&quot;%1&quot;

Do you want to save the changes?</source>
        <translation>Některá nastavení byla změněna:

&quot;%1&quot;

Chcete uložit změny?</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Uložit</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="obsolete">Odmítnout</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="849"/>
        <source>Delete preset:

&quot;%1&quot;

Are you sure?</source>
        <translation>Smazat přednastavení

&quot;%1&quot;

Jste si jistý?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="882"/>
        <source>msec</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="884"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1497"/>
        <source>&amp;Preset Name</source>
        <translation>&amp;Název přednastavení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1499"/>
        <source>&amp;Server Path</source>
        <translation>&amp;Cesta k serveru</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1500"/>
        <source>&amp;Driver</source>
        <translation>&amp;Ovladač</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1501"/>
        <source>&amp;Interface</source>
        <translation>&amp;Rozhraní</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1503"/>
        <source>Sample &amp;Rate</source>
        <translation>Vzorkovací &amp;kmitočet</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1504"/>
        <source>&amp;Frames/Period</source>
        <translation>&amp;Snímky/Údobí (perioda)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1505"/>
        <source>Periods/&amp;Buffer</source>
        <translation>Ú&amp;dobí/Vyrovnávací paměť</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1553"/>
        <source>Startup Script</source>
        <translation>Skript pro spuštění</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1570"/>
        <source>Post-Startup Script</source>
        <translation>Skript pro po-spuštění</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1587"/>
        <source>Shutdown Script</source>
        <translation>Skript pro zastavení chodu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1604"/>
        <source>Post-Shutdown Script</source>
        <translation>Skript pro po-zastavení chodu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1621"/>
        <source>Active Patchbay Definition</source>
        <translation>Činné vymezení zapojovací desky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1623"/>
        <source>Patchbay Definition files</source>
        <translation>Soubory s vymezením zapojovací desky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1639"/>
        <source>Messages Log</source>
        <translation>Zápis s hlášením</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1641"/>
        <source>Log files</source>
        <translation>Soubory se zápisy</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1845"/>
        <source>Some settings have been changed.

Do you want to apply the changes?</source>
        <translation>Některá nastavení byla změněna.

Chcete použít změny?</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="obsolete">Použít</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3589"/>
        <source>&amp;JACK client/port aliases:</source>
        <translation>Vedlejší názvy (přezdívky) pro klienty/přípojky &amp;JACK:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3611"/>
        <source>JACK client/port aliases display mode</source>
        <translation>Režim zobrazení vedlejších názvů (přezdívek) pro klienty/přípojky JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3621"/>
        <source>Default</source>
        <translation>Výchozí</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3626"/>
        <source>First</source>
        <translation>První</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3631"/>
        <source>Second</source>
        <translation>Druhý</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4186"/>
        <source>Defaults</source>
        <translation>Výchozí</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4223"/>
        <source>&amp;Base font size:</source>
        <translation>&amp;Základní velikost písma:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4242"/>
        <source>Base application font size (pt.)</source>
        <translation>Základní velikost písma v aplikaci (pt.)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4254"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4259"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4264"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4269"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4279"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4284"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="319"/>
        <source>net</source>
        <translation>síť</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="324"/>
        <source>netone</source>
        <translation>síť_1</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1585"/>
        <source>Number of microseconds to wait between engine processes (dummy)</source>
        <translation>Počet mikrosekund, po které se čeká mezi strojovými procesy (dummy)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4004"/>
        <source>Whether to enable D-Bus interface</source>
        <translation>Zda povolit rozhraní D-Bus</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4007"/>
        <source>&amp;Enable D-Bus interface</source>
        <translation>&amp;Povolit rozhraní D-Bus</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3878"/>
        <source>Whether to restrict to one single application instance (X11)</source>
        <translation>Zda omezit na úroveň jednoho programu (X11)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3881"/>
        <source>Single application &amp;instance</source>
        <translation>Ú&amp;roveň jednoho programu</translation>
    </message>
</context>
<context>
    <name>qjackctlSocketForm</name>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="33"/>
        <source>Socket - JACK Audio Connection Kit</source>
        <translation>Zásuvka - JACK Audio Connection Kit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="52"/>
        <source>&amp;Socket</source>
        <translation>&amp;Zásuvka</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="64"/>
        <source>&amp;Name (alias):</source>
        <translation>&amp;Název (Přezdívka):</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="80"/>
        <source>Socket name (an alias for client name)</source>
        <translation>Název zásuvky (přezdívka pro název klienta)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="87"/>
        <source>Client name (regular expression)</source>
        <translation>Název pro klienta (pravidelný výraz)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="97"/>
        <source>Add plug to socket plug list</source>
        <translation>Přidat zástrčku do seznamu se zástrčkami do zásuvky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="100"/>
        <source>Add P&amp;lug</source>
        <translation>&amp;Přidat zástrčku</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="106"/>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="113"/>
        <source>&amp;Plug:</source>
        <translation>&amp;Zástrčka:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="129"/>
        <source>Port name (regular expression)</source>
        <translation>Název přípojky (pravidelný výraz)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="142"/>
        <source>Socket plug list</source>
        <translation>Seznam se zástrčkami do zásuvky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="158"/>
        <source>Socket Plugs / Ports</source>
        <translation>Zástrčky do zásuvky/Přípojky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="166"/>
        <source>Edit currently selected plug</source>
        <translation>Upravit nyní vybranou zástrčku</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="169"/>
        <source>&amp;Edit</source>
        <translation>&amp;Upravit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="175"/>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="182"/>
        <source>Remove currently selected plug from socket plug list</source>
        <translation>Odstranit nyní vybranou zástrčku ze seznamu se zástrčkami do zásuvky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="185"/>
        <source>&amp;Remove</source>
        <translation>&amp;Odstranit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="191"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="198"/>
        <source>&amp;Client:</source>
        <translation>&amp;Klient:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="214"/>
        <source>Move down currently selected plug in socket plug list</source>
        <translation>Posunout nyní vybranou zástrčku v seznamu se zástrčkami do zásuvky dolů</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="217"/>
        <source>&amp;Down</source>
        <translation>&amp;Dolů</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="223"/>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="230"/>
        <source>Move up current selected plug in socket plug list</source>
        <translation>Posunout nyní vybranou zástrčku v seznamu se zástrčkami do zásuvky nahoru</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="233"/>
        <source>&amp;Up</source>
        <translation>&amp;Nahoru</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="239"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="262"/>
        <source>Enforce only one exclusive cable connection</source>
        <translation>Vynutit jedno výlučné kabelové spojení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="265"/>
        <source>E&amp;xclusive</source>
        <translation>Vý&amp;lučné</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="268"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="275"/>
        <source>&amp;Forward:</source>
        <translation>&amp;Dopředu:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="291"/>
        <source>Forward (clone) all connections from this socket</source>
        <translation>Předat všechna spojení z této zásuvky (klonovat)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="301"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="313"/>
        <source>Audio socket type (JACK)</source>
        <translation>Zvukový typ zásuvky (napojení) JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="316"/>
        <source>&amp;Audio</source>
        <translation>&amp;Zvuk</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="319"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="326"/>
        <source>MIDI socket type (JACK)</source>
        <translation>MIDI typ zásuvky (napojení) (JACK)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="329"/>
        <source>&amp;MIDI</source>
        <translation>&amp;JACK-MIDI</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="332"/>
        <source>Alt+M</source>
        <translation>Alt+M</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="339"/>
        <source>MIDI socket type (ALSA)</source>
        <translation>MIDI typ zásuvky (napojení) (ALSA)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="342"/>
        <source>AL&amp;SA</source>
        <translation>AL&amp;SA-MIDI</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="345"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="386"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="402"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="152"/>
        <source>Plugs / Ports</source>
        <translation>Zástrčky/Přípojky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="337"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="338"/>
        <source>A socket named &quot;%1&quot; already exists.</source>
        <translation>Zásuvka s názvem &quot;%1&quot; již existuje.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="356"/>
        <source>Warning</source>
        <translation>Varování</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="357"/>
        <source>Some settings have been changed.

Do you want to apply the changes?</source>
        <translation>Některá nastavení byla změněna.

Chcete použít změny?</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="obsolete">Použít</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="obsolete">Odmítnout</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="546"/>
        <source>Add Plug</source>
        <translation>Přidat zástrčku</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="560"/>
        <source>Edit</source>
        <translation>Upravit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="563"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="567"/>
        <source>Move Up</source>
        <translation>Přesunout nahoru</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="570"/>
        <source>Move Down</source>
        <translation>Přesunout dolů</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="736"/>
        <source>(None)</source>
        <translation>(Žádný)</translation>
    </message>
</context>
<context>
    <name>qjackctlSocketList</name>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="329"/>
        <source>Output</source>
        <translation>Výstup</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="339"/>
        <source>Input</source>
        <translation>Vstup</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="352"/>
        <source>Socket</source>
        <translation>Zásuvka</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="487"/>
        <source>&lt;New&gt; - %1</source>
        <translation>&lt;Nový&gt; %1</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="543"/>
        <source>Warning</source>
        <translation>Varování</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="544"/>
        <source>%1 about to be removed:

&quot;%2&quot;

Are you sure?</source>
        <translation>%1 má být odstraněno:

&quot;%2&quot;

Jste si jistý?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Ano</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Ne</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="634"/>
        <source>%1 &lt;Copy&gt; - %2</source>
        <translation>%1 &lt;Kopírovat&gt; - %2</translation>
    </message>
</context>
<context>
    <name>qjackctlSocketListView</name>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="802"/>
        <source>Output Sockets / Plugs</source>
        <translation>Výstupní zásuvky/zástrčky</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="804"/>
        <source>Input Sockets / Plugs</source>
        <translation>Vstupní zásuvky/zástrčky</translation>
    </message>
</context>
<context>
    <name>qjackctlStatusForm</name>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="44"/>
        <source>Status - JACK Audio Connection Kit</source>
        <translation>Stav - JACK Audio Connection Kit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="73"/>
        <source>Statistics since last server startup</source>
        <translation>Statistika od posledního spuštění serveru</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="98"/>
        <source>Description</source>
        <translation>Popis</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="103"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="127"/>
        <source>Reset XRUN statistic values</source>
        <translation>Nastavit znovu údaje ve statistice XRUN</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="130"/>
        <source>Re&amp;set</source>
        <translation>&amp;Nastavit znovu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="136"/>
        <source>Alt+S</source>
        <translation>ALT+S</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="143"/>
        <source>Refresh XRUN statistic values</source>
        <translation>Obnovit údaje ve statistice XRUN</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="146"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Obnovit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="152"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="64"/>
        <source>Server state</source>
        <translation>Stav serveru</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="66"/>
        <source>DSP Load</source>
        <translation>Zatížení DSP</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="68"/>
        <source>Sample Rate</source>
        <translation>Vzorkovací kmitočet</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="70"/>
        <source>Buffer Size</source>
        <translation>Velikost vyrovnávací paměti</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="72"/>
        <source>Realtime Mode</source>
        <translation>Režim skutečného času</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="75"/>
        <source>Transport state</source>
        <translation>Stav předání</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="78"/>
        <source>Transport Timecode</source>
        <translation>Předání časového kódu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="80"/>
        <source>Transport BBT</source>
        <translation>Předání BBT</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="82"/>
        <source>Transport BPM</source>
        <translation>Předání BPM</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="85"/>
        <source>XRUN count since last server startup</source>
        <translation>Počet XRUN od posledního spuštění serveru</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="88"/>
        <source>XRUN last time detected</source>
        <translation>Čas posledního zjištění XRUN</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="90"/>
        <source>XRUN last</source>
        <translation>Poslední XRUN</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="92"/>
        <source>XRUN maximum</source>
        <translation>Největší počet XRUN</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="94"/>
        <source>XRUN minimum</source>
        <translation>Nejmenší počet XRUN</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="96"/>
        <source>XRUN average</source>
        <translation>Průměrný počet XRUN</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="98"/>
        <source>XRUN total</source>
        <translation>Celkový počet XRUN</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="102"/>
        <source>Maximum scheduling delay</source>
        <translation>Největšího zpoždění času</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="105"/>
        <source>Time of last reset</source>
        <translation>Čas posledního znovunastavení</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="62"/>
        <source>Server name</source>
        <translation>Název serveru</translation>
    </message>
</context>
</TS>
