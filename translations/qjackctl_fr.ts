<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr">
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="38"/>
        <source>(default)</source>
        <translation>(par défaut)</translation>
    </message>
    <message>
        <source>Usage</source>
        <translation type="obsolete">Utilisation</translation>
    </message>
    <message>
        <source>options</source>
        <translation type="obsolete">options</translation>
    </message>
    <message>
        <source>command-and-args</source>
        <translation type="obsolete">commande-et-args</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">Options</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="440"/>
        <source>Start JACK audio server immediately</source>
        <translation>Démarrer immédiatement le serveur audio JACK</translation>
    </message>
    <message>
        <source>Set default setings preset name</source>
        <translation type="obsolete">Régler le nom du préréglage des paramètres par défaut</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="448"/>
        <source>Show help about command line options</source>
        <translation>Montrer l&apos;aide à propos des options en ligne de commande</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="450"/>
        <source>Show version information</source>
        <translation>Montrer les informations de version</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="486"/>
        <source>Option -p requires an argument (preset).</source>
        <translation>L&apos;option -p nécessite un argument (préréglage).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="435"/>
        <source>Usage: %1 [options] [command-and-args]</source>
        <translation>Utilisation : %1 [options] [commandes-et-args]</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="438"/>
        <source>Options:</source>
        <translation>Options :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="444"/>
        <source>Set active patchbay definition file</source>
        <translation>Choisir le fichiers de définition de baie de brassage actif</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="446"/>
        <source>Set default JACK audio server name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="495"/>
        <source>Option -a requires an argument (path).</source>
        <translation>L&apos;option -a requiert un argument (chemin).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="505"/>
        <source>Option -n requires an argument (name).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="517"/>
        <source>Qt: %1
</source>
        <translation>Qt : %1
</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="442"/>
        <source>Set default settings preset name</source>
        <translation>Régler le nom du préréglage des paramètres par défaut</translation>
    </message>
</context>
<context>
    <name>qjackctlAboutForm</name>
    <message>
        <location filename="../src/qjackctlAboutForm.ui" line="36"/>
        <source>About QjackCtl</source>
        <translation>À propos de QjackCtl</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.ui" line="80"/>
        <source>About Qt</source>
        <translation>À propos de Qt</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.ui" line="67"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.ui" line="70"/>
        <source>Alt+C</source>
        <translation>Alt+F</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="43"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="44"/>
        <source>Build</source>
        <translation>Compilation</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="47"/>
        <source>Debugging option enabled.</source>
        <translation>Option de débogage activée.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="53"/>
        <source>System tray disabled.</source>
        <translation>Zone de notification système désactivée.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="59"/>
        <source>Transport status control disabled.</source>
        <translation>Contrôle du statut du déplacement désactivé.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="65"/>
        <source>Realtime status disabled.</source>
        <translation>Statut temps réel désactivé.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="71"/>
        <source>XRUN delay status disabled.</source>
        <translation>Statut du délai de désynchronisation (XRUN) désactivé.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="77"/>
        <source>Maximum delay status disabled.</source>
        <translation>Statut du délai maximal désactivé.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="96"/>
        <source>ALSA/MIDI sequencer support disabled.</source>
        <translation>Prise en charge du séquenceur ALSA/MIDI désactivé.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="110"/>
        <source>Website</source>
        <translation>Site web</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="115"/>
        <source>This program is free software; you can redistribute it and/or modify it</source>
        <translation>Ce programme est libre; vous pouvez le redistribuer et/ou le modifier</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="116"/>
        <source>under the terms of the GNU General Public License version 2 or later.</source>
        <translation>selon les termes de la Licence Publique Générale GNU version 2 ou ultérieure.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="83"/>
        <source>JACK MIDI support disabled.</source>
        <translation>Prise en charge de JACK MIDI désactivé.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="89"/>
        <source>JACK Port aliases support disabled.</source>
        <translation>Prise en charge des alias de port JACK désactivé.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="104"/>
        <source>D-Bus interface support disabled.</source>
        <translation>Prise en charge de l&apos;interface D-Bus désactivé.</translation>
    </message>
</context>
<context>
    <name>qjackctlClientListView</name>
    <message>
        <source>Readable Clients</source>
        <translation type="obsolete">Clients en lecture</translation>
    </message>
    <message>
        <source>Output Ports</source>
        <translation type="obsolete">Ports de sortie</translation>
    </message>
    <message>
        <source>Writable Clients</source>
        <translation type="obsolete">Clients en ecriture</translation>
    </message>
    <message>
        <source>Input Ports</source>
        <translation type="obsolete">Ports d&apos;entrée</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="986"/>
        <source>&amp;Connect</source>
        <translation>&amp;Connecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="987"/>
        <source>Alt+C</source>
        <comment>Connect</comment>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="990"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Déconnecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="991"/>
        <source>Alt+D</source>
        <comment>Disconnect</comment>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="994"/>
        <source>Disconnect &amp;All</source>
        <translation>&amp;Tout déconnecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="995"/>
        <source>Alt+A</source>
        <comment>Disconect All</comment>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1000"/>
        <source>Re&amp;name</source>
        <translation>Re&amp;nommer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1001"/>
        <source>Alt+N</source>
        <comment>Rename</comment>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1007"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Rafraîchir</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1008"/>
        <source>Alt+R</source>
        <comment>Refresh</comment>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="664"/>
        <source>Readable Clients / Output Ports</source>
        <translation>Clients en lecture / Ports de sortie</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="666"/>
        <source>Writable Clients / Input Ports</source>
        <translation>Clients en ecriture / Ports d&apos;entrée</translation>
    </message>
</context>
<context>
    <name>qjackctlConnect</name>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1756"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <source>This will suspend sound processing</source>
        <translation type="obsolete">Cela va suspendre le traitement du son</translation>
    </message>
    <message>
        <source>from all client applications.</source>
        <translation type="obsolete">de toutes les applications clientes.</translation>
    </message>
    <message>
        <source>Are you sure?</source>
        <translation type="obsolete">Êtes-vous certain?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Non</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1757"/>
        <source>This will suspend sound processing
from all client applications.

Are you sure?</source>
        <translation>Cela va suspendre le traitement du son
de toutes les applications clientes.

Êtes-vous certain?</translation>
    </message>
</context>
<context>
    <name>qjackctlConnectionsForm</name>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="44"/>
        <source>Connections - JACK Audio Connection Kit</source>
        <translation>Connexions - Kit de Connexion Audio JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="63"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="101"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="219"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="337"/>
        <source>&amp;Connect</source>
        <translation>&amp;Connecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="107"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="225"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="343"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="98"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="216"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="334"/>
        <source>Connect currently selected ports</source>
        <translation>Connecter les ports actuellement sélectionnés</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="117"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="235"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="353"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Déconnecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="123"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="241"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="359"/>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="114"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="232"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="350"/>
        <source>Disconnect currently selected ports</source>
        <translation>Déconnecter les ports actuellement sélectionnés</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="133"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="251"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="369"/>
        <source>Disconnect &amp;All</source>
        <translation>&amp;Tout déconnecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="139"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="257"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="375"/>
        <source>Alt+A</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="130"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="248"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="366"/>
        <source>Disconnect all currently connected ports</source>
        <translation>Déconnecter tous les ports actuellement connectés</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="165"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="283"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="401"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Rafraîchir</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="171"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="289"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="407"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="162"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="280"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="398"/>
        <source>Refresh current connections view</source>
        <translation>Rafraîchir la vue actuelle des connexions</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="181"/>
        <source>MIDI</source>
        <translation>MIDI</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.cpp" line="239"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <source>The preset aliases have been changed:</source>
        <translation type="obsolete">Les alias de préréglage ont été changés:</translation>
    </message>
    <message>
        <source>Do you want to save the changes?</source>
        <translation type="obsolete">Voulez-vous enregistrer les changements?</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Enregistrer</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="obsolete">Ignorer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="299"/>
        <source>ALSA</source>
        <translation>ALSA</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.cpp" line="240"/>
        <source>The preset aliases have been changed:

&quot;%1&quot;

Do you want to save the changes?</source>
        <translation>Les alias de préréglage ont été changés :

&quot;%1&quot;

Voulez-vous enregistrer les changements?</translation>
    </message>
</context>
<context>
    <name>qjackctlConnectorView</name>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1173"/>
        <source>&amp;Connect</source>
        <translation>&amp;Connecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1174"/>
        <source>Alt+C</source>
        <comment>Connect</comment>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1177"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Déconnecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1178"/>
        <source>Alt+D</source>
        <comment>Disconnect</comment>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1181"/>
        <source>Disconnect &amp;All</source>
        <translation>&amp;Tout déconnecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1182"/>
        <source>Alt+A</source>
        <comment>Disconect All</comment>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1187"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Rafraîchir</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1188"/>
        <source>Alt+R</source>
        <comment>Refresh</comment>
        <translation>Alt+R</translation>
    </message>
</context>
<context>
    <name>qjackctlMainForm</name>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="49"/>
        <source>QjackCtl</source>
        <translation>QjackCtl</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="395"/>
        <source>Alt+Q</source>
        <translation>Alt+Q</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="388"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3169"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="385"/>
        <source>Quit processing and exit</source>
        <translation>Quitter le traitement et sortir</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="86"/>
        <source>Alt+S</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="79"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3087"/>
        <source>&amp;Start</source>
        <translation>&amp;Démarrer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="76"/>
        <source>Start the JACK server</source>
        <translation>Démarrer le serveur JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="124"/>
        <source>Alt+T</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="117"/>
        <source>S&amp;top</source>
        <translation>&amp;Arrêter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="114"/>
        <source>Stop the JACK server</source>
        <translation>Arrêter le serveur JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="423"/>
        <source>Levels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="448"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3128"/>
        <source>St&amp;atus</source>
        <translation>S&amp;tatut</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="445"/>
        <source>Show/hide the extended status window</source>
        <translation>Montrer/cacher la fenêtre de statut étendu</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="204"/>
        <source>Alt+B</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="353"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3164"/>
        <source>Ab&amp;out...</source>
        <translation>À propo&amp;s...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="350"/>
        <source>Show information about this application</source>
        <translation>Montrer des informations à propos de cette application</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="620"/>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="613"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3160"/>
        <source>S&amp;etup...</source>
        <translation>R&amp;églages...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="610"/>
        <source>Show settings and options dialog</source>
        <translation>Montrer la fenêtre d&apos;options et de paramètres</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="569"/>
        <source>Alt+M</source>
        <translation>Alt+M</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="562"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3124"/>
        <source>&amp;Messages</source>
        <translation>&amp;Messages</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="559"/>
        <source>Show/hide the messages log window</source>
        <translation>Montrer/cacher la fenêtre du journal des messages</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="524"/>
        <source>&amp;Patchbay</source>
        <translation>&amp;Brassage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="521"/>
        <source>Show/hide the patchbay editor window</source>
        <translation>Montrer/cacher la fenêtre de l&apos;éditeur de baie de brassage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="493"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="486"/>
        <source>&amp;Connect</source>
        <translation>&amp;Connecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="483"/>
        <source>Show/hide the actual connections patchbay window</source>
        <translation>Montrer/cacher la fenêtre des connexions actuelles de la baie de brassage</translation>
    </message>
    <message>
        <source>JACK server state</source>
        <translation type="obsolete">État du serveur JACK</translation>
    </message>
    <message>
        <source>JACK server mode</source>
        <translation type="obsolete">Mode du serveur JACK</translation>
    </message>
    <message>
        <source>CPU Load</source>
        <translation type="obsolete">Charge processeur</translation>
    </message>
    <message>
        <source>Sample rate</source>
        <translation type="obsolete">Fréquence d&apos;échantillonnage</translation>
    </message>
    <message>
        <source>XRUN Count</source>
        <translation type="obsolete">Décompte des désynchronisations (XRUN)</translation>
    </message>
    <message>
        <source>Time display</source>
        <translation type="obsolete">Horloge</translation>
    </message>
    <message>
        <source>Transport state</source>
        <translation type="obsolete">État du déplacement</translation>
    </message>
    <message>
        <source>Transport BPM</source>
        <translation type="obsolete">BPM du déplacement</translation>
    </message>
    <message>
        <source>Transport time</source>
        <translation type="obsolete">Horaire du déplacement</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="197"/>
        <source>&amp;Backward</source>
        <translation>A&amp;rrière</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="194"/>
        <source>Backward transport</source>
        <translation>Déplacer en arrière</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="312"/>
        <source>Alt+F</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="305"/>
        <source>&amp;Forward</source>
        <translation>A&amp;vant</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="302"/>
        <source>Forward transport</source>
        <translation>Déplacer en avant</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="172"/>
        <source>Alt+R</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="165"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3143"/>
        <source>&amp;Rewind</source>
        <translation>Remb&amp;obiner</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="162"/>
        <source>Rewind transport</source>
        <translation>Rembobiner</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+P</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="273"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3152"/>
        <source>Pa&amp;use</source>
        <translation>Pa&amp;use</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="270"/>
        <source>Stop transport rolling</source>
        <translation>Arrêter le déplacement</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="531"/>
        <source>Alt+P</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="232"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3149"/>
        <source>&amp;Play</source>
        <translation>&amp;Lecture</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="229"/>
        <source>Start transport rolling</source>
        <translation>Démarrer le déplacement</translation>
    </message>
    <message>
        <source>Could not open ALSA sequencer as a client.

MIDI patchbay will be not available.</source>
        <translation type="obsolete">Impossible d&apos;ouvrir le sequenceur ALSA comme client.

La baie de brassage MIDI ne sera pas disponible.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="752"/>
        <location filename="../src/qjackctlMainForm.cpp" line="903"/>
        <location filename="../src/qjackctlMainForm.cpp" line="1216"/>
        <location filename="../src/qjackctlMainForm.cpp" line="2261"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3182"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <source>JACK is currently running.</source>
        <translation type="obsolete">JACK fonctionne actuellement.</translation>
    </message>
    <message>
        <source>Do you want to terminate the JACK audio server?</source>
        <translation type="obsolete">Voulez-vous arrêter le serveur audio JACK?</translation>
    </message>
    <message>
        <source>Terminate</source>
        <translation type="obsolete">Arrêter</translation>
    </message>
    <message>
        <source>Leave</source>
        <translation type="obsolete">Laisser</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="856"/>
        <source>successfully</source>
        <translation>avec succès</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="858"/>
        <source>with exit status=%1</source>
        <translation>avec statut de sortie=%1</translation>
    </message>
    <message>
        <source>Could not start JACK.</source>
        <translation type="obsolete">Impossible de démarrer JACK.</translation>
    </message>
    <message>
        <source>Maybe JACK audio server is already started.</source>
        <translation type="obsolete">Peut-être que le serveur audio JACK est déjà démarré.</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="obsolete">Arrêter</translation>
    </message>
    <message>
        <source>Kill</source>
        <translation type="obsolete">Tuer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="938"/>
        <source>Could not load preset &quot;%1&quot;.

Retrying with default.</source>
        <translation>Impossible de charger le préréglage &quot;%1&quot;.

Nouvel essai avec celui par défaut.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="941"/>
        <source>Could not load default preset.

Sorry.</source>
        <translation>Impossible de charger le préréglage par défaut.

Désolé.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="951"/>
        <source>Startup script...</source>
        <translation>Script de démarrage...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="952"/>
        <source>Startup script terminated</source>
        <translation>Script de démarrage terminé</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1190"/>
        <source>JACK is starting...</source>
        <translation>JACK démarre...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2081"/>
        <source>Could not start JACK.

Sorry.</source>
        <translation>Impossible de démarrer JACK.

Désolé.</translation>
    </message>
    <message>
        <source>JACK was started with PID=%1 (0x%2).</source>
        <translation type="obsolete">JACK a été démarrer avec le PID=%1 (0x%2).</translation>
    </message>
    <message>
        <source>Some client audio applications</source>
        <translation type="obsolete">Certaines applications audio clientes</translation>
    </message>
    <message>
        <source>are still active and connected.</source>
        <translation type="obsolete">sont encore actives et connectées.</translation>
    </message>
    <message>
        <source>Do you want to stop the JACK audio server?</source>
        <translation type="obsolete">Voulez-vous arrêter le serveur audio JACK?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1259"/>
        <source>JACK is stopping...</source>
        <translation>JACK s&apos;arrête...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1252"/>
        <source>Shutdown script...</source>
        <translation>Script d&apos;extinction...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1253"/>
        <source>Shutdown script terminated</source>
        <translation>Script d&apos;extinction terminé</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1293"/>
        <location filename="../src/qjackctlMainForm.cpp" line="1422"/>
        <source>Post-shutdown script...</source>
        <translation>Script post-extinction...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1294"/>
        <location filename="../src/qjackctlMainForm.cpp" line="1423"/>
        <source>Post-shutdown script terminated</source>
        <translation>Script post-extinction terminé</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1397"/>
        <source>JACK was stopped</source>
        <translation>JACK a été arrêté</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1520"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1634"/>
        <source>Transport BBT (bar:beat.ticks)</source>
        <translation>MTB (mesure:temps.battement) du déplacement</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1635"/>
        <source>Transport time code</source>
        <translation>Code temporel (Timecode) du déplacement</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1646"/>
        <source>Elapsed time since last reset</source>
        <translation>Temps écoulé depuis la dernière réinitialisation</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1649"/>
        <source>Elapsed time since last XRUN</source>
        <translation>Temps écoulé depuis la dernière désynchronisation (XRUN)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1770"/>
        <source>Could not load active patchbay definition.

Disabled.</source>
        <translation>Impossible de charger la définition de baie de brassage active.

Désactivé.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1773"/>
        <source>Patchbay activated.</source>
        <translation>Baie de brassage activée.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1781"/>
        <source>Patchbay deactivated.</source>
        <translation>Baie de brassage désactivée.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1859"/>
        <source>Statistics reset.</source>
        <translation>Réinitialisation des statistiques.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1998"/>
        <source>msec</source>
        <translation>ms</translation>
    </message>
    <message>
        <source>Audio connection graph change.</source>
        <translation type="obsolete">Changement du graphique des connexions audio.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2042"/>
        <source>XRUN callback (%1).</source>
        <translation>Récupération désynchronisation (XRUN) (%1).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2052"/>
        <source>Buffer size change (%1).</source>
        <translation>Changement de la taille du tampon (%1).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2061"/>
        <source>Shutdown notification.</source>
        <translation>Notification d&apos;extinction.</translation>
    </message>
    <message>
        <source>MIDI connection graph change.</source>
        <translation type="obsolete">Changement du graphique des connexions MIDI.</translation>
    </message>
    <message>
        <source>Audio active patchbay scan</source>
        <translation type="obsolete">Balayage de la baie de brassage audio active</translation>
    </message>
    <message>
        <source>MIDI active patchbay scan</source>
        <translation type="obsolete">Balayage de la baie de brassage MIDI active</translation>
    </message>
    <message>
        <source>Audio connection change.</source>
        <translation type="obsolete">Changement des connexions audio.</translation>
    </message>
    <message>
        <source>MIDI connection change.</source>
        <translation type="obsolete">Changement des connexions MIDI.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2230"/>
        <source>checked</source>
        <translation>vérifié</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2234"/>
        <source>connected</source>
        <translation>connecté</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2238"/>
        <source>disconnected</source>
        <translation>déconnecté</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2243"/>
        <source>failed</source>
        <translation>échoué</translation>
    </message>
    <message>
        <source>Could not connect to JACK server as client.

Please check the messages window for more info.</source>
        <translation type="obsolete">Impossible de connecter le serveur JACK comme client.

Veuillez consulter la fenêtre des messages pour plus d&apos;informations.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2404"/>
        <source>Server configuration saved to &quot;%1&quot;.</source>
        <translation>Configuration du serveur enregistrée dans &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2426"/>
        <source>Client activated.</source>
        <translation>Client activé.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2434"/>
        <source>Post-startup script...</source>
        <translation>Script post-démarrage...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2435"/>
        <source>Post-startup script terminated</source>
        <translation>Script post-démarrage terminé</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2443"/>
        <source>Command line argument...</source>
        <translation>Argument de ligne de commande...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2444"/>
        <source>Command line argument started</source>
        <translation>Argument de ligne de commande démarré</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2469"/>
        <source>Client deactivated.</source>
        <translation>Client désactivé.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2700"/>
        <source>Transport rewind.</source>
        <translation>Déplacement en rembobinage.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2720"/>
        <source>Transport backward.</source>
        <translation>Déplacement en marche arrière.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2745"/>
        <location filename="../src/qjackctlMainForm.cpp" line="2852"/>
        <location filename="../src/qjackctlMainForm.cpp" line="2957"/>
        <source>Starting</source>
        <translation>Démarre</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2747"/>
        <source>Transport start.</source>
        <translation>Déplacement démarré.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2760"/>
        <location filename="../src/qjackctlMainForm.cpp" line="2964"/>
        <source>Stopping</source>
        <translation>S&apos;arrête</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2762"/>
        <source>Transport stop.</source>
        <translation>Déplacement arrêté.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2782"/>
        <source>Transport forward.</source>
        <translation>Déplacement en marche avant.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2798"/>
        <location filename="../src/qjackctlMainForm.cpp" line="2968"/>
        <source>Stopped</source>
        <translation>Arrêté</translation>
    </message>
    <message>
        <source>Hz</source>
        <translation type="obsolete">Hz</translation>
    </message>
    <message>
        <source>frames</source>
        <translation type="obsolete">échantillons</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2845"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2845"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <source>RT</source>
        <translation type="obsolete">TR</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2855"/>
        <source>Rolling</source>
        <translation>Défile</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2858"/>
        <source>Looping</source>
        <translation>Boucle</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2902"/>
        <source>XRUN callback (%1 skipped).</source>
        <translation>Récupération de désynchronisation (XRUN) (%1 sauté).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2961"/>
        <source>Started</source>
        <translation>Démarré</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2971"/>
        <source>Active</source>
        <translation>Actif</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2974"/>
        <source>Activating</source>
        <translation>Activation</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2979"/>
        <source>Inactive</source>
        <translation>Inactif</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3077"/>
        <source>&amp;Hide</source>
        <translation>Cac&amp;her</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3077"/>
        <source>Mi&amp;nimize</source>
        <translation>Mi&amp;nimiser</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3079"/>
        <source>S&amp;how</source>
        <translation>M&amp;ontrer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3079"/>
        <source>Rest&amp;ore</source>
        <translation>R&amp;estaurer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3090"/>
        <source>&amp;Stop</source>
        <translation>&amp;Arrêter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3093"/>
        <source>&amp;Reset</source>
        <translation>Ré&amp;initialiser</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3099"/>
        <source>&amp;Presets</source>
        <translation>&amp;Préréglages</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3132"/>
        <source>&amp;Connections</source>
        <translation>&amp;Connexions</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3136"/>
        <source>Patch&amp;bay</source>
        <translation>&amp;Brassage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3141"/>
        <source>&amp;Transport</source>
        <translation>Déplacemen&amp;t</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3184"/>
        <source>Server settings will be only effective after
restarting the JACK audio server.</source>
        <translation>Les paramètres du serveur ne seront effectifs
qu&apos;après avoir redémarré le serveur audio JACK.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="728"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3207"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3209"/>
        <source>Some settings will be only effective
the next time you start this program.</source>
        <translation>Certain paramètres ne seront effectifs
qu&apos;au prochain démarrage de ce programme.</translation>
    </message>
    <message>
        <source>DSP Load</source>
        <translation type="obsolete">Charge DSP</translation>
    </message>
    <message>
        <source>XRUN Count (notifications)</source>
        <translation type="obsolete">Décompte des désynchronisations (notification XRUN)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2015"/>
        <source>JACK connection graph change.</source>
        <translation>Changement du graphique des connexions JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2115"/>
        <source>ALSA connection graph change.</source>
        <translation>Changement du graphique des connexions ALSA.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2202"/>
        <source>JACK connection change.</source>
        <translation>Changement des connexions JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2211"/>
        <source>ALSA connection change.</source>
        <translation>Changement des connexions ALSA.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="753"/>
        <source>JACK is currently running.

Do you want to terminate the JACK audio server?</source>
        <translation>JACK fonctionne actuellement.

Voulez-vous arrêter le serveur audio JACK?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="649"/>
        <source>D-BUS: Service is available (%1 aka jackdbus).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="680"/>
        <source>D-BUS: Service not available (%1 aka jackdbus).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="730"/>
        <source>The program will keep running in the system tray.

To terminate the program, please choose &quot;Quit&quot;
in the context menu of the system tray icon.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="904"/>
        <source>Could not start JACK.

Maybe JACK audio server is already started.</source>
        <translation>Impossible de démarrer JACK.

Peut-être que le serveur audio JACK est déjà démarré.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1150"/>
        <source>D-BUS: JACK server is starting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1153"/>
        <source>D-BUS: JACK server could not be started.

Sorry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1217"/>
        <source>Some client audio applications
are still active and connected.

Do you want to stop the JACK audio server?</source>
        <translation>Certaines applications audio clientes
sont encore actives et connectées.

Voulez-vous arrêter le serveur audio JACK?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1274"/>
        <source>D-BUS: JACK server is stopping...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1277"/>
        <source>D-BUS: JACK server could not be stopped.

Sorry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1343"/>
        <source>JACK was started with PID=%1.</source>
        <translation>JACK a été démarrer avec le PID=%1.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1351"/>
        <source>D-BUS: JACK server was started (%1 aka jackdbus).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1390"/>
        <source>JACK is being forced...</source>
        <translation>JACK est forcé...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1410"/>
        <source>D-BUS: JACK server was stopped (%1 aka jackdbus).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2085"/>
        <source>JACK has crashed.</source>
        <translation>JACK a planté.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2088"/>
        <source>JACK timed out.</source>
        <translation>JACK n&apos;a pas répondu à temps.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2091"/>
        <source>JACK write error.</source>
        <translation>Erreur d&apos;écriture JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2094"/>
        <source>JACK read error.</source>
        <translation>Erreur de lecture JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2098"/>
        <source>Unknown JACK error (%d).</source>
        <translation>Erreur JACK inconnue (%d).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2339"/>
        <source>Overall operation failed.</source>
        <translation>L&apos;opération a échoué.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2341"/>
        <source>Invalid or unsupported option.</source>
        <translation>Option invalide ou non prise en charge.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2343"/>
        <source>Client name not unique.</source>
        <translation>Nom de client non unique.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2345"/>
        <source>Server is started.</source>
        <translation>Le serveur est démarré.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2347"/>
        <source>Unable to connect to server.</source>
        <translation>Incapable de se connecter au serveur.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2349"/>
        <source>Server communication error.</source>
        <translation>Erreur de communication serveur.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2351"/>
        <source>Client does not exist.</source>
        <translation>Le client n&apos;existe pas.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2353"/>
        <source>Unable to load internal client.</source>
        <translation>Incapable de charger le client interne.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2355"/>
        <source>Unable to initialize client.</source>
        <translation>Incapable d&apos;initialiser le client.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2357"/>
        <source>Unable to access shared memory.</source>
        <translation>Incapable d&apos;accéder à la mémoire partagée.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2359"/>
        <source>Client protocol version mismatch.</source>
        <translation>Mauvaise version du protocole client.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2361"/>
        <source>Could not connect to JACK server as client.
- %1
Please check the messages window for more info.</source>
        <translation>Impossible de connecter le serveur JACK comme client.
- %1
Veuillez consulter la fenêtre des messages pour plus d&apos;informations.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2833"/>
        <source>%1 (%2%)</source>
        <translation>%1 (%2%)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2836"/>
        <source>%1 %</source>
        <translation>%1 %</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2838"/>
        <source>%1 Hz</source>
        <translation>%1 Hz</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2840"/>
        <source>%1 frames</source>
        <translation>%1 échantillons</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2895"/>
        <source>%1 msec</source>
        <translation>%1 ms</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3443"/>
        <source>D-BUS: SetParameterValue(&apos;%1&apos;, &apos;%2&apos;):

%3.
(%4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3474"/>
        <source>D-BUS: ResetParameterValue(&apos;%1&apos;):

%2.
(%3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3505"/>
        <source>D-BUS: GetParameterValue(&apos;%1&apos;):

%2.
(%3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="597"/>
        <source>Could not open ALSA sequencer as a client.

ALSA MIDI patchbay will be not available.</source>
        <translation>Impossible d&apos;ouvrir le sequenceur ALSA comme client.

La baie de brassage ALSA MIDI ne sera pas disponible.</translation>
    </message>
    <message>
        <source>The program will keep running in the system tray.

To terminate the program, please choose &quot;Quit&quot; in the context menu of the system tray entry.</source>
        <translation type="obsolete">Le programme continuera de s&apos;exécuter dans la zone de notification système.

Pour terminer le programme, merci de choisir &quot;Quitter&quot; dans le menu contextuel de la zone de notification système.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2146"/>
        <source>JACK active patchbay scan</source>
        <translation>Balayage de la baie de brassage JACK active</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2156"/>
        <source>ALSA active patchbay scan</source>
        <translation>Balayage de la baie de brassage ALSA active</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2262"/>
        <source>A patchbay definition is currently active,
which is probable to redo this connection:

%1 -&gt; %2

Do you want to remove the patchbay connection?</source>
        <translation>Une définition de baie de brassage est actuellement active,
il est probable que cela refasse cette connexion :

%1 -&gt; %2

Voulez-vous enlever la connexion de la baie de brassage?</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="obsolete">Enlever</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="455"/>
        <source>Alt+A</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="360"/>
        <source>Alt+O</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="280"/>
        <source>Shift+Space</source>
        <translation>Shift+Espace</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="239"/>
        <source>Space</source>
        <translation>Espace</translation>
    </message>
</context>
<context>
    <name>qjackctlMessagesForm</name>
    <message>
        <location filename="../src/qjackctlMessagesForm.ui" line="42"/>
        <source>Messages - JACK Audio Connection Kit</source>
        <translation>Messages - Kit de Connexion Audio JACK</translation>
    </message>
    <message>
        <source>Messages log</source>
        <translation type="obsolete">Messages</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMessagesForm.ui" line="57"/>
        <source>Messages output log</source>
        <translation>Journal des messages de sortie</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMessagesForm.cpp" line="136"/>
        <source>Logging stopped --- %1 ---</source>
        <translation>Journalisation arrêtée --- %1 ---</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMessagesForm.cpp" line="146"/>
        <source>Logging started --- %1 ---</source>
        <translation>Journalisation démarrée --- %1 ---</translation>
    </message>
</context>
<context>
    <name>qjackctlPatchbay</name>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1738"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <source>This will disconnect all sockets.</source>
        <translation type="obsolete">Cela va déconnecter toutes les prises.</translation>
    </message>
    <message>
        <source>Are you sure?</source>
        <translation type="obsolete">Êtes vous certain?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Non</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1739"/>
        <source>This will disconnect all sockets.

Are you sure?</source>
        <translation>Cela va déconnecter toutes les prises.

Êtes vous certain?</translation>
    </message>
</context>
<context>
    <name>qjackctlPatchbayForm</name>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="44"/>
        <source>Patchbay - JACK Audio Connection Kit</source>
        <translation>Baie de brassage - Kit de Connexion Audio JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="451"/>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="457"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="448"/>
        <source>Create a new patchbay profile</source>
        <translation>Créer un nouveau profil de baie de brassage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="470"/>
        <source>&amp;Load...</source>
        <translation>&amp;Charger...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="476"/>
        <source>Alt+L</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="467"/>
        <source>Load patchbay profile</source>
        <translation>Charger un profil de baie de brassage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="489"/>
        <source>&amp;Save...</source>
        <translation>&amp;Enregistrer...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="495"/>
        <source>Alt+S</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="486"/>
        <source>Save current patchbay profile</source>
        <translation>Enregistrer le profil actuel de la baie de brassage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="519"/>
        <source>Current (recent) patchbay profile(s)</source>
        <translation>Profil(s) actuel(s) (récent(s)) de la baie de brassage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="529"/>
        <source>Acti&amp;vate</source>
        <translation>Acti&amp;ver</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="535"/>
        <source>Alt+V</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="526"/>
        <source>Toggle activation of current patchbay profile</source>
        <translation>Basculer l&apos;activation du profil actuel de baie de brassage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="358"/>
        <source>&amp;Connect</source>
        <translation>&amp;Connecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="364"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="355"/>
        <source>Connect currently selected sockets</source>
        <translation>Connecter les prises actuellement sélectionnés</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="374"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Déconnecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="380"/>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="371"/>
        <source>Disconnect currently selected sockets</source>
        <translation>Déconnecter les prises actuellement sélectionnés</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="390"/>
        <source>Disconnect &amp;All</source>
        <translation>&amp;Tout déconnecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="396"/>
        <source>Alt+A</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="387"/>
        <source>Disconnect all currently connected sockets</source>
        <translation>Déconnecter les prises actuellement connectées</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="422"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Rafraîchir</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="428"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="419"/>
        <source>Refresh current patchbay view</source>
        <translation>Rafraîchir la vue actuelle de la baie de brassage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="108"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="236"/>
        <source>Down</source>
        <translation>Vers le Bas</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="105"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="233"/>
        <source>Move currently selected output socket down one position</source>
        <translation>Déplacer d&apos;une position vers le bas la prise de sortie actuellement sélectionnée</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="124"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="284"/>
        <source>Add...</source>
        <translation>Ajouter...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="121"/>
        <source>Create a new output socket</source>
        <translation>Créer une nouvelle prise de sortie</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="156"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="300"/>
        <source>Edit...</source>
        <translation>Editer...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="153"/>
        <source>Edit currently selected input socket properties</source>
        <translation>Editer les propriétés de la prise d&apos;entrée actuellement sélectionnée</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="188"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="316"/>
        <source>Up</source>
        <translation>Vers le haut</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="185"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="313"/>
        <source>Move currently selected output socket up one position</source>
        <translation>Déplacer d&apos;une position vers le haut la prise de sortie actuellement sélectionnée</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="204"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="252"/>
        <source>Remove</source>
        <translation>Enlever</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="201"/>
        <source>Remove currently selected output socket</source>
        <translation>Enlever la prise de sortie actuellement sélectionnée</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="220"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="268"/>
        <source>Copy...</source>
        <translation>Copier...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="217"/>
        <source>Duplicate (copy) currently selected output socket</source>
        <translation>Dupliquer (copier) la prise de sortie actuellement sélectionnée</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="249"/>
        <source>Remove currently selected input socket</source>
        <translation>Enlever la prise d&apos;entrée actuellement sélectionnée</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="265"/>
        <source>Duplicate (copy) currently selected input socket</source>
        <translation>Dupliquer (copier) la prise d&apos;entrée actuellement sélectionnée</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="281"/>
        <source>Create a new input socket</source>
        <translation>Créer une nouvelle prise d&apos;entrée</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="297"/>
        <source>Edit currently selected output socket properties</source>
        <translation>Editer les propriétés de la prise de sortie actuellement sélectionnée</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="221"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <source>The patchbay definition has been changed:</source>
        <translation type="obsolete">La définition de baie de brassage a été changée:</translation>
    </message>
    <message>
        <source>Do you want to save the changes?</source>
        <translation type="obsolete">Voulez-vous enregistrer les changements?</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Enregistrer</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="obsolete">Ignorer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <source>modified</source>
        <translation type="obsolete">modifié</translation>
    </message>
    <message>
        <source>Untitled</source>
        <translation type="obsolete">Sans titre</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="364"/>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="394"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Could not load patchbay definition file:</source>
        <translation type="obsolete">Impossible de charger le fichier de définition de baie de brassage:</translation>
    </message>
    <message>
        <source>Could not save patchbay definition file:</source>
        <translation type="obsolete">Impossible d&apos;enregistrer le fichier de définition de baie de brassage:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="439"/>
        <source>New Patchbay definition</source>
        <translation>Nouvelle définition de baie de brassage</translation>
    </message>
    <message>
        <source>Create patchbay definition as a snapshot</source>
        <translation type="obsolete">Prendre un cliché de toutes les connexions clientes actuelles</translation>
    </message>
    <message>
        <source>of all actual client connections?</source>
        <translation type="obsolete">pour créer une définition de baie de brassage?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Non</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="473"/>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="493"/>
        <source>Patchbay Definition files</source>
        <translation>Fichiers de définition de baie de brassage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="471"/>
        <source>Load Patchbay Definition</source>
        <translation>Charger une définition de baie de brassage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="491"/>
        <source>Save Patchbay Definition</source>
        <translation>Enregistrer la définition de baie de brassage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="598"/>
        <source>active</source>
        <translation>actif</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="222"/>
        <source>The patchbay definition has been changed:

&quot;%1&quot;

Do you want to save the changes?</source>
        <translation>La définition de baie de brassage a été changée :

&quot;%1&quot;

Voulez-vous enregistrer les changements?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="278"/>
        <source>%1 [modified]</source>
        <translation>%1 [modifié]</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="345"/>
        <source>Untitled%1</source>
        <translation>SansTitre%1</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="365"/>
        <source>Could not load patchbay definition file: 

&quot;%1&quot;</source>
        <translation>Impossible de charger le fichier de définition de baie de brassage :

&quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="395"/>
        <source>Could not save patchbay definition file: 

&quot;%1&quot;</source>
        <translation>Impossible d&apos;enregistrer le fichier de définition de baie de brassage :

&quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="440"/>
        <source>Create patchbay definition as a snapshot
of all actual client connections?</source>
        <translation>Prendre un cliché de toutes les connexions clientes actuelles
pour créer une définition de baie de brassage?</translation>
    </message>
</context>
<context>
    <name>qjackctlPatchbayView</name>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1280"/>
        <source>Add...</source>
        <translation>Ajouter...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1282"/>
        <source>Edit...</source>
        <translation>Editer...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1285"/>
        <source>Copy...</source>
        <translation>Copier...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1288"/>
        <source>Remove</source>
        <translation>Enlever</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1292"/>
        <source>Exclusive</source>
        <translation>Exclusif</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1337"/>
        <source>(None)</source>
        <translation>(Aucun)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1298"/>
        <source>Forward</source>
        <translation>Renvoi</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1351"/>
        <source>Move Up</source>
        <translation>Vers le haut</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1354"/>
        <source>Move Down</source>
        <translation>Vers le bas</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1360"/>
        <source>&amp;Connect</source>
        <translation>&amp;Connecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1361"/>
        <source>Alt+C</source>
        <comment>Connect</comment>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1364"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Déconnecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1365"/>
        <source>Alt+D</source>
        <comment>Disconnect</comment>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1368"/>
        <source>Disconnect &amp;All</source>
        <translation>&amp;Tout déconnecter</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1369"/>
        <source>Alt+A</source>
        <comment>Disconect All</comment>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1374"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Rafraîchir</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1375"/>
        <source>Alt+R</source>
        <comment>Refresh</comment>
        <translation>Alt+R</translation>
    </message>
</context>
<context>
    <name>qjackctlSetupForm</name>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="41"/>
        <source>Setup - JACK Audio Connection Kit</source>
        <translation>Réglages - Kit de Connexion Audio JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="66"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="86"/>
        <source>Preset &amp;Name:</source>
        <translation>&amp;Nom du préréglage :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="114"/>
        <location filename="../src/qjackctlSetupForm.ui" line="885"/>
        <location filename="../src/qjackctlSetupForm.ui" line="976"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1046"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1134"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1196"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1216"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1340"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1663"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1863"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4249"/>
        <source>(default)</source>
        <translation>(par défaut)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="107"/>
        <source>Settings preset name</source>
        <translation>Nom du préréglage des paramètres</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="125"/>
        <source>&amp;Save</source>
        <translation>&amp;Enregistrer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="131"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2018"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3769"/>
        <source>Alt+S</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="122"/>
        <source>Save settings as current preset name</source>
        <translation>Enregistrer les paramètres sous le nom du préréglage actuel</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="144"/>
        <source>&amp;Delete</source>
        <translation>E&amp;ffacer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="150"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2037"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3211"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3865"/>
        <source>Alt+D</source>
        <translation>Alt+F</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="141"/>
        <source>Delete current settings preset</source>
        <translation>Effacer le préréglage des paramètres actuel</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="168"/>
        <source>Server</source>
        <translation>Serveur</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="189"/>
        <source>Server &amp;Path:</source>
        <translation>&amp;Chemin du serveur :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="236"/>
        <source>jackstart</source>
        <translation>jackstart</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="226"/>
        <source>jackd</source>
        <translation>jackd</translation>
    </message>
    <message>
        <source>jackd-realtime</source>
        <translation type="obsolete">jackd-realtime</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="219"/>
        <source>The JACK Audio Connection Kit sound server path</source>
        <translation>Chemin du serveur de son du Kit de Connexion Audio JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="250"/>
        <source>Driv&amp;er:</source>
        <translation>Pilot&amp;e :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="279"/>
        <source>dummy</source>
        <translation>factice</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="289"/>
        <source>oss</source>
        <translation>oss</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="294"/>
        <source>alsa</source>
        <translation>alsa</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="299"/>
        <source>portaudio</source>
        <translation>portaudio</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="304"/>
        <source>coreaudio</source>
        <translation>coreaudio</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="309"/>
        <source>freebob</source>
        <translation>freebob</translation>
    </message>
    <message>
        <source>The software driver to use</source>
        <translation type="obsolete">Pilote logiciel à utiliser</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="341"/>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1396"/>
        <source>Number of periods in the hardware buffer</source>
        <translation>Nombre de périodes dans le tampon matériel</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1421"/>
        <source>Priorit&amp;y:</source>
        <translation>Pri&amp;orité :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1443"/>
        <source>&amp;Frames/Period:</source>
        <translation>&amp;Échantillons/Période :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1472"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1730"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1477"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1735"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1482"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1740"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1487"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1778"/>
        <source>128</source>
        <translation>128</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1492"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1783"/>
        <source>256</source>
        <translation>256</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1497"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1788"/>
        <source>512</source>
        <translation>512</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1502"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1793"/>
        <source>1024</source>
        <translation>1024</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1507"/>
        <source>2048</source>
        <translation>2048</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1512"/>
        <source>4096</source>
        <translation>4096</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1465"/>
        <source>Frames per period between process() calls</source>
        <translation>Échantillons par période entre appels de process()</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1526"/>
        <source>Port Ma&amp;ximum:</source>
        <translation>Nombre de port ma&amp;ximal :</translation>
    </message>
    <message>
        <source>&amp;Channel:</source>
        <translation type="obsolete">&amp;Canal:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1592"/>
        <source>21333</source>
        <translation>21333</translation>
    </message>
    <message>
        <source>Number o microseconds to wait between engine processes (dummy)</source>
        <translation type="obsolete">Nombre de microsecondes à attendre entre les traitements du moteur (factice)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1613"/>
        <source>22050</source>
        <translation>22050</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1618"/>
        <source>32000</source>
        <translation>32000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1623"/>
        <source>44100</source>
        <translation>44100</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1628"/>
        <source>48000</source>
        <translation>48000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1633"/>
        <source>88200</source>
        <translation>88200</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1638"/>
        <source>96000</source>
        <translation>96000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1643"/>
        <source>192000</source>
        <translation>192000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1606"/>
        <source>Sample rate in frames per second</source>
        <translation>Fréquence d&apos;échantillonage en échantillons par seconde</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1657"/>
        <source>Scheduler priority when running realtime</source>
        <translation>Priorité de l&apos;ordonnanceur quand fonctionne en temps réel</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1676"/>
        <source>&amp;Word Length:</source>
        <translation>&amp;Résolution (bit) :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1698"/>
        <source>Periods/&amp;Buffer:</source>
        <translation>Périodes/&amp;Tampon :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1723"/>
        <source>Word length</source>
        <translation>Résolution</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1768"/>
        <source>Maximum number of ports the JACK server can manage</source>
        <translation>Nombre maximal de ports que peut gérer le serveur JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1807"/>
        <source>&amp;Wait (usec):</source>
        <translation>&amp;Attente (en µs) :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1835"/>
        <source>Sample &amp;Rate:</source>
        <translation>&amp;Fréquence d&apos;échantillonnage (Hz) :</translation>
    </message>
    <message>
        <source>PCM channel</source>
        <translation type="obsolete">Canal PCM</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1883"/>
        <source>&amp;Timeout (msec):</source>
        <translation>&amp;Décompte (en ms) :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1915"/>
        <source>200</source>
        <translation>200</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1920"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3386"/>
        <source>500</source>
        <translation>500</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1925"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3391"/>
        <source>1000</source>
        <translation>1000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1930"/>
        <source>2000</source>
        <translation>2000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1935"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3401"/>
        <source>5000</source>
        <translation>5000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1940"/>
        <source>10000</source>
        <translation>10000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1905"/>
        <source>Set client timeout limit in miliseconds</source>
        <translation>Régler le limite du décompte client en millisecondes</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="584"/>
        <source>&amp;Realtime</source>
        <translation>Temps &amp;réel</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="587"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2978"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4114"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="581"/>
        <source>Use realtime scheduling</source>
        <translation>Utiliser ordonnancement temps réel</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="603"/>
        <source>No Memory Loc&amp;k</source>
        <translation>P&amp;as de verrouillage mémoire</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="606"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3230"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3808"/>
        <source>Alt+K</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="600"/>
        <source>Do not attempt to lock memory, even if in realtime mode</source>
        <translation>Ne pas essayer de verrouiller la mémoire même en mode temps-réel</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="622"/>
        <source>&amp;Unlock Memory</source>
        <translation>&amp;Déverrouiller la mémoire</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="625"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1999"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3972"/>
        <source>Alt+U</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="619"/>
        <source>Unlock memory of common toolkit libraries (GTK+, QT, FLTK, Wine)</source>
        <translation>Déverrouiller la mémoire des librairies d&apos;interface communes (GTK+, QT, FLTK, Wine)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="641"/>
        <source>So&amp;ft Mode</source>
        <translation>Mode &amp;logiciel</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="644"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3151"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3173"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3315"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3485"/>
        <source>Alt+F</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="638"/>
        <source>Ignore xruns reported by the backend driver</source>
        <translation>Ignorer les désynchronisations (XRUN) rapportées par le pilote principal</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="660"/>
        <source>&amp;Monitor</source>
        <translation>&amp;Écoute de contrôle</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="663"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2801"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3353"/>
        <source>Alt+M</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="657"/>
        <source>Provide output monitor ports</source>
        <translation>Fournir des ports de sortie d&apos;écoute de contrôle</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="679"/>
        <source>Force &amp;16bit</source>
        <translation>Forcer &amp;16bit</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="682"/>
        <source>Alt+1</source>
        <translation>Alt+1</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="676"/>
        <source>Force 16bit mode instead of failing over 32bit (default)</source>
        <translation>Forcer mode 16bit au lieu d&apos;échouer sur 32bit (par défaut)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="698"/>
        <source>H/W M&amp;onitor</source>
        <translation>Éc&amp;oute de contrôle matérielle</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="701"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3953"/>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="695"/>
        <source>Enable hardware monitoring of capture ports</source>
        <translation>Activer l&apos;écoute de contrôle matérielle des ports de capture</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="717"/>
        <source>H/&amp;W Meter</source>
        <translation>Mes&amp;ure matérielle</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="720"/>
        <source>Alt+W</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="714"/>
        <source>Enable hardware metering on cards that support it</source>
        <translation>Activer la mesure matérielle sur les cartes qui le reconnaissent</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="736"/>
        <source>&amp;Ignore H/W</source>
        <translation>&amp;Ignorer matériel</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="739"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2518"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3884"/>
        <source>Alt+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="733"/>
        <source>Ignore hardware period/buffer size</source>
        <translation>Ignore la taille des période/tampon matériels</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="813"/>
        <source>&amp;Output Device:</source>
        <translation>Périphérique de s&amp;ortie :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="835"/>
        <source>&amp;Output Channels:</source>
        <translation>Canaux de s&amp;ortie :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="857"/>
        <source>&amp;Interface:</source>
        <translation>&amp;Interface :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="879"/>
        <source>Maximum input audio hardware channels to allocate</source>
        <translation>Nombre maximal de canaux d&apos;entrée audio matériels à allouer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="898"/>
        <source>&amp;Audio:</source>
        <translation>&amp;Audio :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="920"/>
        <source>&amp;Input Latency:</source>
        <translation>Laten&amp;ce d&apos;entrée :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="948"/>
        <source>Dit&amp;her:</source>
        <translation>Bruit de dispertion (dit&amp;her) :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="970"/>
        <source>External output latency (frames)</source>
        <translation>Latence de sortie externe (en échantillons)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="989"/>
        <source>&amp;Input Device:</source>
        <translation>Pér&amp;iphérique d&apos;entrée :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1015"/>
        <source>Duplex</source>
        <translation>Duplex</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1020"/>
        <source>Capture Only</source>
        <translation>Capture seulement</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1025"/>
        <source>Playback Only</source>
        <translation>Reproduction seulement</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1011"/>
        <source>Provide either audio capture, playback or both</source>
        <translation>Fournir la capture audio, la reproduction audio ou les deux</translation>
    </message>
    <message>
        <source>default</source>
        <translation type="obsolete">par défaut</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1051"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1139"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1221"/>
        <source>hw:0</source>
        <translation>hw:0</translation>
    </message>
    <message>
        <source>hw:1</source>
        <translation type="obsolete">hw:1</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1039"/>
        <source>The PCM device name to use</source>
        <translation>Nom du périphérique PCM à utiliser</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1098"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1290"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1321"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2092"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2178"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2240"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2345"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1095"/>
        <source>Select output device for playback</source>
        <translation>Sélectionner le périphérique de sortie pour la reproduction</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1066"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1154"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1236"/>
        <source>/dev/dsp</source>
        <translation>/dev/dsp</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1127"/>
        <source>Alternate input device for capture</source>
        <translation>Périphérique d&apos;entrée alternatif pour la capture</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1168"/>
        <source>&amp;Output Latency:</source>
        <translation>Laten&amp;ce de sortie :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1190"/>
        <source>Maximum output audio hardware channels to allocate</source>
        <translation>Nombre maximal de canaux de sortie audio matériels à allouer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1209"/>
        <source>Alternate output device for playback</source>
        <translation>Périphérique de sortie alternatif pour la reproduction</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1250"/>
        <source>&amp;Input Channels:</source>
        <translation>Canaux d&apos;E&amp;ntrée :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1287"/>
        <source>Select input device for capture</source>
        <translation>Sélectionner le périphérique d&apos;entrée pour la capture</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1318"/>
        <source>Select PCM device name</source>
        <translation>Sélectionner le nom du périphérique PCM</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1334"/>
        <source>External input latency (frames)</source>
        <translation>Latence d&apos;entrée externe (en échantillons)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1357"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1362"/>
        <source>Rectangular</source>
        <translation>Rectangulaire</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1367"/>
        <source>Shaped</source>
        <translation>Sinusoïdal</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1372"/>
        <source>Triangular</source>
        <translation>Triangulaire</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1353"/>
        <source>Set dither mode</source>
        <translation>Régler le mode du bruit de dispersion (dither)</translation>
    </message>
    <message>
        <source>&amp;Verbose messages output</source>
        <translation type="obsolete">Sortie de messages ba&amp;varde</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="758"/>
        <source>Alt+V</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="752"/>
        <source>Whether to give verbose output on messages</source>
        <translation>Donner une sortie bavarde sur les messages</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="439"/>
        <source>Start De&amp;lay (secs):</source>
        <translation>&amp;Retard du démarrage (en s) :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="461"/>
        <source>Time in seconds that client is delayed after server startup</source>
        <translation>Temps en secondes dont le client est retardé après le démarrage du serveur</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="510"/>
        <source>Latency:</source>
        <translation>Latence :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="552"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="543"/>
        <source>Output latency in milliseconds, calculated based on the period, rate and buffer settings</source>
        <translation>Latence de sortie en millisecondes calculée à partir des réglages de la période, de la fréquence d&apos;échantillonnage et du tampon</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1954"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1972"/>
        <source>Scripting</source>
        <translation>Scripts</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1996"/>
        <source>Execute script on Start&amp;up:</source>
        <translation>Exéc&amp;uter un script au démarrage :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1993"/>
        <source>Whether to execute a custom shell script before starting up the JACK audio server.</source>
        <translation>Exécuter un script de commande personnalisé avant de démarrer le serveur audio JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2015"/>
        <source>Execute script after &amp;Startup:</source>
        <translation>Exécuter un &amp;script après le démarrage :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2012"/>
        <source>Whether to execute a custom shell script after starting up the JACK audio server.</source>
        <translation>Exécuter un script de commande personnalisé après avoir démarré le serveur audio JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2034"/>
        <source>Execute script on Shut&amp;down:</source>
        <translation>Exécuter un script à l&apos;extinctio&amp;n :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2031"/>
        <source>Whether to execute a custom shell script before shuting down the JACK audio server.</source>
        <translation>Exécuter un script de commande personnalisé avant d&apos;éteindre le serveur audio JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2058"/>
        <source>Command line to be executed before starting up the JACK audio server</source>
        <translation>Ligne de commande à exécuter avant de démarrer le serveur audio JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2089"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2175"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2237"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2342"/>
        <source>Scripting argument meta-symbols</source>
        <translation>Méta-symboles des arguments de script</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2123"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2209"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2271"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2376"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2665"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2782"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2120"/>
        <source>Browse for script to be executed before starting up the JACK audio server</source>
        <translation>Pointer sur le script à éxécuter avant de démarrer le serveur audio JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2144"/>
        <source>Command line to be executed after starting up the JACK audio server</source>
        <translation>Ligne de commande à exécuter après avoir démarré le serveur audio JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2206"/>
        <source>Browse for script to be executed after starting up the JACK audio server</source>
        <translation>Pointer sur le script à éxécuter après avoir démarré le serveur audio JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2268"/>
        <source>Browse for script to be executed before shutting down the JACK audio server</source>
        <translation>Pointer sur le script à éxécuter avant d&apos;éteindre le serveur audio JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2292"/>
        <source>Command line to be executed before shutting down the JACK audio server</source>
        <translation>Ligne de commande à exécuter avant d&apos;éteindre le serveur audio JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2311"/>
        <source>Execute script after Shu&amp;tdown:</source>
        <translation>Exécuter un script après l&apos;ex&amp;tinction :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2314"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2946"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4133"/>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2308"/>
        <source>Whether to execute a custom shell script after shuting down the JACK audio server.</source>
        <translation>Exécuter un script de commande personnalisé après avoir éteint le serveur audio JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2373"/>
        <source>Browse for script to be executed after shutting down the JACK audio server</source>
        <translation>Pointer sur le script à éxécuter après avoir éteint le serveur audio JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2397"/>
        <source>Command line to be executed after shutting down the JACK audio server</source>
        <translation>Ligne de commande à exécuter après avoir éteint le serveur audio JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2416"/>
        <source>Statistics</source>
        <translation>Statistiques</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2440"/>
        <source>&amp;Capture standard output</source>
        <translation>&amp;Capturer la sortie standard</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2443"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3788"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2437"/>
        <source>Whether to capture standard output (stdout/stderr) into messages window</source>
        <translation>Capturer la sortie standard (stdout/stderr) dans la fenêtre de messages</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2464"/>
        <source>&amp;XRUN detection regex:</source>
        <translation>Regex de détection des désynchronisations (&amp;XRUN) :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2498"/>
        <source>xrun of at least ([0-9|\.]+) msecs</source>
        <translation>désynchronisation (XRUN) d&apos;au moins ([0-9|\.]+) ms</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2491"/>
        <source>Regular expression used to detect XRUNs on server output messages</source>
        <translation>Expression régulière utilisée pour détecter les désynchronisations (XRUN) dans les messages de sortie du serveur</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2515"/>
        <source>&amp;Ignore first XRUN occurrence on statistics</source>
        <translation>&amp;Ignorer la première occurence de désynchronisation (XRUN) dans les statistiques</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2512"/>
        <source>Whether to ignore the first XRUN on server startup (most likely to occur on pre-0.80.0 servers)</source>
        <translation>Ignorer la première désynchronisation (XRUN) au démarrage du serveur (susceptible de se produire sur les serveurs pre-0.80.0)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2534"/>
        <source>Connections</source>
        <translation>Connexions</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2568"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2573"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4274"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2578"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2583"/>
        <source>30</source>
        <translation>30</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2588"/>
        <source>60</source>
        <translation>60</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2593"/>
        <source>120</source>
        <translation>120</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2561"/>
        <source>Time in seconds between each auto-refresh cycle</source>
        <translation>Temps en secondes entre chaque cycle de rafraîchissement automatique</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2631"/>
        <source>Patchbay definition file to be activated as connection persistence profile</source>
        <translation>Fichier de définition de baie de brassage à activer comme profil de persistance de connexion</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2662"/>
        <source>Browse for a patchbay definition file to be activated</source>
        <translation>Pointer sur un fichier de définition de baie de brassage à activer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2681"/>
        <source>&amp;Auto refresh connections Patchbay, every (secs):</source>
        <translation>Rafraîchir &amp;automatiquement les connexions de la baie de brassage toute les (en s) :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2684"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3913"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2678"/>
        <source>Whether to refresh the connections patchbay automatically</source>
        <translation>Rafraîchir automatiquement les connexions de la baie de brassage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2700"/>
        <source>Activate &amp;Patchbay persistence:</source>
        <translation>Activer la &amp;persistance de baie de brassage :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2703"/>
        <source>Alt+P</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2697"/>
        <source>Whether to activate a patchbay definition for connection persistence profile.</source>
        <translation>Activer une définition de baie de brassage pour le profil de persistance de connexion.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2828"/>
        <source>Display</source>
        <translation>Affichage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2846"/>
        <source>Time Display</source>
        <translation>Horloge</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2875"/>
        <source>Time F&amp;ormat:</source>
        <translation>F&amp;ormat du temps :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2904"/>
        <source>hh:mm:ss</source>
        <translation>hh:mm:ss</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2909"/>
        <source>hh:mm:ss.d</source>
        <translation>hh:mm:ss.d</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2914"/>
        <source>hh:mm:ss.dd</source>
        <translation>hh:mm:ss.dd</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2919"/>
        <source>hh:mm:ss.ddd</source>
        <translation>hh:mm:ss.ddd</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2897"/>
        <source>The general time format on display</source>
        <translation>Format général du temps affiché</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2943"/>
        <source>Transport &amp;Time Code</source>
        <translation>Code &amp;temporel (Timecode) du déplacement</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2959"/>
        <source>Transport &amp;BBT (bar:beat.ticks)</source>
        <translation>MT&amp;B (mesure:temps.battement) du déplacement</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2962"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3651"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4010"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4152"/>
        <source>Alt+B</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2975"/>
        <source>Elapsed time since last &amp;Reset</source>
        <translation>Temps écoulé depuis la dernière &amp;réinitialisation</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2991"/>
        <source>Elapsed time since last &amp;XRUN</source>
        <translation>Temps écoulé depuis la dernière désynchronisation (&amp;XRUN)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2994"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3061"/>
        <source>Sample front panel normal display font</source>
        <translation>Aperçu de l&apos;affichage normal</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3101"/>
        <source>Sample big time display font</source>
        <translation>Aperçu de la grande horloge</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3129"/>
        <source>Big Time display:</source>
        <translation>Grande horloge :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3148"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3170"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3312"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3482"/>
        <source>&amp;Font...</source>
        <translation>&amp;Police...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3145"/>
        <source>Select font for front panel normal display</source>
        <translation>Sélectionner la police pour l&apos;affichage normal</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3167"/>
        <source>Select font for big time display</source>
        <translation>Sélectionner la police pour la grande horloge</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3189"/>
        <source>Normal display:</source>
        <translation>Affichage normal :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3208"/>
        <source>&amp;Display shiny glass light effect</source>
        <translation>&amp;Afficher un effet de vitre éclairée brillante</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3205"/>
        <source>Whether to enable a shiny glass light effect on the main display</source>
        <translation>Activer un effet de vitre éclairée brillante sur l&apos;affichage principal</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3248"/>
        <source>Messages Window</source>
        <translation>Fenêtre de messages</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3281"/>
        <source>Sample messages text font display</source>
        <translation>Aperçu de la police du texte des messages</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3309"/>
        <source>Select font for the messages text display</source>
        <translation>Sélectionner la police pour le texte des messages</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3350"/>
        <source>&amp;Messages limit:</source>
        <translation>Limite des &amp;messages :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3347"/>
        <source>Whether to keep a maximum number of lines in the messages window</source>
        <translation>Garder un nombre maximal de lignes dans la fenêtre de messages</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3376"/>
        <source>100</source>
        <translation>100</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3381"/>
        <source>250</source>
        <translation>250</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3396"/>
        <source>2500</source>
        <translation>2500</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3366"/>
        <source>The maximum number of message lines to keep in view</source>
        <translation>Nombre maximal de ligne de messages à garder dans la vue</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3418"/>
        <source>Connections Window</source>
        <translation>Fenêtre de connexions</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3451"/>
        <source>Sample connections view font</source>
        <translation>Aperçu de la police de la vue des connexions</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3479"/>
        <source>Select font for the connections view</source>
        <translation>Sélectionner la police pour la vue des connexions</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3501"/>
        <source>&amp;Icon size:</source>
        <translation>Taille des &amp;icônes :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3533"/>
        <source>16 x 16</source>
        <translation>16 x 16</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3538"/>
        <source>32 x 32</source>
        <translation>32 x 32</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3543"/>
        <source>64 x 64</source>
        <translation>64 x 64</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3523"/>
        <source>The icon size for each item of the connections view</source>
        <translation>Taille de l&apos;icône pour chaque élément de la vue des connexions</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3648"/>
        <source>Ena&amp;ble client/port aliases editing (rename)</source>
        <translation>Activer l&apos;é&amp;dition (renommage) des aliases de client/port</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3645"/>
        <source>Whether to enable in-place client/port name editing (rename)</source>
        <translation>Activer l&apos;édition (renommage)  en place des aliases de client/port</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3667"/>
        <source>E&amp;nable client/port aliases</source>
        <translation>Activer les aliases de client/p&amp;ort</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3670"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3991"/>
        <source>Alt+N</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3664"/>
        <source>Whether to enable client/port name aliases on the connections window</source>
        <translation>Activer les aliases de client/port dans la fenêtre de connexions</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3686"/>
        <source>Draw connection and patchbay lines as Be&amp;zier curves</source>
        <translation>Dessiner les lignes de connexion et de baie de brassage en courbes de Be&amp;zier</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3689"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3846"/>
        <source>Alt+Z</source>
        <translation>Alt+Z</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3683"/>
        <source>Whether to draw connection lines as cubic Bezier curves</source>
        <translation>Dessiner les lignes de connexion et de baie de brassage en courbes de Bezier cubiques</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3716"/>
        <source>Misc</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3734"/>
        <source>Other</source>
        <translation>Autres</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3766"/>
        <source>&amp;Start JACK audio server on application startup</source>
        <translation>&amp;Démarrer le serveur audio JACK au démarrage de l&apos;application</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3763"/>
        <source>Whether to start JACK audio server immediately on application startup</source>
        <translation>Démarrer le serveur audio JACK immédiatement au démarrage de l&apos;application</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3785"/>
        <source>&amp;Confirm application close</source>
        <translation>&amp;Confirmer la fermeture de l&apos;application</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3782"/>
        <source>Whether to ask for confirmation on application exit</source>
        <translation>Demander une confimation lors de la sortie de l&apos;application</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3805"/>
        <source>&amp;Keep child windows always on top</source>
        <translation>&amp;Garder les fenêtres filles au premier plan</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3802"/>
        <source>Whether to keep all child windows on top of the main window</source>
        <translation>Garder les fenêtres filles au dessus de la fenêtre principale</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3824"/>
        <source>&amp;Enable system tray icon</source>
        <translation>Activ&amp;er l&apos;icône de notification système</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3827"/>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3821"/>
        <source>Whether to enable the system tray icon</source>
        <translation>Activer l&apos;icône de notification système</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3862"/>
        <source>&amp;Delay window positioning at startup</source>
        <translation>&amp;Retarder le positionnement de la fenêtre au démarrage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3859"/>
        <source>Whether to delay window positioning at application startup</source>
        <translation>Retarder le positionnement de la fenêtre au démarrage de l&apos;application</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3910"/>
        <source>S&amp;ave JACK audio server configuration to:</source>
        <translation>&amp;Enregistrer la configuration du serveur audio JACK dans :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3907"/>
        <source>Whether to save the JACK server command-line configuration into a local file (auto-start)</source>
        <translation>Enregistrer la configuration en ligne de commande du serveur JACK dans un fichier local (démarrage automatique)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3933"/>
        <source>.jackdrc</source>
        <translation>.jackdrc</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3926"/>
        <source>The server configuration local file name (auto-start)</source>
        <translation>Nom du fichier local de configuration du serveur (démarrage automatique)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3950"/>
        <source>C&amp;onfigure as temporary server</source>
        <translation>C&amp;onfigurer comme serveur temporaire</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3947"/>
        <source>Whether to exit once all clients have closed (auto-start)</source>
        <translation>Sortir dès que tous les clients ont fermé (démarrage automatique)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3969"/>
        <source>Confirm server sh&amp;utdown</source>
        <translation>Confirmer l&apos;e&amp;xtinction du serveur</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3966"/>
        <source>Whether to ask for confirmation on JACK audio server shutdown</source>
        <translation>Demander un confirmation à l&apos;extinction du serveur audio JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4060"/>
        <source>Buttons</source>
        <translation>Boutons</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4092"/>
        <source>Hide main window &amp;Left buttons</source>
        <translation>Cacher les boutons de &amp;gauche de la fenêtre principale</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4095"/>
        <source>Alt+L</source>
        <translation>Alt+G</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4089"/>
        <source>Whether to hide the left button group on the main window</source>
        <translation>Cacher le groupe de bouton de gauche sur la fenêtre principale</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4111"/>
        <source>Hide main window &amp;Right buttons</source>
        <translation>Cacher les boutons de &amp;droite de la fenêtre principale</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4108"/>
        <source>Whether to hide the right button group on the main window</source>
        <translation>Cacher le groupe de bouton de droite sur la fenêtre principale</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4130"/>
        <source>Hide main window &amp;Transport buttons</source>
        <translation>Cacher les boutons de &amp;déplacement de la fenêtre principale</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4127"/>
        <source>Whether to hide the transport button group on the main window</source>
        <translation>Cacher le groupe de bouton de déplacement sur la fenêtre principale</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4149"/>
        <source>Hide main window &amp;button text labels</source>
        <translation>Cacher le texte des &amp;boutons de la fenêtre principale</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4146"/>
        <source>Whether to hide the text labels on the main window buttons</source>
        <translation>Cacher le texte des boutons sur la fenêtre principale</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4323"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4333"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="796"/>
        <location filename="../src/qjackctlSetupForm.cpp" line="848"/>
        <location filename="../src/qjackctlSetupForm.cpp" line="1844"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <source>Some settings have been changed:</source>
        <translation type="obsolete">Des paramètres ont été changés:</translation>
    </message>
    <message>
        <source>Do you want to save the changes?</source>
        <translation type="obsolete">Voulez-vous enregistrer les changements?</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Enregistrer</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="obsolete">Ignorer</translation>
    </message>
    <message>
        <source>Delete preset:</source>
        <translation type="obsolete">Effacer préréglage:</translation>
    </message>
    <message>
        <source>Are you sure?</source>
        <translation type="obsolete">Êtes vous certain?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="882"/>
        <source>msec</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="884"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1497"/>
        <source>&amp;Preset Name</source>
        <translation>&amp;Nom du préréglage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1499"/>
        <source>&amp;Server Path</source>
        <translation>&amp;Chemin du serveur</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1500"/>
        <source>&amp;Driver</source>
        <translation>Pilot&amp;e</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1501"/>
        <source>&amp;Interface</source>
        <translation>&amp;Interface</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1503"/>
        <source>Sample &amp;Rate</source>
        <translation>&amp;Fréquence d&apos;échantillonnage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1504"/>
        <source>&amp;Frames/Period</source>
        <translation>&amp;Échantillons/Période</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1505"/>
        <source>Periods/&amp;Buffer</source>
        <translation>Périodes/&amp;Tampon</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1553"/>
        <source>Startup Script</source>
        <translation>Script de démarrage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1570"/>
        <source>Post-Startup Script</source>
        <translation>Script post-démarrage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1587"/>
        <source>Shutdown Script</source>
        <translation>Script d&apos;extinction</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1604"/>
        <source>Post-Shutdown Script</source>
        <translation>Script post-extinction</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1623"/>
        <source>Patchbay Definition files</source>
        <translation>Fichiers de définition de baie de brassage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1621"/>
        <source>Active Patchbay Definition</source>
        <translation>Définition de baie de brassage à activer</translation>
    </message>
    <message>
        <source>Some settings have been changed.</source>
        <translation type="obsolete">Des paramètres ont été modifiés.</translation>
    </message>
    <message>
        <source>Do you want to apply the changes?</source>
        <translation type="obsolete">Voulez-vous appliquer les changements?</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="obsolete">Appliquer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="272"/>
        <source>The audio backend driver interface to use</source>
        <translation>Le pilote d&apos;interface audio à utiliser</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="755"/>
        <source>&amp;Verbose messages</source>
        <translation>Messages ba&amp;vards</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="370"/>
        <source>MIDI Driv&amp;er:</source>
        <translation>Pilot&amp;e MIDI :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="399"/>
        <source>none</source>
        <translation>aucun</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="404"/>
        <source>raw</source>
        <translation>brut</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="409"/>
        <source>seq</source>
        <translation>seq</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="392"/>
        <source>The ALSA MIDI backend driver to use</source>
        <translation>Le pilote ALSA MIDI à utiliser</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="231"/>
        <source>jackdmp</source>
        <translation>jackdmp</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="314"/>
        <source>firewire</source>
        <translation>firewire</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1056"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1144"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1226"/>
        <source>plughw:0</source>
        <translation>plughw:0</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="797"/>
        <source>Some settings have been changed:

&quot;%1&quot;

Do you want to save the changes?</source>
        <translation>Des paramètres ont été modifiés :

&quot;%1&quot;

Voulez-vous enregistrer les changements?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="849"/>
        <source>Delete preset:

&quot;%1&quot;

Are you sure?</source>
        <translation>Effecer préréglage :

&quot;%1&quot;

Êtes vous certain?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1845"/>
        <source>Some settings have been changed.

Do you want to apply the changes?</source>
        <translation>Des paramètres ont été modifiés.

Voulez-vous appliquer les changements?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1639"/>
        <source>Messages Log</source>
        <translation>Journal des messages</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1641"/>
        <source>Log files</source>
        <translation>Fichiers du journal</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="284"/>
        <source>sun</source>
        <translation>sun</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="324"/>
        <source>netone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1061"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1149"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1231"/>
        <source>/dev/audio</source>
        <translation>/dev/audio</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1556"/>
        <source>&amp;Channels:</source>
        <translation>&amp;Canaux :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1857"/>
        <source>Maximum number of audio channels to allocate</source>
        <translation>Nombre maximal de canaux audio à allouer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2719"/>
        <source>Logging</source>
        <translation>Journalisation</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2748"/>
        <source>Messages log file</source>
        <translation>Fichier de journal des messages</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2779"/>
        <source>Browse for the messages log file location</source>
        <translation>Pointer sur l&apos;emplacement du fichier de journal des messages</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2795"/>
        <source>Whether to activate a messages logging to file.</source>
        <translation>Activer la journalisation des messages dans un fichier.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2798"/>
        <source>&amp;Messages log file:</source>
        <translation>Fichier de journal des &amp;messages :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3224"/>
        <source>Whether to enable blinking (flashing) of the server mode (RT) indicator</source>
        <translation>Activer le clignotement de l&apos;indicateur de mode serveur (TR)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3227"/>
        <source>Blin&amp;k server mode indicator</source>
        <translation>&amp;Clignotement de l&apos;indicateur de mode serveur</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3589"/>
        <source>&amp;JACK client/port aliases:</source>
        <translation>Alias de client/port &amp;JACK :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3611"/>
        <source>JACK client/port aliases display mode</source>
        <translation>Mode d&apos;affichage des alias de client/port JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3621"/>
        <source>Default</source>
        <translation>Par défaut</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3626"/>
        <source>First</source>
        <translation>Premier</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3631"/>
        <source>Second</source>
        <translation>Deuxième</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3840"/>
        <source>Whether to start minimized to system tray</source>
        <translation>Démarrer minimisé dans la zone de notification système</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3843"/>
        <source>Start minimi&amp;zed to system tray</source>
        <translation>Démarrer minimisé dans la &amp;zone de notification système</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3878"/>
        <source>Whether to restrict to one single application instance (X11)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3881"/>
        <source>Single application &amp;instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3985"/>
        <source>Whether to enable ALSA Sequencer (MIDI) support on startup</source>
        <translation>Activer la prise en charge du séquenceur ALSA (MIDI) au démarrage</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3988"/>
        <source>E&amp;nable ALSA Sequencer support</source>
        <translation>Activer la prise e&amp;n charge du séquenceur ALSA</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4186"/>
        <source>Defaults</source>
        <translation>Par défaut</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4223"/>
        <source>&amp;Base font size:</source>
        <translation>Taille de police de &amp;base :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4242"/>
        <source>Base application font size (pt.)</source>
        <translation>Taille de base de police de l&apos;application (pt.)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4254"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4259"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4264"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4269"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4279"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4284"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="319"/>
        <source>net</source>
        <translation>net</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4004"/>
        <source>Whether to enable D-Bus interface</source>
        <translation>Activer l&apos;interface D-Bus</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4007"/>
        <source>&amp;Enable D-Bus interface</source>
        <translation>Activ&amp;er l&apos;interface D-Bus</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1585"/>
        <source>Number of microseconds to wait between engine processes (dummy)</source>
        <translation>Nombre de microsecondes à attendre entre les traitements du moteur (factice)</translation>
    </message>
</context>
<context>
    <name>qjackctlSocketForm</name>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="33"/>
        <source>Socket - JACK Audio Connection Kit</source>
        <translation>Prise - Kit de Connexion Audio JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="386"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="402"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="52"/>
        <source>&amp;Socket</source>
        <translation>&amp;Prise</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="64"/>
        <source>&amp;Name (alias):</source>
        <translation>&amp;Nom (alias) :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="80"/>
        <source>Socket name (an alias for client name)</source>
        <translation>Nom de la prise (un alias pour le nom du client)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="87"/>
        <source>Client name (regular expression)</source>
        <translation>Nom du client (expression régulière)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="100"/>
        <source>Add P&amp;lug</source>
        <translation>Ajouter une &amp;fiche</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="106"/>
        <source>Alt+L</source>
        <translation>Alt+F</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="97"/>
        <source>Add plug to socket plug list</source>
        <translation>Ajouter la fiche à la liste des prises</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="113"/>
        <source>&amp;Plug:</source>
        <translation>&amp;Fiche :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="129"/>
        <source>Port name (regular expression)</source>
        <translation>Nom du port (expression régulière)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="158"/>
        <source>Socket Plugs / Ports</source>
        <translation>Prises / Ports</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="142"/>
        <source>Socket plug list</source>
        <translation>Liste des prises</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="169"/>
        <source>&amp;Edit</source>
        <translation>&amp;Éditer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="175"/>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>Edit currently selected plug/port</source>
        <translation type="obsolete">Éditer la fiche/le port actuellement sélectionné</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="185"/>
        <source>&amp;Remove</source>
        <translation>Enleve&amp;r</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="191"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="182"/>
        <source>Remove currently selected plug from socket plug list</source>
        <translation>Enlever la fiche actuellement sélectionnée de la liste des prises</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="198"/>
        <source>&amp;Client:</source>
        <translation>&amp;Client :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="217"/>
        <source>&amp;Down</source>
        <translation>Vers le &amp;bas</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="223"/>
        <source>Alt+D</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <source>Move down currently selected plug socket plugst</source>
        <translation type="obsolete">Déplacer vers le bas la fiche actuellement sélectionnée dans la liste des prises</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="233"/>
        <source>&amp;Up</source>
        <translation>Vers le &amp;haut</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="239"/>
        <source>Alt+U</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Move up current selected plug socket plugst</source>
        <translation type="obsolete">Déplacer vers le haut la fiche actuellement sélectionnée dans la liste des prises</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="265"/>
        <source>E&amp;xclusive</source>
        <translation>E&amp;xclusif</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="268"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="262"/>
        <source>Enforce only one exclusive cable connection</source>
        <translation>S&apos;assurer de l&apos;utilisation d&apos;une seule connexion cablée exclusive</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="275"/>
        <source>&amp;Forward:</source>
        <translation>Renvo&amp;i :</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="291"/>
        <source>Forward (clone) all connections from this socket</source>
        <translation>Renvoyer (cloner) toutes les connexions depuis cette prise</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="301"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="316"/>
        <source>&amp;Audio</source>
        <translation>&amp;Audio</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="319"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="313"/>
        <source>Audio socket type (JACK)</source>
        <translation>Type de prise audio (JACK)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="329"/>
        <source>&amp;MIDI</source>
        <translation>&amp;MIDI</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="332"/>
        <source>Alt+M</source>
        <translation>Alt+M</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="339"/>
        <source>MIDI socket type (ALSA)</source>
        <translation>Type de prise MIDI (ALSA)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="152"/>
        <source>Plugs / Ports</source>
        <translation>Fiches / Ports</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="546"/>
        <source>Add Plug</source>
        <translation>Ajouter une Fiche</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="563"/>
        <source>Remove</source>
        <translation>Enlever</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="560"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="337"/>
        <source>Error</source>
        <translation type="unfinished">Erreur</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="338"/>
        <source>A socket named &quot;%1&quot; already exists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="567"/>
        <source>Move Up</source>
        <translation>Vers le haut</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="570"/>
        <source>Move Down</source>
        <translation>Vers le bas</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="736"/>
        <source>(None)</source>
        <translation>(Aucun)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="166"/>
        <source>Edit currently selected plug</source>
        <translation>Éditer la fiche actuellement sélectionnée</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="214"/>
        <source>Move down currently selected plug in socket plug list</source>
        <translation>Déplacer vers le bas la fiche actuellement sélectionnée dans la liste des prises</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="230"/>
        <source>Move up current selected plug in socket plug list</source>
        <translation>Déplacer vers le haut la fiche actuellement sélectionnée dans la liste des prises</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="356"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="357"/>
        <source>Some settings have been changed.

Do you want to apply the changes?</source>
        <translation>Des paramètres ont été modifiés.Voulez-vous appliquer les changements?</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="obsolete">Appliquer</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="obsolete">Ignorer</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="326"/>
        <source>MIDI socket type (JACK)</source>
        <translation>Type de prise midi (JACK)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="342"/>
        <source>AL&amp;SA</source>
        <translation>AL&amp;SA</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="345"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
</context>
<context>
    <name>qjackctlSocketList</name>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="329"/>
        <source>Output</source>
        <translation>Sortie</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="339"/>
        <source>Input</source>
        <translation>Entrée</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="352"/>
        <source>Socket</source>
        <translation>Prise</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="obsolete">Nouvelle</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="543"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <source>about to be removed</source>
        <translation type="obsolete">sur le point d&apos;être enlevée</translation>
    </message>
    <message>
        <source>Are you sure?</source>
        <translation type="obsolete">Êtes-vous certain?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Non</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="obsolete">Copie</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="487"/>
        <source>&lt;New&gt; - %1</source>
        <translation>&lt;Nouvelle&gt; - %1</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="544"/>
        <source>%1 about to be removed:

&quot;%2&quot;

Are you sure?</source>
        <translation>%1 sur le point d&apos;être enlevée :

&quot;%2&quot;
Êtes-vous certain?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="634"/>
        <source>%1 &lt;Copy&gt; - %2</source>
        <translation>%1 &lt;Copie&gt; - %2</translation>
    </message>
</context>
<context>
    <name>qjackctlSocketListView</name>
    <message>
        <source>Output Sockets</source>
        <translation type="obsolete">Prises de Sortie</translation>
    </message>
    <message>
        <source>Plugs</source>
        <translation type="obsolete">Fiches</translation>
    </message>
    <message>
        <source>Input Sockets</source>
        <translation type="obsolete">Prises d&apos;Entrée</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="802"/>
        <source>Output Sockets / Plugs</source>
        <translation>Prises de Sortie / Fiches</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="804"/>
        <source>Input Sockets / Plugs</source>
        <translation>Prises d&apos;Entrée / Fiches</translation>
    </message>
</context>
<context>
    <name>qjackctlStatusForm</name>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="44"/>
        <source>Status - JACK Audio Connection Kit</source>
        <translation>Statut - Kit de Connexion Audio JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="98"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="103"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="73"/>
        <source>Statistics since last server startup</source>
        <translation>Statistiques depuis le dernier démarrage du serveur</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="130"/>
        <source>Re&amp;set</source>
        <translation>Réinitiali&amp;ser</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="136"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="127"/>
        <source>Reset XRUN statistic values</source>
        <translation>Réinitialiser les valeurs statistiques des désynchronisations (XRUN)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="146"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Rafraîchir</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="152"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="143"/>
        <source>Refresh XRUN statistic values</source>
        <translation>Rafraîchir les valeurs statistiques des désynchronisations (XRUN)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="105"/>
        <source>Time of last reset</source>
        <translation>Temps depuis la dernière réinitialisation</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="102"/>
        <source>Maximum scheduling delay</source>
        <translation>Délai d&apos;ordonnancement maximal</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="62"/>
        <source>Server name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="85"/>
        <source>XRUN count since last server startup</source>
        <translation>Décompte des désynchronisations (XRUN) depuis le dernier démarrage du serveur</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="98"/>
        <source>XRUN total</source>
        <translation>Nombre total de désynchronisation (XRUN)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="96"/>
        <source>XRUN average</source>
        <translation>Moyenne de désynchronisation (XRUN)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="94"/>
        <source>XRUN minimum</source>
        <translation>Nombre minimal de désynchronisation (XRUN)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="92"/>
        <source>XRUN maximum</source>
        <translation>Nombre maximal de désynchronisation (XRUN)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="90"/>
        <source>XRUN last</source>
        <translation>Dernière désynchronisation (XRUN)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="88"/>
        <source>XRUN last time detected</source>
        <translation>Horaire de la dernière désynchronisation (XRUN) détectée</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="75"/>
        <source>Transport state</source>
        <translation>État du déplacement</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="82"/>
        <source>Transport BPM</source>
        <translation>BPM du déplacement</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="80"/>
        <source>Transport BBT</source>
        <translation>MTB du déplacement</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="78"/>
        <source>Transport Timecode</source>
        <translation>Code temporel (Timecode) du déplacement</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="72"/>
        <source>Realtime Mode</source>
        <translation>Mode temps réel</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="70"/>
        <source>Buffer Size</source>
        <translation>Taille du tampon</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="68"/>
        <source>Sample Rate</source>
        <translation>Fréquence d&apos;échantillonnage</translation>
    </message>
    <message>
        <source>CPU Load</source>
        <translation type="obsolete">Charge processeur</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="64"/>
        <source>Server state</source>
        <translation>État du serveur</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="66"/>
        <source>DSP Load</source>
        <translation>Charge DSP</translation>
    </message>
</context>
</TS>
