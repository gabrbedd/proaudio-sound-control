<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru">
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="448"/>
        <source>Show help about command line options</source>
        <translation>Показать справку по использованию параметров командной строки</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="440"/>
        <source>Start JACK audio server immediately</source>
        <translation>Немедленно запустить сервер JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="450"/>
        <source>Show version information</source>
        <translation>Показать информацию о версии</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="486"/>
        <source>Option -p requires an argument (preset).</source>
        <translation>Ключ -p требует аргумента (пресет).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="38"/>
        <source>(default)</source>
        <translation>(как обычно)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="435"/>
        <source>Usage: %1 [options] [command-and-args]</source>
        <translation>Использование: %1 [ключи] [команда-и-аргументы]</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="438"/>
        <source>Options:</source>
        <translation>Параметры:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="442"/>
        <source>Set default settings preset name</source>
        <translation>Указать название профиля с параметрами по умолчанию</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="444"/>
        <source>Set active patchbay definition file</source>
        <translation>Указать файл описания активного коммутатора</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="495"/>
        <source>Option -a requires an argument (path).</source>
        <translation>Ключу -a необходим аргумент (расположение)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="517"/>
        <source>Qt: %1
</source>
        <translation>Qt: %1
</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="446"/>
        <source>Set default JACK audio server name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetup.cpp" line="505"/>
        <source>Option -n requires an argument (name).</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qjackctlAboutForm</name>
    <message>
        <location filename="../src/qjackctlAboutForm.ui" line="80"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.ui" line="67"/>
        <source>&amp;Close</source>
        <translation>&amp;Закрыть</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.ui" line="70"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="43"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="44"/>
        <source>Build</source>
        <translation>Сборка</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="59"/>
        <source>Transport status control disabled.</source>
        <translation>Статус управления транспортом отключён.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="65"/>
        <source>Realtime status disabled.</source>
        <translation>Статус режима реального времени отключён.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="110"/>
        <source>Website</source>
        <translation>Домашняя страница</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="115"/>
        <source>This program is free software; you can redistribute it and/or modify it</source>
        <translation>Это программа является свободной; вы можете распространять 
и/или изменять её без ограничений</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="116"/>
        <source>under the terms of the GNU General Public License version 2 or later.</source>
        <translation>на условиях лицензии GNU General Public License версии 2 или более новой.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="47"/>
        <source>Debugging option enabled.</source>
        <translation>Параметр отладки включен.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="53"/>
        <source>System tray disabled.</source>
        <translation>Область уведомления отключена.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="71"/>
        <source>XRUN delay status disabled.</source>
        <translation>Статус задержки XRUN отключен.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="77"/>
        <source>Maximum delay status disabled.</source>
        <translation>Статус максимальной задержки отключен.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="83"/>
        <source>JACK MIDI support disabled.</source>
        <translation>Поддержка JACK MIDI отключена.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="96"/>
        <source>ALSA/MIDI sequencer support disabled.</source>
        <translation>Поддержка секвенсера ALSA/MIDI отключена.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.ui" line="36"/>
        <source>About QjackCtl</source>
        <translation>о QJackCtl</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="89"/>
        <source>JACK Port aliases support disabled.</source>
        <translation>Поддержка алиасов портов JACK отключена.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlAboutForm.cpp" line="104"/>
        <source>D-Bus interface support disabled.</source>
        <translation>Поддержка интерфейса D-Bus отключена.</translation>
    </message>
</context>
<context>
    <name>qjackctlClientListView</name>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="664"/>
        <source>Readable Clients / Output Ports</source>
        <translation>Порты выхода</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="666"/>
        <source>Writable Clients / Input Ports</source>
        <translation>Порты входа</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="986"/>
        <source>&amp;Connect</source>
        <translation>&amp;Соединить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="987"/>
        <source>Alt+C</source>
        <comment>Connect</comment>
        <translation>Alt+с</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="990"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Рассоединить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="991"/>
        <source>Alt+D</source>
        <comment>Disconnect</comment>
        <translation>Alt+р</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="994"/>
        <source>Disconnect &amp;All</source>
        <translation>Рассоединить &amp;все</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="995"/>
        <source>Alt+A</source>
        <comment>Disconect All</comment>
        <translation>Alt+в</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1000"/>
        <source>Re&amp;name</source>
        <translation>Пере&amp;именовать</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1001"/>
        <source>Alt+N</source>
        <comment>Rename</comment>
        <translation>Alt+и</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1007"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Обновить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1008"/>
        <source>Alt+R</source>
        <comment>Refresh</comment>
        <translation>Alt+о</translation>
    </message>
</context>
<context>
    <name>qjackctlConnect</name>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1756"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1757"/>
        <source>This will suspend sound processing
from all client applications.

Are you sure?</source>
        <translation>Обработка звука от клиентских
приложений будет прекращена.

Вы уверены?</translation>
    </message>
</context>
<context>
    <name>qjackctlConnectionsForm</name>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="44"/>
        <source>Connections - JACK Audio Connection Kit</source>
        <translation>Соединения</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="101"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="219"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="337"/>
        <source>&amp;Connect</source>
        <translation>&amp;Соединить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="107"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="225"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="343"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="98"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="216"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="334"/>
        <source>Connect currently selected ports</source>
        <translation>Соединить выбранные сейчас порты</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="117"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="235"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="353"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Рассоединить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="123"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="241"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="359"/>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="114"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="232"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="350"/>
        <source>Disconnect currently selected ports</source>
        <translation>Рассоединить выбранные сейчас порты</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="133"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="251"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="369"/>
        <source>Disconnect &amp;All</source>
        <translation>Рассоединить &amp;все</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="139"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="257"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="375"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="130"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="248"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="366"/>
        <source>Disconnect all currently connected ports</source>
        <translation>Рассоединить все соединённые сейчас порты</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="165"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="283"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="401"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Обновить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="171"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="289"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="407"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="162"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="280"/>
        <location filename="../src/qjackctlConnectionsForm.ui" line="398"/>
        <source>Refresh current connections view</source>
        <translation>Обновить отображение текущих соединений</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="63"/>
        <source>Audio</source>
        <translation>Звук</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="181"/>
        <source>MIDI</source>
        <translation>MIDI</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.cpp" line="239"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.cpp" line="240"/>
        <source>The preset aliases have been changed:

&quot;%1&quot;

Do you want to save the changes?</source>
        <translation>Алиасы профилей изменились:

&quot;%1&quot;

Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Сохранить</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="obsolete">Отказаться</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отменить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnectionsForm.ui" line="299"/>
        <source>ALSA</source>
        <translation>ALSA</translation>
    </message>
</context>
<context>
    <name>qjackctlConnectorView</name>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1173"/>
        <source>&amp;Connect</source>
        <translation>&amp;Соединить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1174"/>
        <source>Alt+C</source>
        <comment>Connect</comment>
        <translation>Alt+с</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1177"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Рассоединить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1178"/>
        <source>Alt+D</source>
        <comment>Disconnect</comment>
        <translation>Alt+р</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1181"/>
        <source>Disconnect &amp;All</source>
        <translation>Рассоединить &amp;все</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1182"/>
        <source>Alt+A</source>
        <comment>Disconect All</comment>
        <translation>Alt+в</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1187"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Обновить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlConnect.cpp" line="1188"/>
        <source>Alt+R</source>
        <comment>Refresh</comment>
        <translation>Alt+о</translation>
    </message>
</context>
<context>
    <name>qjackctlMainForm</name>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="79"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3087"/>
        <source>&amp;Start</source>
        <translation>&amp;Запустить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="76"/>
        <source>Start the JACK server</source>
        <translation>Запустить сервер JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="117"/>
        <source>S&amp;top</source>
        <translation>С&amp;топ</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="114"/>
        <source>Stop the JACK server</source>
        <translation>Остановить сервер JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="388"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3169"/>
        <source>&amp;Quit</source>
        <translation>В&amp;ыход</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="385"/>
        <source>Quit processing and exit</source>
        <translation>Остановить сервер и выйти из программы</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="obsolete">Остановка</translation>
    </message>
    <message>
        <source>Kill</source>
        <translation type="obsolete">Уничтожить</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отменить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1190"/>
        <source>JACK is starting...</source>
        <translation>JACK запускается ...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1259"/>
        <source>JACK is stopping...</source>
        <translation>JACK останавливается ...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1998"/>
        <source>msec</source>
        <translation>мс</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1520"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <source>JACK server state</source>
        <translation type="obsolete">Состояние сервера JACK</translation>
    </message>
    <message>
        <source>Sample rate</source>
        <translation type="obsolete">Частота сэмплирования</translation>
    </message>
    <message>
        <source>Time display</source>
        <translation type="obsolete">Время</translation>
    </message>
    <message>
        <source>Transport state</source>
        <translation type="obsolete">Состояние транспорта</translation>
    </message>
    <message>
        <source>Transport BPM</source>
        <translation type="obsolete">BPM транспорта</translation>
    </message>
    <message>
        <source>Transport time</source>
        <translation type="obsolete">Время транспорта</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="395"/>
        <source>Alt+Q</source>
        <translation>Alt+Q</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="86"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="124"/>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="232"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3149"/>
        <source>&amp;Play</source>
        <translation>&amp;Воспроизвести</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="423"/>
        <source>Levels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="531"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="229"/>
        <source>Start transport rolling</source>
        <translation>Запустить воспроизведение через транспорт</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="273"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3152"/>
        <source>Pa&amp;use</source>
        <translation>Пау&amp;за</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+U</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="270"/>
        <source>Stop transport rolling</source>
        <translation>Остановить воспроизведение с транспорта</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="448"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3128"/>
        <source>St&amp;atus</source>
        <translation>С&amp;татус</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="445"/>
        <source>Show/hide the extended status window</source>
        <translation>Показать/скрыть расширенное окно статуса</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="353"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3164"/>
        <source>Ab&amp;out...</source>
        <translation>О про&amp;грамме...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="350"/>
        <source>Show information about this application</source>
        <translation>Показать информацию о приложении</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="613"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3160"/>
        <source>S&amp;etup...</source>
        <translation>&amp;Параметры</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="620"/>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="610"/>
        <source>Show settings and options dialog</source>
        <translation>Показать диалог настройки программы</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="562"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3124"/>
        <source>&amp;Messages</source>
        <translation>С&amp;ообщения</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="559"/>
        <source>Show/hide the messages log window</source>
        <translation>Показать/скрыть окно с журналом сообщений</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3136"/>
        <source>Patch&amp;bay</source>
        <translation>&amp;Коммутатор</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="204"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="521"/>
        <source>Show/hide the patchbay editor window</source>
        <translation>Показать/скрыть окно коммутатора</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3132"/>
        <source>&amp;Connections</source>
        <translation>Сое&amp;динения</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="493"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="483"/>
        <source>Show/hide the actual connections patchbay window</source>
        <translation>Показать/спрятать окно коммутатора с актуальными соединениями</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">ОК</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="856"/>
        <source>successfully</source>
        <translation>успешно</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2974"/>
        <source>Activating</source>
        <translation>Активируется</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2745"/>
        <location filename="../src/qjackctlMainForm.cpp" line="2852"/>
        <location filename="../src/qjackctlMainForm.cpp" line="2957"/>
        <source>Starting</source>
        <translation>Запуск</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="730"/>
        <source>The program will keep running in the system tray.

To terminate the program, please choose &quot;Quit&quot;
in the context menu of the system tray icon.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="951"/>
        <source>Startup script...</source>
        <translation>Сценарий, выполняемый при запуске...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="952"/>
        <source>Startup script terminated</source>
        <translation>Выполнение стартового сценария прекращено</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2961"/>
        <source>Started</source>
        <translation>Запущен</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2760"/>
        <location filename="../src/qjackctlMainForm.cpp" line="2964"/>
        <source>Stopping</source>
        <translation>Остановка</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1397"/>
        <source>JACK was stopped</source>
        <translation>Cервер JACK остановлен</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1252"/>
        <source>Shutdown script...</source>
        <translation>Послеостановочный сценарий...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1253"/>
        <source>Shutdown script terminated</source>
        <translation>Выполнение послеостановочного сценария прекращено</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2979"/>
        <source>Inactive</source>
        <translation>Не активен</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2971"/>
        <source>Active</source>
        <translation>Активен</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2798"/>
        <location filename="../src/qjackctlMainForm.cpp" line="2968"/>
        <source>Stopped</source>
        <translation>Стоп</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1634"/>
        <source>Transport BBT (bar:beat.ticks)</source>
        <translation>BBT транспорта (такт:доля.тики)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1859"/>
        <source>Statistics reset.</source>
        <translation>Перезапуск статистики</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2061"/>
        <source>Shutdown notification.</source>
        <translation>Уведомление об остановке</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2230"/>
        <source>checked</source>
        <translation>проверено</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2234"/>
        <source>connected</source>
        <translation>соединено</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2243"/>
        <source>failed</source>
        <translation>не удалось</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2426"/>
        <source>Client activated.</source>
        <translation>Клиент активирован</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2434"/>
        <source>Post-startup script...</source>
        <translation>Послестартовый сценарий...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2435"/>
        <source>Post-startup script terminated</source>
        <translation>Выполнение послестартового сценария прекращено</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2469"/>
        <source>Client deactivated.</source>
        <translation>Клиент деактивирован</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2747"/>
        <source>Transport start.</source>
        <translation>Транспорт запущен</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2762"/>
        <source>Transport stop.</source>
        <translation>Транспорт остановлен</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2845"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2845"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2855"/>
        <source>Rolling</source>
        <translation>Играет</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2858"/>
        <source>Looping</source>
        <translation>Петля</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1635"/>
        <source>Transport time code</source>
        <translation>Тайм-код транспорта</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1646"/>
        <source>Elapsed time since last reset</source>
        <translation>Времени с последней перезагрузки</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1649"/>
        <source>Elapsed time since last XRUN</source>
        <translation>Времени с последнего XRUN</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2238"/>
        <source>disconnected</source>
        <translation>рассоединено</translation>
    </message>
    <message>
        <source>Terminate</source>
        <translation type="obsolete">Завершить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2443"/>
        <source>Command line argument...</source>
        <translation>Аргумент для командной строки...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2444"/>
        <source>Command line argument started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="728"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3207"/>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3209"/>
        <source>Some settings will be only effective
the next time you start this program.</source>
        <translation>Некоторые изменения вступят в силу
только при следующем запуске программы.</translation>
    </message>
    <message>
        <source>JACK server mode</source>
        <translation type="obsolete">Режим сервера JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3184"/>
        <source>Server settings will be only effective after
restarting the JACK audio server.</source>
        <translation>Параметры работы сервера JACK вступят в силу
только при следующем запуске сервера.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3077"/>
        <source>&amp;Hide</source>
        <translation>С&amp;крыть</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3079"/>
        <source>S&amp;how</source>
        <translation>&amp;Показать</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3090"/>
        <source>&amp;Stop</source>
        <translation>&amp;Стоп</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="597"/>
        <source>Could not open ALSA sequencer as a client.

ALSA MIDI patchbay will be not available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="649"/>
        <source>D-BUS: Service is available (%1 aka jackdbus).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="680"/>
        <source>D-BUS: Service not available (%1 aka jackdbus).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The program will keep running in the system tray.

To terminate the program, please choose &quot;Quit&quot; in the context menu of the system tray entry.</source>
        <translation type="obsolete">Программа будет скрыта в область уведомления.

Для завершения работы с программой в контекстном меню значка программы в области уведомления выберите пункт «Выход».</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="752"/>
        <location filename="../src/qjackctlMainForm.cpp" line="903"/>
        <location filename="../src/qjackctlMainForm.cpp" line="1216"/>
        <location filename="../src/qjackctlMainForm.cpp" line="2261"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3182"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="753"/>
        <source>JACK is currently running.

Do you want to terminate the JACK audio server?</source>
        <translation>Сервер JACK работает.

Вы хотите остановить его?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="858"/>
        <source>with exit status=%1</source>
        <translation>со статусом выхода %1</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="904"/>
        <source>Could not start JACK.

Maybe JACK audio server is already started.</source>
        <translation>Не удалось запустить JACK.

Возможно, звуковой сервер уже запущен.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="938"/>
        <source>Could not load preset &quot;%1&quot;.

Retrying with default.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="941"/>
        <source>Could not load default preset.

Sorry.</source>
        <translation>Не удалось запустить профиль по умолчанию.

Извините.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1150"/>
        <source>D-BUS: JACK server is starting...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1153"/>
        <source>D-BUS: JACK server could not be started.

Sorry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1217"/>
        <source>Some client audio applications
are still active and connected.

Do you want to stop the JACK audio server?</source>
        <translation>Некоторые клиентские звуковые приложения
всё ещё активны и подсоединены.

Вы хотите остановить звуковой сервер JACK?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1274"/>
        <source>D-BUS: JACK server is stopping...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1277"/>
        <source>D-BUS: JACK server could not be stopped.

Sorry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1293"/>
        <location filename="../src/qjackctlMainForm.cpp" line="1422"/>
        <source>Post-shutdown script...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1294"/>
        <location filename="../src/qjackctlMainForm.cpp" line="1423"/>
        <source>Post-shutdown script terminated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1343"/>
        <source>JACK was started with PID=%1.</source>
        <translation>JACK был запущен с PID=%1.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1351"/>
        <source>D-BUS: JACK server was started (%1 aka jackdbus).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1390"/>
        <source>JACK is being forced...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1410"/>
        <source>D-BUS: JACK server was stopped (%1 aka jackdbus).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1770"/>
        <source>Could not load active patchbay definition.

Disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1773"/>
        <source>Patchbay activated.</source>
        <translation>Коммутатор активирован.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="1781"/>
        <source>Patchbay deactivated.</source>
        <translation>Коммутатор деактивирован.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2015"/>
        <source>JACK connection graph change.</source>
        <translation>Смена графа соединений JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2042"/>
        <source>XRUN callback (%1).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2052"/>
        <source>Buffer size change (%1).</source>
        <translation>Смена размера буфера (%1).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2081"/>
        <source>Could not start JACK.

Sorry.</source>
        <translation>Не удалось запустить JACK.

Извините.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2085"/>
        <source>JACK has crashed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2088"/>
        <source>JACK timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2091"/>
        <source>JACK write error.</source>
        <translation>Ошибка записи JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2094"/>
        <source>JACK read error.</source>
        <translation>Ошибка чтения JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2098"/>
        <source>Unknown JACK error (%d).</source>
        <translation>Неизвестная ошибка JACK (%d).</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2115"/>
        <source>ALSA connection graph change.</source>
        <translation>Смена графа соединений ALSA.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2146"/>
        <source>JACK active patchbay scan</source>
        <translation>Сканирование активного коммутатора  JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2156"/>
        <source>ALSA active patchbay scan</source>
        <translation>Сканирование активного коммутатора  ALSA.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2202"/>
        <source>JACK connection change.</source>
        <translation>Смена соединений JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2211"/>
        <source>ALSA connection change.</source>
        <translation>Смена соединений ALSA.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2262"/>
        <source>A patchbay definition is currently active,
which is probable to redo this connection:

%1 -&gt; %2

Do you want to remove the patchbay connection?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3443"/>
        <source>D-BUS: SetParameterValue(&apos;%1&apos;, &apos;%2&apos;):

%3.
(%4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3474"/>
        <source>D-BUS: ResetParameterValue(&apos;%1&apos;):

%2.
(%3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3505"/>
        <source>D-BUS: GetParameterValue(&apos;%1&apos;):

%2.
(%3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="obsolete">Удалить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2339"/>
        <source>Overall operation failed.</source>
        <translation>Выполнение операции в целом неудачно.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2341"/>
        <source>Invalid or unsupported option.</source>
        <translation>Некорректный или неподдерживаемый параметр</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2343"/>
        <source>Client name not unique.</source>
        <translation>Имя клиента не уникально.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2345"/>
        <source>Server is started.</source>
        <translation>Сервер запущен.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2347"/>
        <source>Unable to connect to server.</source>
        <translation>Не удалось соединиться с сервером.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2349"/>
        <source>Server communication error.</source>
        <translation>Ошибка коммуникации с сервером.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2351"/>
        <source>Client does not exist.</source>
        <translation>Клиент не существует.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2353"/>
        <source>Unable to load internal client.</source>
        <translation>Не удалось загрузить внутренний клиент.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2355"/>
        <source>Unable to initialize client.</source>
        <translation>Не удалось инициализировать клиент.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2357"/>
        <source>Unable to access shared memory.</source>
        <translation>Не удалось получить доступ к разделяемой памяти.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2359"/>
        <source>Client protocol version mismatch.</source>
        <translation>Несовпадение версии клиентского протокола</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2361"/>
        <source>Could not connect to JACK server as client.
- %1
Please check the messages window for more info.</source>
        <translation>Не удалось соединиться с сервером JACK.
- %1
Просмотрите вывод в окне сообщений.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2404"/>
        <source>Server configuration saved to &quot;%1&quot;.</source>
        <translation>Конфигурация сервера сохранена в &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2700"/>
        <source>Transport rewind.</source>
        <translation>Перемотка к началу</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2720"/>
        <source>Transport backward.</source>
        <translation>Перемотка назад</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2782"/>
        <source>Transport forward.</source>
        <translation>Перемотка вперёд</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2833"/>
        <source>%1 (%2%)</source>
        <translation>%1 (%2%)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2836"/>
        <source>%1 %</source>
        <translation>%1%</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2838"/>
        <source>%1 Hz</source>
        <translation>%1Гц</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2840"/>
        <source>%1 frames</source>
        <translation>%1 выборок</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2895"/>
        <source>%1 msec</source>
        <translation>%1 мс</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="2902"/>
        <source>XRUN callback (%1 skipped).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3077"/>
        <source>Mi&amp;nimize</source>
        <translation>&amp;Свернуть</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3079"/>
        <source>Rest&amp;ore</source>
        <translation>&amp;Восстановить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3093"/>
        <source>&amp;Reset</source>
        <translation>С&amp;бросить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3099"/>
        <source>&amp;Presets</source>
        <translation>&amp;Профили</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.cpp" line="3141"/>
        <source>&amp;Transport</source>
        <translation>&amp;Транспорт</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="165"/>
        <location filename="../src/qjackctlMainForm.cpp" line="3143"/>
        <source>&amp;Rewind</source>
        <translation>К &amp;началу</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="49"/>
        <source>QjackCtl</source>
        <translation>QjackCtl</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="569"/>
        <source>Alt+M</source>
        <translation type="unfinished">Alt+M</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="524"/>
        <source>&amp;Patchbay</source>
        <translation>&amp;Коммутатор</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="486"/>
        <source>&amp;Connect</source>
        <translation>&amp;Соединения</translation>
    </message>
    <message>
        <source>DSP Load</source>
        <translation type="obsolete">Загрузка DSP</translation>
    </message>
    <message>
        <source>XRUN Count (notifications)</source>
        <translation type="obsolete">Число XRUN (уведомлений)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="194"/>
        <source>Backward transport</source>
        <translation>Перемотать назад</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="197"/>
        <source>&amp;Backward</source>
        <translation>Н&amp;азад</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="302"/>
        <source>Forward transport</source>
        <translation>Перемотать вперёд</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="305"/>
        <source>&amp;Forward</source>
        <translation>&amp;Вперёд</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="312"/>
        <source>Alt+F</source>
        <translation>Alt+в</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="162"/>
        <source>Rewind transport</source>
        <translation>Перемотать к началу</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="172"/>
        <source>Alt+R</source>
        <translation>Alt+а</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="455"/>
        <source>Alt+A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="360"/>
        <source>Alt+O</source>
        <translation type="unfinished">Alt+к</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="280"/>
        <source>Shift+Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlMainForm.ui" line="239"/>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qjackctlMessagesForm</name>
    <message>
        <location filename="../src/qjackctlMessagesForm.ui" line="42"/>
        <source>Messages - JACK Audio Connection Kit</source>
        <translation>Сообщения</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMessagesForm.cpp" line="136"/>
        <source>Logging stopped --- %1 ---</source>
        <translation>Журналирование остановлено --- %1 ---</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMessagesForm.cpp" line="146"/>
        <source>Logging started --- %1 ---</source>
        <translation>Журналирование запущено --- %1 ---</translation>
    </message>
    <message>
        <location filename="../src/qjackctlMessagesForm.ui" line="57"/>
        <source>Messages output log</source>
        <translation>Журнал выведенных сообщений</translation>
    </message>
</context>
<context>
    <name>qjackctlPatchbay</name>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1738"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1739"/>
        <source>This will disconnect all sockets.

Are you sure?</source>
        <translation>Все сокеты будут рассоединены.

Продолжить?</translation>
    </message>
</context>
<context>
    <name>qjackctlPatchbayForm</name>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="44"/>
        <source>Patchbay - JACK Audio Connection Kit</source>
        <translation>Коммутатор</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="451"/>
        <source>&amp;New</source>
        <translation>&amp;Создать</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="457"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="448"/>
        <source>Create a new patchbay profile</source>
        <translation>Создать новый профиль коммутатора</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="470"/>
        <source>&amp;Load...</source>
        <translation>&amp;Загрузить...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="476"/>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="467"/>
        <source>Load patchbay profile</source>
        <translation>Загрузить профиль коммутатора</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="489"/>
        <source>&amp;Save...</source>
        <translation>&amp;Сохранить...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="495"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="486"/>
        <source>Save current patchbay profile</source>
        <translation>Сохранить текущий профиль коммутатора</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="529"/>
        <source>Acti&amp;vate</source>
        <translation>&amp;Активировать</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="535"/>
        <source>Alt+V</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="358"/>
        <source>&amp;Connect</source>
        <translation>&amp;Соединить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="364"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="355"/>
        <source>Connect currently selected sockets</source>
        <translation>Соединить выбранные сейчас сокеты</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="374"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Рассоединить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="380"/>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="371"/>
        <source>Disconnect currently selected sockets</source>
        <translation>Рассоединить выбранные сейчас сокеты</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="390"/>
        <source>Disconnect &amp;All</source>
        <translation>Рассоединить &amp;все</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="396"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="387"/>
        <source>Disconnect all currently connected sockets</source>
        <translation>Рассоединить все соединённые сейчас сокеты</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="422"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Обновить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="428"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="419"/>
        <source>Refresh current patchbay view</source>
        <translation>Обновить текущий вид коммутатора</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="108"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="236"/>
        <source>Down</source>
        <translation>Вниз</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="105"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="233"/>
        <source>Move currently selected output socket down one position</source>
        <translation>Переместить выбранный сейчас сокет вниз на одну позицию</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="124"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="284"/>
        <source>Add...</source>
        <translation>Добавить...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="121"/>
        <source>Create a new output socket</source>
        <translation>Создать новый сокет выхода</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="156"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="300"/>
        <source>Edit...</source>
        <translation>Изменить...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="153"/>
        <source>Edit currently selected input socket properties</source>
        <translation>Изменить свойства выбранного сейчас сокета входа</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="188"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="316"/>
        <source>Up</source>
        <translation>Вверх</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="185"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="313"/>
        <source>Move currently selected output socket up one position</source>
        <translation>Переместить выбранный сейчас сокет вверх на одну позицию</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="204"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="252"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="201"/>
        <source>Remove currently selected output socket</source>
        <translation>Удалить выбранный сейчас сокет выхода</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="249"/>
        <source>Remove currently selected input socket</source>
        <translation>Удалить выбранный сейчас сокет входа</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="281"/>
        <source>Create a new input socket</source>
        <translation>Создать новый сокет входа</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="297"/>
        <source>Edit currently selected output socket properties</source>
        <translation>Изменить свойства выбранного сейчас сокета выхода</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="221"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Сохранить</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="obsolete">Отказаться</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Отменить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="598"/>
        <source>active</source>
        <translation>активно</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="439"/>
        <source>New Patchbay definition</source>
        <translation>Новое описание коммутатора</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="473"/>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="493"/>
        <source>Patchbay Definition files</source>
        <translation>Файл описания коммутатора</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="471"/>
        <source>Load Patchbay Definition</source>
        <translation>Загрузить описание коммутатора</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="491"/>
        <source>Save Patchbay Definition</source>
        <translation>Сохранить описание коммутатора</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="222"/>
        <source>The patchbay definition has been changed:

&quot;%1&quot;

Do you want to save the changes?</source>
        <translation>Описание коммутатора изменилось:

&quot;%1&quot;

Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="278"/>
        <source>%1 [modified]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="345"/>
        <source>Untitled%1</source>
        <translation>Без имени %1</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="364"/>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="394"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="365"/>
        <source>Could not load patchbay definition file: 

&quot;%1&quot;</source>
        <translation>Не удалось загрузить файл описания коммутатора: 

&quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="395"/>
        <source>Could not save patchbay definition file: 

&quot;%1&quot;</source>
        <translation>Не удалось сохранить файл описания коммутатора: 

&quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.cpp" line="440"/>
        <source>Create patchbay definition as a snapshot
of all actual client connections?</source>
        <translation>Создать описание коммутатора как
снимок активных соединений клиентов?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="217"/>
        <source>Duplicate (copy) currently selected output socket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="220"/>
        <location filename="../src/qjackctlPatchbayForm.ui" line="268"/>
        <source>Copy...</source>
        <translation>Скопировать...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="265"/>
        <source>Duplicate (copy) currently selected input socket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="519"/>
        <source>Current (recent) patchbay profile(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbayForm.ui" line="526"/>
        <source>Toggle activation of current patchbay profile</source>
        <translation>Переключить активность текущего профиля коммутатора</translation>
    </message>
</context>
<context>
    <name>qjackctlPatchbayView</name>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1280"/>
        <source>Add...</source>
        <translation>Добавить...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1282"/>
        <source>Edit...</source>
        <translation>Изменить...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1288"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1351"/>
        <source>Move Up</source>
        <translation>Выше</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1354"/>
        <source>Move Down</source>
        <translation>Ниже</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1360"/>
        <source>&amp;Connect</source>
        <translation>&amp;Соединить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1361"/>
        <source>Alt+C</source>
        <comment>Connect</comment>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1364"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Рассоединить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1365"/>
        <source>Alt+D</source>
        <comment>Disconnect</comment>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1368"/>
        <source>Disconnect &amp;All</source>
        <translation>Рассоединить &amp;все</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1369"/>
        <source>Alt+A</source>
        <comment>Disconect All</comment>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1374"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Обновить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1375"/>
        <source>Alt+R</source>
        <comment>Refresh</comment>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1292"/>
        <source>Exclusive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1285"/>
        <source>Copy...</source>
        <translation>Скопировать...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1298"/>
        <source>Forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="1337"/>
        <source>(None)</source>
        <translation>(Нет)</translation>
    </message>
</context>
<context>
    <name>qjackctlSetupForm</name>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="41"/>
        <source>Setup - JACK Audio Connection Kit</source>
        <translation>Параметры</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4323"/>
        <source>OK</source>
        <translation>ОК</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4333"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="66"/>
        <source>Settings</source>
        <translation>Общие</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="168"/>
        <source>Server</source>
        <translation>Сервер</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="236"/>
        <source>jackstart</source>
        <translation>jackstart</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="226"/>
        <source>jackd</source>
        <translation>jackd</translation>
    </message>
    <message>
        <source>jackd-realtime</source>
        <translation type="obsolete">jackd-realtime</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="219"/>
        <source>The JACK Audio Connection Kit sound server path</source>
        <translation>Путь к серверу JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="279"/>
        <source>dummy</source>
        <translation>псевдо</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="294"/>
        <source>alsa</source>
        <translation>ALSA</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="299"/>
        <source>portaudio</source>
        <translation>PortAudio</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="341"/>
        <source>Parameters</source>
        <translation>Параметры</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1613"/>
        <source>22050</source>
        <translation>22050</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1618"/>
        <source>32000</source>
        <translation>32000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1623"/>
        <source>44100</source>
        <translation>44100</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1628"/>
        <source>48000</source>
        <translation>48000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1633"/>
        <source>88200</source>
        <translation>88200</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1638"/>
        <source>96000</source>
        <translation>96000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1606"/>
        <source>Sample rate in frames per second</source>
        <translation>Частота дискретизации выборок в секунду)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="552"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2568"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4254"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4259"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4264"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4269"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2573"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4274"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1657"/>
        <source>Scheduler priority when running realtime</source>
        <translation>Приоритет планировщика в режиме реального времени</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1592"/>
        <source>21333</source>
        <translation>21333</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1472"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1730"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1477"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1735"/>
        <source>32</source>
        <translation>32</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1421"/>
        <source>Priorit&amp;y:</source>
        <translation>П&amp;риоритет:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1807"/>
        <source>&amp;Wait (usec):</source>
        <translation>О&amp;жидание (мс):</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1443"/>
        <source>&amp;Frames/Period:</source>
        <translation>&amp;Выборок в буфере:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1482"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1740"/>
        <source>64</source>
        <translation>64</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1487"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1778"/>
        <source>128</source>
        <translation>128</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1492"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1783"/>
        <source>256</source>
        <translation>256</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1497"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1788"/>
        <source>512</source>
        <translation>512</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1502"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1793"/>
        <source>1024</source>
        <translation>1024</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1507"/>
        <source>2048</source>
        <translation>2048</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1512"/>
        <source>4096</source>
        <translation>4096</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1465"/>
        <source>Frames per period between process() calls</source>
        <translation>Выборок в период между вызовами process()</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1835"/>
        <source>Sample &amp;Rate:</source>
        <translation>&amp;Частота дискр.:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="717"/>
        <source>H/&amp;W Meter</source>
        <translation>Аппаратный &amp;счётчик</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="720"/>
        <source>Alt+W</source>
        <translation>Alt+W</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1357"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1362"/>
        <source>Rectangular</source>
        <translation>Прямоугольное</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1367"/>
        <source>Shaped</source>
        <translation>По очертаниям</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1372"/>
        <source>Triangular</source>
        <translation>Треугольное</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1015"/>
        <source>Duplex</source>
        <translation>Дуплекс</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1011"/>
        <source>Provide either audio capture, playback or both</source>
        <translation>Разрешить захват звука, его воспроизведение или всё сразу</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1051"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1139"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1221"/>
        <source>hw:0</source>
        <translation>hw:0</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1883"/>
        <source>&amp;Timeout (msec):</source>
        <translation>&amp;Тайм-аут (мс):</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="948"/>
        <source>Dit&amp;her:</source>
        <translation>Подмешивание &amp;шума:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1915"/>
        <source>200</source>
        <translation>200</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1920"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3386"/>
        <source>500</source>
        <translation>500</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1925"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3391"/>
        <source>1000</source>
        <translation>1000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1930"/>
        <source>2000</source>
        <translation>2000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1935"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3401"/>
        <source>5000</source>
        <translation>5000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1940"/>
        <source>10000</source>
        <translation>10000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1905"/>
        <source>Set client timeout limit in miliseconds</source>
        <translation>Установить тайм-аут для клиента в миллисекундах</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="857"/>
        <source>&amp;Interface:</source>
        <translation>&amp;Интерфейс:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="641"/>
        <source>So&amp;ft Mode</source>
        <translation>&amp;Программный режим</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="644"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3151"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3173"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3315"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3485"/>
        <source>Alt+F</source>
        <translation>Alt+F</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="660"/>
        <source>&amp;Monitor</source>
        <translation>&amp;Контроль</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="663"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2801"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3353"/>
        <source>Alt+M</source>
        <translation>Alt+к</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="657"/>
        <source>Provide output monitor ports</source>
        <translation>Задействовать порты контроля выхода</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="584"/>
        <source>&amp;Realtime</source>
        <translation>Режим &amp;реал. времени</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="587"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2978"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4114"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="581"/>
        <source>Use realtime scheduling</source>
        <translation>Использовать планирование в реал. времени</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2684"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3913"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="114"/>
        <location filename="../src/qjackctlSetupForm.ui" line="885"/>
        <location filename="../src/qjackctlSetupForm.ui" line="976"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1046"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1134"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1196"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1216"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1340"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1663"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1863"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4249"/>
        <source>(default)</source>
        <translation>(как обычно)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="758"/>
        <source>Alt+V</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="752"/>
        <source>Whether to give verbose output on messages</source>
        <translation>Подробно ли выводить сообщения</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="510"/>
        <source>Latency:</source>
        <translation>Задержка:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="543"/>
        <source>Output latency in milliseconds, calculated based on the period, rate and buffer settings</source>
        <translation>Задержка выхода в мс, расчитанных на основе настроек периода, частоты и буфера</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1954"/>
        <source>Options</source>
        <translation>Сценарии и журнал</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1972"/>
        <source>Scripting</source>
        <translation>Сценарии</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1996"/>
        <source>Execute script on Start&amp;up:</source>
        <translation>Выполнять сценарий при &amp;запуске:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="625"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1999"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3972"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1993"/>
        <source>Whether to execute a custom shell script before starting up the JACK audio server.</source>
        <translation>Выполнять ли собственный сценарий перед запуском сервера JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2015"/>
        <source>Execute script after &amp;Startup:</source>
        <translation>Выполнять сценарий после з&amp;апуска:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="131"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2018"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3769"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2012"/>
        <source>Whether to execute a custom shell script after starting up the JACK audio server.</source>
        <translation>Выполнять ли собственный сценарий после запуска сервера JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="150"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2037"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3211"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3865"/>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2308"/>
        <source>Whether to execute a custom shell script after shuting down the JACK audio server.</source>
        <translation>Выполнять ли собственный сценарий после остановки сервера JACK.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2058"/>
        <source>Command line to be executed before starting up the JACK audio server</source>
        <translation>Команда, выполняемая перед запуском сервера JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2123"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2209"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2271"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2376"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2665"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2782"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2120"/>
        <source>Browse for script to be executed before starting up the JACK audio server</source>
        <translation>Указать сценарий, выполняемый перед запуском сервера JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2144"/>
        <source>Command line to be executed after starting up the JACK audio server</source>
        <translation>Команда, выполняемая после запуска сервера JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2206"/>
        <source>Browse for script to be executed after starting up the JACK audio server</source>
        <translation>Указать сценарий, выполняемый после запуска сервера JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2373"/>
        <source>Browse for script to be executed after shutting down the JACK audio server</source>
        <translation>Указать сценарий, выполняемый после остановки сервера JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2397"/>
        <source>Command line to be executed after shutting down the JACK audio server</source>
        <translation>Команда, выполняемая после остановки сервера JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2416"/>
        <source>Statistics</source>
        <translation>Статистика</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2464"/>
        <source>&amp;XRUN detection regex:</source>
        <translation>&amp;Рег.выражение для XRUN:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2498"/>
        <source>xrun of at least ([0-9|\.]+) msecs</source>
        <translation>xrun of at least ([0-9|\.]+) msecs</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2491"/>
        <source>Regular expression used to detect XRUNs on server output messages</source>
        <translation>Регулярное выражение для определения рассинхронизаций
среди сообщений от сервера</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="739"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2518"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3884"/>
        <source>Alt+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2512"/>
        <source>Whether to ignore the first XRUN on server startup (most likely to occur on pre-0.80.0 servers)</source>
        <translation>Игнорировать ли первую рассинхронизацию при запуске сервера 
(весьма вероятно в случае с JACK до версии 0.80.0) </translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2534"/>
        <source>Connections</source>
        <translation>Соединения</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2578"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2583"/>
        <source>30</source>
        <translation>30</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2588"/>
        <source>60</source>
        <translation>60</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2593"/>
        <source>120</source>
        <translation>120</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2561"/>
        <source>Time in seconds between each auto-refresh cycle</source>
        <translation>Время в секундах между каждым циклом автообновления</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2631"/>
        <source>Patchbay definition file to be activated as connection persistence profile</source>
        <translation>Файл описания коммутатора, активируемый как профиль постоянного соединения</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2662"/>
        <source>Browse for a patchbay definition file to be activated</source>
        <translation>Указать активируемый файл описания коммутатора</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2681"/>
        <source>&amp;Auto refresh connections Patchbay, every (secs):</source>
        <translation>&amp;Автоматически обновлять соединения в коммутатор, каждые (сек):</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2678"/>
        <source>Whether to refresh the connections patchbay automatically</source>
        <translation>Обновлять ли соединения в коммутаторе автоматически</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2700"/>
        <source>Activate &amp;Patchbay persistence:</source>
        <translation>Активировать &amp;постоянный коммутатор:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2703"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2697"/>
        <source>Whether to activate a patchbay definition for connection persistence profile.</source>
        <translation>Активировать ли описание коммутатора для профиля постоянного соединения.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2828"/>
        <source>Display</source>
        <translation>Интерфейс</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2314"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2946"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4133"/>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2959"/>
        <source>Transport &amp;BBT (bar:beat.ticks)</source>
        <translation>&amp;BBT транспорта (такт:доля.тики)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2962"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3651"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4010"/>
        <location filename="../src/qjackctlSetupForm.ui" line="4152"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2975"/>
        <source>Elapsed time since last &amp;Reset</source>
        <translation>Время, прошедшее с последнего &amp;сброса</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2991"/>
        <source>Elapsed time since last &amp;XRUN</source>
        <translation>Время, пошедшее с последней &amp;рассинхронизации</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2994"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3248"/>
        <source>Messages Window</source>
        <translation>Окно сообщений</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3281"/>
        <source>Sample messages text font display</source>
        <translation>Каким будет шрифт для вывода сообщений сервера</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3148"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3170"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3312"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3482"/>
        <source>&amp;Font...</source>
        <translation>&amp;Шрифт...</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3309"/>
        <source>Select font for the messages text display</source>
        <translation>Выберите шрифт для отображения сообщений сервера</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="882"/>
        <source>msec</source>
        <translation>мс</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="884"/>
        <source>n/a</source>
        <translation>н/д</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1623"/>
        <source>Patchbay Definition files</source>
        <translation>Файлы описания коммутатора</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="189"/>
        <source>Server &amp;Path:</source>
        <translation>&amp;Путь к серверу:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="250"/>
        <source>Driv&amp;er:</source>
        <translation>&amp;Драйвер:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="86"/>
        <source>Preset &amp;Name:</source>
        <translation>Имя &amp;профиля:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="107"/>
        <source>Settings preset name</source>
        <translation>Имя профиля параметров</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="125"/>
        <source>&amp;Save</source>
        <translation>&amp;Сохранить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="122"/>
        <source>Save settings as current preset name</source>
        <translation>Сохранить параметры в текущий профиль</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="144"/>
        <source>&amp;Delete</source>
        <translation>&amp;Удалить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="141"/>
        <source>Delete current settings preset</source>
        <translation>Удалить текущий профиль</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="898"/>
        <source>&amp;Audio:</source>
        <translation>&amp;Звук:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="698"/>
        <source>H/W M&amp;onitor</source>
        <translation>Аппаратный &amp;контроль</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="701"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3953"/>
        <source>Alt+O</source>
        <translation>Alt+к</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="679"/>
        <source>Force &amp;16bit</source>
        <translation>Принудительно &amp;16 бит</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="682"/>
        <source>Alt+1</source>
        <translation>Alt+1</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="676"/>
        <source>Force 16bit mode instead of failing over 32bit (default)</source>
        <translation>Принудительно использовать 16бит-режим вместо 32бит (по умолчанию)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1698"/>
        <source>Periods/&amp;Buffer:</source>
        <translation>Периодов на &amp;буфер:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1250"/>
        <source>&amp;Input Channels:</source>
        <translation>Каналов на &amp;входе:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="835"/>
        <source>&amp;Output Channels:</source>
        <translation>Каналов на в&amp;ыходе:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="439"/>
        <source>Start De&amp;lay (secs):</source>
        <translation>Задержка за&amp;пуска (с):</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="461"/>
        <source>Time in seconds that client is delayed after server startup</source>
        <translation>Сколько секунд клиент ждёт после запуска сервера</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1098"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1290"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1321"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2092"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2178"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2240"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2345"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2089"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2175"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2237"/>
        <location filename="../src/qjackctlSetupForm.ui" line="2342"/>
        <source>Scripting argument meta-symbols</source>
        <translation>Метасимволы аргументов в сценариях</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2440"/>
        <source>&amp;Capture standard output</source>
        <translation>&amp;Захватывать стандартный вывод</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2443"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3788"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2437"/>
        <source>Whether to capture standard output (stdout/stderr) into messages window</source>
        <translation>Захватывать ли стандартный вывод (stdout/stderr) </translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2904"/>
        <source>hh:mm:ss</source>
        <translation>чч:мм:сс</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3734"/>
        <source>Other</source>
        <translation>Другое</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3785"/>
        <source>&amp;Confirm application close</source>
        <translation>За&amp;прашивать подтверждение на выход</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3782"/>
        <source>Whether to ask for confirmation on application exit</source>
        <translation>Спрашивать ли подтверждение на выход из программы</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1497"/>
        <source>&amp;Preset Name</source>
        <translation>Имя &amp;профиля</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1499"/>
        <source>&amp;Server Path</source>
        <translation>&amp;Путь к серверу</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1500"/>
        <source>&amp;Driver</source>
        <translation>&amp;Драйвер</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1501"/>
        <source>&amp;Interface</source>
        <translation>&amp;Интерфейс</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1503"/>
        <source>Sample &amp;Rate</source>
        <translation>&amp;Частота сэмпл.</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1504"/>
        <source>&amp;Frames/Period</source>
        <translation>&amp;Фреймов на период</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1505"/>
        <source>Periods/&amp;Buffer</source>
        <translation>Периодов на &amp;буфер</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="289"/>
        <source>oss</source>
        <translation>OSS</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1526"/>
        <source>Port Ma&amp;ximum:</source>
        <translation>&amp;Макс. кол-во портов:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1768"/>
        <source>Maximum number of ports the JACK server can manage</source>
        <translation>Максимальное кол-во портов, обрабатываемых сервером JACK</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="989"/>
        <source>&amp;Input Device:</source>
        <translation>Устройство &amp;входа:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="813"/>
        <source>&amp;Output Device:</source>
        <translation>Устройство в&amp;ыхода:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1066"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1154"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1236"/>
        <source>/dev/dsp</source>
        <translation>/dev/dsp</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="603"/>
        <source>No Memory Loc&amp;k</source>
        <translation>Б&amp;ез блокировки памяти</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="606"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3230"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3808"/>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="600"/>
        <source>Do not attempt to lock memory, even if in realtime mode</source>
        <translation>Не пытайтесь заблокировать память, даже в режиме реального времени</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="736"/>
        <source>&amp;Ignore H/W</source>
        <translation>&amp;Игнорировать апп. буфер и период</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1676"/>
        <source>&amp;Word Length:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2515"/>
        <source>&amp;Ignore first XRUN occurrence on statistics</source>
        <translation>&amp;Игнорировать первую рассинхронизацию в статистике</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2846"/>
        <source>Time Display</source>
        <translation>Отображение времени</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2943"/>
        <source>Transport &amp;Time Code</source>
        <translation>&amp;Тайм-код транспорта</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3061"/>
        <source>Sample front panel normal display font</source>
        <translation>Пример текста в малом счётчике</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3101"/>
        <source>Sample big time display font</source>
        <translation>Пример текста в большом счётчике</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3189"/>
        <source>Normal display:</source>
        <translation>Малый счётчик:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3129"/>
        <source>Big Time display:</source>
        <translation>Большой счётчик:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2875"/>
        <source>Time F&amp;ormat:</source>
        <translation>&amp;Формат времени:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2909"/>
        <source>hh:mm:ss.d</source>
        <translation>чч:мм:сс.д</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2914"/>
        <source>hh:mm:ss.dd</source>
        <translation>чч:мм:сс.дд</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2919"/>
        <source>hh:mm:ss.ddd</source>
        <translation>чч:мм:сс.ддд</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2897"/>
        <source>The general time format on display</source>
        <translation>Общий формат вывода времени на дисплей</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3350"/>
        <source>&amp;Messages limit:</source>
        <translation>Предел кол-ва &amp;сообщений:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3347"/>
        <source>Whether to keep a maximum number of lines in the messages window</source>
        <translation>Ограничивать ли количество строк в окне сообщений</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3376"/>
        <source>100</source>
        <translation>100</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3381"/>
        <source>250</source>
        <translation>250</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3396"/>
        <source>2500</source>
        <translation>2500</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3366"/>
        <source>The maximum number of message lines to keep in view</source>
        <translation>Максимальное количество строк сообщений, доступное при просмотре</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3766"/>
        <source>&amp;Start JACK audio server on application startup</source>
        <translation>&amp;Запускать звуковой сервер JACK при старте программы</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3763"/>
        <source>Whether to start JACK audio server immediately on application startup</source>
        <translation>Запускать ли сервер JACK при старте QJackCtl</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3805"/>
        <source>&amp;Keep child windows always on top</source>
        <translation>О&amp;кна программы всегда наверху</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3802"/>
        <source>Whether to keep all child windows on top of the main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3824"/>
        <source>&amp;Enable system tray icon</source>
        <translation>&amp;Включить область уведомления</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3827"/>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3821"/>
        <source>Whether to enable the system tray icon</source>
        <translation>Включать ли область уведомления
(системный лоток, system tray)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3910"/>
        <source>S&amp;ave JACK audio server configuration to:</source>
        <translation>Со&amp;хранять конфигурацию JACK в файл:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3907"/>
        <source>Whether to save the JACK server command-line configuration into a local file (auto-start)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3933"/>
        <source>.jackdrc</source>
        <translation>.jackdrc</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3926"/>
        <source>The server configuration local file name (auto-start)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3950"/>
        <source>C&amp;onfigure as temporary server</source>
        <translation>&amp;Настроить как временный сервер</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3947"/>
        <source>Whether to exit once all clients have closed (auto-start)</source>
        <translation>Завершать ли работу при отсоединении все клиентов</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="796"/>
        <location filename="../src/qjackctlSetupForm.cpp" line="848"/>
        <location filename="../src/qjackctlSetupForm.cpp" line="1844"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Сохранить</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="obsolete">Отказаться</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="obsolete">Учесть</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="797"/>
        <source>Some settings have been changed:

&quot;%1&quot;

Do you want to save the changes?</source>
        <translation>Некоторые параметры изменились:

&quot;%1&quot;

Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="849"/>
        <source>Delete preset:

&quot;%1&quot;

Are you sure?</source>
        <translation>Удалить профиль:

&quot;%1&quot;

Вы уверены?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1553"/>
        <source>Startup Script</source>
        <translation>Сценарий, выполняемый при запуске</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1570"/>
        <source>Post-Startup Script</source>
        <translation>Сценарий, выполняемый после запуска</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1587"/>
        <source>Shutdown Script</source>
        <translation>Сценарий, выполняемый при выключении</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1604"/>
        <source>Post-Shutdown Script</source>
        <translation>Сценарий, выполняемый после выключения</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1621"/>
        <source>Active Patchbay Definition</source>
        <translation>Активное описание коммутатора</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1639"/>
        <source>Messages Log</source>
        <translation>Журнал сообщений</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1641"/>
        <source>Log files</source>
        <translation>Файлы журналов</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.cpp" line="1845"/>
        <source>Some settings have been changed.

Do you want to apply the changes?</source>
        <translation>Некоторые параметры были изменены.

Применить эти изменения?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="231"/>
        <source>jackdmp</source>
        <translation>jackdmp</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="272"/>
        <source>The audio backend driver interface to use</source>
        <translation>Используемый звуковой драйвер</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="284"/>
        <source>sun</source>
        <translation>sun</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="304"/>
        <source>coreaudio</source>
        <translation>coreaudio</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="309"/>
        <source>freebob</source>
        <translation>freebob</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="314"/>
        <source>firewire</source>
        <translation>firewire</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="370"/>
        <source>MIDI Driv&amp;er:</source>
        <translation>&amp;Драйвер MIDI:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="392"/>
        <source>The ALSA MIDI backend driver to use</source>
        <translation>Используемый драйвер ALSA MIDI</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="399"/>
        <source>none</source>
        <translation>нет</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="404"/>
        <source>raw</source>
        <translation>raw</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="409"/>
        <source>seq</source>
        <translation>seq</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="619"/>
        <source>Unlock memory of common toolkit libraries (GTK+, QT, FLTK, Wine)</source>
        <translation>Разблокировать память основных библиотек интерфейсов (GTK+, QT, FLTK, Wine)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="622"/>
        <source>&amp;Unlock Memory</source>
        <translation>&amp;Разблокировать память</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="638"/>
        <source>Ignore xruns reported by the backend driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="695"/>
        <source>Enable hardware monitoring of capture ports</source>
        <translation>Включить аппаратный контроль портов захвата</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="714"/>
        <source>Enable hardware metering on cards that support it</source>
        <translation>Включить аппаратное измерение для поддерживающих эту функцию карт</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="733"/>
        <source>Ignore hardware period/buffer size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="755"/>
        <source>&amp;Verbose messages</source>
        <translation>&amp;Подробный вывод</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="879"/>
        <source>Maximum input audio hardware channels to allocate</source>
        <translation>Максимальное число резервируемых каналов входа</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="920"/>
        <source>&amp;Input Latency:</source>
        <translation>&amp;Задержка на входе</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="970"/>
        <source>External output latency (frames)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1020"/>
        <source>Capture Only</source>
        <translation>Только захват</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1025"/>
        <source>Playback Only</source>
        <translation>Только воспроизведение</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1039"/>
        <source>The PCM device name to use</source>
        <translation>Имя используемого устройства PCM</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1056"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1144"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1226"/>
        <source>plughw:0</source>
        <translation>plughw:0</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1061"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1149"/>
        <location filename="../src/qjackctlSetupForm.ui" line="1231"/>
        <source>/dev/audio</source>
        <translation>/dev/audio</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1095"/>
        <source>Select output device for playback</source>
        <translation>Выберите устройство выхода для воспроизведения</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1127"/>
        <source>Alternate input device for capture</source>
        <translation>Выберите устройство входа для захвата</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1168"/>
        <source>&amp;Output Latency:</source>
        <translation>Задержка на в&amp;ыходе</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1190"/>
        <source>Maximum output audio hardware channels to allocate</source>
        <translation>Максимальное резервируемое число звуковых каналов устройства</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1209"/>
        <source>Alternate output device for playback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1287"/>
        <source>Select input device for capture</source>
        <translation>Выберите устройство входа для захвата</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1318"/>
        <source>Select PCM device name</source>
        <translation>Выберите имя устройства PCM</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1334"/>
        <source>External input latency (frames)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1353"/>
        <source>Set dither mode</source>
        <translation>Выберите способ подмешивания шума</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1396"/>
        <source>Number of periods in the hardware buffer</source>
        <translation>Количество периодов в аппаратном буфере</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1556"/>
        <source>&amp;Channels:</source>
        <translation>&amp;Каналов:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1643"/>
        <source>192000</source>
        <translation>192000</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1723"/>
        <source>Word length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1857"/>
        <source>Maximum number of audio channels to allocate</source>
        <translation>Максимальное число резервируемых звуковых каналов</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2031"/>
        <source>Whether to execute a custom shell script before shuting down the JACK audio server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2034"/>
        <source>Execute script on Shut&amp;down:</source>
        <translation>В&amp;ыполнять сценарий при выходе:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2268"/>
        <source>Browse for script to be executed before shutting down the JACK audio server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2292"/>
        <source>Command line to be executed before shutting down the JACK audio server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2311"/>
        <source>Execute script after Shu&amp;tdown:</source>
        <translation>Выполнять сценарий после &amp;выключения:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2719"/>
        <source>Logging</source>
        <translation>Журналирование</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2748"/>
        <source>Messages log file</source>
        <translation>Файл журнала сообщений</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2779"/>
        <source>Browse for the messages log file location</source>
        <translation>Указать расположение файла журнала</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2795"/>
        <source>Whether to activate a messages logging to file.</source>
        <translation>Сохранять ли протокол в файл журнала</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="2798"/>
        <source>&amp;Messages log file:</source>
        <translation>Файл &amp;журнала:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3145"/>
        <source>Select font for front panel normal display</source>
        <translation>Выбрать шрифт для малого счётчика</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3167"/>
        <source>Select font for big time display</source>
        <translation>Выбрать шрифт для большого счётчика</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3205"/>
        <source>Whether to enable a shiny glass light effect on the main display</source>
        <translation>Изображать ли блик на основном дисплее</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3208"/>
        <source>&amp;Display shiny glass light effect</source>
        <translation>Включить эффект &amp;блика на дисплее</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3224"/>
        <source>Whether to enable blinking (flashing) of the server mode (RT) indicator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3227"/>
        <source>Blin&amp;k server mode indicator</source>
        <translation>&amp;Мерцать индикатором режима сервера</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3418"/>
        <source>Connections Window</source>
        <translation>Окно соединений</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3451"/>
        <source>Sample connections view font</source>
        <translation>Шрифт для диалога управления соединениями</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3479"/>
        <source>Select font for the connections view</source>
        <translation>Выберите шрифт для просмотра соединений</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3501"/>
        <source>&amp;Icon size:</source>
        <translation>&amp;Размер значков:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3523"/>
        <source>The icon size for each item of the connections view</source>
        <translation>Размер значка каждого элемента в окне соединений</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3533"/>
        <source>16 x 16</source>
        <translation>16 × 16</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3538"/>
        <source>32 x 32</source>
        <translation>32 × 32</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3543"/>
        <source>64 x 64</source>
        <translation>64 × 64</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3645"/>
        <source>Whether to enable in-place client/port name editing (rename)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3648"/>
        <source>Ena&amp;ble client/port aliases editing (rename)</source>
        <translation>Ра&amp;зрешить переименование алиасов клиентов/портов</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3664"/>
        <source>Whether to enable client/port name aliases on the connections window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3667"/>
        <source>E&amp;nable client/port aliases</source>
        <translation>&amp;Разрешить алиасы клиентов/портов</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3670"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3991"/>
        <source>Alt+N</source>
        <translation>Alt+р</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3683"/>
        <source>Whether to draw connection lines as cubic Bezier curves</source>
        <translation>Рисовать ли соединительные линии в диалогах «Соединения» и «Коммутатор» как кубические кривые Безье</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3686"/>
        <source>Draw connection and patchbay lines as Be&amp;zier curves</source>
        <translation>Рисовать линии соединения как кривые &amp;Безье</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3689"/>
        <location filename="../src/qjackctlSetupForm.ui" line="3846"/>
        <source>Alt+Z</source>
        <translation>Alt+б</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3716"/>
        <source>Misc</source>
        <translation>Разное</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3840"/>
        <source>Whether to start minimized to system tray</source>
        <translation>Запускать ли приложение в области уведомления, скрывая основное окно</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3843"/>
        <source>Start minimi&amp;zed to system tray</source>
        <translation>&amp;Запускать убранным в область уведомления</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3859"/>
        <source>Whether to delay window positioning at application startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3862"/>
        <source>&amp;Delay window positioning at startup</source>
        <translation>За&amp;держивать размещение окна при запуске</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3966"/>
        <source>Whether to ask for confirmation on JACK audio server shutdown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3969"/>
        <source>Confirm server sh&amp;utdown</source>
        <translation>&amp;Запраживать подтверждение на остановку сервера</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3985"/>
        <source>Whether to enable ALSA Sequencer (MIDI) support on startup</source>
        <translation>Включать ли при запуске поддержку MIDI-секвенсера ALSA</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3988"/>
        <source>E&amp;nable ALSA Sequencer support</source>
        <translation>&amp;Включить поддержку секвенсера ALSA</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4060"/>
        <source>Buttons</source>
        <translation>Кнопки</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4089"/>
        <source>Whether to hide the left button group on the main window</source>
        <translation>Скрывать ли группу кнопок в левой части основного окна</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4092"/>
        <source>Hide main window &amp;Left buttons</source>
        <translation>Скрывать &amp;левые кнопки основного окна</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4095"/>
        <source>Alt+L</source>
        <translation>Alt+л</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4108"/>
        <source>Whether to hide the right button group on the main window</source>
        <translation>Скрывать ли группу кнопок в правой части основного окна</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4111"/>
        <source>Hide main window &amp;Right buttons</source>
        <translation>Скрывать прав&amp;ые кнопки основного окна</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4127"/>
        <source>Whether to hide the transport button group on the main window</source>
        <translation>Скрывать ли группу кнопок управления транспортом в основном окне</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4130"/>
        <source>Hide main window &amp;Transport buttons</source>
        <translation>Скрывать кнопки &amp;транспорта</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4146"/>
        <source>Whether to hide the text labels on the main window buttons</source>
        <translation>Скрывать ли надписи на кнопках в основном окне</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4149"/>
        <source>Hide main window &amp;button text labels</source>
        <translation>Скрывать по&amp;дписи кнопок основного окна</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3589"/>
        <source>&amp;JACK client/port aliases:</source>
        <translation>&amp;Алиасы клиентов/портов JACK:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3611"/>
        <source>JACK client/port aliases display mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3621"/>
        <source>Default</source>
        <translation>По умолчанию</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3626"/>
        <source>First</source>
        <translation>Первый</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3631"/>
        <source>Second</source>
        <translation>Второй</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4186"/>
        <source>Defaults</source>
        <translation type="unfinished">Используемые по умолчанию параметры</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4223"/>
        <source>&amp;Base font size:</source>
        <translation>&amp;Кегль шрифта в интерфейсе:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4242"/>
        <source>Base application font size (pt.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4279"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4284"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="319"/>
        <source>net</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4004"/>
        <source>Whether to enable D-Bus interface</source>
        <translation>Влючать ли интерфейс D-Bus</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="4007"/>
        <source>&amp;Enable D-Bus interface</source>
        <translation>Вклю&amp;чить интерфейс D-Bus</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="1585"/>
        <source>Number of microseconds to wait between engine processes (dummy)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="324"/>
        <source>netone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3878"/>
        <source>Whether to restrict to one single application instance (X11)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSetupForm.ui" line="3881"/>
        <source>Single application &amp;instance</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qjackctlSocketForm</name>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="386"/>
        <source>OK</source>
        <translation>ОК</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="402"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="52"/>
        <source>&amp;Socket</source>
        <translation>&amp;Сокет</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="198"/>
        <source>&amp;Client:</source>
        <translation>&amp;Клиент:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="80"/>
        <source>Socket name (an alias for client name)</source>
        <translation>Имя сокета (псевдоним имени клиента)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="64"/>
        <source>&amp;Name (alias):</source>
        <translation>&amp;Имя (псевдоним):</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="158"/>
        <source>Socket Plugs / Ports</source>
        <translation>Сокетовые штепсели / Порты</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="142"/>
        <source>Socket plug list</source>
        <translation>Список штепселей сокета</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="217"/>
        <source>&amp;Down</source>
        <translation>В&amp;низ</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="223"/>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="233"/>
        <source>&amp;Up</source>
        <translation>В&amp;верх</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="239"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="319"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="97"/>
        <source>Add plug to socket plug list</source>
        <translation>Добавить штепсель в список штепселей сокета</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="113"/>
        <source>&amp;Plug:</source>
        <translation>&amp;Штепсель:</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="169"/>
        <source>&amp;Edit</source>
        <translation>И&amp;зменить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="175"/>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="185"/>
        <source>&amp;Remove</source>
        <translation>&amp;Удалить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="191"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="182"/>
        <source>Remove currently selected plug from socket plug list</source>
        <translation>Удалить выбранный штепсель из списка штепселей сокета</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="152"/>
        <source>Plugs / Ports</source>
        <translation>Штепсели / Порты</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="337"/>
        <source>Error</source>
        <translation type="unfinished">Ошибка</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="338"/>
        <source>A socket named &quot;%1&quot; already exists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="546"/>
        <source>Add Plug</source>
        <translation>Добавить штепсель</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="563"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="560"/>
        <source>Edit</source>
        <translation>Изменить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="567"/>
        <source>Move Up</source>
        <translation>Переместить выше</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="570"/>
        <source>Move Down</source>
        <translation>Переместить ниже</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="87"/>
        <source>Client name (regular expression)</source>
        <translation>Имя клиента (регулярное выражение)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="100"/>
        <source>Add P&amp;lug</source>
        <translation>Добавить &amp;штепсель</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="106"/>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="129"/>
        <source>Port name (regular expression)</source>
        <translation>Имя порта (регулярное выражение)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="268"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="301"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="316"/>
        <source>&amp;Audio</source>
        <translation>&amp;Звук</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="313"/>
        <source>Audio socket type (JACK)</source>
        <translation>Тип звукового сокета (JACK)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="329"/>
        <source>&amp;MIDI</source>
        <translation>&amp;MIDI</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="332"/>
        <source>Alt+M</source>
        <translation>Alt+M</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="339"/>
        <source>MIDI socket type (ALSA)</source>
        <translation>Тип MIDI-сокета (ALSA)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="265"/>
        <source>E&amp;xclusive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="262"/>
        <source>Enforce only one exclusive cable connection</source>
        <translation>Принудительно только одно кабельное соединение</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="356"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="357"/>
        <source>Some settings have been changed.

Do you want to apply the changes?</source>
        <translation>Некоторые параметры были изменены.

Применить эти изменения?</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="obsolete">Учесть</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation type="obsolete">Отказаться</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.cpp" line="736"/>
        <source>(None)</source>
        <translation>(Нет)</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="33"/>
        <source>Socket - JACK Audio Connection Kit</source>
        <translation>Сокет</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="166"/>
        <source>Edit currently selected plug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="214"/>
        <source>Move down currently selected plug in socket plug list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="230"/>
        <source>Move up current selected plug in socket plug list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="275"/>
        <source>&amp;Forward:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="291"/>
        <source>Forward (clone) all connections from this socket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="326"/>
        <source>MIDI socket type (JACK)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="342"/>
        <source>AL&amp;SA</source>
        <translation>AL&amp;SA</translation>
    </message>
    <message>
        <location filename="../src/qjackctlSocketForm.ui" line="345"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
</context>
<context>
    <name>qjackctlSocketList</name>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="329"/>
        <source>Output</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="339"/>
        <source>Input</source>
        <translation>Вход</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="352"/>
        <source>Socket</source>
        <translation>Сокет</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="543"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="487"/>
        <source>&lt;New&gt; - %1</source>
        <translation>&lt;Новый&gt; - %1</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="544"/>
        <source>%1 about to be removed:

&quot;%2&quot;

Are you sure?</source>
        <translation>%1 будет удален:

&quot;%2&quot;

Вы уверены?</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="634"/>
        <source>%1 &lt;Copy&gt; - %2</source>
        <translation>%1 &lt;Копия&gt; - %2</translation>
    </message>
</context>
<context>
    <name>qjackctlSocketListView</name>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="802"/>
        <source>Output Sockets / Plugs</source>
        <translation>Гнезда наружу</translation>
    </message>
    <message>
        <location filename="../src/qjackctlPatchbay.cpp" line="804"/>
        <source>Input Sockets / Plugs</source>
        <translation>Гнезда внутрь</translation>
    </message>
</context>
<context>
    <name>qjackctlStatusForm</name>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="44"/>
        <source>Status - JACK Audio Connection Kit</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="98"/>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="103"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="73"/>
        <source>Statistics since last server startup</source>
        <translation>Статистика с последнего запуска сервера</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="130"/>
        <source>Re&amp;set</source>
        <translation>С&amp;бросить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="136"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="127"/>
        <source>Reset XRUN statistic values</source>
        <translation>Обнулить статистику по рассинхронизации</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="146"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Обновить</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="152"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.ui" line="143"/>
        <source>Refresh XRUN statistic values</source>
        <translation>Обновить статистику по рассинхронизации</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="105"/>
        <source>Time of last reset</source>
        <translation>Время последнего сброса</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="85"/>
        <source>XRUN count since last server startup</source>
        <translation>Рассинхронизаций с последнего запуска сервера</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="98"/>
        <source>XRUN total</source>
        <translation>Всего рассинхронизаций</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="96"/>
        <source>XRUN average</source>
        <translation>Средний размер рассинхронизаций</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="94"/>
        <source>XRUN minimum</source>
        <translation>Минимальная рассинхронизация</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="92"/>
        <source>XRUN maximum</source>
        <translation>Максимальная рассинхронизация</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="90"/>
        <source>XRUN last</source>
        <translation>Последняя рассинхронизация</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="88"/>
        <source>XRUN last time detected</source>
        <translation>Последняя обнаруженная рассинхронизация</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="75"/>
        <source>Transport state</source>
        <translation>Состояние транспорта</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="82"/>
        <source>Transport BPM</source>
        <translation>BPM транспорта</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="80"/>
        <source>Transport BBT</source>
        <translation>BBT транспорта</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="78"/>
        <source>Transport Timecode</source>
        <translation>Тайм-код транспорта</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="72"/>
        <source>Realtime Mode</source>
        <translation>Режим реального времени</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="70"/>
        <source>Buffer Size</source>
        <translation>Размер буфера</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="68"/>
        <source>Sample Rate</source>
        <translation>Частота сэмплирования</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="64"/>
        <source>Server state</source>
        <translation>Статус сервера</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="66"/>
        <source>DSP Load</source>
        <translation>Загрузка DSP</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="102"/>
        <source>Maximum scheduling delay</source>
        <translation>Максимальная задержка расписания</translation>
    </message>
    <message>
        <location filename="../src/qjackctlStatusForm.cpp" line="62"/>
        <source>Server name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
